<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ThankYou extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
    }
	
	public function index(){
		if(!$this->session->userdata('userData')){
			if($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('header',$data);
				$this->load->view('left_sidebar');
				$this->load->view('thankyou');
				$this->load->view('footer');
			}else{
				redirect('./login','refresh');
			}
			
		}
    }
}
