<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SinglePoll extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('SinglePoll_model');
		$this->load->model('UserTimeline_model');
		$this->load->model('Dashboard_model');
		$this->load->model('Home_model');
		$this->load->model('ajaxss');
    }
	
	public function index($id){
		$usertimeline = array();
		$datarows = array();
		
		$data['frequest'] = '';
		$data['messages'] = '';
		$polls = $this->Dashboard_model->getUserPolls($id);
		$polls['polloption'] = $this->Dashboard_model->getUserPollOption($id);
		
		$userinfo = $this->Dashboard_model->getUserInformation($polls['user_id']);
		$userid = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$userid = $userData['userId'];
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
					
		if($userid)
			$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $id);
		else
			$upvoted = 0;
		
		$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $id, 0, 'polls'), 'upvoted' => $upvoted, 'data' => $this->Dashboard_model->getTimelineText($polls['user_id'], 'polls', $id));
			
		$this->load->view('header_singlepoll',$data);
		$this->load->view('left_sidebar');
		$this->load->view('singlepoll');
		$this->load->view('right_sidebar');
		$this->load->view('footer');
    }
	
	public function pollVoting(){
		$friends = $this->Dashboard_model->getUserFrndIds($_REQUEST['puserid']);
		$frn = explode(',', $friends);
		$alluser = array();
		
		foreach($frn as $frnds)
			$alluser[$frnds] = $frnds;
		
		unset($alluser[$_REQUEST['puserid']]);
		$alluser = implode(',', $alluser);
			
		$return = $this->Dashboard_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollopt'],$_REQUEST['puserid'],$_SERVER['REMOTE_ADDR'],$alluser);
		$url = $_SERVER['HTTP_REFERER'];
		redirect($url);
	}
}
