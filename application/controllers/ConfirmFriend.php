<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ConfirmFriend extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('FindFriend_Model');
		$this->load->model('Dashboard_model');
    }
	
	public function index(){
		if($this->session->userdata('userData')){
			$html = $total = '';
			$userData = $this->session->userdata('userData');
			$countresult = $this->FindFriend_Model->confirmRequest($userData['userId']);
			if($countresult > 0){
				$total = $countresult;	
			}else{
				$total = 0;
			}
			$html .= '<div class="span10"><div class="vv_content clearfix" id="content" role="main" style="min-height:100px;"><div class="clearfix"><div class="find-friend"><div class="ff-header"><h2 class="ff">Respond to Your '.$total.' Friend Request</h2><div class="sent-request"><a href="'.base_url().'FindFriend">Find Friends</a> | <a href="'.base_url().'SentRequest">View sent requests</a></div></div><div class="ff-request">';
			$confirmRequest = $this->FindFriend_Model->check_request($userData['userId']);
			if(!empty($confirmRequest)){
				foreach($confirmRequest as $cr){
					$html .= '<div class="clearfix userbox">';
					$userdata = $this->FindFriend_Model->userdata($cr->friend_one);
					if(!empty($userdata)){
						if($userdata[0]->picture_url != '' && $userdata[0]->oauth_provider == 'site_login'){
							$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$userdata[0]->picture_url.'"></div></a>';
						}elseif($userdata[0]->picture_url != '' && $userdata[0]->oauth_provider != 'site_login'){
							$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.$userdata[0]->picture_url.'"></div></a>';
						}else{
							$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'assets/front/images/user.png"></div></a>';
						}
						
						$html .= '<div class="clearfix user-detail"><div class="response-container"><div class="response-bttn"><div class="responsesimple"><div class="response-buttons"><button type="button" value="'.$cr->id.'" class="confirm confirm_request">Confirm Request</button><button type="button" value="'.$cr->id.'" class="dlt not_now">Not Now</button></div></div></div></div><div class="nme"><div class="usr_name"><a href="#">'.$userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></div></div></div></div>';
					}
				}	
			}else{
				$html .= '<p style="text-align:center;">No friend request found!</p>';	
			}
			
			$html .= '</div></div></div></div></div>';
			
			$userResult = $this->Dashboard_model->getUserName($userData['userId']);
			$data['logUserName'] = $userResult[0]->username;
			$data['UserFullName'] = $userResult[0]->firstname;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $userResult[0]->picture_url;
			}
			
			$data['htmldata'] = $html;
			$data['userData'] = $userData;
			$data['userLogout'] = $this->session->userdata('userLogout');
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('confirmrequest');
			$this->load->view('footer');
		}else{
			redirect('./login');	
		}
    }
	
	public function add_friend(){
		if($this->session->userdata('userData')){
			$getRequestId = $this->FindFriend_Model->add_friend_request($_REQUEST['user_id'],$_REQUEST['friend_id']);
			$html = '';
			$userData = $this->session->userdata('userData');
			if(!empty($getRequestId)){
				$html .= '<button type="button" value="" class="confirm friend_request_send" style="opacity:0.5;cursor:default;" disabled="disabled">Friend Request Send</button>';	
			}
			echo $html;
		}else{
			redirect('./login');	
		}
    }
	
	public function confirm_request(){
		$result = $this->FindFriend_Model->confirm_request($_REQUEST['id']);
    }
	
	public function delete_request(){
		$result = $this->FindFriend_Model->delete_request($_REQUEST['id']);
    }	
	
}
