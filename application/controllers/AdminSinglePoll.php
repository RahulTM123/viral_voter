<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminSinglePoll extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('AdminPoll_model');
		//$this->load->model('UserTimeline_model');
		$this->load->model('Dashboard_model');
    }
	
	public function index($id){
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$data['messages'] = $message->msg;
			
			$data['polldetail'] = $this->AdminPoll_model->getPollDetails($id);
			$data['pollcount'] = $this->AdminPoll_model->getPollCount($id);
			$data['polluser'] = $this->AdminPoll_model->getUserDetails($id);
			$data['admin'] = $this->AdminPoll_model->getAdminDetail();
			$record = 10;
			$start = 0;
			$data['participation'] = $this->AdminPoll_model->getParticipation($id, $start, $record);
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('adminsinglepoll');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
		else {
			$data['polldetail'] = $this->AdminPoll_model->getPollDetails($id);
			$data['pollcount'] = $this->AdminPoll_model->getPollCount($id);
			$data['polluser'] = $this->AdminPoll_model->getUserDetails($id);
			$data['admin'] = $this->AdminPoll_model->getAdminDetail();
			$record = 10;
			$start = 0;
			$data['participation'] = $this->AdminPoll_model->getParticipation($id, $start, $record);
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('adminsinglepoll');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
    }
	
	public function pollVoting(){
		$return = $this->UserTimeline_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollVoteOpt'],$_REQUEST['puserid']);
		$url = $_SERVER['HTTP_REFERER'];
		redirect($url);
	}
}
