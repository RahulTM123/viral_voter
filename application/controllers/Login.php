<?php 

defined('BASEPATH') OR exit('No direct script access allowed');



class Login extends CI_Controller {

	

	public function __construct(){

        parent::__construct();

		$this->load->library('upload');

		$this->load->library('session');

		$this->load->helper(array('form', 'url'));

		$this->load->library('facebook');

        	$this->load->library('google');

		$this->load->model('user');

		$this->load->model('countries');

		$this->load->model('EmailModel');

    }

	

	public function index(){

		if(isset($_SESSION['referer'])){

			//$data['referer'] = $_SESSION['referer'];

		}

		else {

			//$_SESSION['referer'] = (isset($_SERVER['HTTP_REFERER']))?$_SERVER['HTTP_REFERER']:base_url();

			//$data['referer'] = $_SESSION['referer'];

		}

		$data['countries'] = $this->countries->getCountries();

		$cc = $this->ip_info("Visitor", "Country Code");

		$data['current_country'] = (array)$this->countries->getCurrentCountry($cc);

		if($this->session->flashdata('error')){

			$data['error'] = $this->session->flashdata('error');

			$this->load->view('login',$data);

		}

		if($this->session->flashdata('msg')){

			$data['msg'] = $this->session->flashdata('msg');

			$this->load->view('login',$data);

		}

		if($this->session->userdata('userData')){

			redirect(base_url(),'refresh');

		}

		if(!isset($_REQUEST['page']) && !isset($_REQUEST['oauth_token']) && !$this->session->userdata('userData') && !$this->session->flashdata('error') && !$this->session->flashdata('msg')){

			$this->load->view('login',$data);	

		}

		if(isset($_REQUEST['oauth_token'])){

			$userData = array();

			//Include the twitter oauth php libraries

			include_once APPPATH."libraries/twitter-oauth-php-codexworld/twitteroauth.php";

			

			//Twitter API Configuration

			$consumerKey = 'IDyREOnyS8DwybcyjNKrDy1ZV';

			$consumerSecret = 'Pgsq250BUav17TM31hWMXTGApve6FucsUjSaZBOPGcLppmj68A';

			$oauthCallback = base_url().'login';

			

			//Get existing token and token secret from session

			$sessToken = $this->session->userdata('token');

			$sessTokenSecret = $this->session->userdata('token_secret');

			

			//Get status and user info from session

			$sessStatus = $this->session->userdata('status');

			

			$sessUserData = $this->session->userdata('userData');

			

			if($sessToken == $_REQUEST['oauth_token']){

				//Successful response returns oauth_token, oauth_token_secret, user_id, and screen_name

				$connection = new TwitterOAuth($consumerKey, $consumerSecret, $sessToken, $sessTokenSecret); //print_r($connection);die;

				$accessToken = $connection->getAccessToken($_REQUEST['oauth_verifier']);

				if($connection->http_code == '200'){

					$userInfo = $connection->get('account/verify_credentials');

					//Preparing data for database insertion

					$name = explode(" ",$userInfo->name);

					$first_name = isset($name[0])?$name[0]:'';

					$last_name = isset($name[1])?$name[1]:'';

					$userData = array(

						'oauth_provider' => 'twitter',

						'oauth_uid' => $userInfo->id,

						'username' => $userInfo->screen_name,

						'firstname' => $first_name,

						'lastname' => $last_name,

						'locale' => $userInfo->lang,

						'profile_url' => 'https://twitter.com/'.$userInfo->screen_name,

						'picture_url' => $userInfo->profile_image_url

					);

					

					

					//Insert or update user data

					$userID = $this->user->checkUserTwitter($userData);

					//Store status and user profile info into session

					$userData['accessToken'] = $accessToken;

					$this->session->set_userdata('status','verified');

					$userData['userId'] = $userID;

					$this->session->set_userdata('userData',$userData);

					

					if(!empty($userID)){

						//$data['userData'] = $userData;

						//$data['tlogoutUrl'] = base_url().'/TwitterLogout';

                        $this->session->set_userdata('tlogoutUrl', base_url().'TwitterLogout');

						//redirect($data['referer'],'refresh');

						redirect(base_url().'','refresh');

					}

					//Get latest tweets

					//$data['tweets'] = $connection->get('statuses/user_timeline', array('screen_name' => $userInfo->screen_name, 'count' => 5));

					//$data['userData'] = $userData;

				}else{

					$data['error_msg'] = 'Some problem occurred, please try again later!';

				}

			}else{

				$this->load->view('login');

			}	

		}

    }	

	

	

	public function socialLogin(){

		if($_REQUEST['page'] == 'facebook'){

			require_once(APPPATH.'libraries/Facebook/autoload.php');

			$fb = new Facebook\Facebook([

			  'app_id' => '2051773518371640',

			  'app_secret' => '7e859600a841c1297a92360c9d41c642',

			  'default_graph_version' => 'v2.12',

			  ]);

			  $helper = $fb->getRedirectLoginHelper();

			  $permissions = []; // optional

			  $loginUrl = $helper->getLoginUrl('http://www.viralvoters.com/beta/FbLogin');

			  redirect($loginUrl, 'refresh');

		}elseif($_REQUEST['page'] == 'google'){

			redirect($this->google->loginURL(), 'refresh');

		}else{

			$userData = array();

			//Include the twitter oauth php libraries

			include_once APPPATH."libraries/twitter-oauth-php-codexworld/twitteroauth.php";

			

			//Twitter API Configuration

			$consumerKey = 'IDyREOnyS8DwybcyjNKrDy1ZV';

			$consumerSecret = 'Pgsq250BUav17TM31hWMXTGApve6FucsUjSaZBOPGcLppmj68A';

			$oauthCallback = base_url().'Login';

			

			//Get existing token and token secret from session

			$sessToken = $this->session->userdata('token');

			$sessTokenSecret = $this->session->userdata('token_secret');

			

			//Get status and user info from session

			$sessStatus = $this->session->userdata('status');

			

			$sessUserData = $this->session->userdata('userData');

			

			if(isset($_REQUEST['oauth_token']) && $sessToken == $_REQUEST['oauth_token']){

				

			}else{

				//unset token and token secret from session

				$this->session->unset_userdata('token');

				$this->session->unset_userdata('token_secret');

				

				//Fresh authentication

				$connection = new TwitterOAuth($consumerKey, $consumerSecret);

				$requestToken = $connection->getRequestToken($oauthCallback);

				

				//Received token info from twitter

				$this->session->set_userdata('token',$requestToken['oauth_token']);

				$this->session->set_userdata('token_secret',$requestToken['oauth_token_secret']);

				

				//Any value other than 200 is failure, so continue only if http code is 200

				if($connection->http_code == '200'){

					//redirect user to twitter

					$twitterUrl = $connection->getAuthorizeURL($requestToken['oauth_token']);

					redirect($twitterUrl, 'refresh');

					//header('location:'.$twitterUrl);

					//$this->load->view('login',$UserData);

				}

			}

		}	

    }	

	

	public function userLogin(){

		if(!empty($_REQUEST)){

			$oauth_provider = 'site_login';

			$email = $_REQUEST['email'];

			$pass = md5($_REQUEST['pass']);

			$userData = $this->user->userLogin($oauth_provider,$email,$pass);	

			if(!empty($userData)){

				if(isset($userData['error'])){

					//$data['error'] = $userData['error'];

					$this->session->set_flashdata('error', $userData['error']);	

					redirect('./login','refresh');

				}else{

					$this->session->set_userdata('userData',$userData);	

					redirect('./','refresh');

				}

			}else{

				$this->session->set_flashdata('error', 'Your login credentials are incorrect!');

				redirect('./login','refresh');

			}

		}

    }	

	

	public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {

		$output = NULL;

		if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {

			$ip = $_SERVER["REMOTE_ADDR"];

			if ($deep_detect) {

				if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))

					$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

				if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))

					$ip = $_SERVER['HTTP_CLIENT_IP'];

			}

		}

		$purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));

		$support    = array("country", "countrycode", "state", "region", "city", "location", "address");

		$continents = array(

			"AF" => "Africa",

			"AN" => "Antarctica",

			"AS" => "Asia",

			"EU" => "Europe",

			"OC" => "Australia (Oceania)",

			"NA" => "North America",

			"SA" => "South America"

		);

		if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {

			$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));

			if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {

				switch ($purpose) {

					case "location":

						$output = array(

							"city"           => @$ipdat->geoplugin_city,

							"state"          => @$ipdat->geoplugin_regionName,

							"country"        => @$ipdat->geoplugin_countryName,

							"country_code"   => @$ipdat->geoplugin_countryCode,

							"continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],

							"continent_code" => @$ipdat->geoplugin_continentCode

						);

						break;

					case "address":

						$address = array($ipdat->geoplugin_countryName);

						if (@strlen($ipdat->geoplugin_regionName) >= 1)

							$address[] = $ipdat->geoplugin_regionName;

						if (@strlen($ipdat->geoplugin_city) >= 1)

							$address[] = $ipdat->geoplugin_city;

						$output = implode(", ", array_reverse($address));

						break;

					case "city":

						$output = @$ipdat->geoplugin_city;

						break;

					case "state":

						$output = @$ipdat->geoplugin_regionName;

						break;

					case "region":

						$output = @$ipdat->geoplugin_regionName;

						break;

					case "country":

						$output = @$ipdat->geoplugin_countryName;

						break;

					case "countrycode":

						$output = @$ipdat->geoplugin_countryCode;

						break;

				}

			}

		}

		$output = (!empty($output))?$output:'IN';

		return $output;

	}

	

	public function forgot_password() {

		$data['msg'] = '';

		$data['msguser'] = '';

		$data['template']='forgot_password';

		if(isset($_POST['reset'])) {

			$result = (array)$this->user->resetPassword($_POST['email']);

			if($result) {

				$ref = md5(date('Y-m-d H:i:s'));

				$isertRef = $this->user->isertRef($result['id'],$ref);

				$body = '<table width="600" border="0" cellspacing="0" cellpadding="10">

					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>

					<tr><td>

						<p>Dear Member,<br />Click on the below url to reset your password</p>

						<p><a href="'.base_url().'login/password-reset?ref='.$ref.'&email='.$_POST['email'].'">'.base_url().'login/password-reset?ref='.$ref.'&email='.$_POST['email'].'</a></p>

						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>

						<p>Please do not reply to this email.</p>

						

					</td></tr>

					</table>';

				$subject = 'Password reset from ViralVoters.com';

				$result = $this->EmailModel->phpmailer($_POST['email'], $body, $subject);

				$data['msg'] = 'Password reset instruction sent to your email id.';
				$data['template']='forgot_password';

			}else {

				$data['msguser'] = 'No user registered with email :';
				

			}

		}


		$this->load->view('header', $data);

		$this->load->view('left_sidebar');

		$this->load->view($data['template']);

		$this->load->view('right_sidebar');

		$this->load->view('footer');

	}


//test

public function alternate_email() {

		$data['msg'] = '';

		$data['msguser'] = '';

		$data['template']='alternate_email';

		if(isset($_POST['reset2'])) {

			$result = (array)$this->user->resetPassword_alternate($_POST['alternate_email']);

			if($result) {

				$ref = md5(date('Y-m-d H:i:s'));

				$isertRef = $this->user->isertRef($result['id'],$ref);

				$body = '<table width="600" border="0" cellspacing="0" cellpadding="10">

					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>

					<tr><td>

						<p>Dear Member,<br />Click on the below url to reset your password</p>

						<p><a href="'.base_url().'login/password-reset?ref='.$ref.'&alternate_email='.$_POST['alternate_email'].'">'.base_url().'login/password-reset?ref='.$ref.'&alternate_email='.$_POST['alternate_email'].'</a></p>

						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>

						<p>Please do not reply to this email.</p>

						

					</td></tr>

					</table>';

				$subject = 'Password reset from ViralVoters.com';

				$result = $this->EmailModel->phpmailer($_POST['alternate_email'], $body, $subject);

				$data['msg'] = 'Password reset instruction sent to your email id.';
				$data['template']='alternate_email';




			}else {

				$data['msguser'] = 'No user registered with this alternate email :';
				

			}

		}


		$this->load->view('header', $data);

		$this->load->view('left_sidebar');

		$this->load->view($data['template']);

		$this->load->view('right_sidebar');

		$this->load->view('footer');

	}





//Alternate Mail Check

	public function alternate_emaill(){

		

		$data['msg'] = '';

		$data['msguser'] = '';

		if(isset($_POST['reset2'])) {

			$result = (array)$this->user->resetPassword_alternate($_POST['alternate_email']);

			if($result) {

				$ref = md5(date('Y-m-d H:i:s'));

				$isertRef = $this->user->isertRef($result['id'],$ref);

				$body = '<table width="600" border="0" cellspacing="0" cellpadding="10">

					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>

					<tr><td>

						<p>Dear Member,<br />Click on the below url to reset your password</p>

						<p><a href="'.base_url().'login/password-reset?ref='.$ref.'&email='.$_POST['alternate_email'].'">'.base_url().'login/password-reset?ref='.$ref.'&email='.$_POST['alternate_email'].'</a></p>

						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>

						<p>Please do not reply to this email.</p>

						

					</td></tr>

					</table>';

				$subject = 'Password reset from ViralVoters.com';

				$result = $this->EmailModel->phpmailer($_POST['alternate_email'], $body, $subject);

				$data['msg'] = 'Password reset instruction sent to your email id.';
			

			}else {

				$data['msguser'] = 'No user registered with email :';
				

			}

		}

		$this->load->view('header', $data);

		$this->load->view('left_sidebar');

		$this->load->view('alternate_email');

		$this->load->view('right_sidebar');

		$this->load->view('footer');
		

	}



	

	public function update_password() {

		$data['msg'] = '';

		if(isset($_POST['update'])) {

			$result = $this->user->updatePassword($_POST['email'], $_POST['newpass']);

		}

		if(isset($_POST['update'])) {

			$result = $this->user->updatePassword_alt($_POST['alternate_email'], $_POST['newpass']);

		}

		$this->load->view('header', $data);

		$this->load->view('left_sidebar');

		$this->load->view('reset_password');

		$this->load->view('right_sidebar');

		$this->load->view('footer');

	}

	

	public function password_reset() {

		$data['msg'] = '';



	//	echo $this->input->get('alternate_email');
		

		



		if(isset($_POST['reset']) && isset($_POST['cpass'])) {




				$result = $this->user->updatePassword_alt($_POST['cpass'], $_POST['ref'], $_POST['alternate_email']);

		}

		else {

			if(!empty($_REQUEST['ref']) && !empty($_REQUEST['alternate_email'])) {

				$result = $this->user->getResetUser_alt($_REQUEST['ref'], $_REQUEST['alternate_email']);

				

				if($result) {

					$data['alternate_email'] = $_REQUEST['alternate_email'];

					$data['ref'] = $_REQUEST['ref'];

				}

				else {

					$data['alternate_email'] = '';

					$data['ref'] = '';

					$data['msg'] = 'Information tempered2';

				}

			}

		}


		
		if(isset($_POST['reset']) && isset($_POST['cpass'])) {




				$result = $this->user->updatePassword($_POST['cpass'], $_POST['ref'], $_POST['email']);

		}

		else {

			if(!empty($_REQUEST['ref']) && !empty($_REQUEST['email'])) {

				$result = $this->user->getResetUser($_REQUEST['ref'], $_REQUEST['email']);

				

				if($result) {

					$data['email'] = $_REQUEST['email'];

					$data['ref'] = $_REQUEST['ref'];

				}

				else {

					$data['email'] = '';

					$data['ref'] = '';

					$data['msg'] = 'Information tempered';

				}

			}

		}

		$this->load->view('header', $data);

		$this->load->view('left_sidebar');

		$this->load->view('password_reset');

		$this->load->view('right_sidebar');

		$this->load->view('footer');

	}

}

