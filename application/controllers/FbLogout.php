<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FbLogout extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('facebook');
    }
	
	public function index()
    {
		// Remove local Facebook session
		$this->facebook->destroy_session();
		// Remove user data from session
		$this->session->unset_userdata('userData');
                $this->session->unset_userdata('logoutUrl');
		$this->session->unset_userdata('facebooklogin');
		// Redirect to login page
        redirect('/login');
        
    }
}
