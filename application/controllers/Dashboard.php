<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->model('SinglePoll_model');
		$this->load->model('Dashboard_model');
		$this->load->model('Home_model');
    }
	
	public function index(){
		$start = 0;
		$limit = 10;
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$trending = $this->Home_model->getTrendingPolls(0);
			$data['trending'] = $trending;
			
			$topSixRecentVideo = $this->Home_model->getVideos();
			$data['videoData'] = $topSixRecentVideo;
			
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$data['result'] = $this->Home_model->getActivities($userData['userId'], $start, $limit);
			$data['messages'] = $message->msg;
			//echo '<pre>';print_r($data);
			
			//echo '<pre>';
			//print_r($timeline);
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('dashboard');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
			redirect(base_url());	
		}else{
			redirect('./login');	
		}
    }
	
	
	public function addNewPoll(){
		if($this->session->userdata('userData')){
		if(isset($_REQUEST['pubPoll'])){
			$polloption = array();
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
			
			$data = $_REQUEST;
			
			$interest = (isset($_REQUEST['poll_category']))?$_REQUEST['poll_category']:'';
			
			$friends = $this->Dashboard_model->getUserFrndIds($userid);
			$userlist = $this->Dashboard_model->getUserByInterest($userid, $interest);
			$frn = explode(',', $friends);
			
			$alluser = array();
			foreach($userlist as $user)
				$alluser[$user->id] = $user->id;
			
			foreach($frn as $frnds)
				$alluser[$frnds] = $frnds;
			
			unset($alluser[$userid]);
			$alluser = implode(',', $alluser);
			
			if(($_SESSION['userData']['activestatus'] == 7) || ($_SESSION['userData']['activestatus'] == 15) || ($_SESSION['userData']['activestatus'] == 100)) {
				$this->session->set_flashdata('msg', 'Your profile is blocked, you cannot submit poll/post/comment');
				redirect($_SERVER['HTTP_REFERER']);
			}
			if(($_SESSION['userData']['activestatus'] == 200)) {
				$this->session->set_flashdata('msg', 'Your profile is deleted, you cannot submit poll/post/comment');
				redirect($_SERVER['HTTP_REFERER']);
			}
			$result = $this->Dashboard_model->addNewPoll($data, $alluser, $userid);
						
			if(!empty($result)){
				//$this->session->set_flashdata('msg', 'New poll successfully created');
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				//$this->session->set_flashdata('msg', 'Something wrong here, please try again');
				redirect($_SERVER['HTTP_REFERER']);
			}
			
		}
		}else{
			redirect('./login');	
		}
	}
	
	
	public function pollVoting(){
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
			$friends = $this->Dashboard_model->getUserFrndIds($userid);
			$frn = explode(',', $friends);
			$alluser = array();
			
			foreach($frn as $frnds)
				$alluser[$frnds] = $frnds;
			
			unset($alluser[$userid]);
			$alluser = implode(',', $alluser);
			$alluser = implode(',', $alluser);
			
			if(($_SESSION['userData']['activestatus'] == 7) || ($_SESSION['userData']['activestatus'] == 15) || ($_SESSION['userData']['activestatus'] == 100)) {
				$this->session->set_flashdata('msg', 'Your profile is blocked, you cannot vote');
				redirect($_SERVER['HTTP_REFERER']);
			}
			if(($_SESSION['userData']['activestatus'] == 200)) {
				$this->session->set_flashdata('msg', 'Your profile is deleted, you cannot vote');
				redirect($_SERVER['HTTP_REFERER']);
			}
				
			$return = $this->Dashboard_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollopt'],$userid,$_SERVER['REMOTE_ADDR'],$alluser);
			if(!empty($return)){
				//$this->session->set_flashdata('msg', 'Thank you for submit your opinion');
				redirect($_SERVER['HTTP_REFERER']);
			}else{
				//$this->session->set_flashdata('msg', 'Something wrong here, please try again');
				redirect($_SERVER['HTTP_REFERER']);
			}
		}else{
			redirect('./login');	
		}
	}
	
	
	
	public function addNewPost(){
		if($this->session->userdata('userData')){
			if(isset($_REQUEST['pubPost'])){
				$polloption = array();
				$userData = $this->session->userdata('userData');
				$userid = $userData['userId'];
				$postdata = $postimg = $postvideo = '';
				$dataurl = array();
				
				if(!empty($_REQUEST['postdata'])){
					$postdata = $_REQUEST['postdata'];
					if (filter_var($_REQUEST['postdata'], FILTER_VALIDATE_URL)) {
					$tags = get_meta_tags($_REQUEST['postdata']);
					$dataurl['title'] = (isset($tags['twitter:title']))?$tags['twitter:title']:'';
					$dataurl['image'] = (isset($tags['twitter:image']))?$tags['twitter:image']:'';
					$dataurl['url'] = $_REQUEST['postdata'];
					$dataurl['description'] = (isset($tags['twitter:description']))?$tags['twitter:description']:'';
	
					$sites_html = file_get_contents($_REQUEST['postdata']);
					$sites_html = mb_convert_encoding($sites_html, 'HTML-ENTITIES', "UTF-8");
	
					$html = new DOMDocument();
					@$html->loadHTML($sites_html);
					$meta_og_img = null;
					//Get all meta tags and loop through them.
					foreach($html->getElementsByTagName('meta') as $meta) {
						if($meta->getAttribute('property')=='og:image'){ 
							$dataurl['image'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$dataurl['image'];
						}
						if($meta->getAttribute('property')=='og:title'){ 
							$dataurl['title'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$dataurl['title'];
						}
						if($meta->getAttribute('property')=='og:description'){ 
							$dataurl['description'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$dataurl['description'];
						}
					}
				}
				}
				
				if(!empty($_REQUEST['postvideolink'])){
					$postvideo = $_REQUEST['postvideolink'];
				}
				
				$data = $_REQUEST;			
				$data['dataurl'] = $dataurl;			
				$interest = (isset($_REQUEST['post_category']))?$_REQUEST['post_category']:'';
				
				$friends = $this->Dashboard_model->getUserFrndIds($userid);
				$userlist = $this->Dashboard_model->getUserByInterest($userid, $interest);
				$frn = explode(',', $friends);
				
				$alluser = array();
				foreach($userlist as $user)
					$alluser[$user->id] = $user->id;
				
				foreach($frn as $frnds)
					$alluser[$frnds] = $frnds;
				
				unset($alluser[$userid]);
				$alluser = implode(',', $alluser);
			
				if(($_SESSION['userData']['activestatus'] == 7) || ($_SESSION['userData']['activestatus'] == 15) || ($_SESSION['userData']['activestatus'] == 100)) {
					$this->session->set_flashdata('msg', 'Your profile is blocked, you cannot create post');
					redirect($_SERVER['HTTP_REFERER']);
				}
				if(($_SESSION['userData']['activestatus'] == 200)) {
					$this->session->set_flashdata('msg', 'Your profile is deleted, you cannot create post');
					redirect($_SERVER['HTTP_REFERER']);
				}
				
				$result = $this->Dashboard_model->addNewPost($data, $alluser, $userid);
							
				if(!empty($result)){
					redirect($_SERVER['HTTP_REFERER']);
			}else{
				redirect($_SERVER['HTTP_REFERER']);
			}
			
			if(!empty($result)){
				//$this->session->set_flashdata('msg', 'New post successfully created');
			}else{
				//$this->session->set_flashdata('msg', 'Something wrong here, please try again');
			}
			
		}
		}else{
			redirect('./login');	
		}
	}
	
	
	
	public function uploadfile(){
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			if(isset($_FILES["myfile"])) {
				$ret = array();
				$fileid = array();
				$error =$_FILES["myfile"]["error"];
				$imgren = md5(time());
				$thfileName = 'thumb_'.$imgren.$_FILES['myfile']['name'];
				$fileName = $imgren.$_FILES['myfile']['name'];
				
				if (!file_exists('uploads/user'.$userData['userId'])) {
					mkdir('uploads/user'.$userData['userId'], 0777, true);
				}
				$dir_path = 'uploads/user'.$userData['userId'].'/';
				
				$thumbnail = array(
					'image_library' => 'gd2',
					'source_image' => $_FILES["myfile"]["tmp_name"],
					'new_image' => $dir_path.$thfileName,
					'master_dim' => 'auto',
					'maintain_ratio' => true,
					'width' => 300,
					'height' => 300
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($thumbnail);
				$this->image_lib->resize();
				
				$bigimg = array(
					'image_library' => 'gd2',
					'source_image' => $_FILES["myfile"]["tmp_name"],
					'new_image' => $dir_path.$fileName,
					'maintain_ratio' => TRUE,
					'width' => 1000,
					'height' => 700
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($bigimg);
				$this->image_lib->resize();
				
					//move_uploaded_file($_FILES["myfile"]["tmp_name"],$dir_path.$fileName);
				$fileid = $this->Dashboard_model->addFile($dir_path.$thfileName, $_FILES["myfile"]["type"], $userData['userId']);
				$ret[$fileid]= $dir_path.$fileName;
				
				echo json_encode($ret);
			 }

			/*$html = '';
			$userData = $this->session->userdata('userData');
			
			$categories = $this->Dashboard_model->getCatList();
			$timeline['categories'] = $categories;
			
			$timeline['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$timeline['messages'] = $message->msg;
			//$timeline['data'] = $this->Dashboard_model->getUserTimeline($userData['userId']);*/
			
			//$this->load->view('header',$timeline);
			//$this->load->view('left_sidebar');
			//$this->load->view('dashboard');
			//$this->load->view('right_sidebar');
			//$this->load->view('footer');
		}else{
			redirect('./login');	
		}
	}
}
