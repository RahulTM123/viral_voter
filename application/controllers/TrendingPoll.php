<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class TrendingPoll extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Tranding_model');
		$this->load->model('Home_model');
		$this->load->model('Dashboard_model');
		$this->load->model('ajaxss');
    }
	
	public function index(){
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
		$page = (isset($_POST['page']))?$_POST['page']*10:0;
		
		if(isset($_REQUEST['filter'])) {
			$sort = (isset($_REQUEST['sortby']))?$_REQUEST['sortby']:'';
			$category = (isset($_REQUEST['category']))?$_REQUEST['category']:'';
			$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
			$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'])):'';
			$type = ($_REQUEST['type'])?$_REQUEST['type']:'';
			
			$trending = $this->Home_model->getFilterTrendingList($sort, $category, $type, $page, $dfrom, $dto);
			$data['result'] = $trending;
		}
		else {
			$trending = $this->Home_model->getTrendingList($page);
			$data['result'] = $trending;
		}
			
			$categories = $this->Dashboard_model->getCatList();
			$data['categories'] = $categories;
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('trandingpoll');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
    }
	
	public function ajaxpoll(){
		$page = (isset($_POST['page']))?$_POST['page']*10:0;
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$data['messages'] = $message->msg;
		}else{
		}
		
		if(isset($_POST['filter'])) {
			$sort = ($_POST['sortby'])?$_POST['sortby']:'';
			$category = ($_POST['category'])?$_POST['category']:'';
			$type = ($_POST['type'])?$_POST['type']:'';
			$dfrom = (!empty($_POST['dfrom']))?date('Y-m-d', strtotime($_POST['dfrom'])):'';
			$dto = (!empty($_POST['dto']))?date('Y-m-d', strtotime($_POST['dto'])):'';
			
			$trending = $this->Home_model->getFilterTrendingList($sort, $category, $type, $page, $dfrom, $dto);
			$data['trending'] = $trending;
		}
		else {
			$trending = $this->Home_model->getTrendingList($page);
			$data['trending'] = $trending;
		}
			
			$categories = $this->Dashboard_model->getCatList();
			$data['categories'] = $categories;
			
			$this->load->view('trendingpollajax',$data);
    }
}
