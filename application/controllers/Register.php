<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->helper('url');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('user');
		$this->load->model('clogs');
    }
	
	public function index(){
		if(!empty($_REQUEST)){
			$userData = array(
				'oauth_provider' => 'site_login',
				'firstname' => $_REQUEST['firstname'],
				'lastname' => $_REQUEST['lastname'],
				'email' => $_REQUEST['email'],
				'alternate_email' => $_REQUEST['alternate_email'],
				'state' => $_REQUEST['state'],
				'city' => $_REQUEST['city'],
				'country' => $_REQUEST['country'],
				'password' => md5($_REQUEST['pass']),
				'status' => '0'
			);
			$name = $_REQUEST['firstname'].' '.$_REQUEST['lastname'];
			$city = $_REQUEST['cityajax'];
			$userID = $this->user->register($userData);
			$desctext = 'User register Name '.$_REQUEST['firstname'].' '.$_REQUEST['lastname'].', Email '.$_REQUEST['email'].',, Alternate_Email '.$_REQUEST['alternate_email'].' City ID / Country ID '.$_REQUEST['city'].', '.$_REQUEST['country'].', IP '.$_SERVER['REMOTE_ADDR'].', Date Time '.date('d-m-Y H:i:s').', Access Page '.$_SERVER['HTTP_HOST'].' '.$_SERVER['REQUEST_URI'];
			$logtext = json_encode($_SERVER);
			$logdata = array(
				'userid' => $userID,
				'desctext' => $desctext,
				'logtext' => $logtext,
				'reference' => 'new registration',
				'create_date' => date('Y-m-d H:i:s')
			);
			$this->clogs->add($logdata);
			if(!empty($userID)){
				$return = $this->phpmailer($_REQUEST['email'], $name, $userID, $city);
				if($return == '1'){
					$this->session->set_flashdata('msg','You are successfully register with viralvoters. Please check your email for activation link!');
					$data['msg'] = 'Email verification mail has been sent on your mail id!';
					redirect('./thankyou','refresh');
					//$this->load->view('login',$data);
				}else{
					$this->session->set_flashdata('msg','Something wrong with your email id!');
					$data['msg'] = 'Email verification mail has been sent on your mail id!';
					redirect('./login','refresh');
					//$this->load->view('login',$data);
				}
			}else{
				$this->session->set_flashdata('msg','Email id already exist!');
				$data['msg'] = 'Email verification mail has been sent on your mail id!';
				redirect('./login','refresh');
				//$this->load->view('login',$data);
			}
		}
    }
	
	public function phpmailer($email, $name, $id, $city){
		$this->load->library("PhpMailerLib");
		$smtpDetails = $this->user->getSmtpDetails();
		$mailContent = $this->user->getTemplate();
        $mail = $this->phpmailerlib->load();
        $mailad = $this->phpmailerlib->load();
		$ref = md5(date('Y-m-d H:i:s'));
		$isertRef = $this->user->isertRef($id,$ref);
		if(!empty($smtpDetails) && !empty($mailContent)){
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = $smtpDetails[0]->username;                 // SMTP username
				$mail->Password = $smtpDetails[0]->password;                           // SMTP password
				$mail->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to connect to
				//Recipients
				$mail->setFrom($smtpDetails[0]->fromEmail, $smtpDetails[0]->fromName);
				$mail->addAddress($email, $name);            // Name is optional
				$mail->addReplyTo($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $mailContent[0]->subject;
				//$mail->Body    = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'&ref='.$ref.'</a>';
				$mail->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear Member,<br />Thanks for your registration with ViralVoters.Com, please verify your email.</p>
						
						
						<p><a href="'.base_url().'verifyemail?ref='.$ref.'">'.base_url().'verifyemail?ref='.$ref.'</a></p>
						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>
						<p>Please do not reply to this email.</p>
						
					</td></tr>
					</table>';
				//$mail->AltBody = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'</a>';
	
				$mail->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}
			try {
				//Server settings
				$mailad->SMTPDebug = 0;                                 // Enable verbose debug output
				$mailad->isSMTP();                                      // Set mailer to use SMTP
				$mailad->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
				$mailad->SMTPAuth = true;                               // Enable SMTP authentication
				$mailad->Username = $smtpDetails[0]->username;                 // SMTP username
				$mailad->Password = $smtpDetails[0]->password;                           // SMTP password
				$mailad->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
				$mailad->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to connect to
				//Recipients
				$mailad->setFrom($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
				$mailad->addAddress('info@viralvoters.com', 'ViralVoters');            // Name is optional
				//$mailad->addAddress('sachinsaini.cs@gmail.com', 'ViralVoters');            // Name is optional
				$mailad->addReplyTo($email, $name);
				//$mailad->AddBCC('rajneesh.sharma@yahoo.com');
				//$mailad->AddBCC('shalabh5@hotmail.com');
				$mailad->AddBCC('viralvoters@gmail.com');
				$mailad->isHTML(true);                                  // Set email format to HTML
				$mailad->Subject = $mailContent[0]->subject;
				//$mail->Body    = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'&ref='.$ref.'</a>';
				$mailad->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="http://www.viralvoters.com/beta/assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear Admin,<br />New user registered at ViralVoters.</p>
						<p>Name: '.$name.'<br />Email: '.$email.'<br />City: '.$city.'<br /></p>
						<p>Team ViralVoters<br /><a href="https://www.viralvoters.com">ViralVoters.com</a></p>
						
					</td></tr>
					</table>';
				//$mail->AltBody = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'</a>';
	
				$mailad->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}
		}else{
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP
				$mail->Host = 'mail.viralvoters.com';  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = 'auth@viralvoters.com';                 // SMTP username
				$mail->Password = 'sachin@123';                           // SMTP password
				$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = 465;                                    // TCP port to connect to
				//Recipients
				$mail->setFrom('auth@viralvoters.com', 'ViralVoters');
				$mail->addAddress($email, $name);            // Name is optional
				$mail->addReplyTo('info@viralvoters.com', 'ViralVoters');
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = 'ViralVoters - Email Verification.';
				//$mail->Body    = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'&ref='.$ref.'</a>';
				$mail->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="http://www.viralvoters.com/beta/assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear Member,<br />Thanks for your registration with ViralVoters.Com, please verify your email.</p>
						
						
						<p><a href="'.base_url().'VerifyEmail?ref='.$ref.'">'.base_url().'VerifyEmail?ref='.$ref.'</a></p>
						
						<p>Team ViralVoters<br /><a href="https://www.viralvoters.com">ViralVoters.com</a></p>
						<p>Please do not reply to this email.</p>
						
					</td></tr>
					</table>';
				//$mail->AltBody = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'</a>';
	
				$mail->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}	
			try {
				//Server settings
				$mailad->SMTPDebug = 0;                                 // Enable verbose debug output
				$mailad->isSMTP();                                      // Set mailer to use SMTP
				$mailad->Host = 'mail.viralvoters.com';  // Specify main and backup SMTP servers
				$mailad->SMTPAuth = true;                               // Enable SMTP authentication
				$mailad->Username = 'auth@viralvoters.com';                 // SMTP username
				$mailad->Password = 'sachin@123';                           // SMTP password
				$mailad->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
				$mailad->Port = 465;                                    // TCP port to connect to
				//Recipients
				$mailad->setFrom('auth@viralvoters.com', 'ViralVoters');
				$mailad->addAddress('info@viralvoters.com', 'ViralVoters');            // Name is optional
				//$mailad->addAddress('sachinsaini.cs@gmail.com', 'ViralVoters');            // Name is optional
				$mailad->addReplyTo($email, $name);
				//$mailad->AddBCC('rajneesh.sharma@yahoo.com');
				//$mailad->AddBCC('shalabh5@hotmail.com');
				$mailad->AddBCC('viralvoters@gmail.com');
				$mailad->isHTML(true);                                  // Set email format to HTML
				$mailad->Subject = 'ViralVoters - New User Registration';
				//$mail->Body    = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'&ref='.$ref.'</a>';
				$mailad->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="http://www.viralvoters.com/beta/assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear Admin,<br />New user registered at ViralVoters.</p>
						<p>Name: '.$name.'<br />Email: '.$email.'<br />City: '.$city.'<br /></p>
						<p>Team ViralVoters<br /><a href="https://www.viralvoters.com">ViralVoters.com</a></p>
						
					</td></tr>
					</table>';
				//$mail->AltBody = '<a href="'.base_url().'VerifyEmail?id='.$id.'">'.base_url().'VerifyEmail?id='.$id.'</a>';
	
				$mailad->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}	
		}
		return $i;
		
	}
}
