<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Home_model');
		$this->load->model('SinglePoll_model');
		$this->load->model('Dashboard_model');
		$this->load->model('cities');
		$this->load->model('ajaxss');
    }
	
	public function index(){
		$start = 0;
		$limit = 10;
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		$data['frequest'] = '';
		$data['messages'] = '';
		if(!$this->session->userdata('userData')){
			$trending = $this->Home_model->getTrendingPolls(0);
			$data['trending'] = $trending;
			$topSixRecentVideo = $this->Home_model->getVideos();
			$data['peoples'] = $this->Home_model->getPeopleKnow();
			$data['result'] = $this->Home_model->getActivities('', $start, $limit);
			$data['videoData'] = $topSixRecentVideo;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('index');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}else{
			$userData = $this->session->userdata('userData');
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$trending = $this->Home_model->getTrendingPolls(0);
			$data['trending'] = $trending;
			
			$topSixRecentVideo = $this->Home_model->getVideos();
			$data['videoData'] = $topSixRecentVideo;
			
			$data['result'] = $this->Home_model->getActivities($userData['userId'], $start, $limit);
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
			//echo '<pre>';print_r($data);
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('index');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
    }
	
	public function pollVoting(){
		$friends = $this->Dashboard_model->getUserFrndIds($_REQUEST['puserid']);
		$frn = explode(',', $friends);
		$alluser = array();
		
		foreach($frn as $frnds)
			$alluser[$frnds] = $frnds;
		
		unset($alluser[$_REQUEST['puserid']]);
		$alluser = implode(',', $alluser);
			
		$return = $this->Dashboard_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollopt'],$_REQUEST['puserid'],$_SERVER['REMOTE_ADDR'],$alluser);
		$url = $_SERVER['HTTP_REFERER'];
		if(!empty($return)){
			redirect($url);
		}else{
			redirect($url);
		}
	}
	
	public function polls(){
		$start = 0;
		$limit = 10;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$sortby = (isset($_POST['sortby']))?$_POST['sortby']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		$type = (isset($_POST['type']))?$_POST['type']:'';
		$dfrom = (!empty($_POST['dfrom']))?date('Y-m-d', strtotime($_POST['dfrom'])):'';
		$dto = (!empty($_POST['dto']))?date('Y-m-d', strtotime($_POST['dto'])):'';
			
		if(isset($_POST['search'])){
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result = $this->Home_model->getPollByFilter($start, $limit, $sortby, $category, $type, $dfrom, $dto);
		}
		else{
			$result = $this->Home_model->getActivePolls($start, $limit);
		}
		
		$category = $this->Home_model->getCategories();
		
		$data['result'] = $result;
		$data['category'] = $category;
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}
			
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('frontpoll');
		$this->load->view('right_sidebar');
		$this->load->view('footer');
    
	}
	
	public function archive(){
		$start = 0;
		$limit = 10;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$sortby = (isset($_POST['sortby']))?$_POST['sortby']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
			
		if(isset($_POST['search'])){
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result = $this->Home_model->getArchivePollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category);
		}
		else{
			$result = $this->Home_model->getArchivePolls($start, $limit);
		}
		
		$category = $this->Home_model->getCategories();
		
		$data['result'] = $result;
		$data['category'] = $category;
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}
			
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('frontpoll_archive');
		$this->load->view('footer');
    
	}
	
	public function embedPoll($id){
		$embed = $this->Home_model->getEmbedCode($id, 'polls');
		echo $embed;
	}
	
	public function embedCode($id, $type){
		echo '&lt;iframe src="http://www.viralvoters.com/beta/embed/'.$type.'/'.$id.'" width="100%" height="460"&gt;&lt;/iframe&gt;';
	}
}
