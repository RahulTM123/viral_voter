<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserParticipation extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model','survey');
    }
	
	public function index($id){
		if($this->session->userdata('adminData')){
			$result = $this->survey->userParticipation($id);
			$data['htmlData'] = $result;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/surveyDetails');
			$this->load->view('admin/footer');
		}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
    }
}
