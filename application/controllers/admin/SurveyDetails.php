<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyDetails extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model');
    }
	
	public function index($id){
		if(!$this->session->userdata('adminData')){
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}else{
			$result = $this->Survey_model->getSurveyDetails($id);
			$data['htmlData'] = $result;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/surveyDetails');
			$this->load->view('admin/footer');
		}
    }
	
	public function changeStatusDetail(){
		$html = '';
		$result = $this->Survey_model->changeStatusDetail($_REQUEST['id']);
		if($result){
			redirect('./admin/survey_details?id='.$_REQUEST['id']);
		}
	}
	
	public function deleteSurveyDetail(){
		$html = '';
		$result = $this->Survey_model->deleteSurveyDetail($_REQUEST['id']);
		if($result){
			redirect('./admin/survey_details?id='.$_REQUEST['id']);
		}
	}
}
