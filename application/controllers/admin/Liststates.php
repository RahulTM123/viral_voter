<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Liststates extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('states');
    }
	
	public function index(){
		if(isset($_POST['addstate'])) {
			$this->states->addState($_POST['statename'], $_POST['cid'], $_POST['status']);
		}
		if(isset($_POST['sstatus'])) {
			$status = ($_POST['sstatus']=='Active')?0:1;
			$this->states->updateStatus($status, $_POST['sid']);
		}
		if(isset($_REQUEST['sdelete'])) {
			$this->states->deleteState($_REQUEST['id']);
		}
		$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		$cid = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
		$sname = (isset($_REQUEST['sname']))?$_REQUEST['sname']:'';
		$totals = $this->states->totalStates($cid, $sname);
		$data['states'] = $this->states->allStates($cid, $sname, $page);
		$data['countries'] = $this->states->allCountries();
		$pagination = '<div class="pagination">';
		$tc = ceil($totals[0]->total/$limit);
		for($i=0;$i<$tc;$i++) {
			(($i+1)==$page)?$class = 'active':$class = '';
			if(($cid!='')||($sname!=''))
				$pagination .= '<a href="?cid='.$cid.'&sname='.$sname.'&page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
			else
				$pagination .= '<a href="?page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
		}
		$pagination .= '</div>';
		$data['pagination'] = $pagination;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/liststates',$data);
		$this->load->view('admin/footer');
	}
	
	public function edit($id){
		$data['countries'] = $this->states->allCountries();
		if(isset($_POST['editstate'])) {
			$this->states->editState($_POST['statename'], $_POST['cid'], $_POST['status'], $id);
		}
		$data['state'] = $this->states->singleState($id);
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/editstates',$data);
		$this->load->view('admin/footer');
	}
	
	public function add(){
		$data['countries'] = $this->states->allCountries();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/addstates',$data);
		$this->load->view('admin/footer');
	}
}
