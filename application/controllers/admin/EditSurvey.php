<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class EditSurvey extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model');
    }
	
	public function index($id){
        if($this->session->userdata('adminData')){
			$html = '';
			$result = $this->Survey_model->getSurveyData($id);
			$data['htmlData'] = $result;
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editSurvey');
				$this->load->view('admin/footer');	
			}elseif($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editSurvey');
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editSurvey');
				$this->load->view('admin/footer');	
			}
			
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }
	
}
