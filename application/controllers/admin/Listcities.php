<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Listcities extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('cities');
    }
	
	public function index(){
		if(isset($_POST['addcity'])) {
			$this->cities->addCity($_POST['cityname'], $_POST['sid'], $_POST['status']);
		}
		if(isset($_POST['sstatus'])) {
			$status = ($_POST['sstatus']=='Active')?0:1;
			$this->cities->updateStatus($status, $_POST['ctid']);
		}
		if(isset($_REQUEST['sdelete'])) {
			$this->cities->deleteCity($_REQUEST['id']);
		}
		$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		$cid = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
		$sid = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
		$cname = (isset($_REQUEST['cname']))?$_REQUEST['cname']:'';
		$totals = $this->cities->totalCities($cid, $sid, $cname);
		$data['cities'] = $this->cities->allCities($cid, $sid, $cname, $page);
		$data['states'] = $this->cities->allStates($cid);
		$data['countries'] = $this->cities->allCountries();
		$pagination = '<div class="pagination">';
		$tc = ceil($totals[0]->total/$limit);
		for($i=0;$i<$tc;$i++) {
			(($i+1)==$page)?$class = 'active':$class = '';
			if(($cid!='')||($sid!='')||($cname!=''))
				$pagination .= '<a href="?cid='.$cid.'&sid='.$sid.'&cname='.$cname.'&page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
			else
				$pagination .= '<a href="?page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
		}
		$pagination .= '</div>';
		$data['pagination'] = $pagination;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/listcities',$data);
		$this->load->view('admin/footer');
	}
	
	public function edit($id){
		$data['countries'] = $this->cities->allCountries();
		if(isset($_POST['editcity'])) {
			$this->cities->editCity($_POST['cityname'], $_POST['sid'], $_POST['status'], $id);
		}
		$data['states'] = $this->cities->allStatesByCity($id);
		$data['city'] = $this->cities->singleCity($id);
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/editcities',$data);
		$this->load->view('admin/footer');
	}
	
	public function add(){
		$data['countries'] = $this->cities->allCountries();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/addcities',$data);
		$this->load->view('admin/footer');
	}
	
	public function ajaxstates($cid=''){
		$states = $this->cities->allStates($cid);
		$result = '<option value="">--Select--</option>';
		foreach($states as $state) {
			$result .= '<option value="'.$state->id.'">'.$state->name.'</li>';
		}
		$data['data'] = $result;
		$this->load->view('ajaxss',$data);
	}
	
	public function ajaxcities($sid=''){
		$states = $this->cities->allCitiesByState($sid);
		$result = '<option value="">--Select--</option>';
		foreach($states as $state) {
			$result .= '<option value="'.$state->id.'">'.$state->name.'</li>';
		}
		$data['data'] = $result;
		$this->load->view('ajaxss',$data);
	}
}
