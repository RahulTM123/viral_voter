<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Listcountries extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('countries');
    }
	
	public function index(){
		if(isset($_POST['addcountry'])) {
			$this->countries->addCountry($_POST['countryname'], $_POST['countryshort'], $_POST['status']);
		}
		if(isset($_POST['cstatus'])) {
			$status = ($_POST['cstatus']=='Active')?0:1;
			$this->countries->updateStatus($status, $_POST['cid']);
		}
		if(isset($_REQUEST['cdelete'])) {
			$this->countries->deleteCountry($_REQUEST['id']);
		}
		$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		$catname = (isset($_REQUEST['catname']))?$_REQUEST['catname']:'';
		$totalc = $this->countries->totalCountries($catname);
		$data['countries'] = $this->countries->allCountries($catname, $page);
		$pagination = '<div class="pagination">';
		$tc = ceil($totalc[0]->total/$limit);
		for($i=0;$i<$tc;$i++) {
			(($i+1)==$page)?$class = 'active':$class = '';
			if($catname!='')
				$pagination .= '<a href="?catname='.$catname.'&page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
			else
				$pagination .= '<a href="?page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
		}
		$pagination .= '</div>';
		$data['pagination'] = $pagination;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/listcountries',$data);
		$this->load->view('admin/footer');
	}
	
	public function edit($id){
		if(isset($_POST['editcountry'])) {
			$this->countries->editCountry($_POST['countryname'], $_POST['countryshort'], $_POST['status'], $id);
		}
		$data['country'] = $this->countries->singleCountry($id);
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/editcountries',$data);
		$this->load->view('admin/footer');
	}
	
	public function add(){
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/addcountries');
		$this->load->view('admin/footer');
	}
}
