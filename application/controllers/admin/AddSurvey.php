<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class AddSurvey extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model');
		$this->load->model('countries');
		$this->load->model('states');
    }
	
	public function index(){
		if($this->session->userdata('adminData')){
			$html = '';
			$data['countries'] = $this->countries->getCountries();
			if($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
			}
			$result = $this->Survey_model->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewSurvey');
				$this->load->view('admin/footer');	
			}elseif($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewSurvey');
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewSurvey');
				$this->load->view('admin/footer');	
			}
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
        
    }
	
	public function addSurvey(){
		$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
		$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
		$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
		
		$result = $this->Survey_model->addNewSurvey($_REQUEST['qTitle'],$_REQUEST['questn'],$_REQUEST['catName'],$_REQUEST['surveyImg'],$country_id,$state_id,$city_id);
		if(!empty($result)){
			$this->session->set_flashdata('msg', 'Survey added successfully!');	
		}else{
			$this->session->set_flashdata('error', 'Survey not added!');	
		}
		redirect('./admin/add_survey');
	}
	
	public function editSurvey(){
		$result = $this->Survey_model->editSurvey($_REQUEST['qTitle'],$_REQUEST['questn'],$_REQUEST['qId'],$_REQUEST['catName'],$_REQUEST['surveyImg'],$_REQUEST['save']);
		if($result == 'Success'){
			$this->session->set_flashdata('msg', 'Survey edit successfully!');	
		}else{
			$this->session->set_flashdata('error', 'Survey not edited!');		
		}
		redirect('./admin/edit_survey/'.$_REQUEST['save']);
	}
	
}
