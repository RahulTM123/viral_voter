<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class CompResultDetail extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('CompResult_model');
		$this->load->model('Dashboard_model');
    }
	
	public function index($id){
		if(!$this->session->userdata('adminData')){
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}else{
			
			if(isset($_GET["page"])){
				$page = intval($_GET["page"]);
			}else {
				$page = 1;
			}

			$result = $this->CompResult_model->getCompResultsDetail($id,$page);
			$data['htmlData'] = $result;
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/compResultDetail',$data);
			$this->load->view('admin/footer');
		}
    }
}
