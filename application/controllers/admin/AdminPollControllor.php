<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPollControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('AdminPoll_model','poll');
		$this->load->model('countries');
		$this->load->model('states');
		$this->load->model('cities');
		$this->load->model('User_model','user');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$html = '';
			$categories = $this->poll->getCatList();
			if(isset($_REQUEST['filter'])){
				$result = $this->poll->getAdminPollListByFilter($_REQUEST['cat'],$_REQUEST['cid'],$_REQUEST['sid'],$_REQUEST['ctid'],$_REQUEST['gender'],$_REQUEST['voteFilter'],$_REQUEST['participation'],$_REQUEST['dfrom'],$_REQUEST['dto'],$_REQUEST['keyword'],$page='',$limit='100');
			}else{
				$result = $this->poll->getAdminPollList();
			}
			$data['result'] = $result;
			$data['categories'] = $categories;
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/adminPollList',$data);
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }
	
	public function newPoll(){
		if($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$html = '';
			$data['htmldata'] = '';
			$result = $this->poll->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/addNewPoll',$data);
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	public function addNewPoll(){
		if(isset($_REQUEST['addpoll'])){
			/*$cat = $_REQUEST['poll_category'];
			$polloption = array();
			$qimag = $qus = $qvideolink = '';
			
			$qus = (!empty($_REQUEST['ques']))?$_REQUEST['ques']:'';
			$qimag = (!empty($_REQUEST['quesI']))?$_REQUEST['quesI']:'';
			$qvideolink = (!empty($_REQUEST['videolink']))?$_REQUEST['videolink']:'';
			
			$ans1I = (!empty($_REQUEST['ans1I']))?$_REQUEST['ans1I']:'';
			$ans1 = (!empty($_REQUEST['ans1']))?$_REQUEST['ans1']:'';
			
			$ans2I = (!empty($_REQUEST['ans2I']))?$_REQUEST['ans2I']:'';
			$ans2 = (!empty($_REQUEST['ans2']))?$_REQUEST['ans2']:'';
			
			$ans3I = (!empty($_REQUEST['ans3I']))?$_REQUEST['ans3I']:'';
			$ans3 = (!empty($_REQUEST['ans3']))?$_REQUEST['ans3']:'';
			
			$ans4I = (!empty($_REQUEST['ans4I']))?$_REQUEST['ans4I']:'';
			$ans4 = (!empty($_REQUEST['ans4']))?$_REQUEST['ans4']:'';
			
			$start_date = date('', strtotime($_REQUEST['start_date']));
            $end_date = $_REQUEST['end_date'];*/
			$data = $_REQUEST;
			
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$interest = (isset($_REQUEST['poll_category']))?$_REQUEST['poll_category']:'';
			
			$userlist = $this->user->getUserListByFilter($limit='999999', $country_id, $state_id, $city_id, $gender, $keyword='', $page='', $interest);
			$alluser = array();
			foreach($userlist['data'] as $user)
				$alluser[] = $user->id;
				
			$result = $this->poll->addNewPoll($data, $alluser);
			
			if(!empty($result)){
				$val['msg'] = "Poll added successfully!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewPoll',$val);
				$this->load->view('admin/footer');
			}else{
				$val['msg'] = "Poll not added!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewPoll',$val);
				$this->load->view('admin/footer');
			}
			
		}else{
			if($this->session->userdata('adminData')){
				$html = '';
				$result = $this->poll->getCatList();
				if(!empty($result)){
					$i = 1;
					foreach($result as $value){
						$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
					}
					$data['htmldata'] = $html;
				}
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewPoll',$data);
				$this->load->view('admin/footer');	
			}else{
				$val['msg'] = "Cann't access without login!";
				$this->load->view('admin/login',$val);	
			}	
		}
	}
		
	public function changeAdminPollStatus(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->poll->changeAdminPollStatus($_REQUEST['id']);
		if($result){
			redirect($actual_link);
		}
	}
	
	public function deleteAdminPoll(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->poll->deleteAdminPoll($_REQUEST['id']);
		redirect($actual_link);
	}
	
	
	public function pollVoting(){
		//echo '<pre>';
		//print_r($_REQUEST);
		//print_r($_SERVER);
		$return = $this->poll->addPollVoting($_REQUEST['pollid'],$_REQUEST['adminans'],$_REQUEST['puserid'],$_SERVER['REMOTE_ADDR']);
		if(!empty($result)){
			redirect($_SERVER['HTTP_REFERER']);
		}else{
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	
	
	public function details($pollid){
		$data['stats'] = (array)$this->poll->getPollDetails($pollid);
		$data['result'] = $this->poll->getUserDetails($pollid);
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/adminPollDetail',$data);
		$this->load->view('admin/footer');
	}
	
}
