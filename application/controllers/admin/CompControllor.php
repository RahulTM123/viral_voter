<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class CompControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Contest_model','comp');
		$this->load->model('countries');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$html = '';
			$catList = $this->comp->getCatList();
			// if(!empty($catList)){
			// 	$html .= '<select name="categoryFilter" id="categoryFilter" class="form-control" style="margin-bottom:15px;"><option value="">--Select Category--</option>';
			// 	foreach($catList as $value){
			// 		$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
			// 	}
			// 	$html .= '</select>';
			// }
			if(isset($_POST) && !empty($_POST)){
				$cat = '';
				$dfrom = '';
				$dto = '';
				if(isset($_POST['cat'])){
					$cat = $_POST['cat'];
				}
				if(isset($_POST['dfrom'])){
					$dfrom = $_POST['dfrom'];
				}
				if(isset($_POST['dto'])){
					$dto = $_POST['dto'];
				}
				$result = $this->comp->getCompListByFilter($cat,$dfrom,$dto);
			}else{
				$result = $this->comp->getCompList();
			}
			$data['htmldata'] = $result;
			$data['catList'] = $catList;
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/compList',$data);
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }
	
	
	public function newComp(){
		if($this->session->userdata('adminData')){
			$html = '';
			$qhtml = '';
			$result = $this->comp->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/addNewComp',$data);
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	public function addNewComp(){
		if(isset($_REQUEST['newComp'])){
			$catid = $_REQUEST['catName'];
			$result = $this->comp->addNewComp($catid,$_REQUEST['ques'],$_REQUEST['compeImg']);
			
			if(!empty($result)){
				$val['msg'] = "Compitition added successfully!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp',$val);
				$this->load->view('admin/footer');
			}else{
				$val['msg'] = "Compitition not added!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp',$val);
				$this->load->view('admin/footer');
			}
			
		}else{
			if($this->session->userdata('adminData')){
				$html = '';
				$qhtml = '';
				$result = $this->comp->getCatList();
				if(!empty($result)){
					$i = 1;
					foreach($result as $value){
						$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
					}
					$data['htmldata'] = $html;
				}
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp',$data);
				$this->load->view('admin/footer');	
			}else{
				$val['msg'] = "Cann't access without login!";
				$this->load->view('admin/login',$val);	
			}	
		}
	}
	
	public function compList(){
		
	}
	
	public function changeCompStatus(){
		$html = '';
		$result = $this->comp->changeCompStatus($_REQUEST['id']);
		if($result){
			redirect('./admin/contests');
		}
	}
	
	public function deleteComp(){
		$html = '';
		
		$result = $this->comp->deleteComp($_REQUEST['id']);
		redirect('./admin/contests');
	}
	
}
