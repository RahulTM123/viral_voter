<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxview_admin extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->library("PhpMailerLib");
		$this->load->helper(array('form', 'url'));
		$this->load->model('ajaxss_admin');
		$this->load->model('EmailModel');
		$this->load->model('user');
    }
	
	public function deletethisuser() {
        $this->ajaxss_admin->deleteUser($_POST['uid'], $_POST['deltext']);
		echo 1;
	}
	
	public function fetchComment() {
		$result = '';
		if(!empty($_POST['refid'])) {
			$result .= '&nbsp;<br /><table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr. No.</th><th>User Name</th><th>Comment</th><th>Date</th><th>Status</th><th>Action</th></tr>';
			$comments = $this->ajaxss_admin->getCommentList($_POST['refid'], $_POST['page'], $_POST['ref']);
			$count = 1;
			if($comments) {
				foreach($comments as $comment) {
					
					$prourl = (!empty($comment['username']))?(base_url().''.$comment['username']):base_url().'user/id/'.$comment['userid'];
			
					$result .= '<tr>
								<td>'.($count++).'</td>
								<td><a href="'.$prourl.'">'.$comment['firstname'].' '.$comment['lastname'].'</a></td>
								<td>'.$comment['content'];
								
					if($comment['subcomment']) {
						$result .= '<p><a href="#" class="loadcomment" id="'.$comment['id'].'" rel="0" dir="'.$comment['subcomment'].'" data-type="comment"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> View '.$comment['subcomment'].' Replies</a></p>';
					}
					
					$result .= '</td>
								<td>'.date('d/m/Y h:i:s A', strtotime($comment['created'])).'</td>
								<td><span class="buttonGreen">Active</span></td>
								<td><span class="buttonGreen showstat" style="background: #141414;">Detail</span>';
								
								if($comment['userip']!='') {
								$info = json_decode($comment['userip']);
								$result .= '<div style="display: none;"><div>';
								
								if(isset($info->userip))
								$result .= 'User IP: <a href="https://whatismyipaddress.com/ip/'.$info->userip.'" target="_blank">'.$info->userip.'</a><br />';
								
								if(isset($info->page))
								$result .= 'Page (from): '.$info->page.'<br />';
								
								if(isset($info->system))
								$result .= 'System: '.$info->system.'';
								
								$result .= '</div></div>';
								}
					$result .= '</td>
								</tr>';
				}
			}
			else {
				$result = '0';
			}
			$result .= '</table>';
		}
		else
			$result = 'No comment found';
		
		echo $result;
	}
}


