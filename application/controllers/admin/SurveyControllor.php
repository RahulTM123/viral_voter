<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model','survey');
		$this->load->model('countries');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$html = '';
			$catList = $this->survey->getCatList();
			if(!empty($catList)){
				$html .= '<select name="categoryFilter" id="categoryFilter" class="form-control" style="margin-bottom:15px;"><option value="">--Select Category--</option>';
				foreach($catList as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$html .= '</select>';
			}
			if(isset($_REQUEST['voteFilter']) || isset($_REQUEST['categoryFilter'])){
				$catFil = $voteFil = '';
				if(isset($_REQUEST['categoryFilter'])){
					$catFil = $_REQUEST['categoryFilter'];
				}
				if(isset($_REQUEST['voteFilter'])){
					$voteFil = $_REQUEST['voteFilter'];
				}
				$result = $this->survey->getSurveyListByFilter($catFil,$voteFil);
			}else{
				$result = $this->survey->getSurveyList();
			}
			$data['htmldata'] = $result;
			$data['catList'] = $html;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/surveyList');
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }
	
	public function changeSurveyStatus(){
		$html = '';
		$result = $this->survey->changeSurveyStatus($_REQUEST['id']);
		if($result){
			redirect('./admin/survey_list');
		}
	}
	
	public function deleteSurvey(){
		$html = '';
		$result = $this->survey->deleteSurvey($_REQUEST['id']);
		redirect('./admin/survey_list');
	}
	
}
