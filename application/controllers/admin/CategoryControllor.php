<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class CategoryControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Category_model');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			$html = '';
			$catname = (isset($_REQUEST['catname']))?$_REQUEST['catname']:'';
			$data['categories'] = $this->Category_model->getCatList($catname);
		
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/catList',$data);
			$this->load->view('admin/footer');
				
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }
	
	public function newCat(){
		if($this->session->userdata('adminData')){
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewCat');
				$this->load->view('admin/footer');	
			}elseif($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewCat');
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewCat');
				$this->load->view('admin/footer');	
			}
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	public function addNewCat(){
		if(isset($_REQUEST['catName'])){
			$html = '';
			$result = $this->Category_model->addNewCategory($_REQUEST['catName']);
			if(!empty($result)){
				$this->session->set_flashdata('msg', "Category added successfully!");
			}else{
				$this->session->set_flashdata('error', "Category not added!");
			}
			redirect('./admin/category/newCat','refresh');
		}
	}
	
	public function changeCatStatus(){
		$html = '';
		$result = $this->Category_model->changeCatStatus($_REQUEST['id']);
		if($result){
			redirect('./admin/category');
		}
	}
	
	public function deleteCategory(){
		$html = '';
		$result = $this->Category_model->deleteCategory($_REQUEST['id']);
		redirect('./admin/category');
	}
	
	
	
	public function edit(){
		if($this->session->userdata('adminData')){
			$result = $this->Category_model->editCategory($_REQUEST['catId'],$_REQUEST['catName']);
			$this->session->set_flashdata('msg', "Category edit successfully!");
			redirect('./admin/edit_category/'.$_REQUEST['catId'],'refresh');
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
}
