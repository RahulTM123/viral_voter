<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class EditCompetition extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Contest_model','comp');
		$this->load->model('countries');
		$this->load->model('states');
		$this->load->model('cities');
    }
	
	public function index($id){
        if($this->session->userdata('adminData')){
			$html = '';
			$data['countries'] = $this->countries->getCountries();
			$result1 = $this->comp->getCompByIdEditContest($id);
			if(isset($result1) && count($result1)>0)
			{
				$data['compData'] = $result1[0];
			}
					
			
			$result = $this->comp->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					if(isset($data['compData']) )
					{
						
					
					$html .= '<option value="'.$value->id.'" '.(($value->id==$data['compData']['catid'])?'selected="selected"':"").'>'.$value->cat_name.'</option>';
				
					}
					else
					{
						$html .= '<option value="'.$value->id.'" '.(($value->id==$data['compData']['catid'])?'':"").'>'.$value->cat_name.'</option>';
				
					}
				}
				$data['htmldata'] = $html;
				
			}
			
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editCompetition');
				$this->load->view('admin/footer');	
			}elseif($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editCompetition');
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/editCompetition');
				$this->load->view('admin/footer');	
			}	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }


    public function editComp(){
		if($this->session->userdata('adminData')){
			$date1 = strtr($_POST['start_date'], '/', '-');
			$date2 = strtr($_POST['end_date'], '/', '-');
			$title = $_POST['ques'];
			$catid = $_POST['catName'];
			
			$country = $_POST['country'];
			$state = $_POST['state'];
			$city = $_POST['city'];
			$start_date = (isset($_POST['start_date']))?date("Y-m-d H:i:s", strtotime($date1)):'';
			$end_date = (isset($_POST['start_date']))?date("Y-m-d H:i:s", strtotime($date2)):'';
			$compId = $_POST['save'];
			

			$show_title = (isset($_POST['show_ques']))?$_POST['show_ques']:'none';
			$show_cat = (isset($_POST['show_cat']))?$_POST['show_cat']:'none';
			$show_country = (isset($_POST['show_country']))?$_POST['show_country']:'none';
			$show_state = (isset($_POST['show_state']))?$_POST['show_state']:'none';
			$show_city = (isset($_POST['show_city']))?$_POST['show_city']:'';
			$show_strt_date = (isset($_POST['show_strt_date']))?$_POST['show_strt_date']:'none';
			$show_end_date = (isset($_POST['show_end_date']))?$_POST['show_end_date']:'none';
			$gender = (isset($_POST['gender']))?$_POST['gender']:'';
			$rewards = (isset($_POST['rewards']))?$_POST['rewards']:'';
			$age_from = (isset($_POST['age_from']))?$_POST['age_from']:'';
			$age_to = (isset($_POST['age_to']))?$_POST['age_to']:'';
			$show_gender = (isset($_POST['show_gender']))?$_POST['show_gender']:'';
			$show_rewards = (isset($_POST['show_rewards']))?$_POST['show_rewards']:'';
			$show_age = (isset($_POST['show_age']))?$_POST['show_age']:'';
			$defaulttype= (isset($_POST['defaulttype']))?$_POST['defaulttype']:'image';
			$videolink= (isset($_POST['videoLink']))?$_POST['videoLink']:'';
			$terms= (isset($_POST['terms']))?$_POST['terms']:'';

			
			if($this->session->userdata('adminData')){
					
			$result = $this->comp->editComp($catid,$title,$country,$state,$city,$start_date,$end_date,$compId,$show_title,$show_cat,$show_country,$show_state,$show_city,$show_strt_date,$show_end_date,$gender,$rewards,$age_from,$age_to,$show_gender,$show_rewards,$show_age,$defaulttype,$videolink,$terms);
			
			if(!empty($result)){
				$ImageSavefolder = "./assets/front/contest_image/".$compId."/";
				$upload_contest_image="";
				$upload_contest_video="";
				if (!file_exists($ImageSavefolder)) {
					mkdir($ImageSavefolder, 0777, true);
				}
			
				if(isset($_FILES['compimage']) && $_FILES["compimage"]["name"]!="" ){
					$imageFileType = strtolower(pathinfo($_FILES["compimage"]["name"],PATHINFO_EXTENSION));
					/* if(file_exists($ImageSavefolder."contest.".$imageFileType)) 
					{
						unlink($ImageSavefolder."contest.".$imageFileType);
					} */

					$upload_file = move_uploaded_file($_FILES["compimage"]["tmp_name"] , "".$ImageSavefolder."contest.".$imageFileType);
					if($upload_file)
					{
						$upload_contest_image="contest.".$imageFileType;
					}
					
				}
				else
			{
				$upload_contest_image = $_POST['val_img'];
			}
				if(isset($_FILES['contestVidFile']) && $_FILES["contestVidFile"]["name"]!=""){
					$videoFileType = strtolower(pathinfo($_FILES["contestVidFile"]["name"],PATHINFO_EXTENSION));
					//if(file_exists($ImageSavefolder."contestvideo.".$videoFileType)) unlink($ImageSavefolder."contestvideo.".$videoFileType);

					$upload_video = move_uploaded_file($_FILES["contestVidFile"]["tmp_name"] , "".$ImageSavefolder."contestvideo.".$videoFileType);
					if($upload_video)
					{
						$upload_contest_video="contestvideo.".$videoFileType;
					}
				}
				else
			{
				$upload_contest_video = $_POST['val_video'];
			}
				$result = $this->comp->updateCompFiles($compId,$upload_contest_image,$upload_contest_video);

			
				$this->session->set_flashdata('msg', "Contest updated successfully!");
			}else{
				$this->session->set_flashdata('error', "Contest not updated!");
			}
			redirect('/admin/edit_contest/'.$_REQUEST['save'],'refresh');
			
		}
	}
	
}
}
