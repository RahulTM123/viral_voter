<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class AddCompetition extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Contest_model','comp');
		$this->load->model('countries');
		$this->load->model('states');
		$this->load->model('cities');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$html = '';
			$result = $this->comp->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp');
				$this->load->view('admin/footer');	
			}elseif($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp');
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header',$data);
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewComp');
				$this->load->view('admin/footer');	
			}
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
    }


	
	public function addNewComp(){
		$dates = (isset($_POST['start_date']))?$_POST['start_date']:'';
		$datess = (isset($_POST['end_date']))?$_POST['end_date']:'';
		$date1 = strtr($dates, '/', '-');
		$date2 = strtr($datess, '/', '-');
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['state']))?$_POST['state']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$title = (isset($_POST['ques']))?$_POST['ques']:'';
		$category = (isset($_POST['catName']))?$_POST['catName']:'';
		$start_date = (isset($_POST['start_date']))?date("Y-m-d H:i:s", strtotime($date1)):'';
		$end_date = (isset($_POST['start_date']))?date("Y-m-d H:i:s", strtotime($date2)):'';
		//$img = (isset($_FILES['compimage']))?$_FILES['compimage']['name']:'';
		$gender = (isset($_POST['gender']))?$_POST['gender']:'';
		$rewards = (isset($_POST['rewards']))?$_POST['rewards']:'';
		$age_from = (isset($_POST['age_from']))?$_POST['age_from']:'';
		$age_to = (isset($_POST['age_to']))?$_POST['age_to']:'';

		$show_title = (isset($_POST['show_ques']))?$_POST['show_ques']:'none';
		$show_cat = (isset($_POST['show_cat']))?$_POST['show_cat']:'none';
		$show_country = (isset($_POST['show_country']))?$_POST['show_country']:'none';
		$show_state = (isset($_POST['show_state']))?$_POST['show_state']:'none';
		$show_city = (isset($_POST['show_city']))?$_POST['show_city']:'';
		$show_strt_date = (isset($_POST['show_strt_date']))?$_POST['show_strt_date']:'none';
		$show_end_date = (isset($_POST['show_end_date']))?$_POST['show_end_date']:'none';
		$show_age = (isset($_POST['show_age']))?$_POST['show_age']:'none';
		$show_gender = (isset($_POST['show_gender']))?$_POST['show_gender']:'none';
		$show_rewards = (isset($_POST['show_rewards']))?$_POST['show_rewards']:'none';
		$defaulttype= (isset($_POST['defaulttype']))?$_POST['defaulttype']:'image';
		$videolink= (isset($_POST['videoLink']))?$_POST['videoLink']:'';
		$terms= (isset($_POST['terms']))?$_POST['terms']:'';

//echo $_FILES; exit;
		if($this->session->userdata('adminData')){
			/* if (!empty($img)) {
				$ImageSavefolder = $_SERVER['DOCUMENT_ROOT']."/viralvoters/assets/front/contest_image/";
				 $upload_file = move_uploaded_file($_FILES["compimage"]["tmp_name"] , "".$ImageSavefolder."".$_FILES["compimage"]["name"]);
				 //echo $upload_file; exit;
			}	 */

				
			$result = $this->comp->addNewComp($category,$title,$country_id,$state_id,$city_id,$start_date,$end_date,$show_title,$show_cat,$show_country,$show_state,$show_city,$show_strt_date,$show_end_date,$gender,$rewards,$age_from,$age_to,$show_age,$show_gender,$show_rewards,$defaulttype,$videolink,$terms);
			
			
			if(!empty($result) ){
				$ImageSavefolder = "./assets/front/contest_image/".$result."/";
				$upload_contest_image="";
				$upload_contest_video="";
				if (!file_exists($ImageSavefolder)) {
					mkdir($ImageSavefolder, 0777, true);
				}
				if(isset($_FILES['compimage'])){
					$imageFileType = strtolower(pathinfo($_FILES["compimage"]["name"],PATHINFO_EXTENSION));

					$upload_file = move_uploaded_file($_FILES["compimage"]["tmp_name"] , "".$ImageSavefolder."contest.".$imageFileType);
					if($upload_file)
					{
						$upload_contest_image="contest.".$imageFileType;
					}
				}
				if(isset($_FILES['contestVidFile'])){
					$videoFileType = strtolower(pathinfo($_FILES["contestVidFile"]["name"],PATHINFO_EXTENSION));

					$upload_video = move_uploaded_file($_FILES["contestVidFile"]["tmp_name"] , "".$ImageSavefolder."contestvideo.".$videoFileType);
					if($upload_video)
					{
						$upload_contest_video="contestvideo.".$videoFileType;
					}
				}
				$result = $this->comp->updateCompFiles($result,$upload_contest_image,$upload_contest_video);

			

				$this->session->set_flashdata('msg', "Contest added successfully!");
			}else{

				//$upload_file = move_uploaded_file($_FILES["compimage"]["tmp_name"] , "".$ImageSavefolder."".$_FILES["compimage"]["name"]);


				$this->session->set_flashdata('error', "Contest not added!");
			}
			redirect('admin/add_contest','refresh');
			
		}
	}
	
	
	
}
