<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserPollControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserPoll_model','userPoll');
		$this->load->model('countries');
    }
	
	public function index(){
		$data['countries'] = $this->countries->getCountries();
		$html = '';
		$data['categories']= $this->userPoll->getCatList();
		if(!empty($this->input->get()))
		{
			$data['search']=$this->userPoll->getSearchRecord($this->input->get());
			$record=$this->userPoll->getFilterRecord($this->input->get());
		}
		else
		{
			$record=$this->userPoll->getUserRecord();
		}
		/*foreach ($record as $key) {
			$ke=$key->user_id;
			$ke1=$key->catid;
			//var_dump($ke);die();
			$name=$this->userPoll->getUserName($ke);
			$cat=$this->userPoll->getUserCategories($ke1);
			if(empty($name))
			{
				$key->user_id="";
			}
			else
			{
				$key->user_id=$name->firstname." ".$name->lastname;
			}
			if(empty($cat))
			{
				$key->catid="";
			}
			else
			{
				$key->catid=$cat->cat_name;
			}
		}*/
		$data['record']=$record;
		$catList= $this->userPoll->getCatList();
		if(!empty($catList)){
			$html .= '<select name="categoryFilter" id="categoryFilter" class="form-control" style="margin-bottom:15px;"><option value="">--Select Category--</option>';
			foreach($catList as $value){
				$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
			}
			$html .= '</select>';
		}
		if(isset($_REQUEST['search']) || isset($_REQUEST['voteFilter']) || isset($_REQUEST['categoryFilter'])){
			$catFil = $voteFil = $search = '';
			if(isset($_REQUEST['categoryFilter'])){
				$catFil = $_REQUEST['categoryFilter'];
			}
			if(isset($_REQUEST['voteFilter'])){
				$voteFil = $_REQUEST['voteFilter'];
			}
			if(isset($_REQUEST['search'])){
				$search = $_REQUEST['search'];
			}
        	$result = $this->userPoll->getUserPollListByFilter($catFil,$voteFil,$search);
		}else{
			$result = $this->userPoll->getUserPollList();	
		}
		$data['htmlData'] = $result;
		$data['catList'] = $html;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userPollList',$data);
		$this->load->view('admin/footer');
    }
	
	public function changeUserPollStatus(){
		$html = '';
		$result = $this->userPoll->changeUserPollStatus($_REQUEST['id']);
		if($result){
			redirect('./admin/user_poll_list');
		}
	}
	
	public function deleteUserPoll($id){
		//var_dump($id);die();
		$html = '';
		$result = $this->userPoll->deleteUserPoll1($id);
		redirect('admin/UserPollControllor/index');
	}
	
}