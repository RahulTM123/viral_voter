<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserPollResult extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserPoll_model');
    }
	
	public function index($id){
        $result = $this->UserPoll_model->getUserPollResult($id);
		$data['htmlData'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userPollResult',$data);
		$this->load->view('admin/footer');
    }
	
}