<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class QuizControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Quiz_model');
    }
	
	public function index(){
        //$this->load->view('admin/login');
    }

	public function qName(){
		if($this->session->userdata('adminData')){
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/addQuizName');
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	
	public function addQuizName(){
		if(isset($_REQUEST['addQuizName'])){
			$name = $_REQUEST['quizName'];
			$pages = $_REQUEST['displayPage'];
			$result = $this->Quiz_model->addQuizName($name,$pages);
			if(!empty($result)){
				$val['msg'] = "Quiz Name added successfully!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addQuizName',$val);
				$this->load->view('admin/footer');
			}else{
				$val['msg'] = "Quiz Name not added!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addQuizName',$val);
				$this->load->view('admin/footer');
			}
		}else{
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/addQuizName');
			$this->load->view('admin/footer');
		}
	}
	
	
	public function quizNameList(){
		if($this->session->userdata('adminData')){
			$html = '';
			$result = $this->Quiz_model->getQuizNameList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<tr><td>'.$i.'</td>';
					$html .= '<td>'.$value->quiz_name.'</td>';
					$html .= '<td>'.$value->wheree.'</td>';
					
					if($value->status == 1){
						$html .= '<td><a href="changeQuizNameStatus?id='.$value->id.'">Deactivate</a></td>';
					}else{
						$html .= '<td><a href="changeQuizNameStatus?id='.$value->id.'">Activate</a></td>';
					}
					$html .= '<td><a href="deleteQuizName?id='.$value->id.'">Delete</a></td></tr>';
					$i++;
				}
				$data['htmldata'] = $html;
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/quizNameList',$data);
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/quizNameList');
				$this->load->view('admin/footer');	
			}
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	public function changeQuizNameStatus(){
		$html = '';
		$result = $this->Quiz_model->changeQuizNameStatus($_REQUEST['id']);
		if($result){
			$this->quizNameList();
		}
	}	
	
	
	public function newQuizName(){
		if($this->session->userdata('adminData')){
			$html = '';
			$qhtml = '';
			$result = $this->Quiz_model->getCatList();
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			$qresult = $this->Quiz_model->getQuizNameList();
			if(!empty($qresult)){
				$i = 1;
				foreach($qresult as $value){
					$qhtml .= '<option value="'.$value->id.'">'.$value->quiz_name.'</option>';
				}
				$data['qhtmldata'] = $qhtml;
			}
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/addNewQuiz',$data);
			$this->load->view('admin/footer');	
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	
	public function addNewQuiz(){
		if(isset($_REQUEST['newQuiz'])){
			$catid = $_REQUEST['catName'];
			$quizid = $_REQUEST['quizName'];
			if(!empty($_FILES['quesI']['name']) && !empty($_REQUEST['ques'])){
				$name = md5(time()).'_'.$_FILES['quesI']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('quesI'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$qus = serialize(array($_REQUEST['ques'],$file));
				}else{
					$error['qerror'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else if(!empty($_REQUEST['ques'])){
				$qus = serialize(array($_REQUEST['ques']));
			}else{
				$name = md5(time()).'_'.$_FILES['quesI']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('quesI'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$qus = serialize(array($file));
				}else{
					$error['qerror'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}
			
			
			
			if(!empty($_FILES['ans1I']['name']) && !empty($_REQUEST['ans1'])){
				$name = md5(time()).'_'.$_FILES['ans1I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans1I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans1 = serialize(array($_REQUEST['ans1'],$file));
				}else{
					$error['ans1_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else if(!empty($_REQUEST['ans1'])){
				$ans1 = serialize(array($_REQUEST['ans1']));
			}else if(!empty($_FILES['ans1I']['name'])){
				$name = md5(time()).'_'.$_FILES['ans1I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans1I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans1 = serialize(array($file));
				}else{
					$error['ans1_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else{
				$ans1 ='';
			}
			
			
			
			if(!empty($_FILES['ans2I']['name']) && !empty($_REQUEST['ans2'])){
				$name = md5(time()).'_'.$_FILES['ans2I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans2I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans2 = serialize(array($_REQUEST['ans2'],$file));
				}else{
					$error['ans2_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else if(!empty($_REQUEST['ans2'])){
				$ans2 = serialize(array($_REQUEST['ans2']));
			}else if(!empty($_FILES['ans2I']['name'])){
				$name = md5(time()).'_'.$_FILES['ans2I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans2I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans2 = serialize(array($file));
				}else{
					$error['ans2_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else{
				$ans2 ='';
			}
			
			
			if(!empty($_FILES['ans3I']['name']) && !empty($_REQUEST['ans3'])){
				$name = md5(time()).'_'.$_FILES['ans3I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans3I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans3 = serialize(array($_REQUEST['ans3'],$file));
				}else{
					$error['ans3_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else if(!empty($_REQUEST['ans3'])){
				$ans3 = serialize(array($_REQUEST['ans3']));
			}else if(!empty($_FILES['ans3I']['name'])){
				$name = md5(time()).'_'.$_FILES['ans3I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans3I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans3 = serialize(array($file));
				}else{
					$error['ans3_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else{
				$ans3 ='';
			}
			
			
			
			if(!empty($_FILES['ans4I']['name']) && !empty($_REQUEST['ans4'])){
				$name = md5(time()).'_'.$_FILES['ans1I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans4I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans4 = serialize(array($_REQUEST['ans4'],$file));
				}else{
					$error['ans4_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else if(!empty($_REQUEST['ans4'])){
				$ans4 = serialize(array($_REQUEST['ans4']));
			}else if(!empty($_FILES['ans4I']['name'])){
				$name = md5(time()).'_'.$_FILES['ans4I']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('ans4I'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$ans4 = serialize(array($file));
				}else{
					$error['ans4_error'] = $this->upload->display_errors();
					$this->load->view('admin/addNewQuiz',$error);
				}
			}else{
				$ans4 ='';
			}
			
			$result = $this->Quiz_model->addNewQuiz($catid,$quizid,$qus,$ans1,$ans2,$ans3,$ans4);
			
			if(!empty($result)){
				$val['msg'] = "Quiz added successfully!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewQuiz',$val);
				$this->load->view('admin/footer');
			}else{
				$val['msg'] = "Quiz not added!";
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewQuiz',$val);
				$this->load->view('admin/footer');
			}
			
		}else{
			$session_email = $this->session->userdata('email');
			if(!empty($session_email)){
				$html = '';
				$qhtml = '';
				$result = $this->Quiz_model->getCatList();
				if(!empty($result)){
					$i = 1;
					foreach($result as $value){
						$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
					}
					$data['htmldata'] = $html;
				}
				$qresult = $this->Quiz_model->getQuizNameList();
				if(!empty($qresult)){
					$i = 1;
					foreach($qresult as $value){
						$qhtml .= '<option value="'.$value->id.'">'.$value->quiz_name.'</option>';
					}
					$data['qhtmldata'] = $qhtml;
				}
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/addNewQuiz',$data);
				$this->load->view('admin/footer');	
			}else{
				$val['msg'] = "Cann't access without login!";
				$this->load->view('admin/login',$val);	
			}	
		}
	}
	
	public function quizList(){
		if($this->session->userdata('adminData')){
			$html = '';
			$result = $this->Quiz_model->getQuizList();
			//echo '<pre>';
			//print_r($result);
			//die;
			if(!empty($result)){
				$i = 1;
				foreach($result as $value){
					$html .= '<tr><td>'.$i.'</td>';
					$qunserdata = unserialize($value->quiz_ques);
					if(count($qunserdata) == '2'){
						$html .= '<td><img src="'.base_url().'uploads/'.$qunserdata[1].'" height="50px" width="50px"><br>'.$qunserdata[0].'</td>';
					}else{
						$ext = strtolower(pathinfo($qunserdata[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$html .= '<td><img src="'.base_url().'uploads/'.$qunserdata[0].'" height="50px" width="50px"></td>';
						}else{
							$html .= '<td>'.$qunserdata[0].'</td>';
						}
					}
					
					$html .= '<td>hhh</td>';
					
					if($value->status == 1){
						$html .= '<td><a href="changeQuizStatus?id='.$value->id.'">Deactivate</a></td>';
					}else{
						$html .= '<td><a href="changeQuizStatus?id='.$value->id.'">Activate</a></td>';
					}
					$html .= '<td><a href="deleteQuiz?id='.$value->id.'">Delete</a></td></tr>';
					$i++;
				}
				$data['htmldata'] = $html;
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/quizList',$data);
				$this->load->view('admin/footer');	
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/quizList');
				$this->load->view('admin/footer');	
			}
    	}else{
			$val['msg'] = "Cann't access without login!";
			$this->load->view('admin/login',$val);	
		}
	}
	
	public function changeQuizStatus(){
		$html = '';
		$result = $this->Quiz_model->changeQuizStatus($_REQUEST['id']);
		if($result){
			$this->quizList();
		}
	}
	
	public function deleteQuiz(){
		$html = '';
		$result = $this->Quiz_model->deleteQuiz($_REQUEST['id']);
		if($result){
			$this->quizList();
		}
	}
	
}
