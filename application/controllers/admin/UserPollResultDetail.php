<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserPollResultDetail extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserPoll_model');
    }
	
	public function index(){
        $result = $this->UserPoll_model->getUserPollResultDetail($_REQUEST['optid'],$_REQUEST['qid']);
		$data['htmlData'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userPollResultDetail',$data);
		$this->load->view('admin/footer');
    }
	
}