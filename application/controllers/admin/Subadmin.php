<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Subadmin extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Subadmin_model');
    }
	
	public function index(){
		/*if(isset($_POST['addsub'])) {
		    
		   if(!empty($_POST['verify'])){
		       
		       $vv = 2;
		       
		   }else{
		       
		       $vv=1;
		   }
	
	 if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $v = 2;
    
}	   

		   
 else if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']) )
{
    $v = 5;
    
}		   
 else if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $v = 3;
    
}

 else if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $v = 6;
    
}
 else if(empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $v = 14;
    
}


else  if(empty($_POST['abused_block']) && empty($_POST['abused'])  && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 15;
    
}

else  if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $v = 4;
    
}
else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']) )
{
    $v = 16;
    
}

else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && empty($_POST['pollpost']))
{
    $v = 7;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify'])  && empty($_POST['pollpost']) )
{
    $v = 2;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify'])  && empty($_POST['pollpost']))
{
    $v = 10;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 7;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 11;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 9;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 13;
    
}
else  if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 8;
    
}
else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 9;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $v = 12;
    
}
		$results=	$this->Subadmin_model->add($_POST['fullname'], $_POST['loginemail'], $_POST['password'],$v,$vv);
			$this->session->set_flashdata('msg', "Subadmin added successfully!");
			
			redirect('admin/subadmin','refresh');
		}*/
	/*if(isset($_POST['submitsa'])) {
			$this->Subadmin_model->delete($_POST['subadmin']);
			$this->session->set_flashdata('msg', "Subadmin deleted successfully!");
			redirect('admin/subadmin','refresh');
		}*/
		
		if(isset($_POST['addsub'])) {
			$fullname = (isset($_POST['fullname']))?$_POST['fullname']:'';
			$loginemail = (isset($_POST['loginemail']))?$_POST['loginemail']:'';
			$password = (isset($_POST['password']))?$_POST['password']:'';
			$profile_block = (isset($_POST['profile_block']))?$_POST['profile_block']:0;
			$profile_delete = (isset($_POST['profile_delete']))?$_POST['profile_delete']:0;
			$profile_verification = (isset($_POST['profile_verification']))?$_POST['profile_verification']:0;
			$poll_delete = (isset($_POST['poll_delete']))?$_POST['poll_delete']:0;
			$data = array('fullname' => $fullname,
						'email' => $loginemail,
						'pass' => md5($password),
						'status' => 1,
						'subadmin' => 1,
						'created' => date('Y-m-d H:i:s'),
						'permission_abused' => 1,
						'permission_verify' => $profile_verification,
						'permission_profile_block' => $profile_block,
						'permission_profile_delete' => $profile_delete,
						'permission_poll_delete' => $poll_delete);
			$this->db->insert('vv_login',$data);
			$this->session->set_flashdata('msg', "Subadmin added successfully");
			redirect(base_url().'admin/subadmin', 'refresh');
		}
		
		if(isset($_POST['updatesub'])) {
			$fullname = (isset($_POST['fullname']))?$_POST['fullname']:'';
			$loginemail = (isset($_POST['email']))?$_POST['email']:'';
			$profile_block = (isset($_POST['profile_block']))?$_POST['profile_block']:0;
			$profile_delete = (isset($_POST['profile_delete']))?$_POST['profile_delete']:0;
			$profile_verification = (isset($_POST['profile_verification']))?$_POST['profile_verification']:0;
			$poll_delete = (isset($_POST['poll_delete']))?$_POST['poll_delete']:0;
			$data = array('fullname' => $fullname,
						'email' => $loginemail,
						'status' => 1,
						'subadmin' => 1,
						'created' => date('Y-m-d H:i:s'),
						'permission_abused' => 1,
						'permission_verify' => $profile_verification,
						'permission_profile_block' => $profile_block,
						'permission_profile_delete' => $profile_delete,
						'permission_poll_delete' => $poll_delete);
			$password = (isset($_POST['pass']))?$data['pass'] = md5($_POST['pass']):'';
			$this->db->where('id',$_POST['id']);
			$this->db->update('vv_login',$data);
			$this->session->set_flashdata('msg', "Subadmin updated successfully");
			redirect(base_url().'admin/subadmin', 'refresh');
		}
		
		$data['list']= $this->Subadmin_model->getList();
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/subadmin',$data);
		$this->load->view('admin/footer');
	}
	
		public function changeStatus() {
		   
		 $d  =	$this->Subadmin_model->update( $_GET['id'],$_GET['status']);
		 		echo $d;
		}
		public function editsubadmin($id) {

	$data['res'] =$this->Subadmin_model->editsubadmin($id);
	$this->load->view('admin/header',$id);
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/subadminedit',$data);
		$this->load->view('admin/footer');
		}
    	/*public function updatesubadmin() {
			 		   
	 if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $a = 2;
    
}	   

		   
 else if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']) )
{
    $a = 5;
    
}		   
 else if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $a = 3;
    
}

 else if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $a = 6;
    
}
 else if(empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $a = 14;
    
}


else  if(empty($_POST['abused_block']) && empty($_POST['abused'])  && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 15;
    
}

else  if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && empty($_POST['pollpost']) )
{
    $a = 4;
    
}
else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']) )
{
    $a = 16;
    
}

else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && empty($_POST['pollpost']))
{
    $a = 7;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify'])  && empty($_POST['pollpost']) )
{
    $a = 2;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify'])  && empty($_POST['pollpost']))
{
    $a = 10;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 7;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 11;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 9;
    
}
else  if(empty($_POST['abused_block']) && empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 13;
    
}
else  if(!empty($_POST['abused_block']) && !empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 8;
    
}
else  if(!empty($_POST['abused_block']) && empty($_POST['abused']) && empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a = 9;
    
}
else  if(empty($_POST['abused_block']) && !empty($_POST['abused']) && !empty($_POST['verify']) && !empty($_POST['pollpost']))
{
    $a= 12;
    
}

		if(!empty($_POST['verify']))
		{
		    $v=2; 
		}
		else{
		    $v=1;
		}
		
		$this->Subadmin_model->updatesubadmin($_POST['email'],$_POST['id'],$_POST['pass'],$a,$v);

		}*/
	public function add() {
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/subadminadd');
		$this->load->view('admin/footer');
    }
    public function deletesubadmin() {
        $id = $_GET['id'];
        $a =	$this->Subadmin_model->deletesubadmin($id);
        echo $a;
	
    }
	
}