<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Abusereport extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('AdminAbusereport');
		$this->load->model('User_model','user');
		$this->load->model('cities');
    }
	
	public function index(){
		$data['categories']= $this->user->getCatList();
		$data['countries'] = $this->cities->allCountries();
		
		$adminData = $this->session->userdata('adminData');
		if(empty($adminData))
			redirect('admin/login','refresh');
		
		$start = (isset($_REQUEST['page']) && ($_REQUEST['page'] > 0))?($_REQUEST['page']-1):0;
		$limit = 100;
			
		$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
		$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
		$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
		$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
		$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
		$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
		$dfrom = (isset($_REQUEST['dfrom']))?$_REQUEST['dfrom']:'';
		$dto = (isset($_REQUEST['dto']))?$_REQUEST['dto']:'';
		$type = (isset($_REQUEST['type']))?$_REQUEST['type']:'';
		$causes = (isset($_REQUEST['causes']))?$_REQUEST['causes']:'';
		
		if(isset($_POST['abusedconfirmed'])) {
			$ref = (isset($_POST['ref']) && $_POST['ref'])?$_POST['ref']:'';
			$refid = (isset($_POST['id']) && $_POST['id'])?$_POST['id']:'';
			$message = (isset($_POST['message']) && $_POST['message'])?$_POST['message']:'';
			$blocktimeframe = (isset($_POST['blocktimeframe']) && $_POST['blocktimeframe'])?$_POST['blocktimeframe']:'';
			$ref = (isset($_POST['ref']) && $_POST['ref'])?$_POST['ref']:'';
			$ref = (isset($_POST['ref']) && $_POST['ref'])?$_POST['ref']:'';
			$delete = (isset($_POST['delete']))?$_POST['delete']:'';
			if($ref=='profile') {
				$userinfo = $this->AdminAbusereport->getUserbyref($refid, 'profile');
				$this->AdminAbusereport->reportProfile($message, $blocktimeframe, $refid, $delete);
				$this->phpmailer($userinfo['email'], $userinfo['firstname'].' '.$userinfo['lastname'], $userinfo['id'], $message);
			} else {
				$email = $this->AdminAbusereport->getUserbyref($_POST['id'], 'profile');
				$profile = (!empty($_POST['profile']) && ($_POST['profile']=='yes'))?1:0;
				$this->AdminAbusereport->reportAll($_POST['message'], $_POST['ref'], $profile, $_POST['blocktimeframe'], $_POST['id'], $delete);
				$this->phpmailer($email['email'], $email['firstname'].' '.$email['lastname'], $email['id'], $_POST['message']);
			}
		}
		
		if(!empty($this->input->get()))
		{
			if($country_id > 0)
				$result['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$result['cities'] = $this->cities->allCitiesByState($state_id);
			
			//$result['result'] = $this->user->getFilterVerifyApplication($country_id, $state_id, $city_id, $gender, $keyword, $interest);
		}
		else
		{
			//$result['result'] = $this->user->getVerifyApplication();
		}
		
		if(isset($_REQUEST['reftype'])) {
			$result = $this->AdminAbusereport->getDetail($_REQUEST['reftype'], $_REQUEST['refid'], $_REQUEST['id']);
			$data['result'] = $result['result'];
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/abusereportdetail',$data);
			$this->load->view('admin/footer');
			return;
		}
		
		if(isset($_REQUEST['search'])) {
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result = $this->AdminAbusereport->getListByFilter($limit, $country_id, $state_id, $city_id, $gender, $type, $keyword, $dfrom, $dto, $start, $interest, $causes);
		}
		else {		
			$result = $this->AdminAbusereport->getList($start, $limit);
		}
		$data['result'] = $result['result'];
		
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/abusereport',$data);
		$this->load->view('admin/footer');
	}
	
	public function ajaxstates($cid=''){
		$states = $this->cities->allStates($cid);
		$result = '<option value="">--Select--</option>';
		foreach($states as $state) {
			$result .= '<option value="'.$state->id.'">'.$state->name.'</li>';
		}
		$data['data'] = $result;
		$this->load->view('ajaxss',$data);
	}
	
	public function ajaxcities($sid=''){
		$states = $this->cities->allCitiesByState($sid);
		$result = '<option value="">--Select--</option>';
		foreach($states as $state) {
			$result .= '<option value="'.$state->id.'">'.$state->name.'</li>';
		}
		$data['data'] = $result;
		$this->load->view('ajaxss',$data);
	}
	
	public function phpmailer($email, $name, $id, $msg){
		$this->load->library("PhpMailerLib");
        $mailad = $this->phpmailerlib->load();
		try {
				//Server settings
				$mailad->SMTPDebug = 0;                                 // Enable verbose debug output
				$mailad->isSMTP();                                      // Set mailer to use SMTP
				$mailad->Host = 'mail.viralvoters.com';  // Specify main and backup SMTP servers
				$mailad->SMTPAuth = true;                               // Enable SMTP authentication
				$mailad->Username = 'auth@viralvoters.com';                 // SMTP username
				$mailad->Password = 'sachin@123';                           // SMTP password
				$mailad->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
				$mailad->Port = 465;                                    // TCP port to connect to
				//Recipients
				$mailad->setFrom('no-reply@viralvoters.com', 'ViralVoters');
				$mailad->addAddress($email, $name);            // Name is optional
				//$mailad->addAddress('sachinsaini.cs@gmail.com', 'ViralVoters');            // Name is optional
				$mailad->addReplyTo('info@viralvoters.com', 'ViralVoters');
				//$mailad->AddBCC('rajneesh.sharma@yahoo.com');
				//$mailad->AddBCC('shalabh5@hotmail.com');
				$mailad->AddBCC('viralvoters@gmail.com');
				$mailad->isHTML(true);                                  // Set email format to HTML
				$mailad->Subject = 'Viralvoters.com - Abuse reporting';
				$mailad->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear '.$name.',</p>
						
						
						<p>'.$msg.'</p>
						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>
						<p>Please do not reply to this email.</p>
						
					</td></tr>
					</table>';
	
				$mailad->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}
		
	}
}
