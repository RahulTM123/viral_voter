<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class EmailSection extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('SMTP_model');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
        	$getSmtpDetails = $this->SMTP_model->getSmtpDetails();
			if(!empty($getSmtpDetails)){
				$htmlData = array(
					'id' => $getSmtpDetails[0]->id,
					'fromEmail' => $getSmtpDetails[0]->fromEmail,
					'fromName' => $getSmtpDetails[0]->fromName,
					'smtpPort' => $getSmtpDetails[0]->smtpPort,
					'encrption' => $getSmtpDetails[0]->encrption,
					'replyTo' => $getSmtpDetails[0]->replyTo,
					'host' => $getSmtpDetails[0]->host,
					'username' => $getSmtpDetails[0]->username,
					'password' => $getSmtpDetails[0]->password
				);
			}else{
				$htmlData = array(
					'id' => '',
					'fromEmail' => '',
					'fromName' => '',
					'smtpPort' => '',
					'encrption' => '',
					'replyTo' => '',
					'host' => '',
					'username' => '',
					'password' => ''
				);				
			}
			$data['adminData'] = $this->session->userdata('adminData');
			$data['htmlData'] = $htmlData;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/emailSettings');
			$this->load->view('admin/footer');
		}else{
			redirect('admin/login','refresh');
		}
    }
	
	public function saveSettings(){
		if($this->session->userdata('adminData')){
        	if(isset($_REQUEST['settings'])){
				if(!empty($_REQUEST['smtpId'])){
					$updateSettings = $this->SMTP_model->updateSettings($_REQUEST['smtpId'],$_REQUEST['fromEmail'],$_REQUEST['fromName'],$_REQUEST['replyTo'],$_REQUEST['host'],$_REQUEST['port'],$_REQUEST['encrption'],$_REQUEST['username'],$_REQUEST['password']);
					redirect('admin/EmailSection','refresh');
				}else{
					$insertSettings = $this->SMTP_model->insertSettings($_REQUEST['fromEmail'],$_REQUEST['fromName'],$_REQUEST['replyTo'],$_REQUEST['host'],$_REQUEST['port'],$_REQUEST['encrption'],$_REQUEST['username'],$_REQUEST['password']);
					redirect('admin/EmailSection','refresh');
				}
			}
		}else{
			redirect('admin/login','refresh');
		}
    }
	
	
	
	public function template(){
        if($this->session->userdata('adminData')){
        	$getTemplate = $this->SMTP_model->getTemplate();
			if(!empty($getTemplate)){
				$htmlData = array(
					'id' => $getTemplate[0]->id,
					'subject' => $getTemplate[0]->subject,
					'content' => $getTemplate[0]->content
				);
			}else{
				$htmlData = array(
					'id' => '',
					'subject' => '',
					'content' => ''
				);				
			}
			$data['adminData'] = $this->session->userdata('adminData');
			$data['htmlData'] = $htmlData;
			$this->load->view('admin/header',$data);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/emailTemplate');
			$this->load->view('admin/footer');
		}else{
			redirect('admin/login','refresh');
		}
    }
	
	
	public function saveTemplate(){
		if($this->session->userdata('adminData')){
        	if(isset($_REQUEST['template'])){
				if(!empty($_REQUEST['templateId'])){
					$updateTemplate = $this->SMTP_model->updateTemplate($_REQUEST['templateId'],$_REQUEST['subject'],$_REQUEST['content']);
					redirect('admin/EmailSection/template','refresh');
				}else{
					$insertTemplate = $this->SMTP_model->insertTemplate($_REQUEST['subject'],$_REQUEST['content']);
					redirect('admin/EmailSection/template','refresh');
				}
			}
		}else{
			redirect('admin/login','refresh');
		}
    }
	
	
}
