<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserVote extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model','user');
		$this->load->model('cities');
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			if(isset($_POST['poststatus'])){
				$result = $this->user->updateStatus($_POST['status'], $_POST['userid']);
			}
			$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
			$limit = 10;
			$start = ($page)?($page-1)*$limit:0;
			
			$data['countries'] = $this->cities->allCountries();
			$html = '';
			
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
			
			if(isset($_REQUEST['search'])){
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
				
				$result = $this->user->getUserListByFilter($limit, $country_id, $state_id, $city_id, $gender, $page, $interest);
			}else{
				$result = $this->user->getUserList($limit, $page);
			}
			
			$data['result'] = $result['data'];
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/uservote',$data);
			$this->load->view('admin/footer');
				
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
    }
	
}
