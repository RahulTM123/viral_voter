<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('User_model','user');
		$this->load->model('cities');
			$this->load->model('Category_model');
		$this->load->library("PhpMailerLib");
    }
	
	public function index(){
        if($this->session->userdata('adminData')){
			if(isset($_POST['poststatus'])){
				$result = $this->user->updateStatus($_POST['status'], $_POST['userid']);
				if(isset($_POST['deltext'])) {
					$body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
							<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
							<tr><td>
								<p>Dear Member,</p>
								
								
								<p>'.nl2br($_POST['deltext']).'</p>
								
								<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>
								<p>Please do not reply to this email.</p>
								
							</td></tr>
							</table>';
					$subject = 'Account deactivated from ViralVoters.com';
					$this->user->mailtouser($_POST['userid'], $body, $subject);
				}
			}
			$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
			$limit = 100;
			$start = ($page)?($page-1)*$limit:0;
			
			$data['categories']= $this->user->getCatList();
			$data['countries'] = $this->cities->allCountries();
			$html = '';
			
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
			$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
			$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
			$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'])):'';
			
			if(isset($_REQUEST['search'])){
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
				
				$result = $this->user->getUserListByFilter($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page, $interest, $dfrom, $dto);
			}else{
				$result = $this->user->getUserList($limit, $page);
			}
			
			if($this->session->flashdata('error')){
				$data['error'] = $this->session->flashdata('error');
			}
			if($this->session->flashdata('msg')){
				$data['msg'] = $this->session->flashdata('msg');
			}
			
			$pagination = '<div class="pagination">';
			$tc = ceil($result['numrows']/$limit);
			for($i=0;$i<$tc;$i++) {
				(($i+1)==$page)?$class = 'active':$class = '';
				if(($country_id!='')||($state_id!='')||($city_id!='')||($gender!='')||($keyword!='')||($dfrom!='')||($dto!='')||($interest!=''))
					$pagination .= '<a href="?interest='.$interest.'&cid='.$country_id.'&sid='.$state_id.'&ctid='.$city_id.'&dfrom='.$dfrom.'&dto='.$dto.'&gender='.$gender.'&keyword='.$keyword.'&page='.($i+1).'&search=Search" class="pagination '.$class.'">'.($i+1).'</a>';
				else
					$pagination .= '<a href="?page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
			}
			$pagination .= '</div>';
			
			$data['pagination'] = $pagination;
			$data['result'] = $result['data'];
			$data['totals'] = $result['numrows'];
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/userlist',$data);
			$this->load->view('admin/footer');
				
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
    }


    public function changeUserStatusDeActivate(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->user->changeUserStatusDeActivate($_REQUEST['id']);
		if($result){
			$this->session->set_flashdata("error", "User is de-activated!");	
			redirect($actual_link,'refresh');
		}
	}

    public function changeUserStatusActivate(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->user->changeUserStatusActivate($_REQUEST['id']);
		if($result){
			$this->session->set_flashdata("msg", "User is activated!");	
			redirect($actual_link,'refresh');
		}
	}

    public function changeUserStatusSpam(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->user->changeUserStatusSpam($_REQUEST['id']);
		if($result){
			$this->session->set_flashdata("error", "User marked as a spam successfully!");	
			redirect($actual_link,'refresh');
		}
	}
	
	public function deleteUser(){
		$actual_link = $_SERVER['HTTP_REFERER'];
		$html = '';
		$result = $this->user->deleteUser($_REQUEST['id']);
		if($result){
			$this->session->set_flashdata("error", "User deleted succesfully!");
			redirect($actual_link,'refresh');
		}
	}
	
	public function deletedUser(){
        if($this->session->userdata('adminData')){
			if(isset($_POST['status']) && isset($_POST['userid'])){
				$result = $this->user->updateStatus($_POST['status'], $_POST['userid']);
			}
			
			$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
			$limit = 10;
			$start = ($page)?($page-1)*$limit:0;
			
			$data['countries'] = $this->cities->allCountries();
			$html = '';
			
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
			$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
			$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
			$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'])):'';
			
			if(isset($_REQUEST['search'])) {
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
				
				$result = $this->user->getDeletedUserListByFilter($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page, $interest);
			}else{
				$result = $this->user->getDeletedUserList($limit, $page);
			}
			
			$pagination = '<div class="pagination">';
			$tc = ceil($result['numrows']/$limit);
			for($i=0;$i<$tc;$i++) {
				(($i+1)==$page)?$class = 'active':$class = '';
				if(($country_id!='')||($state_id!='')||($city_id!='')||($gender!='')||($keyword!='')||($dfrom!='')||($dto!='')||($interest!=''))
					$pagination .= '<a href="?interest='.$interest.'&cid='.$country_id.'&sid='.$state_id.'&ctid='.$city_id.'&dfrom='.$dfrom.'&dto='.$dto.'&gender='.$gender.'&keyword='.$keyword.'&page='.($i+1).'&search=Search" class="pagination '.$class.'">'.($i+1).'</a>';
				else
					$pagination .= '<a href="?page='.($i+1).'" class="pagination '.$class.'">'.($i+1).'</a>';
			}
			$pagination .= '</div>';
			
			$data['pagination'] = $pagination;
			$data['result'] = $result['data'];
			$data['totals'] = $result['numrows'];
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/deleteuserlist',$data);
			$this->load->view('admin/footer');
				
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
	}
	
	public function userVerification(){
		$result['countries'] = $this->cities->allCountries();
        if($this->session->userdata('adminData')){
			if(isset($_POST['verify'])) {
				$reject = (isset($_POST['reject_message']))?$_POST['reject_message']:'';
				$result['result'] = $this->user->updateVerifyApplication($_POST['status'], $_POST['userid'], $reject);
				if($_POST['status']==1) $this->session->set_flashdata("error", "User verified successfully");	
				else $this->session->set_flashdata("error", "User rejected successfully");	
			}
				$category_id = (isset($_REQUEST['catid']))?$_REQUEST['catid']:'';
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
			$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
			$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
			$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'])):'';
		
			if(!empty($this->input->get()))
			{
				if($country_id > 0)
					$result['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$result['cities'] = $this->cities->allCitiesByState($state_id);
				
				$result['result'] = $this->user->getFilterVerifyApplication($country_id, $state_id, $city_id, $gender, $keyword, $interest, $dfrom, $dto);
			}
			else
			{
				$result['result'] = $this->user->getVerifyApplication();
			}
			
				
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/userverify',$result);
			$this->load->view('admin/footer');
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
	}
	
	public function userVerificationDetails($userid){
        if($this->session->userdata('adminData')){
			if(isset($_POST['verify'])) {
				$reject = (isset($_POST['reject_message']))?$_POST['reject_message']:'';
				$result['result'] = $this->user->updateVerifyApplication($_POST['status'], $_POST['userid'], $reject);
				if($_POST['status']==1) $this->session->set_flashdata("error", "User verified successfully");	
				else $this->session->set_flashdata("error", "User rejected successfully");	
			}
			
			$result['result'] = $this->user->getVerifyApplicationDetails($userid);
		//print_r($result['result']);
			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/userverifydetail',$result);
			$this->load->view('admin/footer');
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
	}
	
	public function userVerified(){
		$result['countries'] = $this->cities->allCountries();
			//$result['categories']= $this->user->getCatList();
	$catname = (isset($_REQUEST['catid']))?$_REQUEST['catid']:'';
			$result['categories'] = $this->Category_model->getVerifyCatList($catname);
        if($this->session->userdata('adminData')){
          
			$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
			$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
			$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
			$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
			$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
			//$interest = (isset($_REQUEST['interest']))?$_REQUEST['interest']:'';
			  $category_id = (isset($_REQUEST['catid']))?$_REQUEST['catid']:'';
			$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
			$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'], '+1day')):'';
		
		
		
			if(!empty($this->input->get()))
			{
			   
				if($country_id > 0)
					$result['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$result['cities'] = $this->cities->allCitiesByState($state_id);
				
				$result['result'] = $this->user->getFilterVerifiedApplication($country_id, $state_id, $city_id, $gender, $keyword, $category_id, $dfrom, $dto);
			}
			else
			{

			$result['result']= $this->user->getVerifiedApplication();
		//	print_r($result);
				
			}
							

			$this->load->view('admin/header');
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/userverified',$result);
			$this->load->view('admin/footer');
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
	}
	
	public function verificationCat(){
        if($this->session->userdata('adminData')){
			if(isset($_REQUEST['edit']) && $_REQUEST['id']) {
				if(isset($_REQUEST['update']) && $_REQUEST['id']) {
					$this->session->set_flashdata("msg", "Category updated successfully");
					$result['result'] = $this->user->updateVerifycat($_REQUEST['catId'], $_REQUEST['catName']);
					redirect('admin/verificationcat','refresh');
				}
				$result['result'] = $this->user->getVerifycat($_REQUEST['id']);
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/edit_vercat',$result);
				$this->load->view('admin/footer');
			}
			else if(isset($_REQUEST['add']) && $_REQUEST['add']) {
				if(isset($_REQUEST['catName'])) {
					$this->user->addVerifycategory($_REQUEST['catName']);
					$this->session->set_flashdata("msg", "Category added successfully");
					redirect('admin/verificationcat?add=true','refresh');
				}
			
				$result['result'] = $this->user->getVerifycategory();
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/add_vercat');
				$this->load->view('admin/footer');
			}
			else {
				if(isset($_REQUEST['id']) && ($_REQUEST['delete'])) {
					$this->user->deleteVerifycategory($_REQUEST['id']);
					$this->session->set_flashdata("msg", "Category deleted successfully");
					redirect('admin/verificationcat','refresh');
				}
				if(isset($_REQUEST['changestatus'])) {
					$status = ($_REQUEST['changestatus']=='Active')?0:1;
					$this->user->updateVerifycategory($_REQUEST['catid'], $status);
					$this->session->set_flashdata("msg", "Category updated successfully");
				}
			
				$result['result'] = $this->user->getVerifycategory();
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/list_vercat',$result);
				$this->load->view('admin/footer');
			}
    	}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
	}
	
}
