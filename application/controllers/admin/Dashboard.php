<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Login_model');
		$this->load->model('AdminDashboard_model');
    }
	
	public function index(){
		$adminData = $this->session->userdata('adminData');
		if(!empty($adminData)){
			$data['id'] = $this->session->userdata('id');
			$adminData['dashboard'] = array('quickstats' => $this->AdminDashboard_model->QuickStats());
 			$this->load->view('admin/header',$adminData);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/dashboard');
			$this->load->view('admin/footer');
		}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
    }
	
	public function loginuser(){
		$result = $this->Login_model->getUserDetails($_REQUEST);
		if($result != 0){
			$newdata = array(
               'email'     => $_REQUEST['email'],
			   'id'     => $result['id'],
               'logged_in' => TRUE,
               'permission' => array('permission_abused' => $result['permission_abused'], 'permission_verify' => $result['permission_verify'], 'permission_profile_block' => $result['permission_profile_block'], 'permission_profile_delete' => $result['permission_profile_delete'], 'permission_poll_delete' => $result['permission_poll_delete'])
           );
		   $this->session->set_userdata('adminData',$newdata);
		   redirect('admin/Login/dashboard');
		}else{
			$this->session->set_flashdata('error', 'Your login credentials are incorrect!');	
			redirect('admin/login','refresh');
		}
    }
	
	public function main_dashboard(){
		$adminData = $this->session->userdata('adminData');
		if(!empty($adminData)){
			$data['id'] = $this->session->userdata('id');
 			
			$this->load->view('dashboard-2');
			
		}else{
			$this->session->set_flashdata("error", "Cann't access without login!");	
			redirect('admin/login','refresh');
		}
    }
	
	public function changepassword(){
		$adminData = $this->session->userdata('adminData');
		if(!empty($adminData)){
			if(isset($_POST['password']) && $_POST['password']) {
				$result = $this->AdminDashboard_model->change_password($_POST['password']);
				$this->session->set_flashdata("msg", "Password change successfully");	
			}
 			$this->load->view('admin/header',$adminData);
			$this->load->view('admin/sidebar');	
			$this->load->view('admin/changepassword');
			$this->load->view('admin/footer');
		}
    }
}
