<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Statistics extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Userstat_model');
		$this->load->model('UserPoll_model');
		$this->load->model('cities');
    }
	
	public function index(){
		$data['countries'] = $this->cities->allCountries();
		$data['categories']= $this->UserPoll_model->getCatList();
		$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
			
		$country_id = (isset($_REQUEST['cid']))?$_REQUEST['cid']:'';
		$state_id = (isset($_REQUEST['sid']))?$_REQUEST['sid']:'';
		$city_id = (isset($_REQUEST['ctid']))?$_REQUEST['ctid']:'';
		$gender = (isset($_REQUEST['gender']))?$_REQUEST['gender']:'';
		$category = (isset($_REQUEST['cat']))?$_REQUEST['cat']:'';
		$keyword = (isset($_REQUEST['keyword']))?$_REQUEST['keyword']:'';
		$dfrom = (!empty($_REQUEST['dfrom']))?date('Y-m-d', strtotime($_REQUEST['dfrom'])):'';
		$dto = (!empty($_REQUEST['dto']))?date('Y-m-d', strtotime($_REQUEST['dto'])):'';
		
		if(!empty($this->input->get()))
		{
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result=$this->Userstat_model->getFilterStat($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page, $category, $dfrom, $dto);
		}
		else
		{
			$result=$this->Userstat_model->getStat($start, $limit);
		}
				
		$data['result'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userstat',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function poll($userid) {
		$data['categories']= $this->UserPoll_model->getCatList();
		$result=$this->Userstat_model->getUserPoll($userid);				
		$data['result'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userstatpoll',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function pollDetail($pollid) {
		$data['categories']= $this->UserPoll_model->getCatList();
		$q = (isset($_REQUEST['q']))?$_REQUEST['q']:'';
		$result=$this->Userstat_model->getUserPollDetail($pollid, $q);
		$data['result'] = $result;
		//echo '<pre>';
		//print_r($data);
		//die('hello');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		if($q=='participation')
			$this->load->view('admin/userstatpoll_participation',$data);
		if($q=='comments')
			$this->load->view('admin/userstatpoll_comment',$data);
		if($q=='upvote')
			$this->load->view('admin/userstatpoll_upvote',$data);
		if($q=='sharing')
			$this->load->view('admin/userstatpoll_sharing',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function post($userid) {
		$data['categories']= $this->UserPoll_model->getCatList();
		$result=$this->Userstat_model->getUserPost($userid);				
		$data['result'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userstatpost',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function postDetail($postid) {
		$data['categories']= $this->UserPoll_model->getCatList();
		$q = (isset($_REQUEST['q']))?$_REQUEST['q']:'';
		$result=$this->Userstat_model->getUserpostDetail($postid, $q);
		$data['result'] = $result;
		//echo '<pre>';
		//print_r($data);
		//die('hello');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		if($q=='comments')
			$this->load->view('admin/userstatpost_comment',$data);
		if($q=='upvote')
			$this->load->view('admin/userstatpost_upvote',$data);
		if($q=='sharing')
			$this->load->view('admin/userstatpost_sharing',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function comment($userid) {
		$result=$this->Userstat_model->getUserComment($userid);
		$data['categories']= $this->UserPoll_model->getCatList();
		$data['result'] = $result;
		//echo '<pre>';
		//print_r($result);
		//die('hello');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/userstatcomment',$data);
		$this->load->view('admin/footer');
    }
	
	
	public function upvote($userid) {
		$q = (isset($_REQUEST['type']))?$_REQUEST['type']:'';
		$result=$this->Userstat_model->getUserUpvote($userid, $q);
		$data['result'] = $result;
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		switch($q) {
		case 'poll':
			$this->load->view('admin/userstatupvote_poll',$data);
		break;
		
		case 'post':
			$this->load->view('admin/userstatupvote_post',$data);
		break;
		
		case 'comment':
			$this->load->view('admin/userstatupvote_comment',$data);
		break;
		
		default:
			$this->load->view('admin/userstatupvote',$data);
		break;
		}
		$this->load->view('admin/footer');
    }
	
}