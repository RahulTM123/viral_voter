<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Login_model');
    }
	
	public function index(){
		if($this->session->userdata('adminData')){
        	redirect('admin/Dashboard','refresh');
		}else{
			if($this->session->flashdata('error')){
				$data['msg'] = $this->session->flashdata('error');
				$this->load->view('admin/login',$data);
			}else{
				$this->load->view('admin/login','refresh');
			}
		}
    }
	
	public function loginuser(){
		$result = $this->Login_model->getUserDetails($_REQUEST);
		if($result != 0){
			$newdata = array(
               'email'     => $_REQUEST['email'],
			   'id'     => $result['id'],
               'logged_in' => TRUE,
               'permission' => array('permission_abused' => $result['permission_abused'], 'permission_verify' => $result['permission_verify'], 'permission_profile_block' => $result['permission_profile_block'], 'permission_profile_delete' => $result['permission_profile_delete'], 'permission_poll_delete' => $result['permission_poll_delete'])
           );
		   $this->session->set_userdata('adminData',$newdata);
		   redirect('admin/Dashboard','refresh');
		}else{
			$this->session->set_flashdata('error', 'Your login credentials are incorrect!');	
			redirect('admin/login','refresh');
		}
    }
	
	public function logout(){
		$this->session->sess_destroy();
		$this->session->set_flashdata("error", "You are logout successfully!");	
		redirect('admin/login','refresh');
    }
}
