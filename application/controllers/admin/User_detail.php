<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_detail extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserTimeline_model');
		$this->load->model('User_detail_model');
		$this->load->model('cities');
    }
	public function index($username){
		//var_dump($username);die();
		$data = array();
		$data['name'] = $username;
		$profileData = '';
		$getCurrentUserId = $this->UserTimeline_model->getCurrentUserId($username);
			
			$categories = $this->User_detail_model->getCatList();
			$data['categories'] = $categories;
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->User_detail_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($getCurrentUserId);
			
			$message = $this->User_detail_model->getUnreadMessages($userData['userId']);
			$data['messages'] = $message->msg;
		}
		else {
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($getCurrentUserId);
		}
		$timeline = $this->User_detail_model->getUserTimeline($getCurrentUserId);
		/*extra*/
		$data['posts'] = $this->User_detail_model->getUserPost($getCurrentUserId);
		$data['polls'] = $this->User_detail_model->getUserPoll($getCurrentUserId);
		$data['comments'] = $this->User_detail_model->getUserComment($getCurrentUserId);
		/*finish*/
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($getCurrentUserId);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('admin/header',$data);
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/user_detail');
		$this->load->view('admin/footer');
    }
	
	public function userById($id){
		//echo "hello";die();
		$data = array();
		$profileData = '';
		$data['id'] = $id;
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->User_detail_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($id);
		}
		else {
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($id);
		}
		
		$timeline = $this->User_detail_model->getUserTimeline($id);
		/*extra*/
		$data['posts'] = $this->User_detail_model->getUserPost($id);
		$data['polls'] = $this->User_detail_model->getUserPoll($id);
		$data['comments'] = $this->User_detail_model->getUserComment($id);
		/*finish*/
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		
	
		$data['userLogout'] = $this->session->userdata('userLogout');
		//var_dump($data);die();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/user_detail');
		$this->load->view('admin/footer');
    }
	
	public function userFriends($id){
		$data = array();
		$profileData = '';
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->User_detail_model->getUserInfo($userData['userId']);
			$profileData = (array)$this->User_detail_model->getUserInformation($id);
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriends($id,$userData['userId']);
		}
		else {
			$profileData = (array)$this->User_detail_model->getUserInformation($id);
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriends($id,$profileData['userid']);
		}
		$timeline = $this->User_detail_model->getUserTimeline($id);
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$data['profileData'] = $profileData;
		$data['htmlData'] = $getUserAllFriends;
		
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('userfriends');
		$this->load->view('footer');
		
		
    }
	
	
	public function aboutUser($id){
		$data = array();
		$profileData = '';
				
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->User_detail_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->User_detail_model->getFullUserInformation($id);
		}
		else {
			$data['profileData'] = (array)$this->User_detail_model->getFullUserInformation($id);
		}
		//$timeline = $this->User_detail_model->getUserTimeline($id);
		//$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$interest = (array)$this->User_detail_model->getUserInterest($id);
		if(!empty($interest))
		$data['interest'] = implode(',', $interest);
		else
		$data['interest'] = '';
		
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('about');
			$this->load->view('footer');
    }
	
	
	public function polls($id){
		$data = array();
		$profileData = '';
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->User_detail_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($id);
		}
		else {
			$data['profileData'] = (array)$this->User_detail_model->getUserInformation($id);
		}
		$timeline = $this->User_detail_model->getUserTimeline($id);
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('usertimeline');
		$this->load->view('footer');
    }
	
	
	public function addNewPoll(){
		if(isset($_REQUEST['pubPoll'])){
			$polloption = array();
			$userid = $_REQUEST['userid'];
			$qimag = $qus = $qvideolink = '';
			
			if(!empty($_REQUEST['quesI'])){
				$qimag = $_REQUEST['quesI'];
			}
			
			if(!empty($_REQUEST['videolink'])){
				$qvideolink = $_REQUEST['videolink'];
			}
			
			if(!empty($_REQUEST['ques'])){
				$qus = $_REQUEST['ques'];
			}
			
			if(!empty($_REQUEST['ans1I']) && !empty($_REQUEST['ans1'])){
				array_push($polloption,serialize(array($_REQUEST['ans1'],$_REQUEST['ans1I'])));
			}else if(!empty($_REQUEST['ans1'])){
				array_push($polloption,serialize(array($_REQUEST['ans1'])));
			}else if(!empty($_REQUEST['ans1I'])){
				array_push($polloption,serialize(array($_REQUEST['ans1I'])));
			}
			
			if(!empty($_REQUEST['ans2I']) && !empty($_REQUEST['ans2'])){
				array_push($polloption,serialize(array($_REQUEST['ans2'],$_REQUEST['ans2I'])));
			}else if(!empty($_REQUEST['ans2'])){
				array_push($polloption,serialize(array($_REQUEST['ans2'])));
			}else if(!empty($_REQUEST['ans2I'])){
				array_push($polloption,serialize(array($_REQUEST['ans2I'])));
			}
			
			if(!empty($_REQUEST['ans3I']) && !empty($_REQUEST['ans3'])){
				array_push($polloption,serialize(array($_REQUEST['ans3'],$_REQUEST['ans3I'])));
			}else if(!empty($_REQUEST['ans3'])){
				array_push($polloption,serialize(array($_REQUEST['ans3'])));
			}else if(!empty($_REQUEST['ans3I'])){
				array_push($polloption,serialize(array($_REQUEST['ans3I'])));
			}
			
			if(!empty($_REQUEST['ans4I']) && !empty($_REQUEST['ans4'])){
				array_push($polloption,serialize(array($_REQUEST['ans4'],$_REQUEST['ans4I'])));
			}else if(!empty($_REQUEST['ans4'])){
				array_push($polloption,serialize(array($_REQUEST['ans4'])));
			}else if(!empty($_REQUEST['ans4I'])){
				array_push($polloption,serialize(array($_REQUEST['ans4I'])));
			}
			
			if(isset($_REQUEST['frndids'])){
				$frndids = $_REQUEST['frndids'];
			}else{
				$frndids = '';
			}
			$cat = $_REQUEST['poll_category'];
			
			$result = $this->UserTimeline_model->addNewPoll($userid,$qvideolink,$qus,$qimag,$polloption,$cat,$frndids);
			$url = $_SERVER['HTTP_REFERER'];
			if(!empty($result)){
				redirect($url);
			}else{
				redirect($url);
			}
			
		}
	}
	
	
	public function pollVoting(){
		$friends = $this->User_detail_model->getUserFrndIds($_REQUEST['puserid']);
		$frn = explode(',', $friends);
		$alluser = array();
		
		foreach($frn as $frnds)
			$alluser[$frnds] = $frnds;
		
		unset($alluser[$userid]);
		$alluser = implode(',', $alluser);
			
		$return = $this->User_detail_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollopt'],$_REQUEST['puserid'],$_SERVER['REMOTE_ADDR'],$alluser);
		$url = $_SERVER['HTTP_REFERER'];
		if(!empty($return)){
			redirect($url);
		}else{
			redirect($url);
		}
	}
	public function addNewPost(){
		if(isset($_REQUEST['pubPost'])){
			$userid = $_REQUEST['userid'];
			$cat = $_REQUEST['post_category'];
			$postdata = $postimg = $postvideo = '';
			
			if(!empty($_REQUEST['postimg'])){
				$postimg = $_REQUEST['postimg'];
			}
			
			if(!empty($_REQUEST['postdata'])){
				$postdata = $_REQUEST['postdata'];
			}
			
			if(!empty($_REQUEST['postvideolink'])){
				$postvideo = $_REQUEST['postvideolink'];
			}
			
			if(isset($_REQUEST['frndids'])){
				$frndids = $_REQUEST['frndids'];
			}else{
				$frndids = '';
			}
			
			$result = $this->UserTimeline_model->addNewPost($userid,$postdata,$postimg,$postvideo,$cat,$frndids);
			
			$url = $_SERVER['HTTP_REFERER'];
			if(!empty($result)){
				redirect($url);
			}else{
				redirect($url);
			}
			
		}
	}
	public function Edit_user($id)
	{
		$data['edit_user']=$this->User_detail_model->Edit_user($id);
		$data['category']=$this->User_detail_model->Category($id);
		$data['country']=$this->User_detail_model->Country($id);
		//print_r($data['edit_user']->country);die();

				if($data['edit_user']->country > 0)
					$data['states'] = $this->cities->allStates($data['edit_user']->country);
				
				if($data['edit_user']->state > 0)
					$data['cities'] = $this->cities->allCitiesByState($data['edit_user']->state);

				//var_dump($data['cities']);
				
				
			
		//var_dump($data['category']);die();
		$this->load->view('admin/header',$data);
		$this->load->view('admin/sidebar');	
		$this->load->view('admin/edit_user');
		$this->load->view('admin/footer');
	}
	public function Update_user()
	{
		$d=$this->input->post();
		//var_dump($d);die();
		$f=$this->User_detail_model->Update_user($d);
		redirect("admin/user-list");
		
	}
	
}
