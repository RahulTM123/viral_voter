<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SentRequest extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('FindFriend_Model');
		$this->load->model('Dashboard_model');
    }
	
	public function index(){
		if($this->session->userdata('userData')){
			$html = $total = '';
			$userData = $this->session->userdata('userData');
			$result = $this->FindFriend_Model->friend_section_data();
			$countresult = $this->FindFriend_Model->countRequest($userData['userId']);
			if($countresult > 0){
				$total = $countresult;	
			}else{
				$total = 0;
			}
			$html .= '<div class="span10"><div class="vv_content clearfix" id="content" role="main" style="min-height:100px;"><div class="clearfix"><div class="find-friend"><div class="ff-header"><h2 class="ff">You Sent '.$total.' Friend Request</h2><div class="sent-request"><a href="'.base_url().'FindFriend">Find Friends</a> | <a href="'.base_url().'ConfirmFriend">View friend requests</a></div></div><div class="ff-request">';
			if(!empty($result)){
				foreach($result as $v){
					if($userData['userId'] != $v->friend_one){
						$friendRequest = $this->FindFriend_Model->friendRequest($userData['userId'],$v->friend_one);
						if(!empty($friendRequest)){
							foreach($friendRequest as $fr){
								if($fr->friend_one == $userData['userId'] && $fr->status == '0'){
									$html .= '<div class="clearfix userbox">';
									$userdata = $this->FindFriend_Model->userdata($fr->friend_two);
									if(!empty($userdata)){
										if($userdata[0]->picture_url != '' && $userdata[0]->oauth_provider == 'site_login'){
											$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$userdata[0]->picture_url.'"></div></a>';
										}elseif($userdata[0]->picture_url != '' && $userdata[0]->oauth_provider != 'site_login'){
											$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.$userdata[0]->picture_url.'"></div></a>';
										}else{
											$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'assets/front/images/user.png"></div></a>';
										}
										
										$html .= '<div class="clearfix user-detail"><div class="response-container"><div class="response-bttn"><div class="responsesimple"><div class="response-buttons"><button type="button" value="" class="confirm friend_request_send" style="opacity:0.5;cursor:default;" disabled="disabled"><i class="fa fa-user-plus _rqst_sent"></i><span class="_rqst">Friend Request Send</span></button><button type="button" value="'.$fr->id.'" class="dlt cancel_request">Cancel Request</button></div></div></div></div><div class="nme"><div class="usr_name"><a href="#">'.$userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></div></div></div></div>';
									}
								}	
							}
						}
					}
				}
			}
			
			$html .= '</div></div></div></div></div>';
			
			$userResult = $this->Dashboard_model->getUserName($userData['userId']);
			$data['logUserName'] = $userResult[0]->username;
			$data['UserFullName'] = $userResult[0]->firstname;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $userResult[0]->picture_url;
			}
			
			$data['htmldata'] = $html;
			$data['userData'] = $userData;
			$data['userLogout'] = $this->session->userdata('userLogout');
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('sentrequest');
			$this->load->view('footer');
		}else{
			redirect('./login');	
		}
    }
	
	public function cancel_request(){
		$result = $this->FindFriend_Model->confirm_request($_REQUEST['id']);
    }
}
