<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class SinglePost extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('SinglePost_model');
		$this->load->model('UserTimeline_model');
		$this->load->model('Dashboard_model');
		$this->load->model('Home_model');
		$this->load->model('ajaxss');
    }
	
	public function index($id){
		$usertimeline = array();
		$datarows = array();
		$data['frequest'] = '';
		$data['messages'] = '';
		
		$post = $this->Dashboard_model->getUserPosts($id);
					
		if(!empty($post['img'])) {
			$postimg = $this->Dashboard_model->getPostImage($post['img']);
			$post['postimg'] = $postimg;
		}
		
		$userinfo = $this->Dashboard_model->getUserInformation($post['user_id']);
		$userid = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$userid = $userData['userId'];
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
					
		if($userid)
			$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $id);
		else
			$upvoted = 0;
		
		$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $id, 0, 'post'), 'upvoted' => $upvoted, 'data' => $this->Dashboard_model->getTimelineText($post['user_id'], 'post', $id));
			
		$this->load->view('header_singlepost',$data);
		$this->load->view('left_sidebar');
		$this->load->view('singlepost');
		$this->load->view('right_sidebar');
		$this->load->view('footer');
    }
}
