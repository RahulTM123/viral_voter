<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Compition extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Home_model');
		$this->load->model('Compition_model');
		$this->load->model('UserTimeline_model');
    }
	
	public function index($id){
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$result = $this->Compition_model->getSingleCompition($id,$userData['userId']);
			$data['htmlData'] = $result;
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['userLogout'] = $this->session->userdata('userLogout');
			$data['userData'] = $this->session->userdata('userData');
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlecompition');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}elseif($this->session->userdata('adminData')){
			$result = $this->Compition_model->getSingleCompitionForAdmin($id);
			$data['htmlData'] = $result;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlecompition');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}else{
			$result = $this->Compition_model->getSingleCompitionBeforeLogin($id);
			$data['peoples'] = $this->Home_model->getPeopleKnow();
			$data['htmlData'] = $result;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlecompition');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
    }
	
	public function pollVoting(){
		$return = $this->UserTimeline_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollVoteOpt'],$_REQUEST['pUserId']);
		$url = $_SERVER['HTTP_REFERER'];
		redirect($url);
	}
}
