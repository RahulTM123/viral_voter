<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfile extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserTimeline_model');
		$this->load->model('Dashboard_model');
		$this->load->model('Home_model');
		$this->load->model('ajaxss');
    }
	
	public function index($username){
		$start = 0;
		$limit = 10;
		$data = array();
		$profileData = '';
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		$getCurrentUserId = $this->UserTimeline_model->getCurrentUserId($username);
			
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		
		$blocked = '';
		$data['ffstatus'] = '';
		$data['frequest'] = '';
		$data['messages'] = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$blocked = $this->Dashboard_model->getBlockedUser($getCurrentUserId,$userData['userId']);
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($getCurrentUserId);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
			
			if($userData['userId'] != $getCurrentUserId)
				$data['ffstatus'] = $this->UserTimeline_model->friendfollowStatus($userData['userId'], $getCurrentUserId);
		}
		else {
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($getCurrentUserId);
		}
		$timeline = $this->Dashboard_model->getUserTimeline($getCurrentUserId, $start, $limit);
		$data['result'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($getCurrentUserId);
		$data['shortbio'] = $this->Dashboard_model->getShortUserinfo($getCurrentUserId);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('header',$data);
		if($blocked) {
		$this->load->view('left_sidebar');
		$this->load->view('content_unavailable');
		}
		else {
		$this->load->view('left_sidebar_profile');
		$this->load->view('usertimeline');
		}
		$this->load->view('footer');
    }
	
	public function userById($id){
		$start = 0;
		$limit = 10;
		$data = array();
		$profileData = '';
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		
		$data['ffstatus'] = '';
		$data['frequest'] = '';
		$data['messages'] = '';
		$blocked = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$blocked = $this->Dashboard_model->getBlockedUser($id,$userData['userId']);
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($id);
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
			
			if($userData['userId'] != $id)
				$data['ffstatus'] = $this->UserTimeline_model->friendfollowStatus($userData['userId'], $id);
		}
		else {
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($id);
		}
		$timeline = $this->Dashboard_model->getUserTimeline($id, $start, $limit);
		$data['result'] = $timeline;
		$data['shortbio'] = $this->Dashboard_model->getShortUserinfo($id);
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		
		/*if($this->session->userdata('userData')){
			$html = '';
			$userData = $this->session->userdata('userData');
			$rowData = '';
			$value = $this->UserTimeline_model->getAllLogs($id);
			$result = $this->UserTimeline_model->getCatList();
			$totalFriend = $this->UserTimeline_model->totalFriend($id);
			if(!empty($result)){
				$i = 1;
				foreach($result as $r){
					$html .= '<option value="'.$r->id.'">'.$r->cat_name.'</option>';
				}
				$data['htmldata'] = $html;
			}
			if(!empty($value)){
				foreach($value as $v){
					$rowData .= $this->UserTimeline_model->getRowsData($v->created,$v->log_title,$v->log_id,$id,$userData['userId']);
				}
			}
			$data['htmlData'] = $rowData;
			$data['totalFriend'] = $totalFriend;
			$data['userData'] = $this->session->userdata('userData');
			
			$data['friend_id'] = $id;
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			$userResult = $this->UserTimeline_model->getUserName($id);
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$data['userLogout'] = $this->session->userdata('userLogout');
			
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['data'] = $this->Dashboard_model->getUserTimeline($userData['userId']);
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('usertimeline');
			$this->load->view('footer');
		}elseif($this->session->userdata('adminData')){
			$rowData = '';
			$value = $this->UserTimeline_model->getAllLogs($id);
			$totalFriend = $this->UserTimeline_model->totalFriend($id);
			if(!empty($value)){
				foreach($value as $v){
					$rowData .= $this->UserTimeline_model->getRowsDataShowAdmin($v->created,$v->log_title,$v->log_id,$id);
				}
			}
			
			
			$data['adminData'] = $this->session->userdata('adminData');
			$data['htmlData'] = $rowData;
			$data['totalFriend'] = $totalFriend;
			$data['friend_id'] = $id;
			$userResult = $this->UserTimeline_model->getUserName($id);
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('usertimeline');
			$this->load->view('footer');	
		}else{
			$rowData = '';
			$value = $this->UserTimeline_model->getAllLogs($id);
			$result = $this->UserTimeline_model->getCatList();
			$totalFriend = $this->UserTimeline_model->totalFriend($id);
			if(!empty($value)){
				foreach($value as $v){
					$rowData .= $this->UserTimeline_model->getRowsDataBeforeLogin($v->created,$v->log_title,$v->log_id,$id);
				}
			}
			$data['data'] = $this->Dashboard_model->getUserTimeline($id);
			
			
			
			$data['htmlData'] = $rowData;
			$data['totalFriend'] = $totalFriend;
			$data['friend_id'] = $id;
			$userResult = $this->UserTimeline_model->getUserName($id);
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$data['userData'] = '';
			
			$data['data'] = $this->UserTimeline_model->getUserTimeline($id);
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('usertimeline');
			$this->load->view('footer');		
		}*/
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('header',$data);
		if($blocked) {
		$this->load->view('left_sidebar');
		$this->load->view('content_unavailable');
		}
		else {
		$this->load->view('left_sidebar_profile');
		$this->load->view('usertimeline');
		}
		$this->load->view('footer');
    }
	
	public function userFriends($id){
		$start = 0;
		$limit = 10;
		$data = array();
		$profileData = '';
		$data['ffstatus'] = '';
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['messagess'] = '';
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$profileData = (array)$this->Dashboard_model->getUserInformation($id);
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriends($id,$userData['userId']);
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
			
			if($userData['userId'] != $id)
				$data['ffstatus'] = $this->UserTimeline_model->friendfollowStatus($userData['userId'], $id);
		}
		else {
			$profileData = (array)$this->Dashboard_model->getUserInformation($id);
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriends($id,$profileData['userid']);
		}
		$timeline = $this->Dashboard_model->getUserTimeline($id, $start, $limit);
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['shortbio'] = $this->Dashboard_model->getShortUserinfo($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$data['profileData'] = $profileData;
		$data['htmlData'] = $getUserAllFriends;
		
		$this->load->view('header',$data);
		$this->load->view('left_sidebar_profile');
		$this->load->view('userfriends');
		$this->load->view('footer');
		
		/*if($this->session->userdata('userData')){
			if(!preg_match('#[^0-9]#',$id)){
				$getCurrentUserId = $id;
			}else{
				$getCurrentUserId = $this->UserTimeline_model->getCurrentUserId($id);
			}
			$html = '';
			$userData = $this->session->userdata('userData');
			$rowData = '';
			$totalFriend = $this->UserTimeline_model->totalFriend($getCurrentUserId);
			$data['userData'] = $this->session->userdata('userData');
			
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriends($getCurrentUserId,$userData['userId']);
			
			$data['htmlData'] = $getUserAllFriends;
			$data['totalFriend'] = $totalFriend;
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['friend_id'] = $getCurrentUserId;
			$userResult = $this->UserTimeline_model->getUserName($getCurrentUserId);
			$data['userName'] = $userResult[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$data['userLogout'] = $this->session->userdata('userLogout');
			
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($getCurrentUserId);
			$data['data'] = $this->Dashboard_model->getUserTimeline($getCurrentUserId);
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('userfriends');
			$this->load->view('footer');
		}elseif($this->session->userdata('adminData')){
			if(!preg_match('#[^0-9]#',$id)){
				$getCurrentUserId = $id;
			}else{
				$getCurrentUserId = $this->UserTimeline_model->getCurrentUserId($id);
			}
			$html = '';
			$rowData = '';
			$totalFriend = $this->UserTimeline_model->totalFriend($getCurrentUserId);
			
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriendBeforeLogin($getCurrentUserId);
			$data['adminData'] = $this->session->userdata('adminData');
			$data['htmlData'] = $getUserAllFriends;
			$data['totalFriend'] = $totalFriend;
			$data['friend_id'] = $getCurrentUserId;
			$userResult = $this->UserTimeline_model->getUserName($getCurrentUserId);
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('userfriends');
			$this->load->view('footer');
		}else{
			if(!preg_match('#[^0-9]#',$id)){
				$getCurrentUserId = $id;
			}else{
				$getCurrentUserId = $this->UserTimeline_model->getCurrentUserId($id);
			}
			$html = '';
			$rowData = '';
			$totalFriend = $this->UserTimeline_model->totalFriend($getCurrentUserId);
			
			$getUserAllFriends = $this->UserTimeline_model->getUserAllFriendBeforeLogin($getCurrentUserId);
			
			$data['htmlData'] = $getUserAllFriends;
			$data['totalFriend'] = $totalFriend;
			$data['friend_id'] = $getCurrentUserId;
			$userResult = $this->UserTimeline_model->getUserName($getCurrentUserId);
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($getCurrentUserId);
			$data['data'] = $this->Dashboard_model->getUserTimeline($getCurrentUserId);
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('userfriends');
			$this->load->view('footer');
		}*/
    }
	
	
	public function aboutUser($id){
		$start = 0;
		$limit = 10;
		$data = array();
		$profileData = '';
		$data['ffstatus'] = '';
		$data['frequest'] = '';
		$data['messages'] = '';
				
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->Dashboard_model->getFullUserInformation($id);
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
			
			if($userData['userId'] != $id)
				$data['ffstatus'] = $this->UserTimeline_model->friendfollowStatus($userData['userId'], $id);
		}
		else {
			$data['profileData'] = (array)$this->Dashboard_model->getFullUserInformation($id);
		}
		
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['shortbio'] = $this->Dashboard_model->getShortUserinfo($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		$interest = (array)$this->Dashboard_model->getUserInterest($id);
		if(!empty($interest))
			$data['interest'] = implode(',', $interest);
		else
			$data['interest'] = '';
		$this->load->view('header',$data);
		$this->load->view('left_sidebar_profile');
		$this->load->view('about');
		$this->load->view('footer');
    }
	
	
	public function polls($id){
		$start = 0;
		$limit = 10;
		$data = array();
		$profileData = '';
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($id);
		}
		else {
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($id);
		}
		$timeline = $this->Dashboard_model->getUserTimeline($id, $start, $limit);
		$data['data'] = $timeline;
		$totalFriend = $this->UserTimeline_model->totalFriend($id);
		$data['totalFriend'] = $totalFriend->total;
		$data['friendid'] = $totalFriend->friendid;
		
		/*if($this->session->userdata('userData')){
			$html = '';
			$userData = $this->session->userdata('userData');
			$rowData = '';
			$totalFriend = $this->UserTimeline_model->totalFriend($userData['userId']);
			$data['userData'] = $this->session->userdata('userData');
			
			$getUserPolls = $this->UserTimeline_model->getUserPolls($userData['userId']);
			
			$data['htmlData'] = $getUserPolls;
			$data['totalFriend'] = $totalFriend;
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			$data['friend_id'] = $userData['userId'];
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['coverPicture'] = $loggedInUser[0]->cover_pic_url;
			$data['fullName'] = $loggedInUser[0]->firstname.' '.$loggedInUser[0]->lastname;
			$data['userLogout'] = $this->session->userdata('userLogout');
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('userpolls');
			$this->load->view('footer');
		}elseif($this->session->userdata('adminData')){
			$html = '';
			$var = explode('/beta/user/',$_SERVER['REQUEST_URI']);
			$var1 = explode('/polls',$var[1]);
			$getUserId = $this->UserTimeline_model->getUserId($var1[0]);
			$data['adminData'] = $this->session->userdata('adminData');
			$totalFriend = $this->UserTimeline_model->totalFriend($getUserId);
			
			$getUserPolls = $this->UserTimeline_model->getUserPolls($getUserId);
			
			$data['htmlData'] = $getUserPolls;
			$data['totalFriend'] = $totalFriend;
			$userResult = $this->UserTimeline_model->getUserName($getUserId);
			$data['userName'] = $userResult[0]->username;
			$data['UserFullName'] = $userResult[0]->firstname;
			$data['friend_id'] = $getUserId;
			$data['userName'] = $userResult[0]->username;
			$UserInformation = array(
				'overview' => $userResult[0]->overview
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['profilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['profilePicture'] = $userResult[0]->picture_url;
			}
			$data['coverPicture'] = $userResult[0]->cover_pic_url;
			$data['fullName'] = $userResult[0]->firstname.' '.$userResult[0]->lastname;
			$data['userLogout'] = $this->session->userdata('userLogout');
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('userpolls');
			$this->load->view('footer');
		}else{
			redirect('./login');	
		}*/
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('usertimeline');
		$this->load->view('footer');
    }
	
	
	public function addNewPoll(){
		if(isset($_REQUEST['pubPoll'])){
			$polloption = array();
			$userid = $_REQUEST['userid'];
			$qimag = $qus = $qvideolink = '';
			
			if(!empty($_REQUEST['quesI'])){
				$qimag = $_REQUEST['quesI'];
			}
			
			if(!empty($_REQUEST['videolink'])){
				$qvideolink = $_REQUEST['videolink'];
			}
			
			if(!empty($_REQUEST['ques'])){
				$qus = $_REQUEST['ques'];
			}
			
			if(!empty($_REQUEST['ans1I']) && !empty($_REQUEST['ans1'])){
				array_push($polloption,serialize(array($_REQUEST['ans1'],$_REQUEST['ans1I'])));
			}else if(!empty($_REQUEST['ans1'])){
				array_push($polloption,serialize(array($_REQUEST['ans1'])));
			}else if(!empty($_REQUEST['ans1I'])){
				array_push($polloption,serialize(array($_REQUEST['ans1I'])));
			}
			
			if(!empty($_REQUEST['ans2I']) && !empty($_REQUEST['ans2'])){
				array_push($polloption,serialize(array($_REQUEST['ans2'],$_REQUEST['ans2I'])));
			}else if(!empty($_REQUEST['ans2'])){
				array_push($polloption,serialize(array($_REQUEST['ans2'])));
			}else if(!empty($_REQUEST['ans2I'])){
				array_push($polloption,serialize(array($_REQUEST['ans2I'])));
			}
			
			if(!empty($_REQUEST['ans3I']) && !empty($_REQUEST['ans3'])){
				array_push($polloption,serialize(array($_REQUEST['ans3'],$_REQUEST['ans3I'])));
			}else if(!empty($_REQUEST['ans3'])){
				array_push($polloption,serialize(array($_REQUEST['ans3'])));
			}else if(!empty($_REQUEST['ans3I'])){
				array_push($polloption,serialize(array($_REQUEST['ans3I'])));
			}
			
			if(!empty($_REQUEST['ans4I']) && !empty($_REQUEST['ans4'])){
				array_push($polloption,serialize(array($_REQUEST['ans4'],$_REQUEST['ans4I'])));
			}else if(!empty($_REQUEST['ans4'])){
				array_push($polloption,serialize(array($_REQUEST['ans4'])));
			}else if(!empty($_REQUEST['ans4I'])){
				array_push($polloption,serialize(array($_REQUEST['ans4I'])));
			}
			
			if(isset($_REQUEST['frndids'])){
				$frndids = $_REQUEST['frndids'];
			}else{
				$frndids = '';
			}
			$cat = $_REQUEST['poll_category'];
			
			$result = $this->UserTimeline_model->addNewPoll($userid,$qvideolink,$qus,$qimag,$polloption,$cat,$frndids);
			$url = $_SERVER['HTTP_REFERER'];
			if(!empty($result)){
				redirect($url);
			}else{
				redirect($url);
			}
			
		}
	}
	
	
	public function pollVoting(){
		$friends = $this->Dashboard_model->getUserFrndIds($_REQUEST['puserid']);
		$frn = explode(',', $friends);
		$alluser = array();
		
		foreach($frn as $frnds)
			$alluser[$frnds] = $frnds;
		
		unset($alluser[$userid]);
		$alluser = implode(',', $alluser);
			
		$return = $this->Dashboard_model->addPollVoting($_REQUEST['pollid'],$_REQUEST['pollopt'],$_REQUEST['puserid'],$_SERVER['REMOTE_ADDR'],$alluser);
		$url = $_SERVER['HTTP_REFERER'];
		if(!empty($return)){
			redirect($url);
		}else{
			redirect($url);
		}
	}
	
	
	

	
	public function mystats($id){
		$data['frequest'] = '';
		$data['messages'] = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			if($userData['userId'] == $id) {
				$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
				$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($userData['userId']);
				$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
				
				$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
				$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
				$data['messagess'] = $this->ajaxss->notificationActive_admin($userData['userId']);
				$data['result'] = $this->UserTimeline_model->quickStats($id);
				$data['complete'] = $this->UserTimeline_model->completeStats($id);
				
				$total['poll'] = $data['result']['poll']['total'];
				$total['post'] = $data['result']['post']['total'];
				$total['comments'] = $data['result']['comments']['total'];
				$total['upvote'] = $data['result']['poll']['tupvote']+$data['result']['post']['tupvote'];
				$total['sharing'] = $data['result']['poll']['tsharing']+$data['result']['post']['tsharing'];
				
				$data['total'] = $total;
				
				$this->load->view('header',$data);
				$this->load->view('left_sidebar');
				$this->load->view('mystats');
				$this->load->view('footer');
			}
		}
	}


	public function test2(){

			$sql = "SELECT * FROM `vv_users` ";
			$query = $this->db->query($sql);
			
			echo '<pre>';
			print_r($query->result_array());
			echo '</pre>';


	}
	
}
