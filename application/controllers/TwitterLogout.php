<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class TwitterLogout extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('facebook');
    }
	
	public function index()
    {
		$this->session->unset_userdata('token');
		$this->session->unset_userdata('token_secret');
		$this->session->unset_userdata('status');
		$this->session->unset_userdata('userData');
                $this->session->unset_userdata('tlogoutUrl');
		$this->session->unset_userdata('twitterlogin');
        $this->session->sess_destroy();
		redirect('/login');
        
    }
}
