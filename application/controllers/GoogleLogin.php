<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class GoogleLogin extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
                $this->load->library('google');
		$this->load->model('user');
    }
	
	public function index()
    {
		
		
		if(isset($_REQUEST['code'])){
			//authenticate user
			$this->google->getAuthenticate();
			//get user info from google
			$gpInfo = $this->google->getUserInfo();
			
            //preparing data for database insertion
			$userData['oauth_provider'] = 'google';
			$userData['oauth_uid'] 		= $gpInfo['id'];
            $userData['firstname'] 	= $gpInfo['given_name'];
            $userData['lastname'] 		= $gpInfo['family_name'];
            $userData['email'] 			= $gpInfo['email'];
			$userData['gender'] 		= !empty($gpInfo['gender'])?$gpInfo['gender']:'';
			$userData['locale'] 		= !empty($gpInfo['locale'])?$gpInfo['locale']:'';
            $userData['profile_url'] 	= !empty($gpInfo['link'])?$gpInfo['link']:'';
            $userData['picture_url'] 	= !empty($gpInfo['picture'])?$gpInfo['picture']:'';
			
			//insert or update user data to the database
            $userID = $this->user->checkUserGoogle($userData);
			
			//store status & user info in session
			$userData = $userID;
			$this->session->set_userdata('userData', $userData);
			$this->session->set_userdata('loggedIn', true);
			if(!empty($userID)){
				//$data['userData'] = $userData;
				//$data['tlogoutUrl'] = base_url().'GoogleLogout';
                $this->session->set_userdata('tlogoutUrl', base_url().'GoogleLogout');
				if(isset($_SESSION['referer'])){
					$refer = $_SESSION['referer'];
					unset($_SESSION['referer']);
					redirect($refer,'refresh');
				}
				else {
					redirect(base_url(),'refresh');
				}
			}
		} elseif($this->session->userdata('loggedIn') == true){
			$data['userData'] = $this->session->userdata('userData');
			
			if(!empty($userID)){
				$this->session->set_userdata('tlogoutUrl', base_url().'GoogleLogout');
				if(isset($_SESSION['referer'])){
					$refer = $_SESSION['referer'];
					unset($_SESSION['referer']);
					redirect($refer,'refresh');
				}
				else {
					redirect(base_url(),'refresh');
				}
			}
		}else{
			$data['loginURL'] = $this->google->loginURL();
			return $data;
		}
    }
	
	
}
