<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class ViewParticipation extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Survey_model');
		$this->load->model('Dashboard_model');
    }
	
	public function index($id){
		if(!$this->session->userdata('userData')){
			$rowData = $this->Survey_model->getSurveyParticipation($id);
			$data['htmlData'] = $rowData;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('viewSurveyParticipation');
			$this->load->view('footer');
		}else{
			$userData = $this->session->userdata('userData');
			$rowData = $this->Survey_model->getSurveyParticipation($id);	
			$data['htmlData'] = $rowData;
			//$data['userData'] = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$userResult = $this->Dashboard_model->getUserName($userData['userId']);
			$data['logUserName'] = $userResult[0]->username;
			$data['UserFullName'] = $userResult[0]->firstname;
			if(!empty($userResult[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$userResult[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $userResult[0]->picture_url;
			}
			
			if($this->session->userdata('logoutUrl') != ''){
				$this->session->set_userdata('userLogout',$this->session->userdata('logoutUrl'));
				$data['userLogout'] = $this->session->userdata('userLogout');
			}elseif($this->session->userdata('tlogoutUrl') != ''){
				$this->session->set_userdata('userLogout',$this->session->userdata('tlogoutUrl'));
				$data['userLogout'] = $this->session->userdata('userLogout');
			}else{
				$this->session->set_userdata('userLogout',base_url().'UserLogout');
				$data['userLogout'] = $this->session->userdata('userLogout');
			}
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('viewSurveyParticipation');
			$this->load->view('footer');
		}
    }
}
