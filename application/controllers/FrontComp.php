<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontComp extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Home_model');
		$this->load->model('Competitions_model');
		$this->load->model('Dashboard_model');
		$this->load->model('countries');
		$this->load->model('states');
		$this->load->model('cities');
		$this->load->model('Category_model');
    }
	
	public function index(){
		 if($this->session->userdata('userData')){
			$data['countries'] = $this->countries->getCountries();
			$data['peoples'] = $this->Home_model->getPeopleKnow();
			$data['categories'] = $this->Category_model->getAllcategories();

			if(isset($_POST) && !empty($_POST))
			{	
				$strt_date = $_POST['strt_date'];
				$end_date = $_POST['end_date'];
				$category = $_POST['category'];
				$rowData = $this->Competitions_model->getCompBForAdmin($strt_date,$end_date,$category);
				if($this->session->userdata('logoutUrl') != ''){
				$this->session->set_userdata('userLogout',$this->session->userdata('logoutUrl'));
				$data['userLogout'] = $this->session->userdata('userLogout');
			}elseif($this->session->userdata('tlogoutUrl') != ''){
				$this->session->set_userdata('userLogout',$this->session->userdata('tlogoutUrl'));
				$data['userLogout'] = $this->session->userdata('userLogout');
			}else{
				$this->session->set_userdata('userLogout',base_url().'UserLogout');
				$data['userLogout'] = $this->session->userdata('userLogout');
			}
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}

				
			}else
			{
				$strt_date = '';
				$end_date = '';
				$category = '';
				$rowData = $this->Competitions_model->getCompBeforeLogin($strt_date,$end_date,$category);
				
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}
			}

			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('competitions');
			//$this->load->view('right_sidebar');
			$this->load->view('footer');
			
			
			
			
		}
		elseif($this->session->userdata('adminData')){
			$data['countries'] = $this->countries->getCountries();
			$data['peoples'] = $this->Home_model->getPeopleKnow();
			$data['categories'] = $this->Category_model->getAllcategories();

			if(isset($_POST) && !empty($_POST))
			{	
				$strt_date = $_POST['strt_date'];
				$end_date = $_POST['end_date'];
				$category = $_POST['category'];
				$rowData = $this->Competitions_model->getCompBForAdmin($strt_date,$end_date,$category);
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}

				
			}else
			{
				$strt_date = '';
				$end_date = '';
				$category = '';
				$rowData = $this->Competitions_model->getCompBeforeLogin($strt_date,$end_date,$category);
				
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}
			}

			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('competitions');
			//$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
		else{

			$data['countries'] = $this->countries->getCountries();
			$data['peoples'] = $this->Home_model->getPeopleKnow();
			$data['categories'] = $this->Category_model->getAllcategories();

			if(isset($_POST) && !empty($_POST))
			{	
				$strt_date = $_POST['strt_date'];
				$end_date = $_POST['end_date'];
				$category = $_POST['category'];
				$rowData = $this->Competitions_model->getCompBeforeLogin($strt_date,$end_date,$category);
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}

				
			}else
			{
				$strt_date = '';
				$end_date = '';
				$category = '';
				$rowData = $this->Competitions_model->getCompBeforeLogin($strt_date,$end_date,$category);
				
				if(!empty($rowData))
				{
					$data['htmlData'] = $rowData;
				}
				else
				{
					$data['htmlData'] = "<p>No Record Found.</p>";
				}
			}

			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('competitions');
			//$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
    }

public function AllstateByCountry() {
	$c_id = $_POST['c_id'];
	$states = $this->states->AllstateByCountry($c_id);
	echo json_encode($states); 
	//return $states;
	exit;
}
public function getcityBystate() {
	$c_id = $_POST['c_id'];
	$cities = $this->cities->AllcityBystate($c_id);
	echo json_encode($cities); 
	//return $states;
	exit;
}


  
}
