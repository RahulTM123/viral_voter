<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Commentbox extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Dashboard_model');
    }
	
	public function index($id){
		$usertimeline = array();
		if($this->session->userdata('userData')){
			if(isset($_POST)) {
				$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			}
			$userData = $this->session->userdata('userData');
			
			$data['polldata'] = $this->SinglePoll_model->getSinglePoll($id);
			$data['comments'] = $this->SinglePoll_model->getSinglePollComments($id);
			
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$data['messages'] = $message->msg;
			
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlepoll');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}else{
			redirect('./login');
		}
    }
}
