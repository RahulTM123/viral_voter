<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminFrontPoll extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('AdminFrontPoll_model');
		$this->load->model('Dashboard_model');
		$this->load->model('cities');
    }
	
	public function index(){
		$start = 0;
		$limit = 10;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$sortby = (isset($_POST['sortby']))?$_POST['sortby']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
			
		if(isset($_POST['search'])){
			$userData = $this->session->userdata('userData');
			$usercat=$this->AdminFrontPoll_model->userDetail($userData['userId']);
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result = $this->AdminFrontPoll_model->getAdminPollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category,$usercat);
		}
		else{
			$userData = $this->session->userdata('userData');
			$usercat=$this->AdminFrontPoll_model->userDetail($userData['userId']);
			//var_dump($usercat);die();
			//var_dump($userData);die();
			$result = $this->AdminFrontPoll_model->getActivePolls($start, $limit,$userData['userId'],$usercat);
		}
		
		$admin = $this->AdminFrontPoll_model->getAdminDetail();
		$category = $this->AdminFrontPoll_model->getCategories();
		
		$data['result'] = $result;
		$data['admin'] = $admin;
		$data['category'] = $category;
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			//$data['result'] = (array)$this->Dashboard_model->getPollsInfo($userData['userId']);
			//var_dump($data['adminpolls']);die();
		}
			
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('adminfrontpoll');
		$this->load->view('footer');
    }
	
	public function archive(){
		$start = 0;
		$limit = 10;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$sortby = (isset($_POST['sortby']))?$_POST['sortby']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
			
		if(isset($_POST['search'])){
			$userData = $this->session->userdata('userData');
			$usercat=$this->AdminFrontPoll_model->userDetail($userData['userId']);
			if($country_id > 0)
				$data['states'] = $this->cities->allStates($country_id);
			
			if($state_id > 0)
				$data['cities'] = $this->cities->allCitiesByState($state_id);
			
			$result = $this->AdminFrontPoll_model->getArchivePollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category,$usercat);
		}
		else{
			$userData = $this->session->userdata('userData');
			$usercat=$this->AdminFrontPoll_model->userDetail($userData['userId']);
			$result = $this->AdminFrontPoll_model->getArchivePolls($start, $limit,$userData['userId'],$usercat);
		}
		
		$admin = $this->AdminFrontPoll_model->getAdminDetail();
		$category = $this->AdminFrontPoll_model->getCategories();
		
		$data['result'] = $result;
		$data['admin'] = $admin;
		$data['category'] = $category;
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
		}
			
		$this->load->view('header',$data);
		$this->load->view('left_sidebar');
		$this->load->view('adminarchivepoll');
		$this->load->view('footer');
    }
}
