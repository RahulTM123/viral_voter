<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajaxview extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('ajaxss');
        $this->load->library('session');
		$this->load->model('Home_model');
		$this->load->model('Dashboard_model');
		$this->load->helper('url');
    }
	
	public function ajax_cities() {
        $cities = $this->ajaxss->getAjaxCities($_POST['country'], $_POST['keyword']);
		$result = '<ul>';
		foreach($cities as $city) {
			$result .= '<li id="'.$city->id.'" title="'.$city->state_id.'">'.$city->city_name.', '.$city->city_state.'</li>';
		}
		$result .= '</ul>';
		$data['data'] = $result;
        $this->load->view('ajaxss',$data);
	}
	
	public function getUserMSG() {
		$userData = $this->session->userdata('userData');
        $usermessages = $this->ajaxss->getUserMessage($userData['userId']);
        $usermessagess = $this->ajaxss->getAdminMessage($userData['userId']);
		$result = '';
		$result1 = '';
		$refurl = '';
		$prourl = '';

		foreach($usermessages as $um) {
			if($um->username)
				$prourl = base_url().''.$um->username;
			else
				$prourl = base_url().'user/id/'.$um->messageby;
			
			$read = ($um->readstatus)?'':'unread';
			$date = $this->Dashboard_model->datedifference($um->create_date, date('Y-m-d H:i:s'));
			$pic = ($um->profile_pic_url)?base_url().'uploads/'.$um->profile_pic_url:base_url().'assets/front/images/user-grey.jpg';
			$result .= '<tr><td class="'.$read.'" width="46"><a href="#"><img src="'.$pic.'" width="46" height="46" style="margin: 0px 0px;" /></a></td><td style="padding-left: 0px;"><a href="'.$prourl.'"><strong>'.$um->firstname.' '.$um->lastname.'</strong></a> <span>'.$um->textdesc.'</span><br /><small>'.$date.'</small></td></tr>';
		}



		foreach($usermessagess as $ums) {
	
			
			$read = ($um->readstat)?'':'unread';
			$date = $this->Dashboard_model->datedifference($ums->created, date('Y-m-d H:i:s'));
			$pic = ($um->profile_pic_url)?base_url().'uploads/'.$ums->profile_pic_url:base_url().'assets/front/images/user-grey.jpg';
			$pic2 = base_url().'/assets/front/contest_image/'.$ums->id.'/'.$ums->contest_image;




			$result1 .= '<tr><td class="'.$readd.'" width="46"><a href="contest"><img src="'.$pic2.'" width="46" height="46" style="margin: 0px 0px;" /></a></td><td style="padding-left: 0px;"><span><b><span style = "color:red"> New Contest</span><br>Admin Created a contest:</a></b></span> <span><a href ='.base_url().'contest> '.$ums->question.'</a></span><br /><small>'.$date.'</small><h2>'.$ums->firstname.'</h2></td></tr>';
		}



		$result .= '';
		echo $result."";

		$result1 .= '';
		echo $result1;

	
	}
	
	public function submitUpvote() {
		$total = array();
		if($this->session->userdata('userData')){
			$submit = (array)$this->ajaxss->getUpvote($_POST['ref'], $_POST['refid'], $_POST['userid']);
			if($submit['total'] > 0) {
				$total['message'] = 'Already voted';
				echo json_encode($total);
			}
			else {
				$success = $this->ajaxss->submitUpvoting($_POST['ref'], $_POST['refid'], $_POST['userid']);
				$total = (array)$this->ajaxss->getData($_POST['ref'], $_POST['refid']);
				$total['message'] = 'success';
				echo json_encode($total);
			}
		}
		else {
			redirect('./login');
		}
	}
	
	public function commentBox() {
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->ajaxss->getData('user',$userData['userId']);
			$data['ref'] = $_POST['ref'];
			$data['refid'] = $_POST['refid'];
			$data['defaults'] = $_POST['defaults'];
			$this->load->view('commentbox', $data);
		}
		else {
			redirect('./login');
		}
	}
	
	public function getEngagedUser() {
		$pollid = $_POST['pid'];
		$data = $this->ajaxss->getEngagedUserlistBytabs($pollid);
		//print_r($data);
		$result = '<div class="custab"><div class="custablink">';
		foreach($data['options'] as $dt) {
			$words = explode(" ", $dt['title']);
			$title = implode(" ", array_splice($words, 0, 3));
			$class = ($dt['option_id'] == 1)?'class="active"':'';
			$result .= '<a href="#op'.$dt['option_id'].'" '.$class.'>'.$dt['option_id'].' - '.$title.'</a>';
		}
		$result .= '</div><div class="custabcontent">';
		if($data['userlist']) {
			foreach($data['userlist'] as $key => $ol) {
				$class = ($key == 1)?'style="display:block;"':'';
				$result .= '<div id="op'.$key.'" '.$class.'><ul>';
				if(count($ol)) {
					foreach($ol as $ul) {
						$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
						if($ul['username'])
							$prourl = base_url().''.$ul['username'];
						else
							$prourl = base_url().'user/id/'.$ul['id'];
						
						$verified = ($ul['verified'])?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
						$result .= '<li><a href="'.$prourl.'"><img src="'.$pic.'" width="46" height="46" aligh="left" class="profilethumb" /> '.$ul['firstname'].' '.$ul['lastname'].'</a>'.$verified.'';
						
						$isfriend = '';
						if($this->session->userdata('userData')){
							$userData = $this->session->userdata('userData');
							$isfriend = $this->ajaxss->isFriend($ul['id'], $userData['userId']);
							/*if($isfriend)
								$result .= '<button style="float: right;"><i class="fa fa-check" aria-hidden="true"></i> Friend</button>';
							else
								if($ul['id'] == $userData['userId'])
									$result .= '';
								else
									$result .= '<a href="#" style="float: right;" title="Add Friend"><button id="'.$ul['id'].'" rel="'.$userData['userId'].'">Add Friend</button></a>';*/
						}
						
						$result .= '</li>';
					}
				}
				else {
						$result .= '<li>No user engaged</li>';
				}
				$result .= '</ul></div>';
			}
		}
		else {
				$result .= '<li>No user engaged</li>';
		}
		$result .= '</div></div>';
		echo $result;
	}
	
	public function getVotedUser() {
		$type = $_POST['ty'];
		$typeid = $_POST['tid'];
		$userlist = $this->ajaxss->getVotedUserlist($type, $typeid);
		$result = '<ul>';
		if($userlist) {
			foreach($userlist as $ul) {
				$pic = ($ul->profile_pic_url)?base_url().'uploads/'.$ul->profile_pic_url:base_url().'assets/front/images/user-grey.jpg';
				if($ul->username)
					$prourl = base_url().''.$ul->username;
				else
					$prourl = base_url().'user/id/'.$ul->id;
				
				$verified = ($ul->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
				
				$result .= '<li><a href="'.$prourl.'" target="_parent"><img src="'.$pic.'" width="46" height="46" aligh="left" /> '.$ul->firstname.' '.$ul->lastname.'</a>'.$verified.'';
				
				$isfriend = '';
				if($this->session->userdata('userData')){
					$userData = $this->session->userdata('userData');
					$isfriend = $this->ajaxss->isFriend($ul->id, $userData['userId']);
					/*if($isfriend)
						$result .= '<button style="float: right;"><i class="fa fa-check" aria-hidden="true"></i> Friend</button>';
					else
						$result .= '<a href="#" style="float: right;" title="Add Friend"><button id="'.$ul->id.'" rel="'.$userData['userId'].'">Add Friend</button></a>';*/
				}
				
				$result .= '</li>';
			}
		}
		else {
				$result .= '<li>No upvote</li>';
		}
		$result .= '</ul>';
		echo $result;
	}
	
	public function getCommentedUser() {
		$type = $_POST['ty'];
		$typeid = $_POST['tid'];
		$userlist = $this->ajaxss->getCommentedUserlist($type, $typeid);
		$result = '<ul>';
		if($userlist) {
			foreach($userlist as $ul) {
				$pic = ($ul->profile_pic_url)?base_url().'uploads/'.$ul->profile_pic_url:base_url().'assets/front/images/user-grey.jpg';
				if($ul->username)
					$prourl = base_url().''.$ul->username;
				else
					$prourl = base_url().'user/id/'.$ul->id;
				
				$verified = ($ul->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
				
				$result .= '<li><a href="'.$prourl.'" target="_parent"><img src="'.$pic.'" width="46" height="46" aligh="left" /> '.$ul->firstname.' '.$ul->lastname.'</a>'.$verified.'';
				
				$isfriend = '';
				if($this->session->userdata('userData')){
					$userData = $this->session->userdata('userData');
					$isfriend = $this->ajaxss->isFriend($ul->id, $userData['userId']);
					/*if($isfriend)
						$result .= '<button style="float: right;"><i class="fa fa-check" aria-hidden="true"></i> Friend</button>';
					else
						$result .= '<a href="#" style="float: right;" title="Add Friend"><button id="'.$ul->id.'" rel="'.$userData['userId'].'">Add Friend</button></a>';*/
				}
				
				$result .= '</li>';
			}
		}
		else {
				$result .= '<li>No comment</li>';
		}
		$result .= '</ul>';
		echo $result;
	}
	
	public function getSharingUser() {
		$type = $_POST['ty'];
		$typeid = $_POST['tid'];
		$userlist = $this->ajaxss->getSharingUserlist($type, $typeid);
		$result = '<ul>';
		if($userlist) {
			foreach($userlist as $ul) {
				$pic = ($ul->profile_pic_url)?base_url().'uploads/'.$ul->profile_pic_url:base_url().'assets/front/images/user-grey.jpg';
				if($ul->username)
					$prourl = base_url().''.$ul->username;
				else
					$prourl = base_url().'user/id/'.$ul->id;
					
				if($ul->reftype=='polls') {
					$refurl = base_url().'poll/'.$ul->refid;
					$ref = 'Poll';
				}
				else {
					$refurl = base_url().'post/'.$ul->refid;
					$ref = 'Post';
				}
				$cdate = new DateTime($ul->shdate);
				$ndate = new DateTime();
				$cndate = '';
				if((int)date_diff($cdate,$ndate)->format("%d") > 0) {
				  $cndate = date_diff($cdate,$ndate)->format("%dd");
				}
				else {
				  if((int)date_diff($cdate,$ndate)->format("%h") > 0)
					  $cndate = date_diff($cdate,$ndate)->format("%hh");
				  else
					  $cndate = date_diff($cdate,$ndate)->format("%mm");
				}
				$verified = ($ul->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
				
				$result .= '<li><div class="tm_head"><div><a href="'.$prourl.'" target="_parent"><img src="'.$pic.'" width="46" height="46" /></div> <div>'.$ul->firstname.' '.$ul->lastname.'</a>'.$verified.' shared a <a href="'.$refurl.'">'.$ref.'</a> on '.ucfirst($ul->shareref).'<br /><small>'.$cndate.'</small></div>';
				
				$isfriend = '';
				if($this->session->userdata('userData')){
					$userData = $this->session->userdata('userData');
					$isfriend = $this->ajaxss->isFriend($ul->id, $userData['userId']);
					/*if($isfriend)
						$result .= '<button style="float: right;"><i class="fa fa-check" aria-hidden="true"></i> Friend</button>';
					else
						$result .= '<a href="#" style="float: right;" title="Add Friend"><button id="'.$ul->id.'" rel="'.$userData['userId'].'">Add Friend</button></a>';*/
				}
				
				$result .= '</li>';
			}
		}
		else {
				$result .= '<li>Not shared anywhere</li>';
		}
		$result .= '</ul>';
		echo $result;
	}
	
	public function subComment() {		
		$userid = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		$type = $_POST['type'];
		$typeid = $_POST['typeid'];
		$subcomments = $this->ajaxss->getSubComments($type, $typeid);
		$result = '';
		foreach($subcomments as $sc) {
			$cdate = new DateTime($sc->created);
			$ndate = new DateTime();
			$cndate = '';
			if((int)date_diff($cdate,$ndate)->format("%d") > 0) {
			  $cndate = date_diff($cdate,$ndate)->format("%dd");
			}
			else {
			  if((int)date_diff($cdate,$ndate)->format("%h") > 0)
				  $cndate = date_diff($cdate,$ndate)->format("%hh");
			  else
				  $cndate = date_diff($cdate,$ndate)->format("%mm");
			}
			
			$prourl = (!empty($sc->username))?(base_url().''.$sc->username):base_url().'user/id/'.$sc->userid;
			$picurl = (!empty($sc->profile_pic_url))?base_url("uploads/").$sc->profile_pic_url:base_url("assets/front/images/user-grey.jpg");
			$verified = ($sc->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
			$result .= '<div>
					<div><a href="'.$prourl.'" class="propic"><img src="'.$picurl.'" width="35" /></a></div>
					
					<div class="commentcomment">
						<div>
							<strong><a href="'.$prourl.'">'.$sc->firstname.' '.$sc->lastname.'</a></strong>'.$verified.' '.$sc->content.'
						</div>
						<div id="comment_'.$sc->id.'_'.$userid.'">
							<small><a href="#" class="prevent_click" onclick="dovote(\'comment\', \''.$sc->id.'\', \''.$userid.'\', \'#upvote_comment_'.$sc->id.'\');">Upvote</a></small> &nbsp;
							<small><a href="#" class="prevent_click" onclick="docomment(\'comment\', \''.$sc->id.'\', \''.$userid.'\', this);">Reply</a></small> &nbsp;
							<small>'.$cndate.'</small>';
							
			if($sc->total) {
				$result .= '&nbsp; <small><a href="#" class="subcomment"><input type="hidden" value="comment" rel="'.$sc->id.'" />';
				$result .= ($sc->total > 1)?$sc->total.' Comments':$sc->total.' Comment';
				$result .= '</a></small>';
			}
			$result .= '<div class="subcommentarea tm_comment"></div>';
			$result .= '</div>
				</div>';
		}
		$result .= '';
		echo $result;
	}
	
	public function postComments() {
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
			$comment = (isset($_POST['comment']))?$_POST['comment']:'';
			$tagid = explode('href="',$comment);
			$cid = (isset($_POST['cid']))?$_POST['cid']:'';
			$type = (isset($_POST['type']))?$_POST['type']:'';
			$tagged = (isset($_POST['tagged']))?$_POST['tagged']:'';
			$friendsonly = $this->ajaxss->checkblock($userid, $cid, $type);
			if($friendsonly['commenting'] && $friendsonly['friend']) {
				$results = $this->ajaxss->insertComments($userid, $comment, $cid, $type, $tagged);
				$sc = $this->ajaxss->getComment($userid, $results);
				$result = '';
				
				$cdate = new DateTime($sc->created);
				$ndate = new DateTime();
				$cndate = '';
				if((int)date_diff($cdate,$ndate)->format("%d") > 0) {
				  $cndate = date_diff($cdate,$ndate)->format("%dd");
				}
				else {
				  if((int)date_diff($cdate,$ndate)->format("%h") > 0)
					  $cndate = date_diff($cdate,$ndate)->format("%hh");
				  else
					  $cndate = date_diff($cdate,$ndate)->format("%mm");
				}
				
				$prourl = (!empty($sc->username))?(base_url().''.$sc->username):base_url().'user/id/'.$sc->userid;
				$picurl = (!empty($sc->profile_pic_url))?base_url("uploads/").$sc->profile_pic_url:base_url("assets/front/images/user-grey.jpg");
				$verified = ($sc->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
				$result .= '<div class="on_comment" id="comment_'.$sc->id.'">
						<div><a href="'.$prourl.'" class="propic"><img src="'.$picurl.'" width="35" /></a></div>
						
						<div class="commentcomment">
							<div>
								<strong><a href="'.$prourl.'">'.$sc->firstname.' '.$sc->lastname.'</a></strong>'.$verified.' '.$sc->content.'
								<span id="upvote_comment_'.$sc->id.'" class="hidesubcomment"><a href="javascript:;" onclick="votebox(\'#votemodels_comment_'.$sc->id.'\', \'comment\', '.$sc->id.')"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>0</span></a><div style="display: none;"><div id="votemodels_comment_'.$sc->id.'" class="votemodels"><div><img src="'.base_url("/images/loading.gif").'" width="46"></div></div></div></span>
								<div class="comhide"><a href="#">...</a><div style="display: none;"><a href="javascript:;" class="prevent_click" onclick="deletemypollpost(\'comment_'.$sc->id.'\', \'comment\', '.$sc->id.');" title="Delete this">Delete comment</a></div></div>
							</div>
							<div id="comment_'.$sc->id.'_'.$userid.'_comment">
								<small><a href="#" class="prevent_click" onclick="dovote(\'comment\', \''.$sc->id.'\', \''.$userid.'\', \'#upvote_comment_'.$sc->id.'\');">Upvote</a></small> &nbsp;
								<small><a href="#" class="prevent_click" onclick="docomment(\'comment\', \''.$sc->id.'\', \''.$userid.'\', \'#comment_'.$sc->id.'_'.$userid.'_comment\');">Reply</a></small> &nbsp;
								<small>'.$cndate.'</small>';
								
				if($sc->total) {
					$result .= '&nbsp; <small><a href="#" class="subcomment"><input type="hidden" value="comment" rel="'.$sc->id.'" />';
					$result .= ($sc->total > 1)?$sc->total.' Comments':$sc->total.' Comment';
					$result .= '</a></small>';
				}
				$result .= '</div>';
				$result .= '<div class="ajaxcommentbox"></div>
				</div>';
				
				$result .= '';
				echo $result;
			}
			else {
				if($friendsonly['commenting'] == 0) {
					$results = $this->ajaxss->insertComments($userid, $comment, $cid, $type, $tagged);
					$sc = $this->ajaxss->getComment($userid, $results);
					$result = '';
					
					$cdate = new DateTime($sc->created);
					$ndate = new DateTime();
					$cndate = '';
					if((int)date_diff($cdate,$ndate)->format("%d") > 0) {
					  $cndate = date_diff($cdate,$ndate)->format("%dd");
					}
					else {
					  if((int)date_diff($cdate,$ndate)->format("%h") > 0)
						  $cndate = date_diff($cdate,$ndate)->format("%hh");
					  else
						  $cndate = date_diff($cdate,$ndate)->format("%mm");
					}
					
					$prourl = (!empty($sc->username))?(base_url().''.$sc->username):base_url().'user/id/'.$sc->userid;
					$picurl = (!empty($sc->profile_pic_url))?base_url("uploads/").$sc->profile_pic_url:base_url("assets/front/images/user-grey.jpg");
					$verified = ($sc->verified)?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
					$result .= '<div class="on_comment" id="comment_'.$sc->id.'">
							<div><a href="'.$prourl.'" class="propic"><img src="'.$picurl.'" width="35" /></a></div>
							
							<div class="commentcomment">
								<div>
									<strong><a href="'.$prourl.'">'.$sc->firstname.' '.$sc->lastname.'</a></strong>'.$verified.' '.$sc->content.'
									<span id="upvote_comment_'.$sc->id.'" class="hidesubcomment"><a href="javascript:;" onclick="votebox(\'#votemodels_comment_'.$sc->id.'\', \'comment\', '.$sc->id.')"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>0</span></a><div style="display: none;"><div id="votemodels_comment_'.$sc->id.'" class="votemodels"><div><img src="'.base_url("/images/loading.gif").'" width="46"></div></div></div></span>
									<div class="comhide"><a href="#">...</a><div style="display: none;"><a href="javascript:;" class="prevent_click" onclick="deletemypollpost(\'comment_'.$sc->id.'\', \'comment\', '.$sc->id.');" title="Delete this">Delete comment</a></div></div>
								</div>
								<div id="comment_'.$sc->id.'_'.$userid.'_comment">
									<small><a href="#" class="prevent_click" onclick="dovote(\'comment\', \''.$sc->id.'\', \''.$userid.'\', \'#upvote_comment_'.$sc->id.'\');">Upvote</a></small> &nbsp;
									<small><a href="#" class="prevent_click" onclick="docomment(\'comment\', \''.$sc->id.'\', \''.$userid.'\', \'#comment_'.$sc->id.'_'.$userid.'_comment\');">Reply</a></small> &nbsp;
									<small>'.$cndate.'</small>';
									
					if($sc->total) {
						$result .= '&nbsp; <small><a href="#" class="subcomment"><input type="hidden" value="comment" rel="'.$sc->id.'" />';
						$result .= ($sc->total > 1)?$sc->total.' Comments':$sc->total.' Comment';
						$result .= '</a></small>';
					}
					$result .= '</div>';
					$result .= '<div class="ajaxcommentbox"></div>
					</div>';
					
					$result .= '';
					echo $result;
				}
				else {
					echo '';
				}
			}
		}
	}
	
	public function friendRequests() {
            if ($this->session->userdata('userData')) {
            $userData = $this->session->userdata('userData');
            $userid = $userData['userId'];
            $friendid = $_POST['friendid'];
            $results = $this->ajaxss->friendFriend($userid, $friendid);
            echo $results;
        }
    }
	
	public function fetchComment() {
		$result = '';
		if(!empty($_POST['refid'])) {
			$userid = '';
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$userid = $userData['userId'];
			}
			if($_POST['ref']=='comment') {
				$owner = '';
			}
			else
				$owner = $this->ajaxss->getOwnerID($_POST['refid'], $_POST['ref']);
			
			$comments = $this->ajaxss->getCommentList($_POST['refid'], $_POST['page'], $_POST['ref']);
			if($comments) {
				foreach($comments as $comment) {
			
					$cdate = new DateTime($comment['created']);
					$ndate = new DateTime();
					$cndate = '';
					if((int)date_diff($cdate,$ndate)->format("%d") > 0) {
					  $cndate = date_diff($cdate,$ndate)->format("%dd");
					}
					else {
					  if((int)date_diff($cdate,$ndate)->format("%h") > 0)
						  $cndate = date_diff($cdate,$ndate)->format("%hh");
					  else
						  $cndate = date_diff($cdate,$ndate)->format("%mm");
					}
					
					$prourl = (!empty($comment['username']))?(base_url().''.$comment['username']):base_url().'user/id/'.$comment['userid'];
					$picurl = (!empty($comment['profile_pic_url']))?base_url("uploads/").$comment['profile_pic_url']:base_url("assets/front/images/user-grey.jpg");
					$hidesubcomment = ($comment['upvote'])?'':'hidesubcomment';
					$verified = ($comment['verified'])?'<img class="ic_verify_comment" alt="Verified by ViralVoters.com" title="Verified by ViralVoters.com" src="'.base_url().'images/verify_26x26.png">':'';
			
					$result .= '<div class="on_comment comment'.$comment['id'].'">
								  <div><a href="'.$prourl.'" class="propic"><img src="'.$picurl.'" width="35" /></a></div>
								  
								  <div class="commentcomment">
									<div><strong><a href="'.$prourl.'">'.$comment['firstname'].' '.$comment['lastname'].'</a></strong>'.$verified.' '.$comment['content'].'
            
            						<span id="upvote_comment_'.$comment['id'].'" class="'.$hidesubcomment.'"><a href="javascript:;" onclick="votebox(\'#votemodels_comment_'.$comment['id'].'\', \'comment\', '.$comment['id'].')"><i class="fa fa-thumbs-up" aria-hidden="true"></i><span>'.$comment['upvote'].'</span></a><div style="display: none;"><div id="votemodels_comment_'.$comment['id'].'" class="votemodels"><div><img src="'.base_url("/images/loading.gif").'" width="46"></div></div></div></span>';
					
					if($userid && ($userid == $owner))
							$result .= '<div class="comhide"><a href="#">...</a><div style="display: none;"><a href="javascript:;" class="prevent_click" onclick="deletereference(\'comment\', '.$comment['id'].', '.$comment['id'].', this);" title="Delete this">Delete comment</a></div></div>';
							
					else {					
						if($userid && ($userid != $comment['userid']))
							$result .= '<div class="comhide"><a href="#">...</a><div><a href="#" class="hidecomment" id="'.$comment['id'].'" rel="'.$userid.'">Hide comment</a><a href="#feedbackcomment_'.$comment['id'].'" class="feedbackcomment">Give feedback or Report</a></div></div>';
						
						if($userid && ($userid == $comment['userid']))
							$result .= '<div class="comhide"><a href="#">...</a><div style="display: none;"><a href="javascript:;" class="prevent_click" onclick="deletereference(\'comment\', '.$comment['id'].', '.$comment['id'].', this);" title="Delete this">Delete comment</a></div></div>';
					}
					
					/*$result .= '<div style="display: none;"><div id="feedbackcomment_'.$comment['id'].'"><div style="width: 97%;">
                      <h4>Report</h4>
                      <form action="" method="post">
                      <div class="row">
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Hatred" /> Hatred</p></div>
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Voilence" /> Voilence</p></div>
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Its a Spam" /> It\'s a Spam</p></div>
                      </div>
                      <div class="row">
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Fake Profile" /> Fake Profile</p></div>
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Irrelevant" /> Irrelevant</p></div>
                        <div class="col-md-4"><p><input type="radio" name="report_radio" value="Copyright Violation" /> Copyright Violation</p></div>
                      </div>
                      <div class="row">
                        <div class="col-md-6"><p><input type="radio" name="report_radio" value="Targeting Race/Ethnicity/Culture" /> Targeting Race/Ethnicity/Culture</p></div>
                      </div>
                        <p align="center">
                          <textarea placeholder="Why you think so?" rows="3"></textarea>
                        </p>
                        <input type="submit" value="Report" name="reportsubmit" />
                      </form>
                    </div></div></div>';*/
									
					$result .= '</div><div id="comment_'.$comment['id'].'_'.$userid.'_'.$_POST['ref'].'">
										<small><a href="#" class="prevent_click" onclick="dovote(\'comment\', \''.$comment['id'].'\', \''.$userid.'\',\'#upvote_comment_'.$comment['id'].'\');">Upvote</a></small> &nbsp;
										<small><a href="#" class="prevent_click" onclick="docomment(\'comment\', \''.$comment['id'].'\', \''.$userid.'\', this);">Reply</a></small> &nbsp;
										<small>'.$cndate.'</small>
									</div><div class="ajaxcommentbox"></div>';
											
							if($comment['subcomment']) {
								$result .= '<p><a href="#" class="loadcomment" id="'.$comment['id'].'" rel="0" dir="'.$comment['subcomment'].'" data-type="comment"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> View '.$comment['subcomment'].' Replies</a>';
								//$result .= '<span class="ccounter"><span>0</span> of '.$comment['total'].'</span></p>';
							}
			
					$result .= '</div>
								  </div>';
				}
			}
			else {
				$result = '0';
			}
		}
		else
			$result = 'No comment found';
		
		echo $result;
	}
	
	public function hideComment() {
		$result = 'success';
		if(!empty($_POST['comid'])) {
			$result = $this->ajaxss->hideCommentConfirm($_POST['userid'], $_POST['comid']);
		}
		echo 1;
	}
	
	public function deleteComment() {
		$result = 'success';
		if(!empty($_POST['comid'])) {
			$result = $this->ajaxss->deleteCommentConfirm($_POST['userid'], $_POST['comid']);
		}
		echo 1;
	}
	
	public function imgComment() {
		if(isset($_REQUEST['i'])) {
			$userid = '';
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
				$userid = $userData['userId'];
			}
			$postimg = $this->Dashboard_model->getPostByImage($_REQUEST['i']);
			$timeline = $this->Dashboard_model->getTimeline($postimg['userid'], 'post', $postimg['postid']);
			$post = $this->Dashboard_model->getUserPosts($postimg['postid']);
			
			$userinfo = $this->Dashboard_model->getUserInformation($postimg['userid']);
					
			if($userid)
				$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'postdata', $_REQUEST['i']);
			else
				$upvoted = 0;
			
			$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post, 'postimg' => $postimg), 'comments' => $this->Home_model->getComments($userid, $_REQUEST['i'], 0, 'postdata'), 'data' => (array)$timeline, 'upvoted' => $upvoted);
			$this->load->view('iframeposts', $data);
		}
	}
	
	public function deletemypollpost() {
		if(isset($_POST['ty']) && isset($_POST['pid'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
				$this->ajaxss->deletepollpost($_POST['ty'], $_POST['pid'], $userData['userId']);
			}
			echo 1;
		}
	}
	
	public function deletereference() {
		if(isset($_POST['rf']) && isset($_POST['did']) && isset($_POST['rid'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
				if($userData['userId'])
					$this->ajaxss->deletethisreference($_POST['rf'], $_POST['did'], $_POST['rid'], $userData['userId']);
				else
					die(0);
			}
			echo 1;
		}
	}
	
	public function blockusers() {
		if(isset($_POST['userid']) && isset($_POST['userid'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$this->ajaxss->blockedusers($_POST['userid'], $userData['userId']);
				echo 1;
			}
		}
	}
	
	public function unblockusers() {
		if(isset($_POST['userid']) && isset($_POST['userid'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$this->ajaxss->unblockedusers($_POST['userid'], $userData['userId']);
				echo 1;
			}
		}
	}
	/*new methods*/
	public function postSharings() {
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
			$reftype = $_POST['ref'];
			$refid = $_POST['pid'];
			$url = $_POST['url'];
			$shtype = $_POST['st'];
			$uid = $_POST['uid'];
			$results = $this->ajaxss->insertSharing($userid, $reftype, $refid, $url, $shtype, $uid);
			echo 1;
		}
	}
	
	public function searchfriends() {
		if(isset($_POST['keyword'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$frlist = $this->ajaxss->searchfriendsbyname($_POST['keyword'], $userData['userId']);
				$result = '<ul>';
				if($frlist) {
				foreach($frlist as $fr) {
					$result .= '<li id="'.$fr['id'].'">';
					
					if($fr['username'])
						$prourl = base_url().''.$fr['username'];
					else
						$prourl = base_url().'user/id/'.$fr['id'];
					
					if($fr['picture_url'])
						$picurl = $fr['picture_url'];
					else {
						if($fr['profile_pic_url'])
							$picurl = base_url().'uploads/'.$fr['profile_pic_url'];
						else
							$picurl = base_url().'assets/front/images/user-grey.jpg';
					}
						
					$result .= '<a href="'.$prourl.'"><img src="'.$picurl.'" width="46" height="46" aligh="left" /> '.$fr['firstname'].' '.$fr['lastname'].'</a>';
					
					$result .= '<a href="#" style="float: right;" title="Block" onclick="blockusers('.$fr['id'].');"><button id="'.$fr['id'].'" class="trand-button">Block</button></a></li>';
				}
				}
				else {
					$result .= '<li>';
					$result .= 'No user found.';
					$result .= '</li>';
				}
				$result .= '</ul>';
				echo $result;
			}
		}
	}
	
	public function loginemail_availability() {
		if(isset($_POST['email'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$available = $this->ajaxss->checkusername_availability($_POST['email'], $userData['userId']);
				echo $available;
			}
		}
	}
	
	public function unfriend() {
		echo 1;
		if(isset($_POST['fr'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$available = $this->ajaxss->unfriendme($_POST['fr'], $userData['userId']);
				echo $available;
			}
		}
	}
	
	public function submitvoting() {
		if(isset($_POST['pollid']) && isset($_POST['option'])) {
			if($this->session->userdata('userData')){
				$userData = $this->session->userdata('userData');
				$userid = $userData['userId'];
				$friends = $this->Dashboard_model->getUserFrndIds($userid);
				$frn = explode(',', $friends);
				$alluser = array();
				
				foreach($frn as $frnds)
					$alluser[$frnds] = $frnds;
				
				unset($alluser[$userid]);
				$alluser = implode(',', $alluser);
					
				$this->Dashboard_model->addPollVoting($_POST['pollid'],$_POST['option'],$userid,$_SERVER['REMOTE_ADDR'],$alluser);
				$data['data'] = $this->Dashboard_model->getUserPolls($_POST['pollid']);
				$data['data']['polloption'] = $this->Dashboard_model->getUserPollOption($_POST['pollid']);
				$this->load->view('template/ajaxpolloption',$data);
			}
		}
	}
	
	public function loadmoreactivities() {
		$paging = (isset($_POST['page']))?$_POST['page']:0;
		$limit = 10;
		$start = $paging*$limit;
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
		if(!$this->session->userdata('userData')){
			$data['result'] = $this->Home_model->getActivities('', $start, $limit);
			if($data['result'])
				$this->load->view('loadmore_activities',$data);
			else
				echo '0';
		}else{
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['result'] = $this->Home_model->getActivities($userData['userId'], $start, $limit);
			if($data['result'])
				$this->load->view('loadmore_activities',$data);
			else
				echo '0';
		}
	}
	
	public function loadmoreactivitiesfilter() {
		$paging = (isset($_POST['page']))?$_POST['page']:0;
		$sortby = (isset($_POST['sortby']))?$_POST['sortby']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		$limit = 10;
		$start = $paging*$limit;
		if(!$this->session->userdata('userData')){
		}else{
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
		}
		$data['result'] = $this->Home_model->getPollByFilter($start, $limit, '', '', '', $sortby, $category);
		if($data['result'])
			$this->load->view('loadmore_activities',$data);
		else
			echo '0';
	}
	
	public function sharespostpoll() {
		$total = array();
		if($this->session->userdata('userData')){
			echo 1;
		}
		else {
			echo '';
		}
	}
	
	public function previewpost() {
		$data = array();
		if($this->session->userdata('userData')){
			$postdata = $_REQUEST['content'];
			if (filter_var($_REQUEST['content'], FILTER_VALIDATE_URL)) {
				$tags = get_meta_tags($_REQUEST['content']);
				$data['title'] = (isset($tags['twitter:title']))?$tags['twitter:title']:'';
				$data['image'] = (isset($tags['twitter:image']))?$tags['twitter:image']:'';
				$data['url'] = $_REQUEST['content'];
				$data['description'] = (isset($tags['twitter:description']))?$tags['twitter:description']:'';

				$sites_html = file_get_contents($_REQUEST['content']);
				$sites_html = mb_convert_encoding($sites_html, 'HTML-ENTITIES', "UTF-8");

				$html = new DOMDocument();
				@$html->loadHTML($sites_html);
				$meta_og_img = null;
				//Get all meta tags and loop through them.
				foreach($html->getElementsByTagName('meta') as $meta) {
					if($meta->getAttribute('property')=='og:image'){ 
						$data['image'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['image'];
					}
					if($meta->getAttribute('property')=='og:title'){ 
						$data['title'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['title'];
					}
					if($meta->getAttribute('property')=='og:description'){ 
						$data['description'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['description'];
					}
				}
			}
		}
		if($data) {
			echo '<div class="previewcontent"><p><img src="'.$data['image'].'" /></p><h4><a href="'.$data['url'].'" target="_blank">'.$data['title'].'</a></h4><p>'.substr($data['description'], 0, 77).'...</p></div>';
		}
	}
	
	public function previewurl() {
		$data = array();
		if($this->session->userdata('userData')){
			$postdata = $_REQUEST['content'];
			if (filter_var($_REQUEST['content'], FILTER_VALIDATE_URL)) {
				$tags = get_meta_tags($_REQUEST['content']);
				$data['title'] = (isset($tags['twitter:title']))?$tags['twitter:title']:'';
				$data['image'] = (isset($tags['twitter:image']))?$tags['twitter:image']:'';
				$data['url'] = $_REQUEST['content'];
				$data['description'] = (isset($tags['twitter:description']))?$tags['twitter:description']:'';

				$sites_html = file_get_contents($_REQUEST['content']);
				$sites_html = mb_convert_encoding($sites_html, 'HTML-ENTITIES', "UTF-8");

				$html = new DOMDocument();
				@$html->loadHTML($sites_html);
				$meta_og_img = null;
				//Get all meta tags and loop through them.
				foreach($html->getElementsByTagName('meta') as $meta) {
					if($meta->getAttribute('property')=='og:image'){ 
						$data['image'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['image'];
					}
					if($meta->getAttribute('property')=='og:title'){ 
						$data['title'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['title'];
					}
					if($meta->getAttribute('property')=='og:description'){ 
						$data['description'] = ($meta->getAttribute('content'))?$meta->getAttribute('content'):$data['description'];
					}
				}
			}
		}
		if($data) {
			echo '<div class="previewcontent"><p><img src="'.$data['image'].'" /></p><h4><a href="'.$data['url'].'" target="_blank">'.$data['title'].'</a></h4><p>'.substr($data['description'], 0, 77).'...</p></div>';
		}
	}
	
	public function followuser() {
		$total = array();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$available = $this->ajaxss->startfollowing($_POST['fr'], $_POST['ty'], $userData['userId']);
			echo ($_POST['ty'])?'<a href="#" class="prevent_click"><i class="fa fa-check" aria-hidden="true"></i> See First</a>':'<a href="#" class="prevent_click"><i class="fa fa-check" aria-hidden="true"></i> Following</a>';
			echo '<div class="hoverdiv"><ul><li><a href="#" class="prevent_click" title="See First" onclick="followuser('.$_POST['fr'].', 1);">See First</a></li><li><a href="#" class="prevent_click" title="Default" onclick="followuser('.$_POST['fr'].', 0);">Default</a></li><li><a href="#" class="prevent_click" title="Unfollow" onclick="unfollow('.$_POST['fr'].',\'\');">Unfollow</a></li></ul></div>';
		}
		else {
			echo 'Unauthorize access';
		}
	}
	
	public function unfollow() {
		$total = array();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$available = $this->ajaxss->unfollowing($_POST['fr'], $userData['userId']);
			echo '<a href="#" class="prevent_click" onclick="followuser(\''.$_POST['fr'].'\', 0);"><i class="fa fa-rss" aria-hidden="true"></i> Follow</a>';
		}
		else {
			echo 'Unauthorize access';
		}
	}
	
	public function friendtimeout() {
		$total = array();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->friendRequest($userData['userId']);
			echo ($request)?$request:'';
		}
		else {
			echo '';
		}
	}
	
	public function notificationtimeout() {
		$total = array();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->notificationActive($userData['userId']);
			echo ($request)?$request:'';
		}
		else {
			echo '';
		}
	}
	
	public function vvshare() {
		$total = array();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->vvpostpollshare($userData['userId'], $_REQUEST['pid'], $_REQUEST['rt'], $_REQUEST['ref']);
			echo 1;
		}
		else {
			echo '';
		}
	}
	
	public function fetchfriendname() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->fetchingfriends($userData['userId'], $_REQUEST['name']);
			if(count($request) && $request) {
				$result .= '<div class="commentfriends"><ul>';
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					if($ul['username'])
						$prourl = base_url().''.$ul['username'];
					else
						$prourl = base_url().'user/id/'.$ul['id'];
					
					$result .= '<li><img src="'.$pic.'" width="35" height="35" aligh="left" class="profilethumb" /> <span><a href="'.$prourl.'" title="'.$ul['firstname'].' '.$ul['lastname'].'" rel="'.$ul['id'].'">'.$ul['firstname'].' '.$ul['lastname'].'</a> &nbsp;</span></li>';
				}
				$result .= '</ul></div>';
			}
		}
		echo $result;
	}
	
	public function fetchusername() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->fetchinguser($_REQUEST['name']);
			if(count($request) && $request) {
				$result .= '<div class="alluser"><ul>';
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					if($ul['username'])
						$prourl = base_url().''.$ul['username'];
					else
						$prourl = base_url().'user/id/'.$ul['id'];
					
					$result .= '<li title="'.$ul['firstname'].' '.$ul['lastname'].'"><img src="'.$pic.'" width="35" height="35" aligh="left" class="profilethumb" /> <span><a href="'.$prourl.'" title="'.$ul['firstname'].' '.$ul['lastname'].'" rel="'.$ul['id'].'" title="'.$ul['firstname'].' '.$ul['lastname'].'">'.$ul['firstname'].' '.$ul['lastname'].'</a> &nbsp;</span></li>';
				}
				$result .= '</ul></div>';
			}
		}
		echo $result;
	}
	
	public function fetchtagfriend() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->fetchingfriends($userData['userId'], $_REQUEST['name']);
			
			if(count($request) && $request) {
				$result .= '<div class="tagfriendslist"><ul>';
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					
					$result .= '<li><img src="'.$pic.'" width="35" height="35" aligh="left" class="profilethumb" /> <span><a href="javascript:" rel="'.$ul['id'].'" title="'.$ul['firstname'].' '.$ul['lastname'].'">'.$ul['firstname'].' '.$ul['lastname'].'</a> &nbsp;</span></li>';
				}
				$result .= '</ul></div>';
			}
		}
		echo $result;
	}
	
	public function fetchtagnamefriend() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->fetchingfriends($userData['userId'], $_REQUEST['name']);
			
			if(count($request) && $request) {
				$result .= '<div class="shfriendslist"><ul>';
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					
					$result .= '<li><img src="'.$pic.'" width="35" height="35" aligh="left" class="profilethumb" /> <span><a href="javascript:" rel="'.$ul['id'].'" title="'.$ul['firstname'].' '.$ul['lastname'].'">'.$ul['firstname'].' '.$ul['lastname'].'</a> &nbsp;</span></li>';
				}
				$result .= '</ul></div>';
			}
		}
		echo $result;
	}
	
	public function loadfriendrequest() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->friendsrequest($userData['userId']);
			if(count($request) && $request) {
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					if($ul['username'])
						$prourl = base_url().''.$ul['username'];
					else
						$prourl = base_url().'user/id/'.$ul['id'];
					
					$result .= '<tr id="fr_'.$ul['id'].'"><th><a href="'.$prourl.'" title="'.$ul['firstname'].' '.$ul['lastname'].'"><img src="'.$pic.'" alt="Profile Picture" /> '.$ul['firstname'].' '.$ul['lastname'].'</a></th><td><a href="#" class="trand-button prevent_click" title="confirm request" onclick="confirm_friend('.$ul['id'].', \'fr_'.$ul['id'].'\');">Confirm</a><a href="#" class="trand-button-grey prevent_click" title="reject request" onclick="reject_friend('.$ul['id'].', \'fr_'.$ul['id'].'\');">Reject</a></td></tr>';
				}
			}
		}
		echo $result;
	}
	
	/*public function loadusernotify() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$request = $this->ajaxss->notificationActive($userData['userId']);
			if(count($request)) {
				foreach($request as $ul) {
					$pic = ($ul['profile_pic_url'])?base_url().'uploads/'.$ul['profile_pic_url']:base_url().'assets/front/images/user-grey.jpg';
					if($ul['username'])
						$prourl = base_url().''.$ul['username'];
					else
						$prourl = base_url().'user/id/'.$ul['id'];
					
					$result .= '<tr id="fr_'.$ul['id'].'"><th><a href="'.$prourl.'" title="'.$ul['firstname'].' '.$ul['lastname'].'"><img src="'.$pic.'" alt="Profile Picture" /> '.$ul['firstname'].' '.$ul['lastname'].'</a></th><td><a href="#" class="trand-button" title="confirm request" onclick="confirm_friend('.$ul['id'].',\'fr_'.$ul['id'].'\');">Confirm</a><a href="#" class="trand-button-grey" title="reject request" onclick="reject_friend('.$ul['id'].',\'fr_'.$ul['id'].'\');">Reject</a></td></tr>';
				}
			}
		}
		echo $result;
	}*/
	
	public function confirm_friend() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$result = $this->ajaxss->confirmfriend($_POST['uid'], $userData['userId']);
		}
		echo 1;
	}
	
	public function reject_friend() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$result = $this->ajaxss->rejectfriend($_POST['uid'], $userData['userId']);
		}
		echo 1;
	}
	
	public function abusereporting() {
		$result = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			if(isset($_POST['refid']) && isset($_POST['reftype']) && isset($_POST['report']) && isset($_POST['textdesc']))
				$result = $this->ajaxss->abusedSubmit($_POST['refid'], $_POST['reftype'], $_POST['report'], $_POST['textdesc'], $userData['userId']);
		}
		echo 1;
	}
	
	public function sharenow($type) {
		if($this->session->userdata('userData')){
			$userid = '';
			switch($type) {
				case 'poll':
					$userData = $this->session->userdata('userData');
					$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
					$userid = $userData['userId'];
					$polls = $this->Dashboard_model->getUserPolls($_REQUEST['i']);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($_REQUEST['i']);
					
					$userinfo = $this->Dashboard_model->getUserInformation($polls['user_id']);
			
					$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'data' => $this->Dashboard_model->getTimelineText($polls['user_id'], 'polls', $_REQUEST['i']));
					$this->load->view('sharenowpoll', $data);
				break;
				
				case 'post':
					$userData = $this->session->userdata('userData');
					$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
					$userid = $userData['userId'];
					$post = $this->Dashboard_model->getUserPosts($_REQUEST['i']);
					
					$userinfo = $this->Dashboard_model->getUserInformation($post['user_id']);
			
					$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'data' => $this->Dashboard_model->getTimelineText($post['user_id'], 'post', $_REQUEST['i']));
					$this->load->view('sharenowpost', $data);
				break;
			}
			if(isset($_REQUEST['i'])) {
			}
		}
	}
	
	public function sharenow_friends($type) {
		if($this->session->userdata('userData')){
			$userid = '';
			switch($type) {
				case 'poll':
					$userData = $this->session->userdata('userData');
					$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
					$userid = $userData['userId'];
					$polls = $this->Dashboard_model->getUserPolls($_REQUEST['i']);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($_REQUEST['i']);
					
					$userinfo = $this->Dashboard_model->getUserInformation($polls['user_id']);
			
					$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'data' => $this->Dashboard_model->getTimelineText($polls['user_id'], 'polls', $_REQUEST['i']));
					$this->load->view('sharenowpollfriends', $data);
				break;
				
				case 'post':
					$userData = $this->session->userdata('userData');
					$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
					$userid = $userData['userId'];
					$post = $this->Dashboard_model->getUserPosts($_REQUEST['i']);
					
					$userinfo = $this->Dashboard_model->getUserInformation($post['user_id']);
			
					$data['data'] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'data' => $this->Dashboard_model->getTimelineText($post['user_id'], 'post', $_REQUEST['i']));
					$this->load->view('sharenowpostfriends', $data);
				break;
			}
			if(isset($_REQUEST['i'])) {
			}
		}
	}
	
	public function sharesubmit() {
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$friends = array();
			$postdata = '';
			$refid = '';
			$reftype = '';
			$fid = '';
			foreach($_POST['data'] as $val) {
				if($val['name'] == 'postdata')
					$postdata = $val['value'];
					
				if($val['name'] == 'tagfrn[]')
					$friends[] = $val['value'];
					
				if($val['name'] == 'refid')
					$refid = $val['value'];
					
				if($val['name'] == 'reftype')
					$reftype = $val['value'];
					
				if($val['name'] == 'tid')
					$tid = $val['value'];
					
				if($val['name'] == 'tlfrd')
					$fid = $val['value'];
			}
			
        	$result = $this->ajaxss->insertsharesubmit($userData['userId'], $postdata, $friends, $refid, $reftype, $tid, $fid);
			$reftext = ($reftype=='polls')?'Poll':'Post';
			echo 'This has been share on your timeline';
		}
	}
	
	public function sharesubmit_friends() {
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$friends = array();
			$postdata = '';
			$refid = '';
			$reftype = '';
			foreach($_POST['data'] as $val) {
				if($val['name'] == 'postdata')
					$postdata = $val['value'];
					
				if($val['name'] == 'tagfrn[]')
					$friends[] = $val['value'];
					
				if($val['name'] == 'refid')
					$refid = $val['value'];
					
				if($val['name'] == 'reftype')
					$reftype = $val['value'];
			}
			
        	$result = $this->ajaxss->insertsharesubmit($userData['userId'], $postdata, $friends, $refid, $reftype);
			$reftext = ($reftype=='polls')?'Poll':'Post';
			echo $reftext.' has been share on your timeline';
		}
	}
	
	public function imgtitle() {
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			
        	$result = $this->ajaxss->updateImgtitle($userData['userId'], $_POST['title'], $_POST['postdata']);
			echo 1;
		}
	}
}


