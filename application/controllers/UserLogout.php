<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserLogout extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        //$this->load->library('googleplus');
    }
	
	public function index()
    {
		$this->session->sess_destroy();
		// Remove user data from session
		// Redirect to login page
		$this->session->unset_userdata('userData');
		//$this->googleplus->revokeToken();
        redirect('/login');
        
    }
}
