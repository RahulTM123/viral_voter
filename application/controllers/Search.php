<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Home_model');
		$this->load->model('Search_model');
		$this->load->model('Dashboard_model');
		$this->load->model('cities');
		$this->load->model('ajaxss');
    }
	
	public function index(){
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
		
		if(isset($_POST['searchin']) && ($_POST['searchin'] == 'post'))
			$categories = $this->Dashboard_model->getCatList();
		else
			$categories = $this->Dashboard_model->getVerifyCatList();
		
		$data['categories'] = $categories;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		
		$keyword = (isset($_POST['keyword']))?$_POST['keyword']:'';
		$gender = (isset($_POST['gender']))?$_POST['gender']:'';
		
		if(isset($_POST['search'])) {
		
			if(isset($_POST['filter'])) {
				$gender = ($_POST['gender'])?$_POST['gender']:'';
				
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
			}
				
			$searching = $this->Search_model->result($keyword, $country_id, $state_id, $city_id, $gender, $category);
			$data['searching'] = $searching;
			
		}
			
			$this->load->view('header',$data);
			$this->load->view('search_sidebar');
			$this->load->view('search');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
    }
	
	public function user(){
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
			
		//$categories = $this->Dashboard_model->getCatList();
		//$data['categories'] = $categories;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		
		$keyword = (isset($_POST['keyword']))?$_POST['keyword']:'';
		$gender = (isset($_POST['gender']))?$_POST['gender']:'';
		
		if(isset($_POST['search'])) {
		
			if(isset($_POST['filter'])) {
				$gender = ($_POST['gender'])?$_POST['gender']:'';
				
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
			}
				
			$searching = $this->Search_model->resultuser($keyword, $country_id, $state_id, $city_id, $gender);
			$data['searching'] = $searching;
			
		}
			
			$this->load->view('header',$data);
			$this->load->view('search_sidebar');
			$this->load->view('search_user');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
    }
	
	public function poll(){
		$data['frequest'] = '';
		$data['messages'] = '';
		$data['peoples'] = $this->Home_model->getPeopleKnow();
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['peoples'] = $this->Home_model->getPeopleKnow($userData['userId']);
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
		}else{
		}
			
		$categories = $this->Dashboard_model->getCatList();
		$data['categories'] = $categories;
			
		$data['countries'] = $this->cities->allCountries();
		$country_id = (isset($_POST['cid']))?$_POST['cid']:'';
		$state_id = (isset($_POST['sid']))?$_POST['sid']:'';
		$city_id = (isset($_POST['ctid']))?$_POST['ctid']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		
		$keyword = (isset($_POST['keyword']))?$_POST['keyword']:'';
		$category = (isset($_POST['category']))?$_POST['category']:'';
		
		if(isset($_POST['search'])) {
		
			if(isset($_POST['filter'])) {
				$gender = ($_POST['gender'])?$_POST['gender']:'';
				
				if($country_id > 0)
					$data['states'] = $this->cities->allStates($country_id);
				
				if($state_id > 0)
					$data['cities'] = $this->cities->allCitiesByState($state_id);
			}
				
			$searching = $this->Search_model->resultpoll($keyword, $country_id, $state_id, $city_id, $category);
			$data['searching'] = $searching;
			
		}
			
			$this->load->view('header',$data);
			$this->load->view('search_sidebar');
			$this->load->view('search_poll');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
    }
}
