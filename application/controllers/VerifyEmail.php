<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class VerifyEmail extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->model('user');
    }
	
	public function index(){
		if(isset($_REQUEST['ref'])){
			$userID = $this->user->verifyUser($_REQUEST['ref']);
			if(!empty($userID)){
				$data['msg'] = 'Your email id is successfully verified. Click here for <a href="'.base_url().'login" style="font-size: 16px;text-decoration: underline;">Login</a>';
				$this->load->view('header',$data);
				$this->load->view('left_sidebar');
				$this->load->view('thankyou');
				$this->load->view('footer');
			}else{
				redirect('./login','refresh');
			}
		}
    }	
}
