<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FbLogin extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->library('facebook');
		$this->load->model('user');
    }
	
	public function index()
    {
            
            require_once(APPPATH.'libraries/Facebook/autoload.php');
		$fb = new Facebook\Facebook([
			  'app_id' => '2051773518371640',
			  'app_secret' => '7e859600a841c1297a92360c9d41c642',
			  'default_graph_version' => 'v2.12',
			  ]);
		$helper = $fb->getRedirectLoginHelper();
		//$permissions = []; // optional
		//$accessToken = $helper->getAccessToken();
		try {
			if (isset($_SESSION['facebook_access_token'])) {
				$accessToken = $_SESSION['facebook_access_token'];
			} else {
		  		$accessToken = $helper->getAccessToken();
			}
		} catch(Facebook\Exceptions\FacebookResponseException $e) {
		 	// When Graph returns an error
		 	echo 'Graph returned an error: ' . $e->getMessage();
		  	exit;
		} catch(Facebook\Exceptions\FacebookSDKException $e) {
		 	// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  	exit;
		 }
		if (isset($accessToken)) {
			if (isset($_SESSION['facebook_access_token'])) {
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} else {
				// getting short-lived access token
				$_SESSION['facebook_access_token'] = (string) $accessToken;
			  	// OAuth 2.0 client handler
				$oAuth2Client = $fb->getOAuth2Client();
				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
				// setting default access token to be used in script
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}
			// redirect the user back to the same page if it has "code" GET variable
			if (isset($_GET['code'])) {
				//header('Location: ./');
			}
			// getting basic info about user
			try {
				$profile_request = $fb->get('/me?fields=id,first_name,last_name,email,gender,locale,picture');
				$profile = $profile_request->getGraphNode()->asArray();
				$userData['oauth_provider'] = 'facebook';
		            	$userData['oauth_uid'] = $profile['id'];
		           	$userData['firstname'] = $profile['first_name'];
		            	$userData['lastname'] = $profile['last_name'];
		           	$userData['gender'] = $profile['gender'];
		           	$userData['locale'] = $profile['locale'];
		            	$userData['profile_url'] = 'https://www.facebook.com/'.$profile['id'];
		            	$userData['picture_url'] = $profile['picture']['url'];
		           	$userID = $this->user->checkUserFb($userData);
		           	if(!empty($userID)){
		                      //$data['userData'] = $userData;
		                      //$data['logoutUrl'] = $this->facebook->logout_url();
				      $userData['userId'] = $userID;
		                      $this->session->set_userdata('userData',$userData);
		                      $this->session->set_userdata('logoutUrl','http://www.viralvoters.com/beta/FbLogout');
								if(isset($_SESSION['referer'])){
									$refer = $_SESSION['referer'];
									unset($_SESSION['referer']);
									redirect($refer,'refresh');
								}
								else {
									redirect(base_url(),'refresh');
								}
		                }
			} catch(Facebook\Exceptions\FacebookResponseException $e) {
				redirect("./login");
			} catch(Facebook\Exceptions\FacebookSDKException $e) {
				// When validation fails or other local issues
				redirect("./login");
			}
			// get basic page info
			//$page = $fb->get('/funnydemons?fields=username,picture.width(500),cover,');
			//$page = $page->getGraphNode()->asArray();
			//echo "<img src='{$page['cover']['source']}'>";
		  	// Now you can redirect to another page and use the access token from $_SESSION['facebook_access_token']
		} else {
			redirect('./login');
		}
	}
}
