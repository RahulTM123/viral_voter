<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class GoogleLogout extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('facebook');
    }
	
	public function index()
    {
		//delete login status & user info from session
		$this->session->unset_userdata('loggedIn');
		$this->session->unset_userdata('userData');
                $this->session->unset_userdata('tlogoutUrl');
        $this->session->sess_destroy();
        redirect('/login');
        
    }
}
