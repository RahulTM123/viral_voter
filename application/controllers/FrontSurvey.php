<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontSurvey extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('FrontSurvey_model');
		$this->load->model('UserTimeline_model');
		$this->load->model('Dashboard_model');
    }
	
	public function index(){
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$result = $this->FrontSurvey_model->getSurvey($userData['userId']);
			$data['htmlData'] = $result;
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['userLogout'] = $this->session->userdata('userLogout');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlepost');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
		elseif($this->session->userdata('adminData')){
			$result = $this->FrontSurvey_model->getSurveyForAdmin();
			$data['adminData'] = $this->session->userdata('adminData');
			$data['htmlData'] = $result;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('singlepost');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
		else{
			$result = $this->FrontSurvey_model->getSurveyBeforeLogin();
			$data['htmlData'] = $result;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('survey');
			$this->load->view('right_sidebar');
			$this->load->view('footer');
		}
    }
}
