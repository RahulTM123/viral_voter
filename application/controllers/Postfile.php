<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Postfile extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Postfile_model');
		$this->load->model('Dashboard_model');
        $this->load->library('session');
    }
	
	
	
	public function index($id) {
		$data = array();
		$profileData = '';
		
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			
			$filedata = $this->Postfile_model->getFileData($id);
			print_r($filedata);
		}
		else {
			$data['profileData'] = (array)$this->Dashboard_model->getUserInformation($id);
		}
		$data['userLogout'] = $this->session->userdata('userLogout');
		$this->load->view('usertimeline');
    }
}