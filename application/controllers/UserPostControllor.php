<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserPostControllor extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('UserPost_model');
    }
	
	public function index()
    {
		//$this->load->view('header');
    }
	public function addUserPost()
    {
		print_r($_REQUEST);
		die;
        if(isset($_REQUEST['newQuiz'])){
			if(!empty($_FILES['userPostFile']['name']) && !empty($_REQUEST['userPost'])){
				$name = md5(time()).'_'.$_FILES['userPostFile']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "200000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('userPostFile'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$postData = serialize(array($_REQUEST['userPost'],$file));
				}else{
					$error['postError'] = $this->upload->display_errors();
					$this->load->view('admin/dashboard',$error);
				}
			}else if(!empty($_REQUEST['userPost'])){
				$postData = serialize(array($_REQUEST['userPost']));
			}else{
				$name = md5(time()).'_'.$_FILES['userPostFile']['name'];
				$config = array(
					'upload_path' => "./uploads/",
					'allowed_types' => "jpg|png|jpeg",
					'overwrite' => TRUE,
					'file_name' => $name,
					'max_size' => "80000000"  // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if($this->upload->do_upload('userPostFile'))
				{
					$fileData = $this->upload->data();
					$file = $fileData['file_name'];
					$postData = serialize(array($file));
				}else{
					$error['postError'] = $this->upload->display_errors();
					$this->load->view('admin/dashboard',$error);
				}
			}
			$result = $this->UserPost_model->addUserPost($postData);
			
			if(!empty($result)){
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/dashboard');
				$this->load->view('admin/footer');
			}else{
				$this->load->view('admin/header');
				$this->load->view('admin/sidebar');	
				$this->load->view('admin/dashboard');
				$this->load->view('admin/footer');
			}
		}
    }
}
