<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class UserSettings extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('image_lib');
		$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Settings_model');
		$this->load->model('UserTimeline_model');
		$this->load->model('cities');
		$this->load->model('Category_model');
		$this->load->model('Dashboard_model');
		$this->load->model('user');
		$this->load->model('ajaxss');
    }
	
	public function index(){
		$data['frequest'] = '';
		$data['messages'] = '';
		if($this->session->userdata('userData')){
			$userData = $this->session->userdata('userData');
			
			$data['frequest'] = $this->ajaxss->friendRequest($userData['userId']);
			$data['messages'] = $this->ajaxss->notificationActive($userData['userId']);
			if(isset($_POST['submit'])) {
				if($_REQUEST['tab']=='general') {
					$this->Settings_model->updateGeneralData($_REQUEST, $userData['userId']);
					$this->session->set_flashdata('msg', 'General information update successfully');
				}
				if($_REQUEST['tab']=='profile') {
					$this->Settings_model->updateProfileData($_REQUEST, $userData['userId']);
					$this->session->set_flashdata('msg', 'Profile information update successfully');
				}
				if($_REQUEST['tab']=='verification') {
					$filename = '';
					$name = md5(time()).'_'.$_FILES['id_proof']['name'];
					$config = array(
						'upload_path' => "./uploads/verify/",
						'allowed_types' => "jpg|png|jpeg|gif",
						'overwrite' => TRUE,
						'file_name' => $name
					);
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					if($this->upload->do_upload('id_proof'))
					{
						$fileData = $this->upload->data();
						$configer =  array(
						  'image_library'   => 'gd2',
						  'source_image'    =>  $fileData['full_path'],
						  'maintain_ratio'  =>  TRUE,
						  'width'           =>  1000,
						);
						$this->image_lib->clear();
						$this->image_lib->initialize($configer);
						$this->image_lib->resize();
						
						$fileData = $this->upload->data();
						$filename = $fileData['file_name'];
					}
					$this->Settings_model->updateVerificationData($_REQUEST, $userData['userId'], "uploads/verify/".$filename);
				
								
					$this->load->library("PhpMailerLib");
					$smtpDetails = $this->user->getSmtpDetails();
					$mailContent = $this->user->getTemplate();
					$mail = $this->phpmailerlib->load();
					$mailad = $this->phpmailerlib->load();
					
						try {
							//Server settings
							$mailad->SMTPDebug = 0;                                 // Enable verbose debug output
							$mailad->isSMTP();                                      // Set mailer to use SMTP
							$mailad->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
							$mailad->SMTPAuth = true;                               // Enable SMTP authentication
							$mailad->Username = $smtpDetails[0]->username;                 // SMTP username
							$mailad->Password = $smtpDetails[0]->password;                           // SMTP password
							$mailad->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
							$mailad->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to connect to
							//Recipients
							$mailad->setFrom($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
							$mailad->addAddress('info@viralvoters.com', 'ViralVoters');            // Name is optional
							//$mailad->addAddress('sachinsaini.cs@gmail.com', 'ViralVoters');            // Name is optional
							$mailad->addReplyTo($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
							//$mailad->AddBCC('rajneesh.sharma@yahoo.com');
							//$mailad->AddBCC('shalabh5@hotmail.com');
							$mailad->AddBCC('viralvoters@gmail.com');
							$mailad->isHTML(true);                                  // Set email format to HTML
							$mailad->Subject = $userData['firstname'].' '.$userData['lastname'].' <'.$userData['email'].'> Has Applied For Verification';
							$mailad->Body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
								<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="http://www.viralvoters.com/beta/assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
								<tr><td>
									<p>Dear Admin,<br />'.$userData['firstname'].' '.$userData['lastname'].' has applied for verification</p>
									<p></p>
									
								</td></tr>
								</table>';
				
							$mailad->send();
							$i = 1;
						} catch (Exception $e) {
							$i = 0;
						}					
				
					$this->session->set_flashdata('msg', 'Your verification is under process, we will notify you');
				}
				if($_REQUEST['tab']=='password') {
					if(isset($_REQUEST['email'])) {
						$this->Settings_model->updateLoginEmail($_REQUEST, $userData['userId']);
						$this->session->set_flashdata('msg', 'Email updated successfully');
					}
					if(isset($_REQUEST['password'])) {
						$this->Settings_model->updateLoginEmail($_REQUEST, $userData['userId']);
						$this->session->set_flashdata('msg', 'Password updated successfully');
					}
				}
			}
			$data['categories'] = $this->Category_model->getCatList();
			$data['verify_category'] = $this->Category_model->getVerifyCatList();
			$loggedInUser = $this->UserTimeline_model->getUserName($userData['userId']);
			
			$data['general_tabbed'] = $this->Settings_model->getGeneralData($userData['userId']);
			$data['profile_tabbed'] = $this->Settings_model->getProfileData($userData['userId']);
			$data['verification_tabbed'] = $this->Settings_model->getVerificationData($userData['userId']);
			$data['verification_tabbed']['applied'] = ('0000-00-00 00:00:00' == $data['verification_tabbed']['verified_date'])?0:1;
			$data['verification_tabbed']['message'] = '';
			if($data['verification_tabbed']['applied'] == 1) {
				if($data['verification_tabbed']['verified'] == 1) {
					$data['verification_tabbed']['message'] = '<i class="fa fa-check" aria-hidden="true"></i> Your account is successfully verified with ViralVoters.com';
				}
				else {
					$appdate = strtotime($data['verification_tabbed']['verified_date']);
					$curdate = time();
					$datediff = $curdate - $appdate;
					$days = round($datediff / (60 * 60 * 24));
					$data['verification_tabbed']['message'] = '';
					
					if($days < 90) {
						if(($data['verification_tabbed']['verified'] == 0)) {
							$data['verification_tabbed']['message'] = 'You have applied for verification, we will get back to you soon.';
						}
						else {
							$data['verification_tabbed']['message'] = '<i class="fa fa-times" aria-hidden="true"></i> ViralVoters.com declined your verification, you can resubmit verification after 90 days.<br /><strong>Date of Applied:</strong> '.date('d-m-Y', strtotime($data['verification_tabbed']['verified_date']));
						}
					}
					else {
						$data['verification_tabbed']['message'] = '';
					}
				}
			}
			
			$data['logUserName'] = $loggedInUser[0]->username;
			$data['UserFullName'] = $loggedInUser[0]->firstname;
			$UserInformation = array(
				'UserFirstName' => $loggedInUser[0]->firstname, 
				'UserMiddleName' => $loggedInUser[0]->middlename,
				'UserLastName' => $loggedInUser[0]->lastname,
				'c_visibility' => $loggedInUser[0]->c_visibility,
				'd_visibility' => $loggedInUser[0]->d_visibility,
				'gender' => $loggedInUser[0]->gender,
				'dob' => $loggedInUser[0]->dob,
				'state' => $loggedInUser[0]->state,
				'country' => $loggedInUser[0]->country,
				'city' => $loggedInUser[0]->city,
				//'overview' => $loggedInUser[0]->overview,
				'contact' => $loggedInUser[0]->contact,
				'interest' => $loggedInUser[0]->interest
			);
			$data['UserInformation'] = $UserInformation;
			if(!empty($loggedInUser[0]->profile_pic_url)){
				$data['logProfilePicture'] = base_url().'uploads/'.$loggedInUser[0]->profile_pic_url;
			}else{
				$data['logProfilePicture'] = $loggedInUser[0]->picture_url;
			}
			$data['userLogout'] = $this->session->userdata('userLogout');
			$data['userData'] = (array)$this->Dashboard_model->getUserInfo($userData['userId']);
			$data['countries'] = $this->cities->allCountries();
			$data['states'] = $this->cities->allStatesByCity($loggedInUser[0]->city);
			$data['cities'] = $this->cities->allCitiesByCity($loggedInUser[0]->city);
			$message = $this->Dashboard_model->getUnreadMessages($userData['userId']);
			$data['messages'] = $message->msg;
			$this->load->view('header',$data);
			$this->load->view('left_sidebar');
			$this->load->view('settings');
			$this->load->view('footer');
		}else{
			redirect('./login');
		}
    }
}
