<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	public function __construct(){
        parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->model('Ajax_Model');
        $this->load->library('session');
    }
	
	public function index(){
		if(!empty($_FILES['file']['name'])){
			$name = md5(time()).'_'.$_FILES['file']['name'];
			$config = array(
				'upload_path' => "./uploads/",
				'allowed_types' => "jpg|png|jpeg|gif",
				'overwrite' => TRUE,
				'file_name' => $name,
				'max_size' => "5000"
			);
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			if($this->upload->do_upload('file'))
			{
				$fileData = $this->upload->data();
				$configer =  array(
				  'image_library'   => 'gd2',
				  'source_image'    =>  $fileData['full_path'],
				  'maintain_ratio'  =>  TRUE,
				  'width'           =>  1000,
				);
				$this->image_lib->clear();
				$this->image_lib->initialize($configer);
				$this->image_lib->resize();
				
				$msg = $fileData['file_name'];
			}
			else {
				$msg = $this->upload->display_errors();	
			}
			echo $msg;
		}
    }
	
	
	public function saveProfilePic(){
		if(!empty($_REQUEST['name'])){
			$userData = $this->session->userdata('userData');
			$getOldPic = $this->Ajax_Model->getOldPic($userData['userId']);
			if($getOldPic != ''){
				unlink('./uploads/'.$getOldPic);
			}
			$saveProfile = $this->Ajax_Model->saveProfilePic($userData['userId'],$_REQUEST['name']);
		}
    }
	
	
	public function saveCoverPic(){
		if(!empty($_REQUEST['name'])){
			$userData = $this->session->userdata('userData');
			$getOldCoverPic = $this->Ajax_Model->getOldCoverPic($userData['userId']);
			if($getOldCoverPic != ''){
				unlink('./uploads/'.$getOldCoverPic);
			}
			$saveProfile = $this->Ajax_Model->saveCoverPic($_REQUEST['userid'],$_REQUEST['name']);
		}
    }
	
	public function unlinkimg(){
		if(!empty($_REQUEST['name'])){
			$name = $_REQUEST['name'];
			unlink('./uploads/'.$name);
		}
    }
	
	public function friend_section(){
		$html = '';
		$userData = $this->session->userdata('userData');
		$confirmRequest = $this->Ajax_Model->check_request($userData['userId']);
		if(!empty($confirmRequest)){
			foreach($confirmRequest as $cr){
				$userdata = $this->Ajax_Model->userdata($cr->friend_one);
				$userurl = ($userdata[0]->username)?$userdata[0]->username:'user/'.$userdata[0]->id;
				$html .= '<li class="obejct_list"><div class="clearfix followup tm_head" ><a href="#" tabindex="-1" class="followupimg lfloat">';
				$html .= ($userdata[0]->profile_pic_url)?'<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" width="46"/>':'<img src="'.base_url().'images/user.png'.'" width="46"/>';
				
				$html .= '</a><div class="request-detail" style="position: relative; left: -16px;"><div class="requeststatus"><table width="100%" border="0" cellspacing="0" cellpadding="0" height="32"><tr><td><div class="requeststatusblock" aria-hidden="true"><div class="requestname"><div class="requestuser"><span class="request-tittle"><a href="'.$userurl.'">'.$userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span></div></div></div></div></div></td><td><div class="response-container"><div class="response-bttn"><div class=""><div class="response-buttons"><button type="button" value="'.$cr->id.'" class="confirm confirm_request">Confirm</button><button type="button" value="'.$cr->id.'" class="dlt not_now">Not now</button></div></div></div> </div></td></tr></table><div class="clearfix"></div></div></li>';
			}	
		}else{
			$html .= '<p style="text-align:center;margin-top: 5px;margin-bottom: 5px;">No friend request found!</p>';
		}
		echo $html;
    }
	
	public function confirm_request(){
		$result = $this->Ajax_Model->confirm_request($_REQUEST['id']);
    }
	
	
	public function live_section(){
		$result = $this->Ajax_Model->live_section($_REQUEST['pid'],$_REQUEST['likeid'],$_REQUEST['userid'],$_REQUEST['title']);
		echo $result;
    }
	
	public function cmntLike_section(){
		$result = $this->Ajax_Model->cmntLike_section($_REQUEST['cmntId'],$_REQUEST['like'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function share_section(){
		$result = $this->Ajax_Model->share_section($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['logtitle']);
		echo $result;
    }
	
	public function friend_notify(){
		$userData = $this->session->userdata('userData');
		$result = $this->Ajax_Model->friend_notify($userData['userId']);
		echo $result;
    }
		
	
	public function delete_request(){
		$result = $this->Ajax_Model->delete_request($_REQUEST['id']);
    }	
	
	public function deleteBlock(){
		$result = $this->Ajax_Model->deleteLogs($_REQUEST['id'],$_REQUEST['title'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function deleteSharedBlock(){
		$result = $this->Ajax_Model->deleteSharedLogs($_REQUEST['id'],$_REQUEST['title'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function getPollCommentData(){
		$result = $this->Ajax_Model->getPollCommentData($_REQUEST['pid'],$_REQUEST['title'],$_REQUEST['userid']);
		echo $result;
    }	
	
	public function removeComment(){
		$getresult = $this->Ajax_Model->getOneComment($_REQUEST['id']);
		if(empty($getresult[0]->cmntImg)){
			if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImg)){
				unlink('./uploads/'.$getresult[0]->cmntImg);
			}
		}
		
		if(empty($getresult[0]->cmntImgTwo)){
			if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgTwo)){
				unlink('./uploads/'.$getresult[0]->cmntImgTwo);
			}
		}
		
		if(empty($getresult[0]->cmntImgThree)){
			if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgThree)){
				unlink('./uploads/'.$getresult[0]->cmntImgThree);
			}
		}
		$result = $this->Ajax_Model->removeComment($_REQUEST['id'],$_REQUEST['title'],$_REQUEST['dataId']);
		echo $result;
    }
	
	public function removeCommentOnComment(){
		$result = $this->Ajax_Model->removeCommentOnComment($_REQUEST['id'],$_REQUEST['title'],$_REQUEST['dataId']);
		echo $result;
    }
	
	public function addPollComment(){
		$result = $this->Ajax_Model->addPollComment($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntImg'],$_REQUEST['title']);
		echo $result;
    }
	
	
	public function addCompComment(){
		$result = $this->Ajax_Model->addCompComment($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntImgOne'],$_REQUEST['captionOne'],$_REQUEST['cmntImgTwo'],$_REQUEST['captionTwo'],$_REQUEST['cmntImgThree'],$_REQUEST['captionThree'],$_REQUEST['title']);
		echo $result;
    }
	
	public function editCompComment(){
		if(empty($_REQUEST['cmntImgOne']) && !empty($_REQUEST['imageOne'])){
			unlink('./uploads/'.$_REQUEST['imageOne']);
		}
		
		if(empty($_REQUEST['cmntImgTwo']) && !empty($_REQUEST['imageTwo'])){
			unlink('./uploads/'.$_REQUEST['imageTwo']);
		}
		
		if(empty($_REQUEST['cmntImgThree']) && !empty($_REQUEST['imageThree'])){
			unlink('./uploads/'.$_REQUEST['imageThree']);
		}
		
		$result = $this->Ajax_Model->editCompComment($_REQUEST['content'],$_REQUEST['cmntImgOne'],$_REQUEST['captionOne'],$_REQUEST['cmntImgTwo'],$_REQUEST['captionTwo'],$_REQUEST['cmntImgThree'],$_REQUEST['captionThree'],$_REQUEST['editId']);
		echo $result;
    }
	
	
	public function editPComment(){
		if(empty($_REQUEST['cmntImg']) && !empty($_REQUEST['imageOne'])){
			if(file_exists('./uploads/'.$_REQUEST['imageOne'])){
				unlink('./uploads/'.$_REQUEST['imageOne']);
			}
		}
		
		if(!empty($_REQUEST['cmntImg']) && !empty($_REQUEST['imageOne'])){
			if(file_exists('./uploads/'.$_REQUEST['imageOne'])){
				unlink('./uploads/'.$_REQUEST['imageOne']);
			}
		}
		
		$result = $this->Ajax_Model->editPComment($_REQUEST['content'],$_REQUEST['cmntImg'],$_REQUEST['editId']);
		echo $result;
    }
	
	public function addSingleCompComnt(){
		$result = $this->Ajax_Model->addSingleCompComnt($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntImgOne'],$_REQUEST['captionOne'],$_REQUEST['cmntImgTwo'],$_REQUEST['captionTwo'],$_REQUEST['cmntImgThree'],$_REQUEST['captionThree'],$_REQUEST['title']);
		echo $result;
    }
	
	
	public function addPollCommentOnComment(){
		$result = $this->Ajax_Model->addPollCommentOnComment($_REQUEST['pid'],$_REQUEST['title'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntId']);
		echo $result;
    }
	
	public function editPollCommentOnComment(){
		$result = $this->Ajax_Model->editPollCommentOnComment($_REQUEST['content'],$_REQUEST['editId']);
		echo $result;
    }
	
	public function addPollCommentOnCommentSingle(){
		$result = $this->Ajax_Model->addPollCommentOnCommentSingle($_REQUEST['pid'],$_REQUEST['title'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntId']);
		echo $result;
    }
	
	public function addSinglePollComnt(){
		$result = $this->Ajax_Model->addSinglePollComnt($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['cmntImg'],$_REQUEST['title']);
		echo $result;
    }
	
	public function moreComments(){
		$result = $this->Ajax_Model->moreComments($_REQUEST['pid'],$_REQUEST['userid'],$_REQUEST['content'],$_REQUEST['title'],$_REQUEST['row']);
		echo $result;
    }
	
	public function getFrndList(){
		$userData = $this->session->userdata('userData');
		$result = $this->Ajax_Model->getFrndList($_REQUEST['content'],$userData['userId'],$_REQUEST['frndids']);
		echo $result;
    }

    public function getPollList(){
		if(!$this->session->userdata('userData')){
			$result = $this->Ajax_Model->getPollListBeforeLogin($_REQUEST['fetchby'],$_REQUEST['catid']);
		}else{
			$result = $this->Ajax_Model->getPollListAfterLogin($_REQUEST['fetchby'],$_REQUEST['catid']);
		}
		echo $result;
    }


    public function getPollListAfterLogin(){
		$result = $this->Ajax_Model->getPollListAfterLoginOne($_REQUEST['fetchby'],$_REQUEST['userid'],$_REQUEST['catid']);
		echo $result;
    }


    public function userNameAvailability(){
		$result = $this->Ajax_Model->userNameAvailabe($_REQUEST['username']);
		echo $result;
    }
	
	public function changeUserName(){
		$result = $this->Ajax_Model->userNameChange($_REQUEST['username'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function changeUserPrimaryName(){
		$result = $this->Ajax_Model->userPrimaryNameChange($_REQUEST['firstname'],$_REQUEST['middlename'],$_REQUEST['lastname'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function changeUserContact(){
		$result = $this->Ajax_Model->changeUserContactNum($_REQUEST['contact'],$_REQUEST['onlyme'],$_REQUEST['userid']);
		echo $result;
    }
	
	public function deactivateAccount(){
		$result = $this->Ajax_Model->deactivateUserAccount($_REQUEST['userid']);
		echo $result;
    }
	
	
	public function changeUserOtherInformation(){
		$result = $this->Ajax_Model->userOtherInformation($_REQUEST['overview'],$_REQUEST['city'],$_REQUEST['state'],$_REQUEST['country'],$_REQUEST['dob'],$_REQUEST['gender'],$_REQUEST['onlyme'],$_REQUEST['userid'],$_REQUEST['interest']);
		echo $result;
    }
	
	public function loadMorePoll(){
		$result = $this->Ajax_Model->loadMorePolls($_REQUEST['tbl'],$_REQUEST['limitfrom'],$_REQUEST['catid']);
		echo $result;
    }
	
	public function loadMoreSurveyParti(){
		$result = $this->Ajax_Model->loadMoreSurveyParti($_REQUEST['sid'],$_REQUEST['limitfrom']);
		echo $result;
    }
	
	public function loadMorePollByUser(){
		$userData = $this->session->userdata('userData');
		$result = $this->Ajax_Model->loadMorePollsByUser($_REQUEST['tbl'],$_REQUEST['limitfrom'],$_REQUEST['userid'],$_REQUEST['catid']);
		echo $result;
    }
	
	
	public function add_friend(){
		$getRequestId = $this->Ajax_Model->add_friend_request($_REQUEST['user_id'],$_REQUEST['friend_id']);
		$html = '';
		$userData = $this->session->userdata('userData');
		if(!empty($getRequestId)){
			$html .= '<button type="button" value="" class="confirm friend_request_send" style="opacity:0.5;cursor:default;" disabled="disabled"><i class="fa fa-user-plus _rqst_sent"></i><span class="_rqst">Friend Request Send</span></button>';	
		}
		echo $html;
    }
	
	
	public function addAnsSurvey(){
		if(isset($_REQUEST['qidOne'])){
			$qidOne = $_REQUEST['qidOne'];
			$ansOne = $_REQUEST['ansOne'];
		}else{
			$qidOne = '';
			$ansOne = '';	
		}
		
		if(isset($_REQUEST['qidTwo'])){
			$qidTwo = $_REQUEST['qidTwo'];
			$ansTwo = $_REQUEST['ansTwo'];
		}else{
			$qidTwo = '';
			$ansTwo = '';	
		}
		
		if(isset($_REQUEST['qidThree'])){
			$qidThree = $_REQUEST['qidThree'];
			$ansThree = $_REQUEST['ansThree'];
		}else{
			$qidThree = '';
			$ansThree = '';	
		}
		
		if(isset($_REQUEST['qidFour'])){
			$qidFour = $_REQUEST['qidFour'];
			$ansFour = $_REQUEST['ansFour'];
		}else{
			$qidFour = '';
			$ansFour = '';	
		}
		
		$result = $this->Ajax_Model->addAnsSurvey($qidOne,$ansOne,$qidTwo,$ansTwo,$qidThree,$ansThree,$qidFour,$ansFour,$_REQUEST['sid'],$_REQUEST['userid']);
		echo $result;
    }
}
