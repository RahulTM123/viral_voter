<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminPoll_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function addNewPoll($data, $alluser){
		if(!empty($data['videolink'])){
			$videoArr = explode('v=',$qvideolink);
			$qvideolink = 'https://www.youtube.com/embed/'.$videoArr[1];
		}
		
		$start_date = date('Y-m-d', strtotime(str_replace('/','-',$data['start_date'])));
		$end_date = date('Y-m-d', strtotime(str_replace('/','-',$data['end_date'])));
		$this->db->query('insert into vv_adminpolls (catid, question, imag, videourl, city, state, country, start_date, end_date, status, created) values ("'.$data['poll_category'].'", "'.$data['ques'].'", "'.$data['quesI'].'", "'.$data['videolink'].'", "'.$data['ctid'].'", "'.$data['sid'].'", "'.$data['cid'].'", "'.$start_date.'", "'.$end_date.'", "1", now())');
		$pollid = $this->db->insert_id();
		
		if(!empty($pollid)){
			if(!empty($data['ans1']) || !empty($data['ans1I'])) {
				$this->db->query('insert into vv_adminpolloption (option_id, pollid, title, img, created) values ("1", "'.$pollid.'", "'.$data['ans1'].'", "'.$data['ans1I'].'", now())');
			}
			if(!empty($data['ans2']) || !empty($data['ans2I'])) {
				$this->db->query('insert into vv_adminpolloption (option_id, pollid, title, img, created) values ("2", "'.$pollid.'", "'.$data['ans2'].'", "'.$data['ans2I'].'", now())');
			}
			if(!empty($data['ans3']) || !empty($data['ans3I'])) {
				$this->db->query('insert into vv_adminpolloption (option_id, pollid, title, img, created) values ("3", "'.$pollid.'", "'.$data['ans3'].'", "'.$data['ans3I'].'", now())');
			}
			if(!empty($data['ans4']) || !empty($data['ans4I'])) {
				$this->db->query('insert into vv_adminpolloption (option_id, pollid, title, img, created) values ("4", "'.$pollid.'", "'.$data['ans4'].'", "'.$data['ans4I'].'", now())');
			}
		}
		if(!empty($alluser)) {
			foreach($alluser as $user) {
				$dataarr = array(
					'userid' => $user,
					'reference' => 'adminpolls',
					'refid' => $pollid,
					'textdesc' => 'Admin created poll',
					'create_date' => date('Y-m-d H:i:s'),
					'status' => '1'
				);
				$this->db->insert('vv_timeline',$dataarr);
			}
		}
		return  1;
    }
	
	public function getAdminPollList(){
		/*$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$linkArray = explode('&page',$actual_link);
		$actual_link = $linkArray[0];
		$html = '';
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		
		if($catFil != '' && $voteFil != ''){
			$sql ="SELECT * FROM  `vv_adminpollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != ''){
			
			$sql ="SELECT * FROM `vv_adminpolls` WHERE catid = '".$catFil."'";
			
			if($_REQUEST['cid']!='')
				$sql .=" and country = '".$_REQUEST['cid']."'";
			if($_REQUEST['sid']!='')
				$sql .=" and state = '".$_REQUEST['sid']."'";
			if($_REQUEST['ctid']!='')
				$sql .=" and city = '".$_REQUEST['ctid']."'";
			
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT * FROM `vv_adminpolls` WHERE catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();
		}else{
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}
		}
		
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Start Date</th><th>Category</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td>'.$i.'</td>';
				
				if($value->question != ''){
					$html .= '<td>'.$value->question.'</td>';	
				}elseif($value->imag != ''){
					$html .= '<td><img src="'.base_url().'uploads/'.$value->imag.'" style="width:60px" class="img-fluid"/></td>';	
				}else{
					$videoImg = str_replace("https://www.youtube.com/embed/","",$value->videourl);
					$html .= '<td><img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" width="60" class="img-fluid"/></td>';	
				}
				
				$html .= '<td>'.date('M, d Y',strtotime($value->start_date)).'</td>';
				
				$sql ="SELECT cat_name FROM vv_categories WHERE id = ".$value->catid;
				$query = $this->db->query($sql);
				$catName = $query->result();
				
				$html .= '<td>'.$catName[0]->cat_name.'</td>';
				
				if($value->status == 1){
					$html .= '<td class="tdClass"><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activated</p></a><a href="admin-poll/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}else{
					$html .= '<td class="tdClass"><a href="admin-poll/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivated</p></a></td>';
				}
				$html .= '<td class="tdClass"><a href="edit-adminpoll/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen btn-success">Edit</p></a><a href="admin-poll/delete?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
			
		}else{
			$html .= '<tr><td colspan="6">No Result Found!</td></tr>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=".$actual_link."&page=".($prev).">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
					$html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
				   $html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
			   }else{
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=".$actual_link."&page=".($next).">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;*/
		$result = array();
		$sql ="select vv_adminpolls.*, upolls.*, vv_categories.cat_name, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname from vv_adminpolls left join (select vv_adminpollvoting.pollid from vv_adminpollvoting inner join vv_users on vv_adminpollvoting.userid = vv_users.id group by vv_adminpollvoting.pollid) as upolls on vv_adminpolls.id = upolls.pollid left join vv_cities on vv_adminpolls.city = vv_cities.id left join vv_states on vv_adminpolls.state = vv_states.id left join vv_countries on vv_adminpolls.country = vv_countries.id inner join vv_categories on vv_adminpolls.catid = vv_categories.id where 1";
		
		//echo $sql;
		//die('hello');
		$numrows = $this->db->query($sql);
		
		$sql .=" order by id desc limit 0, 100";
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }
	
	
	public function getAdminPollListByFilter($cat,$cid,$sid,$ctid,$gender,$voteFilter,$participation,$dfrom,$dto,$keyword,$page,$limit=100){
		/*$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$linkArray = explode('&page',$actual_link);
		$actual_link = $linkArray[0];
		$html = '';
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		
		if($catFil != '' && $voteFil != ''){
			$sql ="SELECT * FROM  `vv_adminpollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != ''){
			
			$sql ="SELECT * FROM `vv_adminpolls` WHERE catid = '".$catFil."'";
			
			if($_REQUEST['cid']!='')
				$sql .=" and country = '".$_REQUEST['cid']."'";
			if($_REQUEST['sid']!='')
				$sql .=" and state = '".$_REQUEST['sid']."'";
			if($_REQUEST['ctid']!='')
				$sql .=" and city = '".$_REQUEST['ctid']."'";
			
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT * FROM `vv_adminpolls` WHERE catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();
		}else{
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.* FROM `vv_adminpolls` as p LEFT JOIN `vv_adminpollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}
		}
		
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Start Date</th><th>Category</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td>'.$i.'</td>';
				
				if($value->question != ''){
					$html .= '<td>'.$value->question.'</td>';	
				}elseif($value->imag != ''){
					$html .= '<td><img src="'.base_url().'uploads/'.$value->imag.'" style="width:60px" class="img-fluid"/></td>';	
				}else{
					$videoImg = str_replace("https://www.youtube.com/embed/","",$value->videourl);
					$html .= '<td><img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" width="60" class="img-fluid"/></td>';	
				}
				
				$html .= '<td>'.date('M, d Y',strtotime($value->start_date)).'</td>';
				
				$sql ="SELECT cat_name FROM vv_categories WHERE id = ".$value->catid;
				$query = $this->db->query($sql);
				$catName = $query->result();
				
				$html .= '<td>'.$catName[0]->cat_name.'</td>';
				
				if($value->status == 1){
					$html .= '<td class="tdClass"><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activated</p></a><a href="admin-poll/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}else{
					$html .= '<td class="tdClass"><a href="admin-poll/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivated</p></a></td>';
				}
				$html .= '<td class="tdClass"><a href="edit-adminpoll/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen btn-success">Edit</p></a><a href="admin-poll/delete?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
			
		}else{
			$html .= '<tr><td colspan="6">No Result Found!</td></tr>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=".$actual_link."&page=".($prev).">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
					$html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
				   $html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
			   }else{
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=".$actual_link."&page=".($next).">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;*/
		$html = '';
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		$sql ="select vv_adminpolls.*, upolls.*, vv_categories.cat_name, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname from vv_adminpolls left join (select vv_adminpollvoting.pollid from vv_adminpollvoting inner join vv_users on vv_adminpollvoting.userid = vv_users.id group by vv_adminpollvoting.pollid) as upolls on vv_adminpolls.id = upolls.pollid left join vv_cities on vv_adminpolls.city = vv_cities.id left join vv_states on vv_adminpolls.state = vv_states.id left join vv_countries on vv_adminpolls.country = vv_countries.id inner join vv_categories on vv_adminpolls.catid = vv_categories.id where 1";
		if($cid!='')
			$sql .=" and vv_adminpolls.country = '".$cid."'";
		if($sid!='')
			$sql .=" and vv_adminpolls.state = '".$sid."'";
		if($ctid!='')
			$sql .=" and vv_adminpolls.city = '".$ctid."'";
		if($gender!='')
			$sql .=" and upolls.gender = '".$gender."'";
		if($cat!='')
			$sql .=" and vv_adminpolls.catid = '".$cat."'";
		if($keyword!='')
			$sql .=" and vv_adminpolls.question like '".$keyword."'";
		
		//echo $sql;
		//die('hello');
		$numrows = $this->db->query($sql);
		
		$sql .=" order by";
		if($voteFilter!='') {
			if($voteFilter == 'high')
				$sql .=" upvote desc, ";
			else
				$sql .=" upvote asc, ";
		}
		if($participation!='') {
			if($participation == 'high')
				$sql .=" participation desc, ";
			else
				$sql .=" participation asc, ";
		}
		$sql .=" id desc limit $start, $limit";
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }
	
	
	public function changeAdminPollStatus($id){
		$sql ="SELECT status FROM vv_adminpolls WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_adminpolls SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_adminpolls SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function deleteAdminPoll($id){
		$sql ="SELECT * FROM vv_adminpolls WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$poll ="SELECT * FROM vv_adminpolloption WHERE pollid=".$id;
		$queryopt = $this->db->query($poll);
		$pollopt = $queryopt->result();
		
		$qunimag = $value[0]->imag;
		if(!empty($qunimag)){
			unlink('./uploads/'.$qunimag);
		}
		
		
		foreach($pollopt as $polls){
			$aunserdata = unserialize($polls->title);
			if(count($aunserdata) == '2'){
				unlink('./uploads/'.$aunserdata[1]);
			}else{
				$ext = strtolower(pathinfo($aunserdata[0], PATHINFO_EXTENSION));
				if(!empty($ext)){
					unlink('./uploads/'.$aunserdata[0]);
				}
			}
		}
		
		
		
		$sql ="DELETE FROM vv_adminpolls WHERE id=".$id;
		$this->db->query($sql);
		
		$sql ="DELETE FROM vv_adminpolloption WHERE pollid=".$id;
		$this->db->query($sql);
		
		return 1;
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function addPollVoting($pollid,$pollOpt,$userid,$ip){
		$this->load->model('Dashboard_model');
		$friends = $this->Dashboard_model->getUserFrndIds($userid);
		//print_r($friends);
		$input = array('pollid' => $pollid, 'voting_option' => $pollOpt, 'userid' => $userid, 'userip' => $ip);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_adminpollvoting', $input);
		$insertId = $this->db->insert_id();
		
		$sql = 'update vv_adminpolloption set votes = votes + 1 where pollid = '.$pollid.' and option_id = '.$pollOpt;
		$this->db->query($sql);
		
		$sql = 'update vv_adminpolls set participation = participation + 1 where id = '.$pollid;
		$this->db->query($sql);
		
		if($friends) {
			$fr = explode(',', $friends);
			for($i=0;$i<count($fr);$i++) {
				$dataarr = array(
					'userid' => $fr[$i],
					'messageby' => $userid,
					'refid' => $pollid,
					'reference' => 'adminpolls',
					'textdesc' => ' participated on Admin polls',
					'create_date' => date('Y-m-d H:i:s'),
					'status' => '1'
				);
				$this->db->insert('vv_messages',$dataarr);
			}
		}
			
		return  $insertId;
    }
	
	public function getPollCount($pollid){
		$sql ="select vv_adminpolls.*, count(vv_adminpollvoting.id) as voting from vv_adminpolls left join vv_adminpollvoting on vv_adminpolls.id = vv_adminpollvoting.pollid where vv_adminpolls.id = ".$pollid;
		$result = $this->db->query($sql)->row()->voting;
		$value = ($result)?$result:0;
		return $value;
    }
	
	public function getPollDetails($pollid){
		$sql ="select vv_adminpolls.*, vv_adminpolloption.title as options, vv_adminpolloption.img, vv_adminpolloption.votes, vv_adminpolloption.option_id, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname, vv_categories.cat_name from vv_adminpolls inner join vv_adminpolloption on vv_adminpolls.id = vv_adminpolloption.pollid inner join vv_categories on vv_adminpolls.catid = vv_categories.id left join vv_cities on vv_adminpolls.city = vv_cities.id left join vv_states on vv_adminpolls.state = vv_states.id left join vv_countries on vv_adminpolls.country = vv_countries.id where vv_adminpolls.id = ".$pollid;
		$value = $this->db->query($sql)->result();
		return $value;
    }
	
	public function getUserDetails($pollid){
		$sql ="select vv_users.id, firstname, lastname, username, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname, gender, vv_adminpollvoting.* from vv_users inner join vv_adminpollvoting on vv_users.id = vv_adminpollvoting.userid left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id where vv_adminpollvoting.pollid = ".$pollid;
		$value = $this->db->query($sql)->result();
		return $value;
    }
	
	public function getAdminDetail() {
		$sql ="SELECT fullname, img FROM `vv_login`";
		$query = $this->db->query($sql);
		$result = $query->row();
		return $result;
	}
	
	public function getParticipation($pollid, $start, $record) {
		$sql ="SELECT vv_adminpollvoting.*, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url FROM `vv_adminpollvoting` inner join vv_users on vv_adminpollvoting.userid = vv_users.id where pollid = ".$pollid." limit ".$start.", ".$record;
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	
}