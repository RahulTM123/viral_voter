<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AjaxSS_admin extends CI_Model{
	function __construct() {
		$this->load->database();
	}
	
	public function deleteUser($userid, $deltext) {
		if($userid != '') {
			$sql ="update vv_users set status = 3 where id = '".$userid."%'";
			$this->db->query($sql);
			
			$sql ="select email, concat(firstname, lastname) as fullname from vv_users where id = '".$userid."%'";
			$query = $this->db->query($sql);
			$email = $query->row_array();
			
			
			$body = '<table width="600" border="0" cellspacing="0" cellpadding="10">
					<tr><td style="background: #03b4da; padding: 10px 10px;"><img src="'.base_url().'assets/front/images/viral_logo_new.png" width="275px;"></td></tr>
					<tr><td>
						<p>Dear Member,</p>
						
						
						<p>'.nl2br($deltext).'</p>
						
						<p>Team ViralVoters<br /><a href="'.base_url().'">ViralVoters.com</a></p>
						<p>Please do not reply to this email.</p>
						
					</td></tr>
					</table>';
			$subject = 'Deleted from ViralVoters.com';
			$result = $this->phpmailer($email['email'], $email['fullname'], $body, $subject);
		}
		else
			return false;
	}
	
	public function getCommentList($id, $page, $ref){
		$value = array();
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_comment.status = 1 and reftype = '".$ref."' and vv_comment.refid = ".$id." order by vv_comment.id desc";
		$query = $this->db->query($sql);
		$values = $query->result_array();
		foreach($values as $vl) {
			$subsql = "select count(id) as subcomment from vv_comment where refid = ".$vl['id']." and reftype='comment'";
			$subque = $this->db->query($subsql);
			$result = $subque->row_array();
			$vl['subcomment'] = $result['subcomment'];
			$value[] = $vl;
		}
		return $value;
    }
	
	public function phpmailer($email, $name, $body, $subject){
		$smtpDetails = $this->user->getSmtpDetails();
        $mail = $this->phpmailerlib->load();
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP             // Set mailer to use SMTP
				$mail->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = $smtpDetails[0]->username;                 // SMTP username
				$mail->Password = $smtpDetails[0]->password;                           // SMTP password
				$mail->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to con
				//Recipients
				$mail->setFrom($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
				$mail->addAddress($email, $name);            // Name is optional
				$mail->addReplyTo($smtpDetails[0]->replyTo);
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $subject;
				$mail->Body = $body;
	
				$mail->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}
	}
}