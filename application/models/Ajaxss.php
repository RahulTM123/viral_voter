<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class AjaxSS extends CI_Model{
	function __construct() {
		$this->load->database();
	}
	
	public function getAjaxCities($cid, $keyword) {
		if($cid != '') {
			$sql ="select vv_cities.id, vv_cities.name as city_name, vv_cities.state_id, vv_cstates.name as city_state from vv_cities inner join (select vv_states.* from vv_states inner join vv_countries on vv_states.country_id = vv_countries.id where vv_countries.status = 1 and vv_states.status = 1 and vv_countries.id = ".$cid.") as vv_cstates on vv_cities.state_id = vv_cstates.id where vv_cities.status = 1 and vv_cities.name like '".$keyword."%' order by vv_cities.name";
			$query = $this->db->query($sql);
			return $query->result();
		}
		else
			return false;
	}
	
	public function getUserMessage($userid) {
  

		if($userid != '') {
			$sql ="update vv_messages set notify_status = 0 where userid = ".$userid." and notify_status = 1";
			$query = $this->db->query($sql);


			
			$sql ="select vv_messages.*, vv_users.profile_pic_url, vv_users.firstname, vv_users.lastname, vv_users.username
				   from vv_messages
				   inner join vv_users on vv_messages.messageby = vv_users.id
				   where vv_messages.userid = ".$userid."
				   order by id desc limit 0,7";



			$query = $this->db->query($sql);
			return $query->result();
		}
		else
			return false;
	}


	public function getAdminMessage($userid) {


			 $user_id=$this->session->userdata('userData')['userId'];
				$get_id=$this->db->select('*')
		                 ->where('id',$user_id)
		                 ->get('vv_users')->row();

		   /*   $interest = $get_id->interest;
		      if($interest == NULL){
		      	print_r($interest = 0);


		      }

		      else{

		      	$interest = $get_id->interest;

		      }
		      */
		      		$interest =	($get_id->interest == NULL || $get_id->interest == "")? 0 : $get_id->interest ;
		
       	   
		if($userid != '') {
		
			$sql ="update vv_contest set notify_status = 0 where city= $get_id->city  and notify_status = 1";
			$query = $this->db->query($sql);


				

	  $sql = "select id,catid, question,contest_image, created from vv_contest
					where city= $get_id->city or catid IN ($interest) order by id desc" ;
	
	

			$query = $this->db->query($sql);
			return $query->result();
		}
		else
			return false;
	}



	
	public function submitUpvoting($ref, $refid, $userid) {
		$dataarr = array(
				'refid' => $refid,
				'userid' => $userid,
				'reference' => $ref,
				'textdesc' => '',
				'create_date' => date('Y-m-d H:i:s'),
				'modify_date' => '',
				'status' => 1,
				);
		$this->db->insert('vv_upvote',$dataarr);
		
		switch($ref) {
			case 'polls';
				$sql ="update vv_pollls set upvote = upvote+1 where id = ".$refid;
			break;
			
			case 'post';
				$sql ="update vv_post set upvote = upvote+1 where id = ".$refid;
			break;
			
			case 'comment';
				$sql ="update vv_comment set upvote = upvote+1 where id = ".$refid;
			break;
			
			case 'postdata';
				$sql ="update vv_postfile set upvote = upvote+1 where id = ".$refid;
			break;
		}
		$query = $this->db->query($sql);
	}
	
	public function getData($type, $id) {
		$result = array();
		switch($type) {
			case 'polls':
				$sql ="select * from vv_pollls where id = ".$id;
				$query = $this->db->query($sql);
				$result = $query->row();
			break;
			
			case 'user':
				$sql ="SELECT id as userid, CONCAT(firstname, ' ', lastname) as fullname, profile_pic_url, username, verified FROM vv_users WHERE id = ".$id;
				$query = $this->db->query($sql);
				$result = $query->row();
			break;
		}
		
		return $result;
	}
	
	public function getUpvote($type, $id, $userid) {
		$sql ="select count(*) as total from vv_upvote where reference = '".$type."' and refid = '".$id."' and userid = '".$userid."'";
		$query = $this->db->query($sql);
		$result = $query->row();
		
		return $result;
	}
	
	public function getEngagedUserlist($id){
		$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url from vv_users inner join vv_pollvoting on vv_users.id = vv_pollvoting.userid where vv_pollvoting.pollid = ".$id." and status != 0";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getEngagedUserlistBytabs($id){
		$sql = "SELECT * FROM `vv_polloption` where pollid = ".$id."";
		$query = $this->db->query($sql);
		$options = $query->result_array();
		$value['options'] = $options;
		
		foreach($options as $op) {
			$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_pollvoting on vv_users.id = vv_pollvoting.userid where vv_pollvoting.pollid = ".$id." and vv_pollvoting.vote = ".$op['option_id']." and status != 0";
			$query = $this->db->query($sql);
			$value['userlist'][$op['option_id']] = $query->result_array();
		}
		
		return $value;
    }
	
	public function getVotedUserlist($type, $typeid){
		switch($type) {
			case 'polls';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'post';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'comment';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'postdata';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
		}
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getCommentedUserlist($type, $typeid){
		switch($type) {
			case 'polls';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_comment.id as cid from vv_users inner join vv_comment on vv_users.id = vv_comment.userid where vv_comment.pid = ".$typeid." and vv_comment.ptype='".$type."' group by vv_users.id";
			break;
			case 'post';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_comment.id as cid from vv_users inner join vv_comment on vv_users.id = vv_comment.userid where vv_comment.pid = ".$typeid." and vv_comment.ptype='".$type."' group by vv_users.id";
			break;
			case 'postdata';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_comment.id as cid from vv_users inner join vv_comment on vv_users.id = vv_comment.userid where vv_comment.pid = ".$typeid." and vv_comment.ptype='".$type."' group by vv_users.id";
			break;
		}
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getSharingUserlist($type, $typeid){
		switch($type) {
			case 'polls';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_share.id as sid, vv_share.shareref, vv_share.create_date as shdate from vv_users inner join vv_share on vv_users.id = vv_share.userid where vv_share.refid = ".$typeid." and vv_share.reftype='".$type."'";
			break;
			case 'post';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_share.id as sid, vv_share.shareref, vv_share.create_date as shdate from vv_users inner join vv_share on vv_users.id = vv_share.userid where vv_share.refid = ".$typeid." and vv_share.reftype='".$type."'";
			break;
			case 'postdata';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified, refid, reftype, vv_share.id as sid, vv_share.shareref, vv_share.create_date as shdate from vv_users inner join vv_share on vv_users.id = vv_share.userid where vv_share.refid = ".$typeid." and vv_share.reftype='".$type."'";
			break;
		}	
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getSharedUserlist($type, $typeid){
		switch($type) {
			case 'polls';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'post';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'comment';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
			case 'postdata';
				$sql = "select vv_users.id, firstname, lastname, username, profile_pic_url, verified from vv_users inner join vv_upvote on vv_users.id = vv_upvote.userid where vv_upvote.refid = ".$typeid." and vv_upvote.reference='".$type."'";
			break;
		}
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function isFriend($id, $userid){
		$sql = "select id from vv_friends where ((friend_one = ".$id." and friend_two = ".$userid.") or (friend_one = ".$userid." and friend_two = ".$id.")) and friend_one != friend_two";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	public function getComment($userid, $id){
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, vv_users.verified FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_users.id = '".$userid."' and vv_comment.status = 1 and vv_comment.id = ".$id." limit 1";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	public function getSubComments($ref, $id){
		$val = 0;
		$html = '';
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, vv_users.verified FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where reftype = '".$ref."' and vv_comment.status = 1 and vv_comment.refid = ".$id." order by vv_comment.id desc";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function insertComments($userid, $comment, $refid, $reftype, $tagged){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);
		
		$pid = $refid;
		$ptype = $reftype;
		if($reftype == 'comment') {
			$pref = $this->ajaxss->getSuperReference($refid);
			$pid = $pref['refid'];
			$ptype = $pref['reftype'];
		}

		$sql ="insert into vv_comment (userid, refid, reftype, created, pid, ptype, userip, userlog) values ('".$userid."', '".$refid."', '".$reftype."', now(), '".$pid."', '".$ptype."', '".$server."', '".$iplog."')";
		$this->db->query($sql);
		$comid = $this->db->insert_id();
		
		$sql ="insert into vv_messages (userid, messageby, refid, reference, textdesc, notify_status, readstatus, create_date, status) values ('".$tagged."', '".$userid."', '".$refid."', '".$reftype."', ' tagged you in comment', 1, 0, now(), 1)";
		$this->db->query($sql);
		
		if($comid > 0) {
			$com = addslashes($comment);
			$sql ="insert into vv_commentdata (comment_id, content) values ('".$comid."', '".$com."')";
			$this->db->query($sql);
			
			switch($reftype) {
				case 'polls':
					$sql ="update vv_pollls set comments = (comments+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'post':
					$sql ="update vv_post set comments = (comments+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'comment':
					$rt = $this->ajaxss->getSuperReference($refid);
					if($rt['reftype']=='post') {
						$sql ="update vv_post set comments = (comments+1) where id = ".$rt['refid'];
						$this->db->query($sql);
					}
					if($rt['reftype']=='polls') {
						$sql ="update vv_pollls set comments = (comments+1) where id = ".$rt['refid'];
						$this->db->query($sql);
					}
					if($rt['reftype']=='postdata') {
						$sql ="update vv_postfile set comments = (comments+1) where id = ".$rt['refid'];
						$this->db->query($sql);
					}
				break;
				
				case 'postdata':
					$sql ="update vv_postfile set comments = (comments+1) where id = ".$refid;
					$this->db->query($sql);
				break;
			}
		}
		$this->session->set_flashdata('msg', 'New comment successfully posted');
		return $comid;
    }
	
	public function insertSharing($userid, $reftype, $refid, $url, $shtype, $uid){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);

		$sql ="insert into vv_share (userid, refid, reftype, create_date, shareref, userip, userlog) values ('".$userid."', '".$refid."', '".$reftype."', now(), '".$shtype."', '".$server."', '".$iplog."')";
		$this->db->query($sql);
		$sid = $this->db->insert_id();
		
		/*if($shtype == 'viralvoters')
		$sql ="insert into vv_messages (userid, messageby, refid, reference, textdesc, notify_status, readstatus, create_date, status) values ('".$tagged."', '".$userid."', '".$refid."', '".$reftype."', ' tagged you in comment', 1, 0, now(), 1)";
		$this->db->query($sql);*/
		
		if($sid > 0) {
			switch($reftype) {
				case 'polls':
					$sql ="update vv_pollls set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'post':
					$sql ="update vv_post set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'postdata':
					$sql ="update vv_postfile set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
			}
		}
		$this->session->set_flashdata('msg', 'Sharing successfully posted');
		return $sid;
    }
	
	public function friendFriend($userid, $friendid){
                
        //$this->db->insert('Customer_Orders', $data);
        //$this->db->insert('Customer_Orders', $data);

        $sql = "insert into vv_friends (friend_one, friend_two, status, notify_status, created) values ('" . $userid . "', '" . $friendid . "', '0', 1, now())";
        $this->db->query($sql);

        $sql = "insert into vv_follower (userid, followby, followtype, status, create_date) values (" . $friendid . ", " . $userid . ", 0, 1, now())";
        $this->db->query($sql);
        return 1;
    }
	
	public function getCommentList($id, $page, $ref){
		$start = ($page > 0)?($page - 1) * 10 + 4:$page;
		$end = ($page == 0)?4:10;
		$value = array();
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, vv_users.verified FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_comment.status = 1 and reftype = '".$ref."' and vv_comment.refid = ".$id." order by vv_comment.id desc limit ".$start.", ".$end."";
		$query = $this->db->query($sql);
		$values = $query->result_array();
		foreach($values as $vl) {
			$subsql = "select count(id) as subcomment from vv_comment where refid = ".$vl['id']." and reftype='comment'";
			$subque = $this->db->query($subsql);
			$result = $subque->row_array();
			$vl['subcomment'] = $result['subcomment'];
			$value[] = $vl;
		}
		return $value;
    }
	
	public function getOwnerID($id, $ref){
		switch($ref) {
			case 'polls':
				$sql ="select user_id as owner from vv_pollls where id = ".$id."";
			break;
			
			case 'post':
				$sql ="select user_id as owner from vv_post where id = ".$id."";
			break;
		}
		$query = $this->db->query($sql);
		$result = $query->row_array();
		$value = ($result['owner'])?$result['owner']:'';
		return $value;
    }
	
	public function deleteCommentConfirm($userid, $comid){
		$sql = "delete from vv_comment where id = ".$comid." and userid = ".$userid."";
		$this->db->query($sql);
		
		$sql = "delete from vv_commentdata where comment_id = ".$comid."";
		$this->db->query($sql);
    }
	
	public function hideCommentConfirm($userid, $comid){
		$sql = "select hideuser from vv_comment where id = ".$comid."";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		
		if($result)
			$hideuser = $result['hideuser'].','.$userid;
		else
			$hideuser = $userid;
		
		$hideuser = trim($hideuser);
		
		$sql = "update vv_comment set hideuser = '".$hideuser."' where id = ".$comid."";
		$this->db->query($sql);
    }
	
	public function getFileContent($id){
		$sql = "SELECT * FROM `vv_postfile` where id = ".$id."";
		$query = $this->db->query($sql);
		$postimg = $query->row_array();
		
		$postsql = "SELECT vv_post.*, concat(vv_users.firstname, ' ', vv_users.lastname) as fullname, vv_users.username, vv_users.profile_pic_url, vv_users.verified, vv_categories.cat_name as category FROM `vv_post` inner join vv_users on vv_post.user_id = vv_users.id inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$postimg['postid']."";
		$querysql = $this->db->query($postsql);
		$postarr = $querysql->row_array();
		
		$result = array('postimg' => $postimg, 'postarr' => $postarr);
		return $result;
    }
	
	public function deletePollPost($type, $id, $userid){
		$tlsql = "update vv_timeline set status = 2 where refid = ".$id." and reference = '".$type."'";
		$this->db->query($tlsql);
		
		$refsql = '';
		switch($type) {
			case 'polls':
			$refsql = "update vv_pollls set status = 2 where id = ".$id." and user_id = ".$userid."";
			break;
			
			case 'post':
			$refsql = "update vv_post set status = 2 where id = ".$id." and user_id = ".$userid."";
			break;
			
			case 'comment':
			$rt = $this->ajaxss->getSuperReference($id);
			if($rt['reftype']=='post') {
				$sql ="update vv_post set comments = (comments-1) where id = ".$rt['refid'];
				$this->db->query($sql);
			}
			if($rt['reftype']=='polls') {
				$sql ="update vv_pollls set comments = (comments-1) where id = ".$rt['refid'];
				$this->db->query($sql);
			}
			$refsql = "update vv_comment set status = 2 where id = ".$id."";
			break;
		}
		$this->db->query($refsql);
    }
	
	public function deletethisreference($type, $did, $rid, $userid){
		$refsql = '';
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);
		switch($type) {
			case 'timeline':
			$refsql = "update vv_timeline set status = 2 where id = ".$rid." and reference = '".$type."'";
			$this->db->query("insert into vv_deletelogs values ('', '".$type."', '".$rid."', '".$userid."', now(), '".$server."', '".$iplog."', 1)");
			break;
			
			case 'polls':
			$refsql = "update vv_timeline set status = 2 where id = ".$rid." and reference = '".$type."'";
			$this->db->query("update vv_pollls set status = 2 where id = ".$did." and user_id = ".$userid."");
			$this->db->query("insert into vv_deletelogs values ('', '".$type."', '".$did."', '".$userid."', now(), '".$server."', '".$iplog."', 1)");
			break;
			
			case 'post':
			$refsql = "update vv_timeline set status = 2 where id = ".$rid." and reference = '".$type."'";
			$this->db->query("update vv_post set status = 2 where id = ".$did." and user_id = ".$userid."");
			$this->db->query("insert into vv_deletelogs values ('', '".$type."', '".$did."', '".$userid."', now(), '".$server."', '".$iplog."', 1)");
			break;
			
			case 'comment':
				$rt = $this->ajaxss->getSuperReference($did);
				$this->db->query("insert into vv_deletelogs values ('', '".$type."', '".$did."', '".$userid."', now(), '".$server."', '".$iplog."', 1)");
				if($rt['reftype']=='post') {
					$sql ="update vv_post set comments = (comments-1) where id = ".$rt['refid'];
					$this->db->query($sql);
				}
				if($rt['reftype']=='polls') {
					$sql ="update vv_pollls set comments = (comments-1) where id = ".$rt['refid'];
					$this->db->query($sql);
				}
				$refsql = "update vv_comment set status = 2 where id = ".$did."";
			break;
		}
		$this->db->query($refsql);
    }
	
	public function blockedusers($id, $userid){
		$sql = "delete from vv_friends where (friend_one = ".$id." and friend_two = ".$userid.") or (friend_one = ".$userid." and friend_two = ".$id.") limit 1";
		$this->db->query($sql);
		
		$tlsql = "insert into vv_users_blocked values ('', ".$userid.", ".$id.", now())";
		$this->db->query($tlsql);
    }
	
	public function unblockedusers($id, $userid){
		$tlsql = "delete from vv_users_blocked where userid = ".$userid." and blockusers = ".$id."";
		$this->db->query($tlsql);
    }
	
	public function checkblock($userid, $cid, $type){
		$blocked = array();
		
		switch($type) {
			case 'polls':			
			$sql = 'select user_id from vv_pollls where id = '.$cid;
			$query = $this->db->query($sql);
			$uid = $query->row_array();
		
			if($userid == $uid['user_id']) {
				$blocked['commenting'] = 0;
				return $blocked;
			}
			
			$comsql = 'select commenting from vv_usermeta where userid = '.$uid['user_id'];
			$comquery = $this->db->query($comsql);
			$commenting = $comquery->row_array();
			$blocked['commenting'] = $commenting['commenting'];
			
			if($commenting['commenting']) {
				$frsql ="SELECT * FROM vv_friends WHERE ((friend_one = '".$uid['user_id']."' and friend_two = '".$userid."') or  (friend_one = '".$userid."' and friend_two = '".$uid['user_id']."')) && status = '1'";
				$frquery = $this->db->query($frsql);
				$fr = $frquery->num_rows();
				$blocked['friend'] = $fr;
			}
			break;
			
			case 'post':			
			$sql = 'select user_id from vv_post where id = '.$cid;
			$query = $this->db->query($sql);
			$uid = $query->row_array();
		
			if($userid == $uid['user_id']) {
				$blocked['commenting'] = 0;
				return $blocked;
			}
			
			$comsql = 'select commenting from vv_usermeta where userid = '.$uid['user_id'];
			$comquery = $this->db->query($comsql);
			$commenting = $comquery->row_array();
			$blocked['commenting'] = $commenting['commenting'];
			
			if($commenting['commenting']) {
				$frsql ="SELECT * FROM vv_friends WHERE ((friend_one = '".$uid['user_id']."' and friend_two = '".$userid."') or  (friend_one = '".$userid."' and friend_two = '".$uid['user_id']."')) && status = '1'";
				$frquery = $this->db->query($frsql);
				$fr = $frquery->num_rows();
				$blocked['friend'] = $fr;
			}
			break;
			
			case 'comment':			
			$sql = 'select userid from vv_comment where id = '.$cid;
			$query = $this->db->query($sql);
			$uid = $query->row_array();
		
			if($userid == $uid['userid']) {
				$blocked['commenting'] = 0;
				return $blocked;
			}
			
			$comsql = 'select commenting from vv_usermeta where userid = '.$uid['userid'];
			$comquery = $this->db->query($comsql);
			$commenting = $comquery->row_array();
			$blocked['commenting'] = $commenting['commenting'];
			
			if($commenting['commenting']) {
				$frsql ="SELECT * FROM vv_friends WHERE ((friend_one = '".$uid['userid']."' and friend_two = '".$userid."') or  (friend_one = '".$userid."' and friend_two = '".$uid['userid']."')) && status = '1'";
				$frquery = $this->db->query($frsql);
				$fr = $frquery->num_rows();
				$blocked['friend'] = $fr;
			}
			break;
			
			case 'postdata':			
			$sql = 'select userid from vv_postfile where id = '.$cid;
			$query = $this->db->query($sql);
			$uid = $query->row_array();
		
			if($userid == $uid['userid']) {
				$blocked['commenting'] = 0;
				return $blocked;
			}
			
			$comsql = 'select commenting from vv_usermeta where userid = '.$uid['userid'];
			$comquery = $this->db->query($comsql);
			$commenting = $comquery->row_array();
			$blocked['commenting'] = $commenting['commenting'];
			
			if($commenting['commenting']) {
				$frsql ="SELECT * FROM vv_friends WHERE ((friend_one = '".$uid['userid']."' and friend_two = '".$userid."') or  (friend_one = '".$userid."' and friend_two = '".$uid['userid']."')) && status = '1'";
				$frquery = $this->db->query($frsql);
				$fr = $frquery->num_rows();
				$blocked['friend'] = $fr;
			}
			break;
		}
		return $blocked;
    }
	
	public function searchfriendsbyname($keyword, $userid){
		$keys = explode(' ', $keyword);
		$names = '';
		$frlist = '';
		for($i=0;$i<count($keys);$i++) {
			$names .= ' firstname like "%'.$keys[$i].'%" or lastname like "%'.$keys[$i].'%" or ';
		}
		$names = rtrim($names, ' or ');
		
		$sqlfo = "SELECT vv_users.id, vv_users.oauth_provider, vv_users.picture_url, vv_users.profile_pic_url, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.verified FROM `vv_friends` inner join vv_users on vv_friends.friend_one = vv_users.id where (vv_friends.friend_one = ".$userid." or vv_friends.friend_two = ".$userid.") and vv_friends.friend_one != vv_friends.friend_two and (".$names.")";
		$queryfo = $this->db->query($sqlfo);
		$resultfo = $queryfo->result_array();
		foreach($resultfo as $res) {
			$frlist[$res['id']] = $res;
		}
		
		$sqlfs = "SELECT vv_users.id, vv_users.oauth_provider, vv_users.picture_url, vv_users.profile_pic_url, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.verified FROM `vv_friends` inner join vv_users on vv_friends.friend_two = vv_users.id where (vv_friends.friend_one = ".$userid." or vv_friends.friend_two = ".$userid.") and vv_friends.friend_one != vv_friends.friend_two and (".$names.")";
		$queryfs = $this->db->query($sqlfs);
		$resultfs = $queryfs->result_array();
		foreach($resultfs as $res) {
			$frlist[$res['id']] = $res;
		}
		return $frlist;
    }
	
	public function checkusername_availability($email, $userid){
		$sql = "select id, email from vv_users where email = '".$email."'";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		$available = '';
		if($query->num_rows()) {
			if($result['id'] == $userid) {
				$available = 'own';
			}
			else {
				$available = 'no';
			}
		}
		else {
			$available = 'yes';
		}
		return $available;
    }
	
	public function getSuperReference($refid){
		$sql = "select refid, reftype from vv_comment where id = ".$refid."";
		$query = $this->db->query($sql);
		$result = $query->row_array();
		
		if($result['reftype']=='comment')
			$result = $this->ajaxss->getSuperReference($result['refid']);
		
		return $result;
    }
	
	public function unfriendme($id, $userid){
		$sql = "delete from vv_friends where (friend_one = ".$id." and friend_two = ".$userid.") or (friend_one = ".$userid." and friend_two = ".$id.") limit 1";
		$this->db->query($sql);
    }
	
	public function startfollowing($fr, $ty, $userid){
		$sql = "select * from vv_follower where followby = ".$userid." and userid = ".$fr."";
		$query = $this->db->query($sql);
		
		if($query->num_rows()) {
			$sql = "update vv_follower set followtype = ".$ty." where followby = ".$userid." and userid = ".$fr."";
			$this->db->query($sql);
		}
		else {
			$sql = "insert into vv_follower (userid, followby, followtype, status, create_date) values (".$fr.", ".$userid.", ".$ty.", 1, now())";
			$this->db->query($sql);
		}
    }
	
	public function unfollowing($fr, $userid){
		$sql = "delete from vv_follower where followby = ".$userid." and userid = ".$fr."";
		$query = $this->db->query($sql);
    }
	
	public function friendRequest($userid){
		$sql = "SELECT * FROM `vv_friends` where friend_two = ".$userid." and status = '0' and notify_status = 1";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
    }
	
	public function notificationActive($userid){
		$sql = "SELECT * FROM `vv_messages` where userid = ".$userid." and notify_status = 1";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
    }


    public function notificationActive_admin($userid){

    	 $user_id=$this->session->userdata('userData')['userId'];
				$get_id=$this->db->select('*')
		                 ->where('id',$user_id)
		                 ->get('vv_users')->row();

	  	

		$sql = "SELECT * FROM `vv_contest` where city = $get_id->city and status = 1 and notify_status = 1";
		$query = $this->db->query($sql);
		
		return $query->num_rows();
    }
	
	public function vvpostpollshare($userid, $refid, $reftype, $tid){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);

		$create_date = date('Y-m-d H:i:s');
		switch($reftype) {
			case 'poll':
				$sql = "SELECT vv_pollls.*, vv_users.firstname, vv_users.lastname, vv_users.username, vv_categories.cat_name FROM vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$refid."";
				$query = $this->db->query($sql);
				$result = $query->row_array();
				
				if($result['username'])
					$prourl = base_url().''.$result['username'];
				else
					$prourl = base_url().'user/id/'.$result['id'];
				
				$textdesc = ' shared a <a href="'.base_url().'poll/'.$refid.'">Poll</a>';
				$dataarr = array(
					'userid' => $userid,
					'refid' => $tid,
					'reference' => 'timeline',
					'textdesc' => $textdesc,
					'shareby' => $userid,
					'create_date' => $create_date,
					'status' => '1'
				);
				$this->db->insert('vv_timeline',$dataarr);
			break;
			case 'post':
				$sql = "SELECT vv_post.*, vv_users.firstname, vv_users.lastname, vv_users.username, vv_categories.cat_name FROM vv_post inner join vv_users on vv_post.user_id = vv_users.id inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$refid."";
				$query = $this->db->query($sql);
				$result = $query->row_array();
				
				if($result['username'])
					$prourl = base_url().''.$result['username'];
				else
					$prourl = base_url().'user/id/'.$result['id'];
				
				$textdesc = ' shared a <a href="'.base_url().'post/'.$refid.'">Post</a>';
				$dataarr = array(
					'userid' => $userid,
					'refid' => $tid,
					'reference' => 'timeline',
					'textdesc' => $textdesc,
					'shareby' => $userid,
					'create_date' => $create_date,
					'status' => '1'
				);
				$this->db->insert('vv_timeline',$dataarr);
			break;
		}
		
		$reftype = ($reftype=='poll')?'polls':$reftype;

		$sql ="insert into vv_share (userid, refid, reftype, create_date, shareref, userip, userlog) values ('".$userid."', '".$refid."', '".$reftype."', now(), 'viralvoters', '".$server."', '".$iplog."')";
		$this->db->query($sql);
		$sid = $this->db->insert_id();
		
		if($sid > 0) {
			switch($reftype) {
				case 'polls':
					$sql ="update vv_pollls set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'post':
					$sql ="update vv_post set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'postdata':
					$sql ="update vv_postfile set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
			}
		}
    }
	
	public function fetchingfriends($userid, $name){
		$namearr = explode(' ', $name);
		$result = '';
		if(count($namearr) < 4) {
			$sql ="SELECT GROUP_CONCAT(friend_one, ',', friend_two) as friendid FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
			$query = $this->db->query($sql);
			$friend = $query->row_array();
			
			if($friend['friendid']) {
				$sql = "SELECT * FROM vv_users where id in (".$friend['friendid'].") and (";
				$sqlqr = "";
				for($i=0;$i<count($namearr);$i++) {
					$sqlqr .= " firstname like '".$namearr[$i]."%' or lastname like '".$namearr[$i]."%' or";
				}
				$sql .= trim($sqlqr, 'or');
				$sql .= ")";
				$query = $this->db->query($sql);
				$result = $query->result_array();
			}
			return $result;
		}
    }
	
	public function fetchinguser($name){
		$namearr = explode(' ', $name);
		$result = '';
		if(count($namearr) < 4) {
			$sql = "SELECT * FROM vv_users where id <> 1 and (";
			$sqlqr = "";
			for($i=0;$i<count($namearr);$i++) {
				$sqlqr .= " firstname like '".$namearr[$i]."%' or lastname like '".$namearr[$i]."%' or";
			}
			$sql .= trim($sqlqr, 'or');
			$sql .= ")";
			$query = $this->db->query($sql);
			$result = $query->result_array();
			return $result;
		}
    }
	
	public function friendsrequest($userid){
		$result = '';
		$sql ="SELECT GROUP_CONCAT(friend_one) as friendid FROM vv_friends WHERE (friend_two = '".$userid."') and (friend_one != friend_two) and status = '0'";
		$query = $this->db->query($sql);
		$friend = $query->row_array();
		
		$sql ="update vv_friends set notify_status = 0 WHERE (friend_two = '".$userid."') and (friend_one != friend_two) and status = '0'";
		$query = $this->db->query($sql);
		
		if($friend['friendid']) {
			$sql = "SELECT * FROM vv_users where id in (".$friend['friendid'].") and id != '".$userid."' and status = 1";
			$query = $this->db->query($sql);
			$result = $query->result_array();
		}
		return $result;
    }
	
	public function confirmfriend($uid, $userid){
		$sql ="update vv_friends set status = '1' WHERE (friend_two = '".$userid."') and (friend_one = '".$uid."')";
		$query = $this->db->query($sql);
    }
	
	public function rejectfriend($uid, $userid){
		$sql ="delete from vv_friends WHERE (friend_two = '".$userid."') and (friend_one = '".$uid."') and status = '0'";
		$query = $this->db->query($sql);
    }
	
	public function abusedSubmit($refid, $reftype, $report, $textdesc, $reportby){
		$sql ="insert into vv_report (refid, reftype, report, textdesc, userip, created, reportby) values (".$refid.", '".$reftype."', '".$report."', '".$textdesc."', '".$_SERVER['REMOTE_ADDR']."', now(), ".$reportby.")";
		$query = $this->db->query($sql);
    }
	
	public function insertsharesubmit($userid, $postdata, $friends, $refid, $reftype, $tid, $fid){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);

		$sql ="insert into vv_share (userid, refid, reftype, create_date, shareref, userip, userlog) values ('".$userid."', '".$refid."', '".$reftype."', now(), 'viralvoters', '".$server."', '".$iplog."')";
		$this->db->query($sql);
		$sid = $this->db->insert_id();
		
		if($sid > 0) {
			switch($reftype) {
				case 'polls':
					$sql ="update vv_pollls set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'post':
					$sql ="update vv_post set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'postdata':
					$sql ="update vv_postfile set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
			}
		}
		
		$create_date = date('Y-m-d H:i:s');

		$sql ="insert into vv_share (userid, refid, reftype, create_date, shareref, userip, userlog) values ('".$userid."', '".$refid."', '".$reftype."', now(), 'viralvoters', '".$server."', '".$iplog."')";
		$this->db->query($sql);
		$sid = $this->db->insert_id();
		
		if($sid > 0) {
			switch($reftype) {
				case 'polls':
					$sql ="update vv_pollls set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'post':
					$sql ="update vv_post set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
				
				case 'postdata':
					$sql ="update vv_postfile set sharing = (sharing+1) where id = ".$refid;
					$this->db->query($sql);
				break;
			}
		}
		
		$with = '';
		
		if(count($friends) && $friends) {
			$tagging = array();
			$looping = 0;
			
			foreach($friends as $tf) {
				if($looping++ > 1) break;
				$tagging[] = (array)$this->Dashboard_model->getUserInfo($tf);
			}
			
			if(count($friends) > 2) {
				$with .= ' with ';
				$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'polls\',\''.$pollid.'\',\''.$userid.'\',\'tagfrn_polls_'.$pollid.'\');">'.(count($tagfrn) - 1).' others</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'polls\',\''.$pollid.'\',\''.$userid.'\',\'tagfrn_polls_'.$pollid.'\');">'.(count($tagfrn) - 1).' others</a> ';
			}
			
			else {
				if(count($friends) == 2) {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and ';
					$with .= ($tagging[1]['username'])?'<a href="'.base_url().''.$tagging[1]['username'].'">'.ucwords($tagging[1]['fullname']).'</a>':'<a href="'.base_url().'user/id/'.$tagging[1]['userid'].'">'.ucwords($tagging[1]['fullname']).'</a>';
				}
				else {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> ';
				}
			}
		}
		
		
		if($reftype=='polls')
			$textdesc = ' shared a <a href="'.base_url().'poll/'.$refid.'">Poll</a>'.$with;
		if($reftype=='post')
			$textdesc = ' shared a <a href="'.base_url().'post/'.$refid.'">Post</a>'.$with;
		
		$shareby = $userid;
		$userid = ($fid!='')?$fid:$userid;
		$dataarr = array(
			'userid' => $userid,
			'refid' => $tid,
			'reference' => 'timeline',
			'textdesc' => $textdesc,
			'shareby' => $shareby,
			'content' => $postdata,
			'create_date' => $create_date,
			'status' => '1'
		);
		$this->db->insert('vv_timeline',$dataarr);
		
		$reftype = ($reftype=='poll')?'polls':$reftype;
    }
	
	public function updateImgtitle($userId, $title, $postdata){
		$sql ="update vv_postfile set title = '".$title."' where id = '".$postdata."' and userid = '".$userId."'";
		$query = $this->db->query($sql);
    }
}