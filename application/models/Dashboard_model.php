<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getAllLogs($userid,$friend_ids){
		$sql = "SELECT * FROM vv_tagfriends WHERE tagid = '$userid'";
		$tagquery = $this->db->query($sql);
		$tagdata = $tagquery->result();
		$tag_ids = array();
		if(!empty($tagdata)){
			foreach($tagdata as $t){
				array_push($tag_ids,$t->tagid);
			}
		}
		
		if(!empty($friend_ids) && !empty($tag_ids)){
			$tids = implode(',',$tag_ids);
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || userid IN (".$friend_ids.") || userid IN (".$tids.") || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}elseif(!empty($friend_ids)){
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || userid IN (".$friend_ids.") || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}elseif(!empty($tag_ids)){
			$tids = implode(',',$tag_ids);
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || userid IN (".$tids.") || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}else{
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }	
    
	public function getRowsData($logcreated,$title,$id,$userid){
		$idNum = $this->session->userdata('idNum');
		$html = '';
		$date = date('m/d/Y');
		//admin poll section start
		/*if($title == 'admin-poll'){
			$opt = 1;
			$val = 0;
			$sql ="SELECT * FROM vv_adminpolls WHERE id = '".$id."' AND (start_date <= '".$date."' AND end_date >= '".$date."') ";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql1 ="SELECT vote FROM vv_adminpollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_adminpolloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<form method="post" enctype="multipart/form-data" action="'.base_url().'/admin/AdminPollControllor/pollVoting?pollid='.$r->id.'"><div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left"><img src="'.base_url("assets/front/images/user.png").'" style="width:40px"></div><div class="media-body"><h6 class="media-heading"><a href="#" class="usr">ViralVoters</a><span class=""> Post a Poll</span></h6><p>'.$pDate.'</p></div></div><div class="row"><div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5>Q. '.$r->question.'</h5></div></div><div class="trand row">';
				
				
				$sql ="SELECT vote FROM vv_adminpollvoting WHERE pollid = '".$r->id."' AND userid = '".$userid."'";
				$query = $this->db->query($sql);
				$getvalue = $query->result();
				
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						if(empty($getvalue)){
							if($opt == 1){
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" checked="checked" class="trnd-radio"/>';
							}else{
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
						}
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								if($opt == 1){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" checked="checked" class="trnd-radio"/>';
								}else{
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
							}
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								if($opt == 1){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" checked="checked" class="trnd-radio"/>';
								}else{
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
							}
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
					$opt++;
				}
				
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				//poll vote counting end here
				//print_r($pollvotearray);
				//print_r($pollperarray);
				$html .= '</div></div><div class="col-md-4 col-sm-4"><div class="vote"><h6>Upvotes</h6></div>';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
				
				if(!empty($getvalue)){
					$html .= '';
				}else{
					$html .= '<input type="submit" class="trand-button" value="Submit Vote" name="pollVote">';
				}
				
				$html .= '</div><div class="button"><input type="hidden" value="'.$userid.'" name="pUserId"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><a href="#"><i class="fa fa-eye"></i></a>10,000</span> <span class="v_likes"><a href="#"><i class="fa fa-thumbs-up"></i></a>500</span> <span class="v_likes"><a href="#"><i class="fa fa-comment"></i></a>600</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class="v_like"><i class="fa fa-share"></i>Share</span></div>  </div></div></article></div></div></form>';
				
			}
		}*/
		//user poll section end
		
		
		//admin compition section start here
		if($title == 'compition'){
			$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					
					$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
					
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'compitition'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "compitition" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "compitition">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="compitition"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="compition_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment_comp lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalse usr_content_block"><a href="#" class=" cancelEdit">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-3 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgOne" id="compImgOne'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					<div class="col-md-3 colMdTwo" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgTwo" id="compImgTwo'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgTwo" class="hiddenCompImgTwo"></span></div>
					<div class="col-md-3 colMdThree" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgThree" id="compImgThree'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgThree" class="hiddenCompImgThree"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveTheComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButton" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>

					<div class="enter"><label title="Attach a photo"><span class="commentPhoto"></span></label></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
			
		}
		//admin compition section end here
		
		
		
		//admin compition shared section start here
		if($title == 'compition_shared'){
			$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($logcreated));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					if($curntusers[0]->username != ''){
						$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
					}
					
					$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared viralvoters <a href="#">Compition</a></span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text" style="font-size:40px;"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
					
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'compitition'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "compitition" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "compitition">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="compitition"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="compition_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment_comp lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalse usr_content_block"><a href="#" class=" cancelEdit">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-3 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgOne" id="compImgOne'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					<div class="col-md-3 colMdTwo" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgTwo" id="compImgTwo'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgTwo" class="hiddenCompImgTwo"></span></div>
					<div class="col-md-3 colMdThree" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgThree" id="compImgThree'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgThree" class="hiddenCompImgThree"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveTheComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButton" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>

					<div class="enter"><label title="Attach a photo"><span class="commentPhoto"></span></label></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//admin compition shared section end here
		
		
		
		
		
		//admin survey section start here
		if($title == 'survey'){
			$sql ="SELECT * FROM vv_survey WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$sql ="SELECT * FROM vv_surveyQustn WHERE sid = ".$id;
					$query = $this->db->query($sql);
					$survey = $query->result();
					
					$sql ="SELECT COUNT(id) as total FROM vv_surveyParti WHERE sid = '".$id."' && userid != '".$userid."'";
					$query = $this->db->query($sql);
					$surveyParti = $query->result();
					
					$sql ="SELECT id FROM vv_surveyParti WHERE sid = '".$id."' && userid = '".$userid."'";
					$query = $this->db->query($sql);
					$surveyPartiResult = $query->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					
					$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> Posted a Survey</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="#" class="questn" style="font-size:20px;">Q. '.$r->question.'</a></p></div>
					
					<div class="col-md-12 surveyParticipate" style="margin-bottom:50px;"> <span>';
					
					if(!empty($surveyPartiResult) && !empty($surveyParti)){
						if($curntusers[0]->username != ''){
							$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
						}
						
						$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a>';
						if($surveyParti[0]->total == 0 ){
							$html .=  ' Participated in this Survey</span> <br /><br />';
						}elseif($surveyParti[0]->total == 1){
							$html .=  ' and '.$surveyParti[0]->total.' member Participated</span> <br /><br />';	
						}else{
							$html .=  ' and '.$surveyParti[0]->total.' members Participated</span> <br /><br />';
						}
					}else{
						if($surveyParti[0]->total < 2 ){
							$html .=  $surveyParti[0]->total.' member Participated</span> <br /><br />';
						}else{
							$html .=  $surveyParti[0]->total.' members Participated</span> <br /><br />';	
						}	
					}
				    
				    if(empty($surveyPartiResult)){
					   if($surveyParti[0]->total > 0){
					   		$html .= '<a href="'.base_url().'view-participation/'.$id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';   
					   }
					   $html .= '<a style="margin-left:-10px;" class="participate"><button type="button" class="trand-button">Participate</button></a>';
				    }else{
						$html .= '<a href="'.base_url().'view-participation/'.$id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';
					}
					 
					 
					$html .= '</div><div class="col-md-12 surveyQuestn" style="margin-bottom:50px; display:none;">';
					$x = 1;
					  foreach($survey as $s){
						  $html .= '<div style="margin-top:30px;"><label>'.$s->question.'</label><div><textarea style="width:100%; background:#fff; height:80px; resize:none; border:1px solid #ddd;" class="survey_u_q survey'.$x.'" qid="'.$s->id.'"></textarea></div></div>';
						  $x++;
					  }
					  
				    $html .= '<div style="margin-top:30px;"><a style="margin-left:-10px;"><button type="button" class="trand-button submitSurvey" sid="'.$id.'">Submit</button></a></div></div>';
					
					
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'survey'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'survey'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'survey'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'survey'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition">  <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "survey" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "survey">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="survey"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="survey_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
			
		}
		//admin survey section end here
		
		
		
		
		
		
		//user post section start here
		if($title == 'user_post'){
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= '</div><div class="media-body"><h6 class="media-heading">';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'post'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Post with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}
						}
					}else{
						if($r->postdata != '' && $r->postimg != ''){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->postdata != '' && $r->videolink!= ''){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->postimg){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->videolink){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}else{
							$html .= ' Created a Post';
						}
					}
					
					
					
					
					$html .= '</span></h6><p>'.$r->publish_date.'</p></div>';
					
					if($userid == $r->user_id){
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
					}
					
					$html .= '</div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">
							  <li class="_vv54ni __vvMenuItem deleteBlock" id="'.$r->id.'" role="post"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<div class="col-md-12 col-sm-12"><div class="row">';
					
					if($r->postdata != '' && $r->postimg != ''){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}elseif($r->postdata != '' && $r->videolink!= ''){
						$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}elseif($r->postdata){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
					}elseif($r->videolink){
						$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%"  height="400px"></iframe></div>';
					}else{
						$html .= '<div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'post_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$sql ="SELECT views FROM vv_postvideos WHERE postid = '".$r->id."'";
					$post = $this->db->query($sql);
					$postViews = $post->result();
					if(!empty($postViews)){
						$view = $postViews[0]->views;
					}else{
						$view = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>'.$view.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "post_like" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "post_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_post"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_post_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user psot section end here
		
		
		//user post share section start here
		if($title == 'user_post_shared'){
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($logcreated));
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					if($curntusers[0]->username != ''){
						$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
					}
					
					$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared ';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname."'s <a href='".base_url()."post/".$r->id."' class='usr'>Post</a></span></h6><p>".$pDate."</p></div>";
					
					if($userid == $r->user_id){
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
					}
					
					$html .= '</div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">
							  <li class="_vv54ni __vvMenuItem deleteSharedBlock" id="'.$id.'" role="post"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<div class="col-md-12 col-sm-12"><div class="row">';
					
					if($r->postdata != '' && $r->postimg != ''){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}elseif($r->postdata != '' && $r->videolink != ''){
						$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}elseif($r->postdata){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
					}elseif($r->videolink){
						$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}else{
						$html .= '<div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'post_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$sql ="SELECT views FROM vv_postvideos WHERE postid = '".$r->id."'";
					$post = $this->db->query($sql);
					$postViews = $post->result();
					if(!empty($postViews)){
						$view = $postViews[0]->views;
					}else{
						$view = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>'.$view.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "post_like" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "post_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_post"><i class="fa fa-comment"></i>Comment</span>  <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_post_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user psot share section end here
		
		
		//user poll section start
		if($title == 'user_poll'){
			//$opt = 1;
			$val = 0;
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
					$query1 = $this->db->query($sql1);
					$pollresult = $query1->result();
					
					$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
					$pollquery = $this->db->query($poll);
					$polloption = $pollquery->result();
					
					
					
									
					$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'poll'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Poll with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}	
						}
					}else{
						$html .= ' Created a Poll';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$pDate.'</p></div>';
					
					if($userid == $r->user_id){
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
					$squery = $this->db->query($sql);
					$totalShare = $squery->result();
					
					$html .= '</div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">';
								
							if(empty($pollresult) && $totalShare[0]->total == 0){
								$html .= '<li class="_vv54ni __vvMenuItem" id="'.$r->id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Edit Poll</span></span></a></li>';	
							}
							$html .= '<li class="_vv54ni __vvMenuItem deleteBlock" id="'.$r->id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<form method="post" enctype="multipart/form-data" action="'.base_url().'Dashboard/pollVoting?pollid='.$r->id.'"><div class="row">';
				
					if($r->question != '' && $r->imag != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}elseif($r->question != '' && $r->videourl != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}elseif($r->question){
						$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
					}elseif($r->videourl){
						$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}else{
						$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}
					
					$html .= '<div class="trand row">';
					
					
					$sql ="SELECT vote FROM vv_pollvoting WHERE pollid = '".$r->id."' AND userid = '".$userid."'";
					$query = $this->db->query($sql);
					$getvalue = $query->result();
					
					foreach($polloption as $pollopt){
						$html .= '<div class="col-md-6 col-sm-6">';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
							}else{
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}
						}
						$html .= '</div>';
					}
					
					//poll vote counting start here
					$pollvotearray = array();
					$pollperarray = array();
					$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
					$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
					if(!empty($pollresult)){
						$totalvote = count($pollresult);
						foreach($pollresult as $p){
							if($p->vote == 1){
								$optonevote++;	
							}
							if($p->vote == 2){
								$opttwovote++;	
							}
							if($p->vote == 3){
								$optthreevote++;	
							}
							if($p->vote == 4){
								$optfourvote++;	
							}
						}
						if($optonevote > 0){
							$optoneper = ($optonevote/$totalvote)*100;
						}
						if($opttwovote > 0){
							$opttwoper = ($opttwovote/$totalvote)*100;
						}
						if($optthreevote > 0){
							$optthreeper = ($optthreevote/$totalvote)*100;
						}
						if($optfourvote > 0){
							$optfourper = ($optfourvote/$totalvote)*100;
						}
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}else{
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}
					$html .= '</div></div><div class="col-md-4 col-sm-4">';
					
					foreach($polloption as $pollopt){
						$img = $text = '';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
							}else{
								$text = $title[0];
							}
						}
						if(!empty($img) && !empty($text)){
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}elseif(!empty($img)){
							$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
						}else{
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}
						$val++;
					}
					
					$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
					
					if(!empty($getvalue)){
						$html .= '';
					}else{
						$html .= '<button type="button" class="pollVote trand-button" value="" name="pollVote" disabled="disabled">Submit Vote</button>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'poll_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div><div class="button"><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div><div class="beforePostSubmit"><div class="beforePostSubmit-content"><div class="beforePostSubmit-header"><span class="beforePostSubmit-close">&times;</span><h2>Want to submit vote?</h2></div><div class="beforePostSubmit-body"><div class="col-md-12" style="text-align:center;"><input type="submit" class="trand-button" value="Yes" name="pollVote" style="width:100px;"/><button type="button" class="trand-button noSubmitVote" style="width:100px;">No</button></div></div></div></div></form>';
					
					$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "poll_like" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "poll_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_poll"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_poll_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					 
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user poll section end
		
		
		
		
		//user poll shared section start
		if($title == 'user_poll_shared'){
			//$opt = 1;
			$val = 0;
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($logcreated));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
					$query1 = $this->db->query($sql1);
					$pollresult = $query1->result();
					
					$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
					$pollquery = $this->db->query($poll);
					$polloption = $pollquery->result();
					
					
					
									
					$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					if($curntusers[0]->username != ''){
						$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
					}
					
					$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared ';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.' <a href="'.base_url().'poll/'.$r->id.'" class="questn">Poll</a></span></h6><p>'.$pDate.'</p></div>';
					
					
					$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div></div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">
							  <li class="_vv54ni __vvMenuItem deleteSharedBlock" id="'.$id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<form method="post" enctype="multipart/form-data" action="'.base_url().'Dashboard/pollVoting?pollid='.$r->id.'"><div class="row">';
				
					if($r->question != '' && $r->imag != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}elseif($r->question != '' && $r->videourl != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}elseif($r->question){
						$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
					}elseif($r->videourl){
						$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}else{
						$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}
					
					$html .= '<div class="trand row">';
					
					
					$sql ="SELECT vote FROM vv_pollvoting WHERE pollid = '".$r->id."' AND userid = '".$userid."'";
					$query = $this->db->query($sql);
					$getvalue = $query->result();
					
					foreach($polloption as $pollopt){
						$html .= '<div class="col-md-6 col-sm-6">';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
							}else{
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}
						}
						$html .= '</div>';
					}
					
					//poll vote counting start here
					$pollvotearray = array();
					$pollperarray = array();
					$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
					$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
					if(!empty($pollresult)){
						$totalvote = count($pollresult);
						foreach($pollresult as $p){
							if($p->vote == 1){
								$optonevote++;	
							}
							if($p->vote == 2){
								$opttwovote++;	
							}
							if($p->vote == 3){
								$optthreevote++;	
							}
							if($p->vote == 4){
								$optfourvote++;	
							}
						}
						if($optonevote > 0){
							$optoneper = ($optonevote/$totalvote)*100;
						}
						if($opttwovote > 0){
							$opttwoper = ($opttwovote/$totalvote)*100;
						}
						if($optthreevote > 0){
							$optthreeper = ($optthreevote/$totalvote)*100;
						}
						if($optfourvote > 0){
							$optfourper = ($optfourvote/$totalvote)*100;
						}
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}else{
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}
					$html .= '</div></div><div class="col-md-4 col-sm-4">';
					
					foreach($polloption as $pollopt){
						$img = $text = '';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
							}else{
								$text = $title[0];
							}
						}
						if(!empty($img) && !empty($text)){
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}elseif(!empty($img)){
							$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
						}else{
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}
						$val++;
					}
					
					$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
					
					if(!empty($getvalue)){
						$html .= '';
					}else{
						$html .= '<button type="button" class="pollVote trand-button" value="" name="pollVote" disabled="disabled">Submit Vote</button>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'poll_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
					$squery = $this->db->query($sql);
					$totalShare = $squery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div><div class="button"><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div><div class="beforePostSubmit"><div class="beforePostSubmit-content"><div class="beforePostSubmit-header"><span class="beforePostSubmit-close">&times;</span><h2>Want to submit vote?</h2></div><div class="beforePostSubmit-body"><div class="col-md-12" style="text-align:center;"><input type="submit" class="trand-button" value="Yes" name="pollVote" style="width:100px;"/><button type="button" class="trand-button noSubmitVote" style="width:100px;">No</button></div></div></div></div></form>';
					
					$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "poll_like">';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "poll_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_poll"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_poll_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user poll shared section end
		return $html;
	}
	
	public function addNewPoll($data, $alluser, $userid){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);
		
		$qvideolink = '';
		if(!empty($data['videolink'])){
			$videoArr = explode('v=',$data['videolink']);
			$qvideolink = 'https://www.youtube.com/embed/'.$videoArr[1];
		}
		
		$start_date = date('Y-m-d');
		$end_date = date('Y-m-d', strtotime('+ 15 days'));
		$create_date = date('Y-m-d H:i:s');
		$this->db->query('insert into vv_pollls (catid, user_id, question, imag, videourl, start_date, end_date, status, created, userip, userlog) values ("'.$data['poll_category'].'", "'.$userid.'", "'.$data['ques'].'", "'.$data['quesI'].'", "'.$qvideolink.'", "'.$start_date.'", "'.$end_date.'", "1", now(), \''.$server.'\', \''.$iplog.'\')');
		$pollid = $this->db->insert_id();
		
		if(!empty($pollid)){
			if(!empty($data['ans1']) || !empty($data['ans1I'])) {
				$this->db->query('insert into vv_polloption (option_id, pollid, title, img, created) values ("1", "'.$pollid.'", "'.$data['ans1'].'", "'.$data['ans1I'].'", now())');
			}
			if(!empty($data['ans2']) || !empty($data['ans2I'])) {
				$this->db->query('insert into vv_polloption (option_id, pollid, title, img, created) values ("2", "'.$pollid.'", "'.$data['ans2'].'", "'.$data['ans2I'].'", now())');
			}
			if(!empty($data['ans3']) || !empty($data['ans3I'])) {
				$this->db->query('insert into vv_polloption (option_id, pollid, title, img, created) values ("3", "'.$pollid.'", "'.$data['ans3'].'", "'.$data['ans3I'].'", now())');
			}
			if(!empty($data['ans4']) || !empty($data['ans4I'])) {
				$this->db->query('insert into vv_polloption (option_id, pollid, title, img, created) values ("4", "'.$pollid.'", "'.$data['ans4'].'", "'.$data['ans4I'].'", now())');
			}
		}
		
		$sql ="SELECT * FROM vv_users where id = '".$userid."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		
		$tagfrn = (count(array_filter($_POST['frndids'])))?array_filter($_POST['frndids']):array();
		$with = '';
		
		if(count($tagfrn) && $tagfrn) {
			$tagging = array();
			$looping = 0;
			
			foreach($tagfrn as $tf) {
				if($looping++ > 1) break;
				$tagging[] = (array)$this->Dashboard_model->getUserInfo($tf);
			}
			
			if(count($tagfrn) > 2) {
				$with .= ' with ';
				$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'polls\',\''.$pollid.'\',\''.$userid.'\',\'tagfrn_polls_'.$pollid.'\');">'.(count($tagfrn) - 1).' others</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'polls\',\''.$pollid.'\',\''.$userid.'\',\'tagfrn_polls_'.$pollid.'\');">'.(count($tagfrn) - 1).' others</a> ';
			}
			
			else {
				if(count($tagfrn) == 2) {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and ';
					$with .= ($tagging[1]['username'])?'<a href="'.base_url().''.$tagging[1]['username'].'">'.ucwords($tagging[1]['fullname']).'</a>':'<a href="'.base_url().'user/id/'.$tagging[1]['userid'].'">'.ucwords($tagging[1]['fullname']).'</a>';
				}
				else {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> ';
				}
			}
			
			foreach($tagfrn as $tf) {
				$tstatus = $this->Dashboard_model->getTagStatus($tf);
				$ts = ($tstatus['tagging'])?0:1;
				$dataarr = array(
					'userid' => $tf,
					'taggedby' => $userid,
					'reference' => 'polls',
					'refid' => $pollid,
					'textdesc' => ' created a <a href="'.base_url().'poll/'.$pollid.'">poll</a> ' .$with,
					'create_date' => $create_date,
					'status' => $ts
				);
				$this->db->insert('vv_timeline',$dataarr);
			}
		}
		
		
		$dataarr = array(
			'userid' => $userid,
			'reference' => 'polls',
			'refid' => $pollid,
			'textdesc' => ' created a <a href="'.base_url().'poll/'.$pollid.'">poll</a> ' .$with,
			'create_date' => $create_date,
			'status' => '1'
		);
		$this->db->insert('vv_timeline',$dataarr);
		
		if($alluser) {
			$textdesc = ' created a <a href="'.base_url().'poll/'.$pollid.'">poll</a> ';
			$fr = explode(',', $alluser);
			for($i=0;$i<count($fr);$i++) {
				if(in_array($fr[$i], $tagfrn) && $tagfrn)
					$textdesc .= 'and tagged you ';
				$dataarr = array(
					'userid' => $fr[$i],
					'messageby' => $userid,
					'refid' => $pollid,
					'reference' => 'polls',
					'textdesc' =>  $textdesc,
					'create_date' => $create_date,
					'status' => '1'
				);
				$this->db->insert('vv_messages',$dataarr);
			}
		}
		//$this->session->set_flashdata('msg', 'New poll successfully created');
		
		return  1;
    }
	 
	public function addNewPost($data, $alluser, $userid){
		$server = json_encode(array('page' => $_SERVER['HTTP_REFERER'], 'system' => $_SERVER['HTTP_USER_AGENT'], 'userip' => $_SERVER['REMOTE_ADDR']));
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL,'http://api.ipstack.com/'.$_SERVER['REMOTE_ADDR'].'?access_key=194c8c752317b951180952b6ba4851ba');
		$iplog=curl_exec($ch);
		curl_close($ch);
		
		$datadiv = '';
		if($data['dataurl']) {
			$datadiv = '<div class="previewcontent"><p><img src="'.$data['dataurl']['image'].'" /></p><h4><a href="'.$data['dataurl']['url'].'" target="_blank">'.$data['dataurl']['title'].'</a></h4><p>'.substr($data['dataurl']['description'], 0, 77).'...</p></div>';
		}
		$postvideo = '';
		if(!empty($data['postvideolink'])){
			$videoArr = explode('v=',$data['postvideolink']);
			$postvideo = 'https://www.youtube.com/embed/'.$videoArr[1];
		}
		$create_date = date('Y-m-d H:i:s');
		
		$postimg = (!empty($data['fileid']))?implode(',',$data['fileid']):'';
		
		$input = array('user_id' => $userid, 'catid' => $data['post_category'], 'img' => $postimg, 'vid' => $postvideo, 'postdata' => $datadiv.$data['postdata'], 'publish_date' => $create_date, 'userip' => $server, 'userlog' => $iplog);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_post', $input);
		$postid = $this->db->insert_id();
		
		if(!empty($postid)){
			//$data = array('userid' => $userid, 'catid' => $cat, 'log_id' => $postid, 'log_title' => 'user_post', 'ip' => $_SERVER['SERVER_ADDR']);
			//$this->db->set('created', 'NOW()', FALSE);
			//$this->db->set('modified', 'NOW()', FALSE);
			//$this->db->insert('vv_logs', $data);
		}
		
		$sql ="SELECT * FROM vv_users where id = '".$userid."'";
		$query = $this->db->query($sql);
		$value = $query->row();		
		
		$tagfrn = (isset($_POST['frndids']) && count(array_filter($_POST['frndids'])))?array_filter($_POST['frndids']):'';
		$with = '';
		
		if(count($tagfrn) && $tagfrn) {
			$tagging = array();
			$looping = 0;
			
			foreach($tagfrn as $tf) {
				if($looping++ > 1) break;
				$tagging[] = (array)$this->Dashboard_model->getUserInfo($tf);
			}
			
			if(count($tagfrn) > 2) {
				$with .= ' with ';
				$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'posts\',\''.$postid.'\',\''.$userid.'\',\'tagfrn_posts_'.$postid.'\');">'.(count($tagfrn) - 1).' others</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and <a href="#" class="prevent_click" onclick="showtags(\'posts\',\''.$postid.'\',\''.$userid.'\',\'tagfrn_posts_'.$postid.'\');">'.(count($tagfrn) - 1).' others</a> ';
			}
			
			else {
				if(count($tagfrn) == 2) {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> and ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> and ';
					$with .= ($tagging[1]['username'])?'<a href="'.base_url().''.$tagging[1]['username'].'">'.ucwords($tagging[1]['fullname']).'</a>':'<a href="'.base_url().'user/id/'.$tagging[1]['userid'].'">'.ucwords($tagging[1]['fullname']).'</a>';
				}
				else {
					$with .= ' with ';
					$with .= ($tagging[0]['username'])?'<a href="'.base_url().''.$tagging[0]['username'].'">'.ucwords($tagging[0]['fullname']).'</a> ':'<a href="'.base_url().'user/id/'.$tagging[0]['userid'].'">'.ucwords($tagging[0]['fullname']).'</a> ';
				}
			}
			
			foreach($tagfrn as $tf) {
				$tstatus = $this->Dashboard_model->getTagStatus($tf);
				$ts = ($tstatus['tagging'])?0:1;
				$dataarr = array(
					'userid' => $tf,
					'taggedby' => $userid,
					'reference' => 'post',
					'refid' => $postid,
					'textdesc' => ' created a <a href="'.base_url().'post/'.$postid.'">post</a> ' .$with,
					'create_date' => $create_date,
					'status' => $ts
				);
				$this->db->insert('vv_timeline',$dataarr);
			}
		}
		
		if($postimg) {
			$sql ="update vv_postfile set postid = '".$postid."' where id in (".$postimg.")";
			$query = $this->db->query($sql);
		}
		
		$dataarr = array(
			'userid' => $userid,
			'reference' => 'post',
			'refid' => $postid,
			'textdesc' => ' created a <a href="'.base_url().'post/'.$postid.'">post</a> ' .$with,
			'create_date' => $create_date,
			'status' => '1'
		);
		$this->db->insert('vv_timeline',$dataarr);
		
		if($alluser) {
			$textdesc = ' created a <a href="'.base_url().'post/'.$postid.'">post</a> ';
			$fr = explode(',', $alluser);
			for($i=0;$i<count($fr);$i++) {
				if($tagfrn) {
					if(in_array($fr[$i], $tagfrn))
						$textdesc .= 'and tagged you ';
				}
				$dataarr = array(
					'userid' => $fr[$i],
					'messageby' => $userid,
					'refid' => $postid,
					'reference' => 'polls',
					'textdesc' => $textdesc,
					'create_date' => $create_date,
					'status' => '1'
				);
				$this->db->insert('vv_messages',$dataarr);
			}
		}
		//$this->session->set_flashdata('msg', 'New post successfully created');
		return  $postid;
    }
	
	
	public function addPollVoting($pollid,$pollOpt,$userid,$userip,$alluser){
		$input = array('pollid' => $pollid, 'vote' => $pollOpt, 'userid' => $userid, 'userip' => $userip);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_pollvoting', $input);
		$insertId = $this->db->insert_id();
		$this->db->query('update vv_polloption set votes = votes+1 where pollid = '.$pollid.' and option_id = '.$pollOpt);
		$this->db->query('update vv_pollls set participation = participation+1 where id = '.$pollid);
		
		if($alluser) {
			$fr = explode(',', $alluser);
			$textdesc = ' participated in a <a href="'.base_url().'poll/'.$pollid.'">poll</a> ';
			for($i=0;$i<count($fr);$i++) {
				$dataarr = array(
					'userid' => $fr[$i],
					'messageby' => $userid,
					'refid' => $pollid,
					'reference' => 'polls',
					'textdesc' => $textdesc,
					'create_date' => date('Y-m-d H:i:s'),
					'status' => '1'
				);
				$this->db->insert('vv_messages',$dataarr);
			}
		}
		/*if(!empty($insertId)){
			$sql ="SELECT total_vote as total,id FROM vv_pollresult WHERE logid = '".$pollid."' AND title = 'poll'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('total_vote' => $total);
				$update = $this->db->update('vv_pollresult',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pollid, 'total_vote' => '1', 'title' => 'poll');
				$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('vv_pollresult', $input);
			}
		}*/
		return  $insertId;
     }
	 
	 
	 public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	 
	 
	 public function getVerifyCatList(){
		$sql ="SELECT * FROM vv_verify_category ORDER BY name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getUserName($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getUserInfo($userid){
		$sql ="SELECT id as userid, CONCAT(firstname, ' ', lastname) as fullname, profile_pic_url, username, verified FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	
	public function getUserInformation($userid){
		$sql ="SELECT id as userid, CONCAT(firstname, ' ', lastname) as fullname, username, profile_url, picture_url, profile_pic_url, cover_pic_url, email, dob, gender, contact, c_visibility, d_visibility, verified FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getShortUserinfo($userid){
		$sql ="SELECT overview, vv_countries.name as country, vv_states.name as state, vv_cities.name as city, city_privacy, state_privacy, country_privacy FROM `vv_users` inner join vv_usermeta on vv_users.id = vv_usermeta.userid left join vv_countries on vv_users.country = vv_countries.id left join vv_states on vv_users.state = vv_states.id left join vv_cities on vv_users.city = vv_cities.id WHERE vv_users.id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getFullUserInformation($userid){		
		$sql ="SELECT vv_users.gender, CONCAT(firstname, ' ', lastname) as fullname, username, profile_url, picture_url, profile_pic_url, cover_pic_url, vv_users.contact, vv_users.c_visibility, vv_users.interest, verified, vv_usermeta.*, vv_countries.name as country, vv_states.name as state, vv_cities.name as city, city_privacy, state_privacy, country_privacy FROM `vv_users` inner join vv_usermeta on vv_users.id = vv_usermeta.userid left join vv_countries on vv_users.country = vv_countries.id left join vv_states on vv_users.state = vv_states.id left join vv_cities on vv_users.city = vv_cities.id WHERE vv_users.id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getUserInterest($userid){
		$sql ="select interest from vv_users where id = ".$userid;
		$interest = $this->db->query($sql)->row();
		
		$value = array();
		if(!empty($interest->interest)) {
			$sql ="select * from vv_categories where id in (".$interest->interest.")";
			$categories = $this->db->query($sql)->result();
			
			foreach($categories as $cat)
				$value[] = $cat->cat_name;
		}
		
		return $value;
    }
	
	public function getUserFrndIds($userid){
		$sql ="SELECT friend_one,friend_two FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
		$query = $this->db->query($sql);
		$value = $query->result();
		$ids = array();
		foreach($value as $v){
			if($v->friend_one == $userid){
				array_push($ids,$v->friend_two);
			}
			
			if($v->friend_two == $userid){
				array_push($ids,$v->friend_one);
			}
		}
		return implode(",",$ids);
    }
	
	public function getUserByInterest($userid, $interest){
		//$sql ="SELECT id FROM vv_users WHERE id <> ".$userid." and find_in_set('".$interest."', interest) <> 0";
		$sql ="SELECT id FROM vv_users WHERE find_in_set('".$interest."', interest) <> 0";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getUserTimeline($userid, $start, $limit){
		$sql ="SELECT * FROM `vv_timeline` where userid in (".$userid.") and status = 1 order by id desc limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		
		$content = '';
		$datarows = array();
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$loginid = $userid = $userData['userId'];
		}
		
		foreach($result as $rs) {
			switch($rs->reference) {
				case 'post':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($loginid, 'post', $rs->refid);
					else
						$upvoted = 0;
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$post = $this->Dashboard_model->getUserPosts($rs->refid);
					
					if(!empty($post['img'])) {
						$postimg = $this->Dashboard_model->getPostImage($post['img']);
						$post['postimg'] = $postimg;
					}
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'post'), 'data' => (array)$rs, 'upvoted' => $upvoted);
				break;
				
				case 'polls':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($loginid, 'polls', $rs->refid);
					else
						$upvoted = array();
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$polls = $this->Dashboard_model->getUserPolls($rs->refid);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs->refid);
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'polls'), 'data' => (array)$rs, 'upvoted' => $upvoted);
					
				break;
				
				case 'timeline':
					$sql ="SELECT * FROM `vv_timeline` where status = 1 and id = ".$rs->refid;
				
					$query = $this->db->query($sql);
					$resultt = $query->row();
					
					$shareusers['userinfo'] = $this->Dashboard_model->getUserInformation($rs->shareby);
					$shareusers['tiinfo'] = (array)$rs;
					
					if($resultt) {
					switch($resultt->reference) {
						case 'post':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
							
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($loginid, 'post', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$post = $this->Dashboard_model->getUserPosts($resultt->refid);
							
							if(!empty($post['img'])) {
								$postimg = $this->Dashboard_model->getPostImage($post['img']);
								$post['postimg'] = $postimg;
							}
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'post'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
						break;
						
						case 'polls':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
								
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($loginid, 'polls', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$polls = $this->Dashboard_model->getUserPolls($resultt->refid);
							$polls['polloption'] = $this->Dashboard_model->getUserPollOption($resultt->refid);
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'polls'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
							
						break;
					}
					}
					
				break;
			}
		}
		return $datarows;
    }
	
	
	public function getUserPolls($pollid){
		$sql ="SELECT vv_pollls.*, paruser, vv_categories.cat_name as category FROM `vv_pollls` left join (SELECT group_concat(userid) as paruser, pollid FROM `vv_pollvoting` where vv_pollvoting.pollid = ".$pollid.") as voting on vv_pollls.id = voting.pollid left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$pollid;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getUserPollsByCategory($category, $pollid){
		$sql ="SELECT vv_pollls.*, paruser, vv_categories.cat_name as category FROM `vv_pollls` left join (SELECT group_concat(userid) as paruser, pollid FROM `vv_pollvoting` where vv_pollvoting.pollid = ".$pollid.") as voting on vv_pollls.id = voting.pollid left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$pollid." and catid = ".$category." and vv_pollls.status = 1";
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getUserPollOption($pollid){
		$sql ="SELECT * FROM vv_polloption where pollid = ".$pollid;
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }
	
	
	public function getUserPosts($postid){
		$sql ="SELECT vv_post.*, vv_categories.cat_name as category FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$postid;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getUserPostsByCategory($category, $postid){
		$sql ="SELECT vv_post.*, vv_categories.cat_name as category FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$postid." and catid = ".$category." and vv_post.status = 1";
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getPostImage($imgid){
		$sql ="select * from vv_postfile where id in (".$imgid.")";
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }
	
	
	public function getPostByImage($imgid){
		$sql ="select * from vv_postfile where id = ".$imgid."";
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	
	public function getAdminPolls($pollid){
		$sql ="SELECT vv_adminpolls.*, vv_adminpolloption.option_id, vv_adminpolloption.title, vv_adminpolloption.img, vv_adminpolloption.votes, paruser FROM `vv_adminpolls` inner join vv_adminpolloption on vv_adminpolls.id = vv_adminpolloption.pollid left join (SELECT group_concat(userid) as paruser, pollid FROM `vv_adminpollvoting` where vv_adminpollvoting.pollid = ".$pollid.") as voting on vv_adminpolls.id = voting.pollid where vv_adminpolls.id = ".$pollid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getAdminInfo(){
		$sql ="select fullname, img as profile_pic_url from vv_login";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	
	public function getUnreadMessages($userid){
		if($userid != '') {

			
			$sql ="select count(vv_messages.id) as msg from vv_messages
				   inner join vv_users on vv_messages.messageby = vv_users.id
				   where vv_messages.userid = ".$userid." order by vv_messages.id desc";


			$query = $this->db->query($sql);
			return $query->row();
		}
		else
			return false;
    }
	
	
	public function getTagStatus($userid){
			$sql = "select tagging from vv_usermeta where userid = ".$userid;
			$query = $this->db->query($sql);
			$result = $query->row_array();
			return $result;
    }
	
	
	public function addFile($path, $type, $userid){
		if($userid != '') {
			$sql ="insert into vv_postfile (userid, filepath, filetype, status, created) values ('".$userid."', '".$path."', '".$type."', '1', now())";
			$query = $this->db->query($sql);
			return $this->db->insert_id();
		}
		else
			return false;
    }
    public function getUserPost($userid){
		//$sql ="SELECT id FROM vv_users WHERE id <> ".$userid." and find_in_set('".$interest."', interest) <> 0";
		$sql ="SELECT * FROM vv_post WHERE user_id=$userid";
		$query = $this->db->query($sql);
		return $query->num_rows();
		//die();
		 //return $value = $query->result();
		//var_dump($value);die();
    }
    public function getUserPoll($userid){
		//$sql ="SELECT id FROM vv_users WHERE id <> ".$userid." and find_in_set('".$interest."', interest) <> 0";
		$sql ="SELECT * FROM vv_pollls WHERE user_id=$userid";
		$query = $this->db->query($sql);
		return $query->num_rows();
		// return $value = $query->result();
		/*var_dump($value);
		echo "polls";die();*/
    }
    public function getUserComment($userid){
		//$sql ="SELECT id FROM vv_users WHERE id <> ".$userid." and find_in_set('".$interest."', interest) <> 0";
		$sql ="SELECT * FROM vv_comment WHERE userid=$userid";
		$query = $this->db->query($sql);
		return $query->num_rows();
		//return $value = $query->result();
		/*var_dump($value);
		echo "Cooooooooo";die();*/
    }
    public function getBlockedUser($userid, $blockid){
		$sql ="SELECT * FROM vv_users_blocked WHERE userid = $userid and blockusers = $blockid";
		$query = $this->db->query($sql);
		return $query->num_rows();
    }
    public function getUserUpvoted($userid, $ref, $refid){
		$sql ="SELECT id FROM vv_upvote WHERE userid = ".$userid." and reference = '".$ref."' and refid = ".$refid."";
		$query = $this->db->query($sql);
		return $query->num_rows();
    }
    public function getTimelineText($userid, $ref, $refid){
		$sql ="SELECT id, textdesc FROM `vv_timeline` where userid = ".$userid." and refid = ".$refid." and reference = '".$ref."' and taggedby=0";
		$query = $this->db->query($sql);
		return $query->row_array();
    }
    public function getTimeline($userid, $ref, $refid){
		$sql ="SELECT * FROM `vv_timeline` where userid = ".$userid." and refid = ".$refid." and reference = '".$ref."' and taggedby=0";
		$query = $this->db->query($sql);
		return $query->row_array();
    }
    public function datedifference($d1, $d2){
		$date = '';
		$date1 = new DateTime($d1);
		$date2 = $date1->diff(new DateTime($d2));
		if(($date2->y > 0) || ($date2->m > 0) || ($date2->d > 1)) {
			$date = date('d M Y H:i', strtotime($d1));
		}
		else {
			if($date2->d > 0)
				$date .= $date2->d.'d ';
			if($date2->h > 0)
				$date .= $date2->h.'h ';
			
			$date .= $date2->i.'m ';
		}
		return $date;
    }
	
}