<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminFrontPoll_model extends CI_Model {
    public function __construct()
    {
        $this->load->database();
    }
	
	public function getActivePolls($start, $limit,$id,$usercat) {
		//var_dump($usercat);die();
		$result=array();
		if(!empty($usercat))
		{
			$cat=explode(",",$usercat);
			foreach($cat as $key) 
			{	
				$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date >= now() AND vv_adminpolls.catid=".$key." order by id desc limit ".$start.", ".$limit."";
				$query = $this->db->query($sql);
				if(!empty($query->row()))
				{
					$result[] = $query->row();
				}
			}
		}
		return $result;
		
	}
	
	public function getAdminPollByFilter1($start, $limit, $country_id, $state_id, $city_id, $sortby, $category) {
		$result = array();
		$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date >= now() AND vv_adminpolls.catid=$category";
		if($country_id!='')
			$sql .=" and country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and city = '".$city_id."'";
		if($category!='')
			$sql .=" and  catid = '".$category."'";		
		if($sortby!='')
			$sql .=" order by ".$sortby." desc";
		else
			$sql .=" order by id desc";
		
		$sql .=" limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getArchivePolls1($start, $limit) {
		$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date < now() order by id desc limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getArchivePollByFilter1($start, $limit, $country_id, $state_id, $city_id, $sortby, $category,$usercat) {
		$result = array();
		$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date < now() ";
		if($country_id!='')
			$sql .=" and country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and city = '".$city_id."'";
		if($category!='')
			$sql .=" and  catid = '".$category."'";		
		if($sortby!='')
			$sql .=" order by ".$sortby." desc";
		else
			$sql .=" order by id desc";
		
		$sql .=" limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getAdminDetail() {
		$sql ="SELECT fullname, img FROM `vv_login`";
		$query = $this->db->query($sql);
		$result = $query->row();
		return $result;
	}
	
	public function getCategories() {
		$sql ="SELECT * FROM `vv_categories` where status = 1 order by cat_name asc";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
    

	
	
	

	
	
	public function getUserName($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
    	/*latest polls detail*/
	public function getPollsInfo($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->row();
		//var_dump($value->interest);die();
		if(!empty($value->interest))
		{
			date_default_timezone_set('Asia/Kolkata');
			$date=date("Y-m-d H:i:s");
			//echo (date("Y-m-d H:i:s"));
			//die();
			$sql ="SELECT * FROM vv_adminpolls WHERE catid =$value->interest AND start_date>='".$date."'AND end_date<='".$date."'";
		$query = $this->db->query($sql);
		$value = $query->result();

		}
		return $value;
    }
   public function userDetail($id)
   {
   	$sql="SELECT * FROM vv_users WHERE id=".$id;
   	$query=$this->db->query($sql);
   	$value=$query->row();
   	return $value->interest;
   	//die();

   }

   /* My functions */
   public function getAdminPollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category,$usercat) {
   	//var_dump($limit);die();
		$result = array();
		$cat=explode(",",$usercat);
		if(!empty($usercat))
		{
			foreach ($cat as $key) 
			{ 
				if($category==$key)
				{
			
					$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date >= now() AND vv_adminpolls.catid=".$key;
					if($country_id!='')
						$sql .=" and country = '".$country_id."'";
					if($state_id!='')
						$sql .=" and state = '".$state_id."'";
					if($city_id!='')
						$sql .=" and city = '".$city_id."'";
					if($category!='')
						$sql .=" and  catid = '".$category."'";		
					if($sortby!='')
						$sql .=" order by ".$sortby." desc";
					else
						$sql .=" order by id desc";
					
					$sql .=" limit ".$start.", ".$limit."";
					$query = $this->db->query($sql);

					if(!empty($query->result()))
					{
						$result = $query->result();
					}
				}
			}
			
		}
		return $result;
	}
	public function getArchivePollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category,$usercat) {
		//var_dump($limit);die();
		$result = array();
		$cat=explode(",",$usercat);
		if(!empty($usercat))
		{
			foreach ($cat as $key) 
			{ 
				if($category==$key)
				{
					$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date < now() AND vv_adminpolls.catid=".$key;
					if($country_id!='')
						$sql .=" and country = '".$country_id."'";
					if($state_id!='')
						$sql .=" and state = '".$state_id."'";
					if($city_id!='')
						$sql .=" and city = '".$city_id."'";
					if($category!='')
						$sql .=" and  catid = '".$category."'";		
					if($sortby!='')
						$sql .=" order by ".$sortby." desc";
					else
						$sql .=" order by id desc";
					
					$sql .=" limit ".$start.", ".$limit."";
					$query = $this->db->query($sql);
					if(!empty($query->result()))
					{
						$result = $query->result();
					}
				}
			}
			
		}
		return $result;
	}
	public function getArchivePolls($start, $limit,$id,$usercat) {
		$result1=array();
		if(!empty($usercat))
		{
			$cat=explode(",",$usercat);
			foreach($cat as $key) 
			{
				$sql ="SELECT vv_adminpolls.*, vv_categories.cat_name FROM `vv_adminpolls` inner join vv_categories on vv_adminpolls.catid = vv_categories.id where end_date < now() AND vv_adminpolls.catid=".$key." order by id desc limit ".$start.", ".$limit."";
				
				$query = $this->db->query($sql);
				$result = $query->row();
				
				if(isset($result))
				{
					$result1[] = $query->row();
				}
			}
		}
		//print_r($result1);die();
		return $result1;
	}
	

}