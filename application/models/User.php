<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model{
	function __construct() {
		$this->tableName = 'vv_users';
		$this->primaryKey = 'id';
	}
	public function checkUserFb($data = array()){
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
		$prevQuery = $this->db->get();
		$prevCheck = $prevQuery->num_rows();
		
		if($prevCheck > 0){
			$prevResult = $prevQuery->row_array();
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$prevResult['id']));
			$userID = $prevResult['id'];
		}else{
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
			if($userID){
				$input = array('friend_one' => $userID, 'friend_two' => $userID, 'status' => '2');
				$data['created'] = date("Y-m-d H:i:s");
				$data['modified'] = date("Y-m-d H:i:s");
				$insert = $this->db->insert('vv_friends',$input);
				$frndID = $this->db->insert_id();	
			}	
		}

		return $userID?$userID:FALSE;
        }

        public function checkUserTwitter($data = array()){
		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
		$prevQuery = $this->db->get();
		$prevCheck = $prevQuery->num_rows();
		
		if($prevCheck > 0){
			$prevResult = $prevQuery->row_array();
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$prevResult['id']));
			$userID = $prevResult['id'];
		}else{
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id(); 
			if($userID){
				$input = array('friend_one' => $userID, 'friend_two' => $userID, 'status' => '2');
				$data['created'] = date("Y-m-d H:i:s");
				$data['modified'] = date("Y-m-d H:i:s");
				$insert = $this->db->insert('vv_friends',$input);
				$frndID = $this->db->insert_id();	
			}	
		}

		return $userID?$userID:FALSE;
        }

       public function checkUserGoogle($data = array()){
 		$this->db->select();
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$data['oauth_provider'],'oauth_uid'=>$data['oauth_uid']));
		$query = $this->db->get();
		$check = $query->num_rows();
		$res=$query->row();
		
		if($check > 0){
			$result = $query->row_array();
			$data1['modified'] = date("Y-m-d H:i:s");
			//echo '<pre>';
			//print_r($result);
			
			
			$update = $this->db->update($this->tableName,$data1,array('id'=>$result['id']));
			//$userID = $result['id'];
			$userID = array(
				'firstname' => $result['firstname'],
				'lastname' => $result['lastname'],
				'userId' => $result['id'],
				'email' => $result['email'],
				'picture_url' => $result['picture_url'],
				'activestatus' => $result['status'],
				'blockeddate' => $result['blocked_date']
			);
			//print_r($userID);
			//die('hello');
		}else{
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
			
			$input = array('userid' => $userID, 'secondary_email' => $data['email']);
			$insert = $this->db->insert('vv_usermeta',$input);
			$frndID = $this->db->insert_id();
				
			if($userID){
				$input = array('friend_one' => $userID, 'friend_two' => $userID, 'status' => '2');
				$data['created'] = date("Y-m-d H:i:s");
				$data['modified'] = date("Y-m-d H:i:s");
				$insert = $this->db->insert('vv_friends',$input);
				$frndID = $this->db->insert_id();	
			}	
		}

		return $userID?$userID:false;
      }
	  
	  public function register($data = array()){
 		$this->db->select($this->primaryKey);
		$this->db->from($this->tableName);
		$this->db->where(array('email'=>$data['email']));
		$query = $this->db->get();
		$check = $query->num_rows();
		
		if($check <= 0){
			$data['created'] = date("Y-m-d H:i:s");
			$data['modified'] = date("Y-m-d H:i:s");
			$insert = $this->db->insert($this->tableName,$data);
			$userID = $this->db->insert_id();
			
			$this->db->query('insert into vv_usermeta (userid) values ('.$userID.')');
		
			if($userID){
				$input = array('friend_one' => $userID, 'friend_two' => $userID, 'status' => '2');
				$data['created'] = date("Y-m-d H:i:s");
				$data['modified'] = date("Y-m-d H:i:s");
				$insert = $this->db->insert('vv_friends',$input);
				$frndID = $this->db->insert_id();	
			}
		}else{
			$userID = '';	
		}

		return $userID?$userID:false;
      }
	  
	  
	  public function userLogin($oauth_provider,$email,$pass){
 		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where(array('oauth_provider'=>$oauth_provider,'email'=>$email,'password'=>$pass));
		$query = $this->db->get();
		$check = $query->num_rows();
		if($check > 0){
			$result = $query->row_array();
			if($result['status'] == '0'){
				$userData['error'] = "Your email id is not verified or You can't login!";	
			}elseif($result['status'] == '2' || $result['status'] == '3' || $result['status'] == '99'){
				$userData['error'] = "You havn't permission to login!";
			}else{
				$userData = array(
					'firstname' => $result['firstname'],
					'lastname' => $result['lastname'],
					'userId' => $result['id'],
					'email' => $result['email'],
					'picture_url' => $result['picture_url'],
					'activestatus' => $result['status'],
					'blockeddate' => $result['blocked_date']
				);
			}
		}else{
			$userData = array();
		}

		return $userData?$userData:false;
      }
	  
	  
	  
	  
	  public function verifyUser($ref){
 		$this->db->select('*');
		$this->db->from($this->tableName);
		$this->db->where(array('ref'=>$ref));
		$query = $this->db->get();
		$prevResult = $query->row_array();
		$check = $query->num_rows();
		if($check > 0){
			$data['status'] = '1';
			$data['modified'] = date("Y-m-d H:i:s");
			$update = $this->db->update($this->tableName,$data,array('id'=>$prevResult['id']));
			$userID = $prevResult['id'];
		}else{
			$userID = '';
		}

		return $userID;
      }

      public function getSmtpDetails(){
		$sql ="SELECT * FROM vv_smtpsettings";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	  }
	
	  public function getTemplate(){
		$sql ="SELECT * FROM vv_emailtemplate";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	  }
	  
	  public function isertRef($id,$ref){
		$data['ref'] = $ref;
		$update = $this->db->update($this->tableName,$data,array('id'=>$id));
	  }
	  
	public function resetPassword($email) {
		$sql ="SELECT * FROM vv_users where email = '".$email."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
	}


	public function resetPassword_alternate($alternate_email) {
		$sql ="SELECT * FROM vv_users where alternate_email = '".$alternate_email."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
	}
	  
	public function getResetUser($ref, $email) {
		$sql ="SELECT * FROM vv_users where email = '".$email."' and ref = '".$ref."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
	}

	public function getResetUser_alt($ref, $alternate_email) {
		$sql ="SELECT * FROM vv_users where alternate_email = '".$alternate_email."' and ref = '".$ref."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
	}
	  
	public function updatePassword($pass, $ref, $email) {
		$sql ="update vv_users set password = md5('".$pass."') where email = '".$email."' and ref = '".$ref."'";
		$query = $this->db->query($sql);
	}

	public function updatePassword_alt($pass, $ref, $alternate_email) {
		$sql ="update vv_users set password = md5('".$pass."') where alternate_email = '".$alternate_email."' and ref = '".$ref."'";
		$query = $this->db->query($sql);
	}
}

