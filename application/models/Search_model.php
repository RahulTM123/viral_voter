<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Search_model extends CI_Model {

    public function __construct()

    {
        $this->load->database();
    }
    
	public function index() {
		return 'No data';
    }
    
	public function result($keyword, $country_id, $state_id, $city_id, $gender) {
		$sql = 'select vv_users.* from vv_users inner join vv_countries on vv_users.country = vv_countries.id inner join vv_states on vv_users.state = vv_states.id inner join vv_cities on vv_users.city = vv_cities.id where 1';
		
		$kw = explode(' ', $keyword);
		if(is_array($kw)) {
		for($i=0;$i<count($kw);$i++)
			$sql .=" and (firstname like '%".$kw[$i]."%' or lastname like '%".$kw[$i]."%')";
		}
		else {
			$sql .=" and (firstname like '%".$keyword."%' or lastname like '%".$keyword."%')";
		}
		
		if($country_id)
			$sql .=" and vv_users.country = ".$country_id;
		
		if($gender)
			$sql .=" and vv_users.gender = '".$gender."'";
			
			$sql .=" limit 6";
		
		$query = $this->db->query($sql);
		$users = $query->result();
		$result = array();
		if(!empty($users)) {
			foreach($users as $us){
				$result['users'][] = $us;
			}
		} else {
				$result['users'] = '';
		}
		
		
		$kw = explode(' ', $keyword);
		arsort($kw);
		$keyword1 = implode(' ', $kw);
		
		$keyword = str_replace(' ', '%', $keyword);
		$keyword1 = str_replace(' ', '%', $keyword1);
		$psql = 'select vv_pollls.*, vv_users.firstname, vv_users.lastname, vv_users.profile_pic_url, vv_users.username from vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id where vv_pollls.end_date >= now() and vv_pollls.question like "%'.$keyword.'%" or vv_pollls.question like "%'.$keyword1.'%" limit 6';
		
		$pquery = $this->db->query($psql);
		$polls = $pquery->result();
		if(!empty($polls)) {
			foreach($polls as $pl){
				$result['polls'][] = $pl;
			}
		} else {
				$result['polls'] = '';
		}
		
		
		$keyword = str_replace(' ', '%', $keyword);
		$asql = 'select vv_adminpolls.* from vv_adminpolls where vv_adminpolls.end_date >= now() and vv_adminpolls.question like "%'.$keyword.'%" limit 6';
		
		$aquery = $this->db->query($asql);
		$apolls = $aquery->result();
		if(!empty($apolls)) {
			foreach($apolls as $ap){
				$result['apolls'][] = $ap;
			}
		} else {
				$result['apolls'] = '';
		}
		
		$adsql ="SELECT fullname, img FROM `vv_login`";
		$adquery = $this->db->query($adsql);
		$result['admin'] = $adquery->row();
		
		
		
		return $result;
    }
    
	public function resultuser($keyword, $country_id, $state_id, $city_id, $gender) {
		$sql = 'select vv_users.* from vv_users inner join vv_countries on vv_users.country = vv_countries.id inner join vv_states on vv_users.state = vv_states.id inner join vv_cities on vv_users.city = vv_cities.id where 1';
		
		$kw = explode(' ', $keyword);
		if(is_array($kw)) {
		for($i=0;$i<count($kw);$i++)
			$sql .=" and (firstname like '%".$kw[$i]."%' or lastname like '%".$kw[$i]."%')";
		}
		else {
			$sql .=" and (firstname like '%".$keyword."%' or lastname like '%".$keyword."%')";
		}
		
		if($country_id)
			$sql .=" and vv_users.country = ".$country_id;
		
		if($gender)
			$sql .=" and vv_users.gender = '".$gender."'";
			
			$sql .=" limit 12";
		
		$query = $this->db->query($sql);
		$users = $query->result();
		$result = array();
		if(!empty($users)) {
			foreach($users as $us){
				$result['users'][] = $us;
			}
		} else {
				$result['users'] = '';
		}		
		
		return $result;
    }
    
	public function resultpoll($keyword, $country_id, $state_id, $city_id, $category) {
		$psql = 'select vv_pollls.*, vv_users.firstname, vv_users.lastname, vv_users.profile_pic_url, vv_users.username from vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id where vv_pollls.end_date >= now() and 1';
		
		$kw = explode(' ', $keyword);
		if(is_array($kw)) {
		for($i=0;$i<count($kw);$i++)
			$psql .=" or vv_pollls.question like '%".$kw[$i]."%'";
		}
		else {
			$psql .=" or vv_pollls.question like '%".$keyword."%'";
		}
		
		if($country_id)
			$psql .=" and vv_users.country = ".$country_id;
		
		if($category)
			$psql .=" and vv_pollls.catid = ".$category;
		
			$psql .=" limit 6";
		
		$pquery = $this->db->query($psql);
		$polls = $pquery->result();
		if(!empty($polls)) {
			foreach($polls as $pl){
				$result['polls'][] = $pl;
			}
		} else {
				$result['polls'] = '';
		}
		
		return $result;
    }
}