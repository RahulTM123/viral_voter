<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class EmailModel extends CI_Model{
	function __construct() {
		$this->load->database();
	}
		
	public function phpmailer($to, $body, $subject){
		$this->load->library("PhpMailerLib");
		$smtpDetails = $this->user->getSmtpDetails();
        $mail = $this->phpmailerlib->load();
        $mailad = $this->phpmailerlib->load();
		try {
			//Server settings
			$mailad->SMTPDebug = 0;                                 // Enable verbose debug output
			$mailad->isSMTP();                                      // Set mailer to use SMTP
			$mailad->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
			$mailad->SMTPAuth = true;                               // Enable SMTP authentication
			$mailad->Username = $smtpDetails[0]->username;                 // SMTP username
			$mailad->Password = $smtpDetails[0]->password;                           // SMTP password
			$mailad->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
			$mailad->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to connect to
			//Recipients
			$mailad->setFrom($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
			$mailad->addAddress($to);            // Name is optional
			$mailad->addReplyTo($to);
			//$mailad->AddBCC('rajneesh.sharma@yahoo.com');
			//$mailad->AddBCC('shalabh5@hotmail.com');
			$mailad->AddBCC('sachinsaini.cs@gmail.com');
			$mailad->isHTML(true);                                  // Set email format to HTML
			$mailad->Subject = $subject;
			$mailad->Body = $body;

			$mailad->send();
			$i = 1;
		} catch (Exception $e) {
			$i = 0;
		}
		return $i;
		
	}
}

