<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getUserData($userID){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userID;
		$query = $this->db->query($sql);
		$users = $query->result();
		return $users;
    }
	
	public function getGeneralData($userID){
		$sql ="SELECT commenting, tagging FROM vv_usermeta WHERE userid = ".$userID;
		$query = $this->db->query($sql);
		$comtag = $query->row_array();
		
		$blsql ="select vv_users.id, firstname, lastname, username, profile_pic_url from vv_users inner join vv_users_blocked on vv_users.id = vv_users_blocked.blockusers where vv_users_blocked.userid = ".$userID;
		$blquery = $this->db->query($blsql);
		$blocked = $blquery->result_array();
		return array('general' => $comtag, 'blocked' => $blocked);
    }
	
	public function getProfileData($userID){		
		$sql ="select * from vv_users inner join vv_usermeta on vv_users.id = vv_usermeta.userid where vv_users.id = ".$userID;
		$query = $this->db->query($sql);
		$userinfo = $query->row_array();
		return $userinfo;
    }
	
	public function getVerificationData($userID){		
		$sql ="SELECT vv_users.verified, vv_usermeta.verified_date FROM `vv_users` inner join vv_usermeta on vv_users.id = vv_usermeta.userid where vv_users.id = ".$userID;
		$query = $this->db->query($sql);
		$verified = $query->row_array();
		return $verified;
    }
	
	public function updateGeneralData($data, $userID){
		$sql ="update vv_usermeta set tagging = '".$data['tagging']."', commenting = '".$data['commeting']."' where userid = ".$userID;
		$query = $this->db->query($sql);
    }
	
	public function updateProfileData($data, $userID){
		$sql ="update vv_usermeta set dob_date = '".$data['day']."', dob_month = '".$data['month']."', dob_year = '".$data['year']."', school = '".$data['school']."', overview = '".$data['bio']."', working = '".$data['working']."', designation = '".$data['designation']."', secondary_email = '".$data['email']."'";
		
		$dobpr = (isset($data['dob_privacy']))?$data['dob_privacy']:0;
			$sql .= ", dob_privacy = '".$dobpr."'";
		
		$genpr = (isset($data['gender_privacy']))?$data['gender_privacy']:0;
			$sql .= ", gender_privacy = '".$genpr."'";
		
		$empr = (isset($data['email_privacy']))?$data['email_privacy']:0;
			$sql .= ", email_privacy = '".$empr."'";
		
		$adbpr = (isset($data['address_privacy']))?$data['address_privacy']:0;
			$sql .= ", address_privacy = '".$adbpr."'";
		
		$ctpr = (isset($data['city_privacy']))?$data['city_privacy']:0;
			$sql .= ", city_privacy = '".$ctpr."'";
		
		$stpr = (isset($data['state_privacy']))?$data['state_privacy']:0;
			$sql .= ", state_privacy = '".$stpr."'";
		
		$cnpr = (isset($data['country_privacy']))?$data['country_privacy']:0;
			$sql .= ", country_privacy = '".$cnpr."'";
		
		$scpr = (isset($data['school_privacy']))?$data['school_privacy']:0;
			$sql .= ", school_privacy = '".$scpr."'";
		
		$wkpr = (isset($data['working_privacy']))?$data['working_privacy']:0;
			$sql .= ", working_privacy = '".$wkpr."'";
		
		$dspr = (isset($data['designation_privacy']))?$data['designation_privacy']:0;
			$sql .= ", designation_privacy = '".$dspr."'";
		
		$sql .= " where userid = ".$userID;
		$query = $this->db->query($sql);
		
		$interest = (isset($data['interest']))?implode(',', $data['interest']):'';
		$sql ="update vv_users set firstname = '".$data['first_name']."', lastname = '".$data['last_name']."', gender = '".$data['gender']."', state = '".$data['sid']."', city = '".$data['ctid']."', country = '".$data['cid']."', interest = '".$interest."',alternate_email = '".$data['altemail']."'";
		
		if(!empty($data['latestusername']))
			$sql .= ", username = '".$data['latestusername']."'";
		
		$sql .=" where id = ".$userID;
		
		$query = $this->db->query($sql);
    }
	
	public function updateVerificationData($data, $userID, $filename){
		$sql ="update vv_usermeta set facebook = '".$data['verify_facebook']."', twitter = '".$data['verify_twitter']."', linkedin = '".$data['verify_linkedin']."', youtube = '".$data['verify_youtube']."', verify_profession = '".$data['profession']."', verify_proof = '".$data['idproof']."', verify_about = '".$data['verifiesbio']."', verified_docfile = '".$filename."', verified_date = now() where userid = ".$userID;
		$query = $this->db->query($sql);
    }
	
	public function updatePasswordData($data, $userID){
		$sql ="update vv_users set password = md5('".$data['password']."') where id = ".$userID;
		$query = $this->db->query($sql);
    }
	
	public function updateLoginEmail($data, $userID){
		$sql ="update vv_users set email = '".$data['email']."' where id = ".$userID;
		$query = $this->db->query($sql);
    }
	
}