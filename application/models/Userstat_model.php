<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserStat_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getStat($start, $limit){
		$sql = 'SELECT * FROM `vv_users`';
		$query = $this->db->query($sql);
		$users = $query->result_array();
		
		$result = array();
		foreach($users as $user) {
			$result[$user['id']] = array('user' => $user);
			
			$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(participation) as tparticipation, SUM(upvote) as tupvote from vv_pollls where user_id = ".$user['id']."";
			$query = $this->db->query($sql);
			$result[$user['id']]['poll'] = $query->row_array();
			
			$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(upvote) as tupvote from vv_post where user_id = ".$user['id']."";
			$query = $this->db->query($sql);
			$result[$user['id']]['post'] = $query->row_array();
			
			$sql ="SELECT sum(upvote) as tupvote, count(id) as total FROM `vv_comment` where userid = ".$user['id']."";
			$query = $this->db->query($sql);
			$result[$user['id']]['comments'] = $query->row_array();
		}
		
		return $result;
    }
	
	public function getFilterStat($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page, $category, $dfrom, $dto){
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id WHERE 1 ";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($gender!='')
			$sql .=" and gender = '".$gender."'";
		if($keyword!='')
			$sql .=" and (firstname like '%".$keyword."%' or middlename like '%".$keyword."%' or lastname like '%".$keyword."%' or email like '%".$keyword."%')";
		
		$numrows = $this->db->query($sql);
		
		$sql .=" order by id desc";
		//echo $sql.'<br />';
		$query = $this->db->query($sql);
		$users = $query->result_array();
		
		foreach($users as $user) {
			$result[$user['id']] = array('user' => $user);
			
			$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(participation) as tparticipation, SUM(upvote) as tupvote from vv_pollls where user_id = ".$user['id']." and status = 1";
			
			if($dfrom!='')
				$sql .=" and start_date >= '".$dfrom."'";
			if($dto!='')
				$sql .=" and end_date <= '".$dto."'";
			if($category!='')
				$sql .=" and catid = '".$category."'";
			
			//echo 'vv_pollls'.$sql.'<br />';
			$query = $this->db->query($sql);
			$result[$user['id']]['poll'] = $query->row_array();
			
			$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(upvote) as tupvote from vv_post where user_id = ".$user['id']." and status = 1";
			if($dfrom!='')
				$sql .=" and created >= '".$dfrom."'";
			if($dto!='')
				$sql .=" and created <= '".$dto."'";
			if($category!='')
				$sql .=" and catid = '".$category."'";
			
			$query = $this->db->query($sql);
			//echo 'vv_post'.$sql.'<br />';
			$result[$user['id']]['post'] = $query->row_array();
			
			$sql ="SELECT sum(upvote) as tupvote, count(id) as total FROM `vv_comment` where userid = ".$user['id']." and status = 1";
			
			if($dfrom!='')
				$sql .=" and created >= '".$dfrom."'";
			if($dto!='')
				$sql .=" and created <= '".$dto."'";
			
			$query = $this->db->query($sql);
			//echo 'vv_comment'.$sql.'<br />';
			$result[$user['id']]['comments'] = $query->row_array();
		}
		return $result;
    }
	
	public function getUserPoll($userid){
		$result = array();
		$sql = 'SELECT * FROM `vv_users` where id = '.$userid.'';
		$query = $this->db->query($sql);
		$users = $query->row_array();
		
		$sql = 'SELECT vv_pollls.*, vv_categories.cat_name FROM `vv_pollls` left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.user_id = '.$userid.'';
		
		if(isset($_REQUEST['search'])) {		
			if($_REQUEST['dfrom']!='')
				$sql .=" and start_date >= '".date('Y-m-d', strtotime($_REQUEST['dfrom']))."'";
			if($_REQUEST['dto']!='')
				$sql .=" and end_date <= '".date('Y-m-d', strtotime($_REQUEST['dto']))."'";
			if($_REQUEST['cat']!='')
				$sql .=" and catid = '".$_REQUEST['cat']."'";
		}
		
		$sql .= ' order by id desc';
		
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$result['users'] = $users;
		$result['data'] = $data;
		
		return $result;
    }
	
	public function getUserPost($userid){
		$result = array();
		$sql = 'SELECT * FROM `vv_users` where id = '.$userid.'';
		$query = $this->db->query($sql);
		$users = $query->row_array();
		
		
		$sql = 'select vv_post.*, vv_categories.cat_name from vv_post left join vv_categories on vv_post.catid = vv_categories.id where vv_post.user_id = '.$userid.'';
		
		if(isset($_REQUEST['search'])) {		
			if($_REQUEST['dfrom']!='')
				$sql .=" and created >= '".date('Y-m-d', strtotime($_REQUEST['dfrom']))."'";
			if($_REQUEST['dto']!='')
				$sql .=" and created <= '".date('Y-m-d', strtotime($_REQUEST['dto']))."'";
			if($_REQUEST['cat']!='')
				$sql .=" and catid = '".$_REQUEST['cat']."'";
		}
		
		$sql .= ' order by id desc';
		$query = $this->db->query($sql);
		$data = $query->result_array();
		
		$result['users'] = $users;
		$result['data'] = $data;
		
		return $result;
    }
	
	public function getUserComment($userid){
		$result = array();
		$sql = 'SELECT * FROM `vv_users` where id = '.$userid.'';
		$query = $this->db->query($sql);
		$users = $query->row_array();
		
		
		$sql = 'SELECT *, vv_commentdata.content FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where userid = '.$userid.'';
		
		if(isset($_REQUEST['search'])) {
			if($_REQUEST['dfrom']!='')
				$sql .=" and created >= '".date('Y-m-d', strtotime($_REQUEST['dfrom']))."'";
			if($_REQUEST['dto']!='')
				$sql .=" and created <= '".date('Y-m-d', strtotime($_REQUEST['dto']))."'";
			if($_REQUEST['type']!='')
				$sql .=" and ptype = '".$_REQUEST['type']."'";
		}
		
		$sql .= ' order by created desc';
		$query = $this->db->query($sql);
		$results = $query->result_array();
		$comments = array();
		$cat = (isset($_REQUEST['cat']))?$_REQUEST['cat']:'';
		
		foreach($results as $comment) {
			$reference = $comment;
			$switch_reference = (!empty($_REQUEST['type']) && isset($_REQUEST['search']))?$_REQUEST['type']:$reference['ptype'];
			$ref = '';
			
			switch($switch_reference) {
				case 'polls':		
					$sql = 'SELECT vv_categories.cat_name, vv_pollls.id as pid FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = (select pid from vv_comment where vv_comment.id = '.$reference['id'].')';						
					if(isset($_REQUEST['search'])) {		
						if($_REQUEST['cat']!='')
							$sql .=" and vv_pollls.catid = '".$_REQUEST['cat']."'";
					}				
					$query = $this->db->query($sql);
					$ref = $query->row_array();
				break;
				
				case 'post':
					$sql = 'SELECT vv_categories.cat_name, vv_post.id as pid FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = (select pid from vv_comment where vv_comment.id = '.$reference['id'].')';		
					if(isset($_REQUEST['search'])) {		
						if($_REQUEST['cat']!='')
							$sql .=" and vv_post.catid = '".$_REQUEST['cat']."'";
					}				
					$query = $this->db->query($sql);
					$ref = $query->row_array();
				break;
			}
			
			if($ref)
				$comments[] = array('content' => $comment, 'reference' => $ref);
		}
		
		/*foreach($results as $comment) {
			if($comment['reftype'] == 'comment') {
				$reference = $this->Userstat_model->getParentRef($comment['refid']);
				$switch_reference = $reference['reftype'];
				if($_REQUEST['type']!='')
					$switch_reference = $_REQUEST['type'];
				$ref = '';
				switch($switch_reference) {
					case 'polls':		
						$sql = 'SELECT vv_categories.cat_name, vv_pollls.id as pid, com.* FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id inner join (SELECT vv_comment.*, vv_commentdata.content FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where vv_comment.id = '.$reference['id'].') as com on vv_pollls.id = com.refid where vv_pollls.id = (select refid from vv_comment where vv_comment.id = '.$reference['id'].')';						
						if(isset($_REQUEST['search'])) {		
							if($_REQUEST['cat']!='')
								$sql .=" and vv_pollls.catid = '".$_REQUEST['cat']."'";
						}				
						$query = $this->db->query($sql);
						$ref = $query->row_array();
					break;
					
					case 'post':
						$sql = 'SELECT vv_categories.cat_name, vv_post.id as pid, com.* FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id inner join (SELECT vv_comment.*, vv_commentdata.content FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where vv_comment.id = '.$reference['id'].') as com on vv_post.id = com.refid where vv_post.id = (select refid from vv_comment where vv_comment.id = '.$reference['id'].')';		
						if(isset($_REQUEST['search'])) {		
							if($_REQUEST['cat']!='')
								$sql .=" and vv_post.catid = '".$_REQUEST['cat']."'";
						}				
						$query = $this->db->query($sql);
						$ref = $query->row_array();
					break;
				}
				if($ref)
				$comments[] = array('content' => $comment, 'reference' => $ref);
				echo '<p>';
				print_r($comments);
				echo '</p>';
			}
			else {
				$reference = $comment;
				$switch_reference = $reference['reftype'];
				if($_REQUEST['type']!='')
					$switch_reference = $_REQUEST['type'];
				$ref = '';
				switch($switch_reference) {
					case 'polls':		
						echo $sql = 'SELECT vv_categories.cat_name, vv_pollls.id as pid, com.* FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id inner join (SELECT vv_comment.*, vv_commentdata.content FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where vv_comment.id = '.$comment['id'].') as com on vv_pollls.id = com.refid where vv_pollls.id = (select refid from vv_comment where vv_comment.id = '.$comment['id'].')';						
						if(isset($_REQUEST['search'])) {		
							if($_REQUEST['cat']!='')
								$sql .=" and vv_pollls.catid = '".$_REQUEST['cat']."'";
						}				
						$query = $this->db->query($sql);
						$ref = $query->row_array();
					break;
					
					case 'post':
						$sql = 'SELECT vv_categories.cat_name, vv_post.id as pid, com.* FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id inner join (SELECT vv_comment.*, vv_commentdata.content FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where vv_comment.id = '.$comment['id'].') as com on vv_post.id = com.refid where vv_post.id = (select refid from vv_comment where vv_comment.id = '.$comment['id'].')';		
						if(isset($_REQUEST['search'])) {		
							if($_REQUEST['cat']!='')
								$sql .=" and vv_post.catid = '".$_REQUEST['cat']."'";
						}				
						$query = $this->db->query($sql);
						$ref = $query->row_array();
					break;
				}
				if($ref)
				$comments[] = array('content' => $comment, 'reference' => $ref);
				echo '<p>';
				print_r($comments);
				echo '</p>';
			}
		}*/
		
		
		$result['users'] = $users;
		$result['data'] = $comments;
		
		return $result;
    }
	
	public function getUserUpvote($userid, $q){
		$result = array();
		$sql = 'SELECT * FROM `vv_users` where id = '.$userid.'';
		$query = $this->db->query($sql);
		$users = $query->row_array();		
		
		$sql = 'SELECT sum(upvote) as total FROM `vv_pollls` where user_id = '.$userid.' and status = 1';
		$sql = 'SELECT sum(upvote) as total FROM `vv_pollls` where user_id = '.$userid.'';
		$query = $this->db->query($sql);
		$poll = $query->row_array();
		
		$sql = 'SELECT sum(upvote) as total FROM `vv_post` where user_id = '.$userid.' and status = 1';
		$sql = 'SELECT sum(upvote) as total FROM `vv_post` where user_id = '.$userid.'';
		$query = $this->db->query($sql);
		$post = $query->row_array();
		
		$sql = 'SELECT sum(upvote) as total FROM `vv_comment` where userid = '.$userid.' and status = 1';
		$sql = 'SELECT sum(upvote) as total FROM `vv_comment` where userid = '.$userid.'';
		$query = $this->db->query($sql);
		$comment = $query->row_array();
		
		if(!empty($q)) {
			switch($q) {
				case 'poll':
					$sql ="SELECT vv_upvote.*, vv_pollls.question, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_upvote` inner join vv_pollls on vv_upvote.refid = vv_pollls.id inner join vv_users on vv_upvote.userid = vv_users.id where vv_upvote.reference = 'polls' and vv_pollls.user_id = ".$userid;
					$query = $this->db->query($sql);
					$data['result'] = $query->result_array();
					$data['users'] = $users;
				break;
				
				case 'post':
					$sql ="SELECT vv_upvote.*, vv_post.postdata, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_upvote` inner join vv_post on vv_upvote.refid = vv_post.id inner join vv_users on vv_upvote.userid = vv_users.id where vv_upvote.reference = 'post' and vv_post.user_id = ".$userid;
					$query = $this->db->query($sql);
					$data['result'] = $query->result_array();
					$data['users'] = $users;
				break;
				
				case 'comment':
					$sql ="SELECT vv_upvote.*, comment.*, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_upvote` inner join (select vv_comment.id as cid, pid, ptype, vv_commentdata.content from vv_comment inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where userid = ".$userid.") as comment on vv_upvote.refid = comment.cid inner join vv_users on vv_upvote.userid = vv_users.id where vv_upvote.reference = 'comment'";
					$query = $this->db->query($sql);
					$data['result'] = $query->result_array();
					$data['users'] = $users;
				break;
			}
		}
		
		$data['users'] = $users;
		$data['poll'] = $poll;
		$data['post'] = $post;
		$data['comment'] = $comment;
		
		return $data;
    }
	
	
	public function getParentRef($id){
		$result = array();
		
		$sql ="select * from vv_comment where id = ".$id."";
		$query = $this->db->query($sql);
		$comments = $query->result_array();
		
		foreach($comments as $comment) {
			if($comment['reftype'] == 'comment')
				$result = $this->Userstat_model->getParentRef($comment['refid']);
			else
				$result = $comment;
		}
		
		return $result;
    }
	
	
	public function getParentinfo($id, $type, $cat){
		$result = array();
		
		switch($type) {
			case 'polls':
				$sql ="SELECT vv_categories.cat_name, vv_pollls.id as pid FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$id."";
				$query = $this->db->query($sql);
				$result = $query->row_array();
			break;
			
			case 'post':
				$sql ="SELECT vv_categories.cat_name, vv_post.id as pid FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$id."";
				$query = $this->db->query($sql);
				$result = $query->row_array();
			break;
		}
		
		return $result;
    }
	
	
	public function getUserpollDetail($pollid, $q){
		$data = array();
		$sql ="select * from vv_pollls inner join vv_polloption on vv_pollls.id = vv_polloption.pollid where vv_pollls.id = ".$pollid."";
		$query = $this->db->query($sql);
		$data['poll'] = $query->result_array();
		
		switch($q) {
			case 'participation':
				$sql ="SELECT * FROM `vv_pollvoting` inner join vv_users on vv_pollvoting.userid = vv_users.id where vv_pollvoting.pollid = ".$pollid."";
				$query = $this->db->query($sql);
				$data['result'] = $query->result_array();
			break;
			
			case 'upvote':
				$sql ="SELECT * FROM `vv_upvote` inner join vv_users on vv_upvote.userid = vv_users.id where vv_upvote.refid = ".$pollid." and vv_upvote.reference='polls'";
				$query = $this->db->query($sql);
				$data['result'] = $query->result_array();
			break;
			
			case 'sharing':
				$sql ="SELECT * FROM `vv_share` inner join vv_users on vv_share.userid = vv_users.id where vv_share.refid = ".$pollid." and vv_share.reftype='polls'";
				$query = $this->db->query($sql);
				$data['result'] = $query->result_array();
			break;
			
			case 'comments':
				$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, tt.total FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id left join (select count(id) as total, refid, reftype from vv_comment where vv_comment.refid = ".$pollid." and reftype = 'polls') as tt on vv_comment.refid = tt.refid where vv_comment.refid = ".$pollid." and vv_comment.reftype = 'polls' order by vv_comment.id desc";
				$query = $this->db->query($sql);
				$results = $query->result_array();
				
				$value = array();
				foreach($results as $key => $result) {
					$subsql = "select count(id) as subcomment from vv_comment where refid = ".$result['id']." and reftype='comment'";
					$subque = $this->db->query($subsql);
					$vl = $subque->row_array();
					$result['subcomment'] = $vl['subcomment'];
					$value[$key] = $result;
				}
				$data['result'] = $value;
			break;
		}
		return $data;
    }
	
	
	public function getUserpostDetail($postid, $q){
		$data = array();
		$sql ="select * from vv_post where vv_post.id = ".$postid."";
		$query = $this->db->query($sql);
		$data['post'] = $query->result_array();
		
		switch($q) {
			case 'upvote':
				$sql ="SELECT * FROM `vv_upvote` inner join vv_users on vv_upvote.userid = vv_users.id where vv_upvote.refid = ".$postid." and vv_upvote.reference='post'";
				$query = $this->db->query($sql);
				$data['result'] = $query->result_array();
			break;
			
			case 'sharing':
				$sql ="SELECT * FROM `vv_share` inner join vv_users on vv_share.userid = vv_users.id where vv_share.refid = ".$postid." and vv_share.reftype='post'";
				$query = $this->db->query($sql);
				$data['result'] = $query->result_array();
			break;
			
			case 'comments':
				$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, tt.total FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id left join (select count(id) as total, refid, reftype from vv_comment where vv_comment.refid = ".$postid." and reftype = 'post') as tt on vv_comment.refid = tt.refid where vv_comment.refid = ".$postid." and vv_comment.reftype = 'post' order by vv_comment.id desc";
				$query = $this->db->query($sql);
				$results = $query->result_array();
				
				$value = array();
				foreach($results as $key => $result) {
					$subsql = "select count(id) as subcomment from vv_comment where refid = ".$result['id']." and reftype='comment'";
					$subque = $this->db->query($subsql);
					$vl = $subque->row_array();
					$result['subcomment'] = $vl['subcomment'];
					$value[$key] = $result;
				}
				$data['result'] = $value;
			break;
		}
		return $data;
    }
}