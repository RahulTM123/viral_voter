<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comp_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function addCompName($name,$pages){
		$input = array('com_name' => $name, 'wheree' => $pages );
		$this->db->insert('vv_com_name', $input);
		$insertId = $this->db->insert_id();
		return  $insertId;
    }
	
	public function getCompNameList(){
		$sql ="SELECT * FROM vv_com_name";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function changeCompNameStatus($id){
		$sql ="SELECT status FROM vv_com_name WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_com_name SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_com_name SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	
	
	public function addNewComp($category,$title,$img,$country_id,$state_id,$city_id,$start_date,$end_date,$show_title,$show_cat,$show_country,$show_state,$show_city,$show_strt_date,$show_end_date,$gender,$rewards,$age_from,$age_to,$show_age,$show_gender,$show_rewards){
		//echo $img; exit;
		$input = array('catid' => $category, 'question' => $title, 'image' => $img, 'country' => $country_id, 'state' => $state_id, 'city' => $city_id, 'start_date' => $start_date, 'end_date' => $end_date,'show_title'=>$show_title,'show_cat'=>$show_cat,'show_country'=>$show_country,'show_state'=>$show_state,'show_city'=>$show_city,'show_strt_date'=>$show_strt_date,'show_end_date'=>$show_end_date,'gender'=>$gender,'rewards'=>$rewards,'age_from'=>$age_from,'age_to'=>$age_to,'show_age'=>$show_age,'show_gender'=>$show_gender,'show_rewards'=>$show_rewards);
		//print_r($input); exit;
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_compitition', $input);
		$insertId = $this->db->insert_id();
		
		$data = array('catid' => $category, 'log_id' => $insertId, 'log_title' => 'compition', 'ip' => $_SERVER['SERVER_ADDR']);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_logs', $data);
		
		return  $insertId;
    }
	
	public function editComp($category,$title,$img,$country,$state,$city,$start_date,$end_date,$compId,$show_title,$show_cat,$show_country,$show_state,$show_city,$show_strt_date,$show_end_date,$gender,$rewards,$age_from,$age_to,$show_gender,$show_rewards,$show_age){
		
		$input = array('catid' => $category, 'question' => $title, 'image' => $img,'country' => $country, 'state' => $state,'city' => $city,'start_date' => $start_date,'end_date' => $end_date,'show_title'=>$show_title,'show_cat'=>$show_cat,'show_country'=>$show_country,'show_state'=>$show_state,'show_city'=>$show_city,'show_strt_date'=>$show_strt_date,'show_end_date'=>$show_end_date,'gender'=>$gender,'rewards'=>$rewards,'age_from'=>$age_from,'age_to'=>$age_to,'show_gender'=>$show_gender,'show_rewards'=>$show_rewards,'show_age'=>$show_age);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->update('vv_compitition', $input,array('id' => $compId));
		
		$data = array('catid' => $category);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->update('vv_logs', $data,array('log_id' => $compId, 'log_title' => 'compition'));
		
		return  'success';
    }
	
	public function getCompList(){
		$html = '';
		
		$sql ="SELECT * FROM vv_compitition";
		$query = $this->db->query($sql);
		$val = $query->result();	
		$count = count($val);
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($count/$recordsPerPage);
		$lpm1 = $lastpage - 1; 
		
		$sql ="SELECT * FROM vv_compitition LIMIT ".$start.",".$recordsPerPage;
		$query = $this->db->query($sql);
		$result = $query->result();
		
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td>'.$i.'</td>';
				
				$html .= '<td><a href="'.base_url().'admin/competition_result/'.$value->id.'">'.$value->question.'</a></td>';
				
				if($value->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activate</p></a><a href="competition_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}else{
					$html .= '<td><a href="competition_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivate</p></a></td>';
				}
				$html .= '<td><a href="'.base_url().'admin/edit_competition/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen btn-success">Edit</p></a><a href="'.base_url().'admin/competition_list/delete?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
		}else{
			$html .= '<tr><td colspan="4">No Result Found!</td></tr></tbody></table>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=\"?page=".($prev)."\">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
					$html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
				   $html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
			   }else{
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=\"?page=".($next)."\">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		
		return $html;
    }
	
	
	public function getCompListByFilter($cat,$dfrom,$dto){
		// $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		// $linkArray = explode('&page',$actual_link);
		// $actual_link = $linkArray[0];
		 $html = '';
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		
		// $sql ="SELECT * FROM  `vv_comment` WHERE log_title = 'compitition'";
		// $query = $this->db->query($sql);
		// $voteList = $query->result();
		
		// if($catFil != '' && $voteFil != ''){
		// 	if($voteFil == 'high'){
		// 		if(!empty($voteList)){
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE c.catid = '".$catFil."' && cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total DESC";
		// 			$query = $this->db->query($sql);
		// 			$val = $query->result();	
		// 			$count = count($val);
		// 			$lastpage = ceil($count/$recordsPerPage);
		// 			$lpm1 = $lastpage - 1; 
					
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE c.catid = '".$catFil."' && cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
		// 			$query = $this->db->query($sql);
		// 			$result = $query->result();	
		// 		}else{
		// 			$lastpage = '';
		// 			$result = '';	
		// 		}
		// 	}else{
		// 		if(!empty($voteList)){
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE c.catid = '".$catFil."' && cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total ASC";
		// 			$query = $this->db->query($sql);
		// 			$val = $query->result();	
		// 			$count = count($val);
		// 			$lastpage = ceil($count/$recordsPerPage);
		// 			$lpm1 = $lastpage - 1; 
					
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE c.catid = '".$catFil."' && cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
		// 			$query = $this->db->query($sql);
		// 			$result = $query->result();
		// 		}else{
		// 			$lastpage = '';
		// 			$result = '';	
		// 		}	
		// 	}
			
		// }elseif($catFil != ''){
		// 	$sql ="SELECT * FROM `vv_compitition` WHERE catid = '".$catFil."'";
		// 	$query = $this->db->query($sql);
		// 	$val = $query->result();	
		// 	$count = count($val);
		// 	$lastpage = ceil($count/$recordsPerPage);
		// 	$lpm1 = $lastpage - 1; 
			
		// 	$sql ="SELECT * FROM `vv_compitition` WHERE catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
		// 	$query = $this->db->query($sql);
		// 	$result = $query->result();
		// }else{
		// 	if($voteFil == 'high'){
		// 		if(!empty($voteList)){
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total DESC";
		// 			$query = $this->db->query($sql);
		// 			$val = $query->result();	
		// 			$count = count($val);
		// 			$lastpage = ceil($count/$recordsPerPage);
		// 			$lpm1 = $lastpage - 1; 
					
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
		// 			$query = $this->db->query($sql);
		// 			$result = $query->result();
		// 		}else{
		// 			$lastpage = '';
		// 			$result = '';	
		// 		}
		// 	}else{
		// 		if(!empty($voteList)){
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total ASC";
		// 			$query = $this->db->query($sql);
		// 			$val = $query->result();	
		// 			$count = count($val);
		// 			$lastpage = ceil($count/$recordsPerPage);
		// 			$lpm1 = $lastpage - 1; 
					
		// 			$sql ="SELECT COUNT(c.id) as total, c.* FROM `vv_compitition` as c LEFT JOIN `vv_comment` as cmt ON c.id = cmt.logid WHERE cmt.log_title = 'compitition' GROUP BY cmt.logid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
		// 			$query = $this->db->query($sql);
		// 			$result = $query->result();
		// 		}else{
		// 			$lastpage = '';
		// 			$result = '';	
		// 		}
		// 	}
		// }
		$andWhere = '';
    	if(isset($dfrom) && !empty($dfrom)) 
    	{
    		$andWhere .= ' and DATE(start_date)="'.$dfrom.'"';
    	}
    	if(isset($dto) && !empty($dto)) 
    	{
    		$andWhere .= ' and DATE(end_date)="'.$dto.'"';
    	}
    	
    	if(isset($cat) && !empty($cat)) 
    	{
    		$andWhere .= ' and catid='.$cat.'';
    	}
    	$sql ="SELECT * FROM vv_compitition WHERE created != '' ".$andWhere."";
				//echo $sql; exit;
				$query = $this->db->query($sql);
				$result = $query->result();
				
				$count = count($result);
	 			$lastpage = ceil($count/$recordsPerPage);
	 			$lpm1 = $lastpage - 1;
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td>'.$i.'</td>';
				
				$html .= '<td><a href="'.base_url().'admin/competition_result/'.$value->id.'">'.$value->question.'</a></td>';
				
				if($value->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activate</p></a><a href="competition_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}else{
					$html .= '<td><a href="competition_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivate</p></a></td>';
				}
				$html .= '<td><a href="'.base_url().'admin/edit_competition/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen btn-success">Edit</p></a><a href="'.base_url().'admin/competition_list/delete?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
		}else{
			$html .= '<tr><td colspan="4">No Result Found!</td></tr></tbody></table>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=".$actual_link."&page=".($prev).">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
					$html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
				   $html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
			   }else{
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=".$actual_link."&page=".($next).">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
    }
	
	
	public function changeCompStatus($id){
		$sql ="SELECT status FROM vv_compitition WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_compitition SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_compitition SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function deleteComp($id){
		$sql ="SELECT * FROM vv_compitition WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$qunserdata = unserialize($value[0]->com_ques);
		if(count($qunserdata) == '2'){
			unlink('./uploads/'.$qunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($qunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$qunserdata[0]);
			}
		}
		
		$aunserdata = unserialize($value[0]->ans1);
		if(count($aunserdata) == '2'){
			unlink('./uploads/'.$aunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($aunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$aunserdata[0]);
			}
		}
		
		$a1unserdata = unserialize($value[0]->ans2);
		if(count($a1unserdata) == '2'){
			unlink('./uploads/'.$a1unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a1unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a1unserdata[0]);
			}
		}
		
		$a2unserdata = unserialize($value[0]->ans3);
		if(count($a2unserdata) == '2'){
			unlink('./uploads/'.$a2unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a2unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a2unserdata[0]);
			}
		}
		
		$a3unserdata = unserialize($value[0]->ans4);
		if(count($a3unserdata) == '2'){
			unlink('./uploads/'.$a3unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a3unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a3unserdata[0]);
			}
		}
		
		$sql ="DELETE FROM vv_compitition WHERE id=".$id;
		$this->db->query($sql);
		return 1;
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	
	public function getCompById($id){ 
		$sql ="SELECT * FROM vv_compitition WHERE id = ".$id;
		$query = $this->db->query($sql);
		$result = $query->result();	
		$data = array(
			'id' => $result[0]->id,
			'catid' => $result[0]->catid,
			'question' => $result[0]->question,
			'country' => $result[0]->country,
			'state' => $result[0]->state,
			'city' => $result[0]->city,
			'image' => $result[0]->image,
			'start_date' => $result[0]->start_date,
			'end_date' => $result[0]->end_date,
			'show_title' => $result[0]->show_title,
			'show_cat' => $result[0]->show_cat,
			'show_country' => $result[0]->show_country,
			'show_state' => $result[0]->show_state,
			'show_city' => $result[0]->show_city,
			'show_strt_date' => $result[0]->show_strt_date,
			'show_end_date' => $result[0]->show_end_date,
			'gender' => $result[0]->gender,
			'rewards' => $result[0]->rewards,
			'age_from' => $result[0]->age_from,
			'age_to' => $result[0]->age_to,
			'show_gender' => $result[0]->show_gender,
			'show_rewards' => $result[0]->show_rewards,
			'show_age' => $result[0]->show_age,
		);
		return $data;
    }
	
	
	public function getCatListById($id){
		$html = '';
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$catList = $query->result();
		if(!empty($catList)){
			$html .= '<select name="catName" id="catName" class="form-control" style="margin-bottom:15px;"><option value="">--Select Category--</option>';
			foreach($catList as $value){
				if($value->id == $id){
					$html .= '<option value="'.$value->id.'" selected="selected">'.$value->cat_name.'</option>';
				}else{
					$html .= '<option value="'.$value->id.'">'.$value->cat_name.'</option>';
				}
			}
			$html .= '</select>';
		}else{
			$html .= '<select name="catName" id="catName" class="form-control" style="margin-bottom:15px;"><option value="">--Select Category--</option></select>';
		}
		return $html;
    }
	
}