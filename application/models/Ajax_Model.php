<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax_Model extends CI_Model{
	function __construct() {
		$this->load->database();
	}
	
	public function userdata($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$userquery = $this->db->query($sql);
		$userdata = $userquery->result();
		return $userdata;
	}
	
	public function saveProfilePic($userid,$pics){ 
		$input = array('profile_pic_url' => $pics);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
	}
	
	public function getOldPic($userid){
		$sql ="SELECT profile_pic_url FROM vv_users WHERE id = ".$userid;
		$userquery = $this->db->query($sql);
		$userdata = $userquery->result();
		return $userdata[0]->profile_pic_url;
	}
	
	
	public function saveCoverPic($userid,$pics){ 
		$input = array('cover_pic_url' => $pics);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
	}
	
	public function add_friend_request($user_id,$friend_id){
		$data = array('friend_one' => $user_id, 'friend_two' => $friend_id,'notify_status' => '0');
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_friends', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	
	public function live_section($pid,$likeid,$userid,$title){
		if(!empty($likeid)){
			$this->db->where('id', $likeid);
   			$this->db->delete('vv_like');
			$sql ="SELECT totalLike as total,id FROM vv_likecounter WHERE logid = '".$pid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total - 1;
				$data = array('totalLike' => $total);
				$update = $this->db->update('vv_likecounter',$data,array('id' => $getResult[0]->id));
			}
			$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$pid."' AND log_title = '".$title."'";
			$pqry = $this->db->query($sql);
			$pdata = $pqry->result();
			return 'dislike,'.$pdata[0]->total;
		}else{
			$input = array('logid' => $pid, 'userid' => $userid, 'log_title' => $title, 'status' => '1');
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->set('modified', 'NOW()', FALSE);
			$this->db->insert('vv_like', $input);
			$insertId = $this->db->insert_id();
			if(!empty($insertId)){
				$sql ="SELECT totalLike as total,id FROM vv_likecounter WHERE logid = '".$pid."' AND title = '".$title."'";
				$query = $this->db->query($sql);
				$getResult = $query->result();
				if(!empty($getResult)){
					$total = $getResult[0]->total;
					$total = $total + 1;
					$data = array('totalLike' => $total);
					$update = $this->db->update('vv_likecounter',$data,array('id' => $getResult[0]->id));
				}else{
					$input = array('logid' => $pid, 'totalLike' => '1', 'title' => $title);
					$this->db->insert('vv_likecounter', $input);
				}
			}
			$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$pid."' AND log_title = '".$title."'";
			$pqry = $this->db->query($sql);
			$pdata = $pqry->result();
			return $insertId.','.$pdata[0]->total;
		}
	}
	
	
	
	
	public function cmntLike_section($cmntId,$likeid,$userid){
		if(!empty($likeid)){
			$this->db->where('id', $likeid);
   			$this->db->delete('vv_cmntLike');
			$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$cmntId."' AND userid = '".$userid."'";
			$pqry = $this->db->query($sql);
			$pdata = $pqry->result();
			return 'dislike,'.$pdata[0]->total;
		}else{
			$input = array('cmntId' => $cmntId, 'userid' => $userid);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->set('modified', 'NOW()', FALSE);
			$this->db->insert('vv_cmntLike', $input);
			$insertId = $this->db->insert_id();
			$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$cmntId."' AND userid = '".$userid."'";
			$pqry = $this->db->query($sql);
			$pdata = $pqry->result();
			return $insertId.','.$pdata[0]->total;
		}
	}
	
	
	
	
	public function share_section($pid,$userid,$logtitle){
		$input = array('log_id' => $pid, 'userid' => $userid, 'log_title' => $logtitle, 'ip' => $_SERVER['SERVER_ADDR'], 'status' => '1');
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_logs', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalShare as total,id FROM vv_sharecounter WHERE logid = '".$pid."' AND title = '".$logtitle."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalShare' => $total);
				$update = $this->db->update('vv_sharecounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pid, 'totalShare' => '1', 'title' => $logtitle);
				$this->db->insert('vv_sharecounter', $input);
			}
		}
		return $insertId;
	}
	
	public function getOldCoverPic($userid){
		$sql ="SELECT cover_pic_url FROM vv_users WHERE id = ".$userid;
		$userquery = $this->db->query($sql);
		$userdata = $userquery->result();
		return $userdata[0]->cover_pic_url;
	}
	
	
	public function check_request($user_id){
		$data = array('notify_status' => '1');
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_friends',$data,array('friend_two'=>$user_id,'status'=>'0'));
		
		$sql ="SELECT * FROM vv_friends WHERE friend_two=".$user_id." AND status = '0'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	}
	
	public function friend_notify($user_id){
		$total = '';
		$sql ="SELECT count(id) as total FROM vv_friends WHERE friend_two=".$user_id." AND status = '0' AND notify_status = '0'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->total > 0){
			$total = $value[0]->total;	
		}
		return $total;
	}
	
	
	public function confirm_request($id){
		$data = array('status' => '1');
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_friends',$data,array('id'=>$id));
		return $id;
	}
	
	public function delete_request($id){
		$this->db->where('id', $id);
   		$this->db->delete('vv_friends'); 
	}
	
	public function getPollCommentData($id,$title,$userid){
		$html = '';
		
		if($title == 'compitition'){
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = '".$title."' ORDER BY id DESC LIMIT 5";
			$cmntquery = $this->db->query($sql);
			$cmntdata = $cmntquery->result();
			if(!empty($cmntdata)){
				foreach($cmntdata as $data){
					$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					$html .= '<div class="clearfix clearComp"><table style="width:100%"><tbody><tr><td style="width:10%;"><div class="user"> <span class="sm-pic">';
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</a></span> </div></td><td style="width:30%;"><div class="user_cmmnt_block"> <span>';
					
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
					}
					
					$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
					
					if(!empty($data->content)){
						$html .= '<span> '.$data->content.'</span>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$likeCountData = $query->result();
					
					$sql ="SELECT * FROM vv_cmntLike WHERE cmntId = '".$data->id."' && userid = '".$userid."'";
					$query = $this->db->query($sql);
					$likequery = $query->result();
					
					$html .= '</div></td><td style="width:30%;"><p style="margin-bottom:5px;">';
				
					if($data->cmntImg != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionOne != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionOne.'</figcaption>';	
						}
						$html .= '</figure>';
					}
					
					if($data->cmntImgTwo != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionTwo != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionTwo.'</figcaption>';	
						}
						$html .= '</figure>';
					}
					
					if($data->cmntImgThree != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgThree.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionThree != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionThree.'</figcaption>';	
						}
						$html .= '</figure>';
					}
				
					$html .= '</p></td style="width:30%;"><td><div class="poll-list"><ul class="poll-statistic" style="text-align: center;"><li><span class="question-views">'.$likeCountData[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$cmmntCountData[0]->total.' </span><i class="fa fa-comment"></i>Comment</li></ul></div></td></tr></tbody></table>
					
					<span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContent" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContent" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
						
						if($data->captionOne != ''){
							$html .= '<input type="hidden" class="commentCaptionOne" value="'.$data->captionOne.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionOne" value="" />';	
						}
						
						if($data->cmntImgTwo != ''){
							$html .= '<input type="hidden" class="commentImageTwo" value="'.$data->cmntImgTwo.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageTwo" value="" />';	
						}
						
						if($data->captionTwo != ''){
							$html .= '<input type="hidden" class="commentCaptionTwo" value="'.$data->captionTwo.'"  />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionTwo" value="" />';	
						}
						
						if($data->cmntImgThree != ''){
							$html .= '<input type="hidden" class="commentImageThree" value="'.$data->cmntImgThree.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageThree" value="" />';	
						}
						
						if($data->captionThree != ''){
							$html .= '<input type="hidden" class="commentCaptionThree" value="'.$data->captionThree.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionThree" value="" />';	
						}
					
					$html .= '</span><div class="usr_content_block"><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntComp" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					
					
					
					$html .= '</div></div>';
				}
			}else{
				$html = '';	
			}
		}else{
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = '".$title."' ORDER BY id DESC LIMIT 5";
			$cmntquery = $this->db->query($sql);
			$cmntdata = $cmntquery->result();
			if(!empty($cmntdata)){
				foreach($cmntdata as $data){
					$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					
					$sql ="SELECT * FROM vv_cmntLike WHERE cmntId = '".$data->id."' && userid = '".$userid."'";
					$query = $this->db->query($sql);
					$likequery = $query->result();
					
					$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
					
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
					}
					
					$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
					if(!empty($data->cmntImg) && !empty($data->content)){
						$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContentP" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
					
						$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}
							
					}elseif(!empty($data->cmntImg)){
						$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContentP" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
					
						$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}
						
					}else{
						$html .= '<span> '.$data->content.'</span></div><span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContentP" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
					
						$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
	  <div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}	
					}
					$html .= '</div></div>';
				}
				
			}else{
				$html = '';	
			}
		}
		return $html;
    }
	
	
	public function addPollComment($pollid,$userid,$content,$cmntImg,$title){
		$html = '';
		$input = array('logid' => $pollid, 'userid' => $userid, 'log_title' => $title, 'content' => $content, 'cmntImg' => $cmntImg);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pollid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pollid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}
		
		$sql ="SELECT * FROM vv_comment WHERE logid = '".$pollid."' && log_title = '".$title."' ORDER BY id DESC LIMIT 5";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				if(!empty($data->cmntImg) && !empty($data->content)){
					$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}	
					
				}elseif(!empty($data->cmntImg)){
					$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
					
					
				}else{
					$html .= '<span> '.$data->content.'</span></div><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
  <div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
				}
				$html .= '</div></div>';
			}
		}else{
			$html = '';	
		}
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pollid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt;
    }
	
	
	public function addSinglePollComnt($pollid,$userid,$content,$cmntImg,$title){
		$html = '';
		$input = array('logid' => $pollid, 'userid' => $userid, 'log_title' => $title, 'content' => $content,'cmntImg' => $cmntImg);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pollid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pollid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}
		
		
		$sql ="SELECT * FROM vv_comment WHERE id = '".$insertId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				
				if(!empty($data->cmntImg) && !empty($data->content)){
					$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
				
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
						
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}	
					
				}elseif(!empty($data->cmntImg)){
					$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
				
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
						
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
					
					
				}else{
					$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
  <div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
				
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
						
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
				}
				
				$html .= '</div></div>';
			}
		}else{
			$html = '';	
		}
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pollid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt;
    }
	
	
	
	public function moreComments($pollid,$userid,$content,$title,$row){
		$html = '';
		$showperpage = 5;
		
		if($title == 'compitition'){
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$pollid."' && log_title = '".$title."' ORDER BY id ASC LIMIT ".$row.",".$showperpage;
			$cmntquery = $this->db->query($sql);
			$cmntdata = $cmntquery->result();
			if(!empty($cmntdata)){
				foreach($cmntdata as $data){
					$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					$html .= '<div class="clearfix clearComp"><table style="width:100%"><tbody><tr><td style="width:10%;"><div class="user"> <span class="sm-pic">';
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</a></span> </div></td><td style="width:30%;"><div class="user_cmmnt_block"> <span>';
					
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
					}
					
					$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
					
					if(!empty($data->content)){
						$html .= '<span> '.$data->content.'</span>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$likeCountData = $query->result();
					
					$html .= '</div></td><td style="width:30%;"<p style="margin-bottom:5px;">';
				
					if($data->cmntImg != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionOne != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionOne.'</figcaption>';	
						}
						$html .= '</figure>';
					}
					
					if($data->cmntImgTwo != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionTwo != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionTwo.'</figcaption>';	
						}
						$html .= '</figure>';
					}
					
					if($data->cmntImgThree != ''){
						$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgThree.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
						if($data->captionThree != ''){
							$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionThree.'</figcaption>';	
						}
						$html .= '</figure>';
					}
				
					$html .= '</p></td style="width:30%;"><td><div class="poll-list"><ul class="poll-statistic" style="text-align: center;"><li><span class="question-views">'.$likeCountData[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$cmmntCountData[0]->total.' </span><i class="fa fa-comment"></i>Comment</li></ul></div></td></tr></tbody></table><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContent" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContent" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					if($data->captionOne != ''){
						$html .= '<input type="hidden" class="commentCaptionOne" value="'.$data->captionOne.'" />';	
					}else{
						$html .= '<input type="hidden" class="commentCaptionOne" value="" />';	
					}
					
					if($data->cmntImgTwo != ''){
						$html .= '<input type="hidden" class="commentImageTwo" value="'.$data->cmntImgTwo.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageTwo" value="" />';	
					}
					
					if($data->captionTwo != ''){
						$html .= '<input type="hidden" class="commentCaptionTwo" value="'.$data->captionTwo.'"  />';	
					}else{
						$html .= '<input type="hidden" class="commentCaptionTwo" value="" />';	
					}
					
					if($data->cmntImgThree != ''){
						$html .= '<input type="hidden" class="commentImageThree" value="'.$data->cmntImgThree.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageThree" value="" />';	
					}
					
					if($data->captionThree != ''){
						$html .= '<input type="hidden" class="commentCaptionThree" value="'.$data->captionThree.'" />';	
					}else{
						$html .= '<input type="hidden" class="commentCaptionThree" value="" />';	
					}
					
					$html .= '</span><div class="usr_content_block"><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					if($userid == $data->userid){
						$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntComp" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					}
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							if($userid == $cdata->userid){
								$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							}
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					
					
					
					$html .= '</div></div>';
				}
			}else{
				$html = '';	
			}
		}else{
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$pollid."' && log_title = '".$title."' ORDER BY id ASC LIMIT ".$row.",".$showperpage;
			$cmntquery = $this->db->query($sql);
			$cmntdata = $cmntquery->result();
			if(!empty($cmntdata)){
				foreach($cmntdata as $data){
					$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</a></span> </div><div class="user_cmmnt_block"> <span>';
					
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
					}
					
					$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
					
					if(!empty($data->cmntImg) && !empty($data->content)){
						$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
						if($userid == $data->userid){
							$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
						}
						
						$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}	
						
					}elseif(!empty($data->cmntImg)){
						$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
						if($userid == $data->userid){
							$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
						}
						
						$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}
						
						
					}else{
						$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
	  <div class="_khz _4sz1 _4rw5 _3wv2">';
						if(!empty($likequery)){
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
						}else{
							$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
						}
						$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
						if($userid == $data->userid){
							$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
						}
						
						$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
						
						$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntdata = $query->result();
						
						if(!empty($cmmntdata)){
							foreach($cmmntdata as $cdata){
								$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
								$userquery = $this->db->query($sql);
								$cmntuser = $userquery->result();
								
								$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
								}
								if(!empty($cmntuser[0]->profile_pic_url)){
									$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
								}elseif(!empty($cmntuser[0]->picture_url)){
									$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
								}else{
									$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
								}
								
								$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
								
								if($cmntuser[0]->username != ''){
									$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
								}
								
								$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
								if($userid == $cdata->userid){
									$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
								}
								
								$html .= '</div><span class="editCommentChildData">';
					
								if($cdata->content != ''){
									$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
								}else{
									$html .= '<div class="commentChildContent" style="display:none;"></div>';
								}
								$html .= '</span></div>';
							}
						}
						
						$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
						$query = $this->db->query($sql);
						$cmmntCountData = $query->result();
						if($cmmntCountData[0]->total > 0){
							$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
						}
					}
					
					$html .= '</div></div>';
				}
			}else{
				$html = '';	
			}
		}
		return $html;
    }
	
	
	
	
	
	public function getFrndList($content,$userid,$frndsid){
		$html = '';
		$friends_id = array();
		$sql ="SELECT * FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $v){
				if($v->friend_one == $userid){
					array_push($friends_id,$v->friend_two);	
				}else{
					array_push($friends_id,$v->friend_one);		
				}
			}
			if(!empty($frndsid)){
				$remove = explode(',',$frndsid);
				for($i = 0; $i< count($remove); $i++){
					if (($key = array_search($remove[$i], $friends_id)) !== false) {
						unset($friends_id[$key]);
					}
				}
				$friend_ids = implode(',',$friends_id);
			}else{
				$friend_ids = implode(',',$friends_id);
			}
			
			$sql ="SELECT * FROM vv_users WHERE (firstname like '%$content%' || lastname like '%$content%') AND id IN (".$friend_ids.") ORDER BY firstname ASC LIMIT 8";
			$userquery = $this->db->query($sql);
			$userdata = $userquery->result();
			
			foreach($userdata as $u){
				$html .= '<div class="display_box" align="left">';	
				if(!empty($u->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$u->profile_pic_url.'" style="width:25px; float:left; margin-right:6px">';
				}elseif(!empty($u->picture_url)){
					$html .= '<img src="'.$u->picture_url.'" style="width:25px; float:left; margin-right:6px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:25px; float:left; margin-right:6px">';
				}
				$html .= ' <a href="" class="addname" title="'.$u->firstname.' '.$u->lastname.'" id="'.$u->id.'">'.$u->firstname.' '.$u->lastname.'</a></div>';
			}
		}
		return $html;
    }




    public function getPollListBeforeLogin($fetchby,$catid){
		$html = '';
		$value = $polllValue = $shortId = $count = '';
		if($fetchby == 'vote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(first.id) as total FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollresult ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollresult ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}	
			$count = $totalCount[0]->total-1;
			$shortId = 'pollresult';		
		}elseif($fetchby == 'upvote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(first.id) as total FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(id) as total FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();		
			}	
			$count = $totalCount[0]->total-1;
			$shortId = 'likecounter';				
		}elseif($fetchby == 'comment'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
				
				$sql ="SELECT COUNT(first.id) as total FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}else{
				$sql ="SELECT * FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
			
				$sql ="SELECT COUNT(id) as total FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}	
			$count = $totalCount[0]->total-1;	
			$shortId = 'commentcounter';				
		}else{
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollls ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();	
				
				$sql ="SELECT COUNT(id) as total FROM vv_pollls ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}
				
			$count = $totalCount[0]->total-1;
			$shortId = 'pollsDesc';
			if(!empty($rvalue)){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';	
					}					
				}
				if($count > 0){	
				    if(!empty($catid)){
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid="'.$catid.'"></li>';
					}else{
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid=""></li>';
					}
				}
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		
		if($fetchby != 'recent' && $fetchby != ''){
			if(!empty($value)){
				foreach($value as $v){
					$sql ="SELECT * FROM vv_pollls WHERE id = '".$v->logid."'";
					$query = $this->db->query($sql);
					$pollvalue = $query->result();	
					foreach($pollvalue as $pv){
						$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$pv->id;
						$pollquery = $this->db->query($sql);
						$polldata = $pollquery->result();
						$totalpoll = $polldata[0]->total;
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$pv->user_id;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						if(!empty($userdata)){
							$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$pv->id.'" class="questn poll-title">'.$pv->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$html .= '<span class="author-avatar">';
							
							
							if($userdata[0]->profile_pic_url != ''){
								$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
							}elseif($userdata[0]->picture_url != ''){
								$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
							}else{
								$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
							}
							
							$html .= '</span></a>';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$pv->id."' && log_title = 'poll_like'";
							$tquery = $this->db->query($sql);
							$totalLike = $tquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$pv->id."' && log_title = 'user_poll'";
							$cquery = $this->db->query($sql);
							$totalComnt = $cquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$pv->id."' && log_title = 'user_poll_shared'";
							$squery = $this->db->query($sql);
							$totalShare = $squery->result();
							
							$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($pv->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';	
						}
					}		
				}
				if($count > 0){	
					if(!empty($catid)){
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid="'.$catid.'"></li>';
					}else{
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid=""></li>';
					}
				}
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		return $html;
    }
	
	
	
	
	
	public function getPollListAfterLogin($fetchby,$catid){
		$html = '';
		$value = $polllValue = $shortId = $count = '';
		if($fetchby == 'vote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(first.id) as total FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollresult ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollresult ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}
				
			$count = $totalCount[0]->total-1;
			$shortId = 'pollresult';		
		}elseif($fetchby == 'upvote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(first.id) as total FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(id) as total FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();		
			}
				
			$count = $totalCount[0]->total-1;
			$shortId = 'likecounter';				
		}elseif($fetchby == 'comment'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
				
				$sql ="SELECT COUNT(first.id) as total FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}else{
				$sql ="SELECT * FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
			
				$sql ="SELECT COUNT(id) as total FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}
				
			$count = $totalCount[0]->total-1;
			$shortId = 'commentcounter';				
		}else{
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollls ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();	
				
				$sql ="SELECT COUNT(id) as total FROM vv_pollls ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}
			$count = $totalCount[0]->total-1;
			$shortId = 'pollsDesc';
			if(!empty($rvalue)){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
				if($count > 0){	
					if(!empty($catid)){
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid="'.$catid.'"></li>';
					}else{
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid=""></li>';
					}
				}
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		
		if($fetchby != 'recent' && $fetchby != ''){
			if(!empty($value)){
				foreach($value as $v){
					$sql ="SELECT * FROM vv_pollls WHERE id = '".$v->logid."'";
					$query = $this->db->query($sql);
					$pollvalue = $query->result();
					foreach($pollvalue as $pv){
						$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$pv->id;
						$pollquery = $this->db->query($sql);
						$polldata = $pollquery->result();
						$totalpoll = $polldata[0]->total;
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$pv->user_id;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						if(!empty($userdata)){
							$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$pv->id.'" class="questn poll-title">'.$pv->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$html .= '<span class="author-avatar">';
							
							
							if($userdata[0]->profile_pic_url != ''){
								$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
							}elseif($userdata[0]->picture_url != ''){
								$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
							}else{
								$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
							}
							
							$html .= '</span></a>';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$pv->id."' && log_title = 'poll_like'";
							$tquery = $this->db->query($sql);
							$totalLike = $tquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$pv->id."' && log_title = 'user_poll'";
							$cquery = $this->db->query($sql);
							$totalComnt = $cquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$pv->id."' && log_title = 'user_poll_shared'";
							$squery = $this->db->query($sql);
							$totalShare = $squery->result();
							
							$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($pv->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
							
						}
					}		
				}
				if($count > 0){	
					if(!empty($catid)){
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid="'.$catid.'"></li>';
					}else{
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid=""></li>';
					}
				}
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		return $html;
    }
	
	
	
	public function getPollListAfterLoginOne($fetchby,$userid,$catid){
		$html = '';
		$value = $polllValue = $shortId = $count = '';
		if($fetchby == 'vote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(first.id) as total FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollresult ORDER BY total_vote DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollresult ORDER BY total_vote DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}		
			$count = $totalCount[0]->total-1;
			$shortId = 'pollresult';		
		}elseif($fetchby == 'upvote'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' && first.totalLike > '0' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(first.id) as total FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' && first.totalLike > '0' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_likecounter WHERE title = 'poll_like' && totalLike > '0' ORDER BY totalLike DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();	
						
				$sql ="SELECT COUNT(id) as total FROM vv_likecounter WHERE title = 'poll_like' && totalLike > '0' ORDER BY totalLike DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();		
			}	
			$count = $totalCount[0]->total-1;
			$shortId = 'likecounter';				
		}elseif($fetchby == 'comment'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
				
				$sql ="SELECT COUNT(first.id) as total FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}else{
				$sql ="SELECT * FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC LIMIT 1";
				$query = $this->db->query($sql);
				$value = $query->result();
			
				$sql ="SELECT COUNT(id) as total FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();
			}	
			$count = $totalCount[0]->total-1;
			$shortId = 'commentcounter';				
		}else{
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE catid = '".$catid."' && user_id = '".$userid."' ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
					
				$sql ="SELECT COUNT(id) as total FROM vv_pollls WHERE catid = '".$catid."' && user_id = '".$userid."' ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}else{
				$sql ="SELECT * FROM vv_pollls WHERE user_id = '".$userid."' ORDER BY id DESC LIMIT 1";
				$query = $this->db->query($sql);
				$rvalue = $query->result();	
				
				$sql ="SELECT COUNT(id) as total FROM vv_pollls WHERE user_id = '".$userid."' ORDER BY id DESC";
				$query = $this->db->query($sql);
				$totalCount = $query->result();	
			}	
			$count = $totalCount[0]->total-1;
			$shortId = 'pollsDesc';
			if(!empty($rvalue)){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
				if($count > 0){	
					if(!empty($catid)){
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePollByUser" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid="'.$catid.'"></li>';
					}else{
						$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePoll" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'" catid=""></li>';
					}
				}	
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		
		if($fetchby != 'recent' && $fetchby != ''){
			if(!empty($value)){
				foreach($value as $v){
					$sql ="SELECT * FROM vv_pollls WHERE id = '".$v->logid."' && user_id = '".$userid."'";
					$query = $this->db->query($sql);
					$pollvalue = $query->result();	
					if(!empty($pollvalue)){
						foreach($pollvalue as $v){
							$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
							$pollquery = $this->db->query($sql);
							$polldata = $pollquery->result();
							$totalpoll = $polldata[0]->total;
							
							$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
							$userquery = $this->db->query($sql);
							$userdata = $userquery->result();
							if(!empty($userdata)){
								$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
								
								if($userdata[0]->username != ''){
									$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
								}
								
								$html .= '<span class="author-avatar">';
								
								
								if($userdata[0]->profile_pic_url != ''){
									$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
								}elseif($userdata[0]->picture_url != ''){
									$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
								}else{
									$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
								}
								
								$html .= '</span></a>';
								
								if($userdata[0]->username != ''){
									$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
								}else{
									$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
								}
								
								$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
								$tquery = $this->db->query($sql);
								$totalLike = $tquery->result();
								
								$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
								$cquery = $this->db->query($sql);
								$totalComnt = $cquery->result();
								
								$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
								$squery = $this->db->query($sql);
								$totalShare = $squery->result();
								
								$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
								
							}
						}
					}else{
						$html .= '<li class="poll-item">No Polls Found</li>';		
					}
				}
				if($count > 0){	
					$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePollByUser" style="margin:5px;">Load More</button><input type="hidden" class="" value="'.$count.'" id="shortId" tbl="'.$shortId.'"></li>';
				}
			}else{
				$html .= '<li class="poll-item">No Polls Found</li>';
			}
		}
		return $html;
    }
	
	
	
	public function userNameAvailabe($username){
		$sql ="SELECT username FROM vv_users WHERE username = '".$username."'";
		$userquery = $this->db->query($sql);
		$userdata = $userquery->result();
		if(!empty($userdata)){
			$html = 'No';	
		}else{
			$html = 'Yes';	
		}
		return $html;
	}
	
	
	public function userNameChange($username,$userid){
		$input = array('username' => $username);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
		return 'success';
	}
	
	public function userPrimaryNameChange($firstname,$middlename,$lastname,$userid){
		$input = array('firstname' => $firstname, 'middlename' => $middlename, 'lastname' => $lastname);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
		return 'success';
	}
	
	
	public function changeUserContactNum($contact,$onlyme,$userid){
		$input = array('contact' => $contact, 'c_visibility' => $onlyme);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
		return 'success';
	}
	
	
	public function deactivateUserAccount($userid){
		$input = array('status' => '99');
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
		return 'success';
	}
	
	public function userOtherInformation($overview,$city,$state,$country,$dob,$gender,$onlyme,$userid,$interest){
		$input = array('overview' => $overview, 'city' => $city, 'state' => $state, 'country' => $country, 'dob' => $dob, 'gender' => $gender, 'd_visibility' => $onlyme, 'interest' => $interest);
		$data['modified'] = date("Y-m-d H:i:s");
		$update = $this->db->update('vv_users',$input,array('id'=>$userid));
		return 'success';
	}
	
	
	
	public function loadMorePolls($tbl,$limitform,$catid){
		$html = '';
		$perpage = 1;
		$value = '';
		if($tbl == 'pollresult'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();
			}else{
				$sql ="SELECT * FROM vv_pollresult ORDER BY total_vote DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();	
			}
		}elseif($tbl == 'likecounter'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid ORDER BY totalLike DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();
			}else{
				$sql ="SELECT * FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();	
			}
		}elseif($tbl == 'commentcounter'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();
			}else{
				$sql ="SELECT * FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$value = $query->result();	
			}
		}elseif($tbl == 'pollAsc'){
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id ASC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
			}else{
				$sql ="SELECT * FROM vv_pollls ORDER BY id ASC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$rvalue = $query->result();	
			}
			if(!empty($rvalue)){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
			}else{
				$html .= 'Finish';	
			}
			$nextrow = $limitform + 1;
			return $html.',,'.$nextrow;
		}else{
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE catid = '".$catid."' ORDER BY id DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
			}else{
				$sql ="SELECT * FROM vv_pollls ORDER BY id DESC LIMIT ".$limitform.",".$perpage."";
				$query = $this->db->query($sql);
				$rvalue = $query->result();
			}
			
			
			if(!empty($rvalue) && $limitform > 0){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
			}else{
				$html .= 'Finish';
			}
			$nextrow = $limitform - 1;
			return $html.',,'.$nextrow;
		}
		
		if($tbl != 'pollAsc' || $tbl != 'pollDesc'){
			if(!empty($value) && $limitform > 0){
				foreach($value as $v){
					$sql ="SELECT * FROM vv_pollls WHERE id = '".$v->logid."'";
					$query = $this->db->query($sql);
					$pollvalue = $query->result();	
					foreach($pollvalue as $v){
						$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
						$pollquery = $this->db->query($sql);
						$polldata = $pollquery->result();
						$totalpoll = $polldata[0]->total;
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						if(!empty($userdata)){
							$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$html .= '<span class="author-avatar">';
							
							
							if($userdata[0]->profile_pic_url != ''){
								$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
							}elseif($userdata[0]->picture_url != ''){
								$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
							}else{
								$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
							}
							
							$html .= '</span></a>';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
							$tquery = $this->db->query($sql);
							$totalLike = $tquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
							$cquery = $this->db->query($sql);
							$totalComnt = $cquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
							$squery = $this->db->query($sql);
							$totalShare = $squery->result();
							
							$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
							
						}
					}		
				}
			}else{
				$html .= 'Finish';
			}
			$nextrow = $limitform - 1;
			return $html.',,'.$nextrow;
		}
    }
	
	
	
	public function loadMorePollsByUser($tbl,$limitform,$userid,$catid){
		$html = '';
		$perpage = 1;
		$value = '';
		if($tbl == 'pollresult'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_pollresult as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE sec.catid = '".$catid."' ORDER BY total_vote DESC LIMIT ".$limitform.",".$perpage."";
			}else{
				$sql ="SELECT * FROM vv_pollresult ORDER BY total_vote DESC LIMIT ".$limitform.",".$perpage."";
			}
			$query = $this->db->query($sql);
			$value = $query->result();
		}elseif($tbl == 'likecounter'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_likecounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'poll_like' && sec.catid = '".$catid."' ORDER BY totalLike DESC LIMIT ".$limitform.",".$perpage."";		
			}else{
				$sql ="SELECT * FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC LIMIT ".$limitform.",".$perpage."";
			}
			$query = $this->db->query($sql);
			$value = $query->result();	
		}elseif($tbl == 'commentcounter'){
			if(!empty($catid)){
				$sql ="SELECT first.* FROM vv_commentcounter as first LEFT JOIN vv_pollls as sec ON sec.id = first.logid WHERE first.title = 'user_poll' && sec.catid = '".$catid."' ORDER BY totalComment DESC LIMIT ".$limitform.",".$perpage."";
			}else{
				$sql ="SELECT * FROM vv_commentcounter WHERE title = 'user_poll' ORDER BY totalComment DESC LIMIT ".$limitform.",".$perpage."";
			}
			$query = $this->db->query($sql);
			$value = $query->result();
		}elseif($tbl == 'pollAsc'){
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE user_id = '".$userid."' && catid = '".$catid."' ORDER BY id ASC LIMIT ".$limitform.",".$perpage."";
			}else{
				$sql ="SELECT * FROM vv_pollls WHERE user_id = '".$userid."' ORDER BY id ASC LIMIT ".$limitform.",".$perpage."";
			}
			$query = $this->db->query($sql);
			$rvalue = $query->result();	
			if(!empty($rvalue)){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
			}else{
				$html .= 'Finish';	
			}
			$nextrow = $limitform + 1;
			return $html.',,'.$nextrow;
		}else{
			if(!empty($catid)){
				$sql ="SELECT * FROM vv_pollls WHERE user_id = '".$userid."' && catid = '".$catid."' ORDER BY id DESC LIMIT ".$limitform.",".$perpage."";
			}else{
				$sql ="SELECT * FROM vv_pollls WHERE user_id = '".$userid."' ORDER BY id DESC LIMIT ".$limitform.",".$perpage."";
			}
			$query = $this->db->query($sql);
			$rvalue = $query->result();
			if(!empty($rvalue) && $limitform > 0){
				foreach($rvalue as $v){
					$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
					$pollquery = $this->db->query($sql);
					$polldata = $pollquery->result();
					$totalpoll = $polldata[0]->total;
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
					$userquery = $this->db->query($sql);
					$userdata = $userquery->result();
					if(!empty($userdata)){
						$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$html .= '<span class="author-avatar">';
						
						
						if($userdata[0]->profile_pic_url != ''){
							$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
						}elseif($userdata[0]->picture_url != ''){
							$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
						}else{
							$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
						}
						
						$html .= '</span></a>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
						$cquery = $this->db->query($sql);
						$totalComnt = $cquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
					}		
				}
			}else{
				$html .= 'Finish';
			}
			$nextrow = $limitform - 1;
			return $html.',,'.$nextrow;
		}
		
		if(($tbl != 'pollAsc' || $tbl != 'pollDesc') && $tbl != ''){
			if(!empty($value) && $limitform > 0){
				foreach($value as $v){
					$sql ="SELECT * FROM vv_pollls WHERE id = '".$v->logid."' && WHERE user_id = '".$userid."'";
					$query = $this->db->query($sql);
					$pollvalue = $query->result();	
					foreach($pollvalue as $v){
						$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = ".$v->id;
						$pollquery = $this->db->query($sql);
						$polldata = $pollquery->result();
						$totalpoll = $polldata[0]->total;
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$v->user_id;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						if(!empty($userdata)){
							$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$html .= '<span class="author-avatar">';
							
							
							if($userdata[0]->profile_pic_url != ''){
								$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
							}elseif($userdata[0]->picture_url != ''){
								$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
							}else{
								$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
							}
							
							$html .= '</span></a>';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
							$tquery = $this->db->query($sql);
							$totalLike = $tquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
							$cquery = $this->db->query($sql);
							$totalComnt = $cquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
							$squery = $this->db->query($sql);
							$totalShare = $squery->result();
							
							$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
							
						}
					}		
				}
			}else{
				$html .= 'Finish';
			}
			$nextrow = $limitform - 1;
			return $html.',,'.$nextrow;
		}
    }
	
	
	
	
	
	
	public function addPollCommentOnComment($pid,$title,$userid,$content,$cmntId){
		$html = '';
		$input = array('userid' => $userid, 'content' => $content, 'cmntId' => $cmntId);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}

		
		$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$cmntId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		$cmntCount = count($cmntdata);
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span><span> '.$data->content.'</span> </div><p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$data->id.'">Delete</a></p></div><span class="editCommentChildData">';
					
				if($data->content != ''){
					$html .= '<div class="commentChildContent" style="display:none;">'.$data->content.'</div>';
				}else{
					$html .= '<div class="commentChildContent" style="display:none;"></div>';
				}
				$html .= '</span></div>';
			}
		}else{
			$html = '';	
		}
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt.',,'.$cmntCount;
    }
	
	
	public function addPollCommentOnCommentSingle($pid,$title,$userid,$content,$cmntId){
		$html = '';
		$input = array('userid' => $userid, 'content' => $content, 'cmntId' => $cmntId);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}

		
		$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$cmntId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		$cmntCount = count($cmntdata);
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span><span> '.$data->content.'</span> </div><p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUCSingle" id="'.$data->id.'">Delete</a></p></div></div>';
			}
		}else{
			$html = '';	
		}
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt.',,'.$cmntCount;
    }
	
	public function getOneComment($id){
		$sql ="SELECT * FROM vv_comment WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	
	public function removeComment($id,$title,$logid){
		$html = '';
		if(empty($_REQUEST['cmntImg']) && !empty($_REQUEST['imageOne'])){
			if(file_exists('./uploads/'.$_REQUEST['imageOne'])){
				unlink('./uploads/'.$_REQUEST['imageOne']);
			}
		}
		$sql ="SELECT count(id) as total FROM vv_comment WHERE id = '".$id."' OR cmntId = '".$id."'";
		$query = $this->db->query($sql);
		$totalComment = $query->result();
		
		$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$logid."' AND title = '".$title."'";
		$query = $this->db->query($sql);
		$getResult = $query->result();
		
		$total = $getResult[0]->total - $totalComment[0]->total;
		$data = array('totalComment' => $total);
		$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));

		$sql ="DELETE FROM vv_comment WHERE id = '".$id."' OR cmntId = '".$id."'";
		$query = $this->db->query($sql);
		
		
		$cmnt = '<i class="fa fa-comment"></i>'.$total;
		return $cmnt;
    }
	
	
	public function removeCommentOnComment($id,$title,$logid){
		$html = '';
		$sql ="SELECT count(id) as total,cmntId FROM vv_comment WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$totalComment = $query->result();
		
		$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$logid."' AND title = '".$title."'";
		$query = $this->db->query($sql);
		$getResult = $query->result();
		
		$total = $getResult[0]->total - $totalComment[0]->total;
		$data = array('totalComment' => $total);
		$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));

		$sql ="DELETE FROM vv_comment WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		
		$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$totalComment[0]->cmntId."'";
		$query = $this->db->query($sql);
		$totalCom = $query->result();
		
		$cmnt = '<i class="fa fa-comment"></i>'.$total.',,'.$totalCom[0]->total;
		return $cmnt;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function addCompComment($pollid,$userid,$content,$cmntImg,$caption,$cmntImgTwo,$captionTwo,$cmntImgThree,$captionThree,$title){
		$html = '';
		$input = array('logid' => $pollid, 'userid' => $userid, 'log_title' => $title, 'content' => $content, 'cmntImg' => $cmntImg, 'captionOne' => $caption, 'cmntImgTwo' => $cmntImgTwo, 'captionTwo' => $captionTwo, 'cmntImgThree' => $cmntImgThree, 'captionThree' => $captionThree);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pollid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pollid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}
		
	
		$sql ="SELECT * FROM vv_comment WHERE logid = '".$pollid."' && log_title = '".$title."' ORDER BY id DESC LIMIT 5";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix clearComp"><table style="width:100%"><tbody><tr><td style="width:10%;"><div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div></td><td style="width:30%;"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				
				if(!empty($data->content)){
					$html .= '<span> '.$data->content.'</span>';
				}
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntCountData = $query->result();
				
				$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$likeCountData = $query->result();
				
				$html .= '</div></td><td style="width:30%;"><p style="margin-bottom:5px;">';
				
				if($data->cmntImg != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionOne != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionOne.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgTwo != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionTwo != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionTwo.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgThree != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgThree.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionThree != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionThree.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				$html .= '</p></td style="width:30%;"><td><div class="poll-list"><ul class="poll-statistic" style="text-align: center;"><li><span class="question-views">'.$likeCountData[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$cmmntCountData[0]->total.' </span><i class="fa fa-comment"></i>Comment</li></ul></div></td></tr></tbody></table><span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContent" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContent" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
						
						if($data->captionOne != ''){
							$html .= '<input type="hidden" class="commentCaptionOne" value="'.$data->captionOne.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionOne" value="" />';	
						}
						
						if($data->cmntImgTwo != ''){
							$html .= '<input type="hidden" class="commentImageTwo" value="'.$data->cmntImgTwo.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageTwo" value="" />';	
						}
						
						if($data->captionTwo != ''){
							$html .= '<input type="hidden" class="commentCaptionTwo" value="'.$data->captionTwo.'"  />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionTwo" value="" />';	
						}
						
						if($data->cmntImgThree != ''){
							$html .= '<input type="hidden" class="commentImageThree" value="'.$data->cmntImgThree.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageThree" value="" />';	
						}
						
						if($data->captionThree != ''){
							$html .= '<input type="hidden" class="commentCaptionThree" value="'.$data->captionThree.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionThree" value="" />';	
						}
					
					$html .= '</span><div class="usr_content_block"><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
				if(!empty($likequery)){
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
				}else{
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
				}
				$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntComp" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
				
				$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntdata = $query->result();
				
				if(!empty($cmmntdata)){
					foreach($cmmntdata as $cdata){
						$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
						$userquery = $this->db->query($sql);
						$cmntuser = $userquery->result();
						
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
						}
						if(!empty($cmntuser[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($cmntuser[0]->picture_url)){
							$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div><p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p></div><span class="editCommentChildData">';
					
						if($cdata->content != ''){
							$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
						}else{
							$html .= '<div class="commentChildContent" style="display:none;"></div>';
						}
						$html .= '</span></div>';
					}
				}
				
				$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
				
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
				
				
				
				
				$html .= '</div></div>';
			}
		}else{
			$html = '';	
		}
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pollid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function addSingleCompComnt($pollid,$userid,$content,$cmntImg,$caption,$cmntImgTwo,$captionTwo,$cmntImgThree,$captionThree,$title){
		$html = '';
		$input = array('logid' => $pollid, 'userid' => $userid, 'log_title' => $title, 'content' => $content, 'cmntImg' => $cmntImg, 'captionOne' => $caption, 'cmntImgTwo' => $cmntImgTwo, 'captionTwo' => $captionTwo, 'cmntImgThree' => $cmntImgThree, 'captionThree' => $captionThree);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_comment', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			$sql ="SELECT totalComment as total,id FROM vv_commentcounter WHERE logid = '".$pollid."' AND title = '".$title."'";
			$query = $this->db->query($sql);
			$getResult = $query->result();
			if(!empty($getResult)){
				$total = $getResult[0]->total;
				$total = $total + 1;
				$data = array('totalComment' => $total);
				$update = $this->db->update('vv_commentcounter',$data,array('id' => $getResult[0]->id));
			}else{
				$input = array('logid' => $pollid, 'totalComment' => '1', 'title' => $title);
				$this->db->insert('vv_commentcounter', $input);
			}
		}
		
		
		$sql ="SELECT * FROM vv_comment WHERE id = '".$insertId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix clearComp"><table style="width:100%"><tbody><tr><td style="width:10%;"><div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div></td><td style="width:30%;"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				
				if(!empty($data->content)){
					$html .= '<span> '.$data->content.'</span>';
				}
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntCountData = $query->result();
				
				$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$likeCountData = $query->result();
				
				$html .= '</div></td><td style="width:30%;"><p style="margin-bottom:5px;">';
				
				if($data->cmntImg != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionOne != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionOne.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgTwo != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionTwo != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionTwo.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgThree != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgThree.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionThree != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionThree.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				$html .= '</p></td style="width:30%;"><td><div class="poll-list"><ul class="poll-statistic" style="text-align: center;"><li><span class="question-views">'.$likeCountData[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$cmmntCountData[0]->total.' </span><i class="fa fa-comment"></i>Comment</li></ul></div></td></tr></tbody></table><span class="editCommentData">';
				
				if(!empty($data->content)){
					$html .= '<div class="commentContent" style="display:none;">'.$data->content.'</div>';
				}else{
					$html .= '<div class="commentContent" style="display:none;"></div>';
				}
				
				if($data->cmntImg != ''){
					$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
				}else{
					$html .= '<input type="hidden" class="commentImageOne" value="" />';
				}
				
				if($data->captionOne != ''){
					$html .= '<input type="hidden" class="commentCaptionOne" value="'.$data->captionOne.'" />';	
				}else{
					$html .= '<input type="hidden" class="commentCaptionOne" value="" />';	
				}
				
				if($data->cmntImgTwo != ''){
					$html .= '<input type="hidden" class="commentImageTwo" value="'.$data->cmntImgTwo.'" />';
				}else{
					$html .= '<input type="hidden" class="commentImageTwo" value="" />';	
				}
				
				if($data->captionTwo != ''){
					$html .= '<input type="hidden" class="commentCaptionTwo" value="'.$data->captionTwo.'"  />';	
				}else{
					$html .= '<input type="hidden" class="commentCaptionTwo" value="" />';	
				}
				
				if($data->cmntImgThree != ''){
					$html .= '<input type="hidden" class="commentImageThree" value="'.$data->cmntImgThree.'" />';
				}else{
					$html .= '<input type="hidden" class="commentImageThree" value="" />';	
				}
				
				if($data->captionThree != ''){
					$html .= '<input type="hidden" class="commentCaptionThree" value="'.$data->captionThree.'" />';	
				}else{
					$html .= '<input type="hidden" class="commentCaptionThree" value="" />';	
				}
					
				$html .= '</span><div class="usr_content_block"><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
				if(!empty($likequery)){
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
				}else{
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
				}
				$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
				
				if($userid == $data->userid){
					$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntComp" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmntSingle" id="'.$data->id.'">Delete</a>';
				}
				
				$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
				
				$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntdata = $query->result();
				
				if(!empty($cmmntdata)){
					foreach($cmmntdata as $cdata){
						$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
						$userquery = $this->db->query($sql);
						$cmntuser = $userquery->result();
						
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
						}
						if(!empty($cmntuser[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($cmntuser[0]->picture_url)){
							$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
						}
						
						$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
						
						if($userid == $cdata->userid){
							$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUCSingle" id="'.$cdata->id.'">Delete</a></p>';
						}
						
						$html .= '</div></div>';
					}
				}
				
				$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
				
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table></div></div></div></div></div>';
				
				
				
				
				$html .= '</div></div>';
			}
		}else{
			$html = '';	
		}
		
		$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$pollid."' && title = '".$title."'";
		$cmntquery = $this->db->query($sql);
		$totalComnt = $cmntquery->result();
		$cmnt = '<i class="fa fa-comment"></i>'.$totalComnt[0]->totalComment;
		return $html.',,'.$cmnt;
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function editCompComment($content,$cmntImg,$caption,$cmntImgTwo,$captionTwo,$cmntImgThree,$captionThree,$editId){
		$html = '';
		$input = array('content' => $content, 'cmntImg' => $cmntImg, 'captionOne' => $caption, 'cmntImgTwo' => $cmntImgTwo, 'captionTwo' => $captionTwo, 'cmntImgThree' => $cmntImgThree, 'captionThree' => $captionThree);
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_comment',$input,array('id'=>$editId));
	
		$sql ="SELECT * FROM vv_comment WHERE id = '".$editId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<table style="width:100%"><tbody><tr><td style="width:10%;"><div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div></td><td style="width:30%;"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				
				if(!empty($data->content)){
					$html .= '<span> '.$data->content.'</span>';
				}
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntCountData = $query->result();
				
				$sql ="SELECT count(id) as total FROM vv_cmntLike WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$likeCountData = $query->result();
				
				$html .= '</div></td><td style="width:30%;"><p style="margin-bottom:5px;">';
				
				if($data->cmntImg != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionOne != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionOne.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgTwo != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionTwo != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionTwo.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				if($data->cmntImgThree != ''){
					$html .= '<figure class="cmtFig"><img src="'.base_url().'uploads/'.$data->cmntImgThree.'" style="border-radius: 15px; width:70px;" class="img-fluid">';
					if($data->captionThree != ''){
						$html .= '<figcaption style="width: 70px;font-size: 12px;word-wrap: break-word;">'.$data->captionThree.'</figcaption>';	
					}
					$html .= '</figure>';
				}
				
				$html .= '</p></td style="width:30%;"><td><div class="poll-list"><ul class="poll-statistic" style="text-align: center;"><li><span class="question-views">'.$likeCountData[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$cmmntCountData[0]->total.' </span><i class="fa fa-comment"></i>Comment</li></ul></div></td></tr></tbody></table><span class="editCommentData">';
						if(!empty($data->content)){
							$html .= '<div class="commentContent" style="display:none;">'.$data->content.'</div>';
						}else{
							$html .= '<div class="commentContent" style="display:none;"></div>';
						}
						
						if($data->cmntImg != ''){
							$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageOne" value="" />';
						}
						
						if($data->captionOne != ''){
							$html .= '<input type="hidden" class="commentCaptionOne" value="'.$data->captionOne.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionOne" value="" />';	
						}
						
						if($data->cmntImgTwo != ''){
							$html .= '<input type="hidden" class="commentImageTwo" value="'.$data->cmntImgTwo.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageTwo" value="" />';	
						}
						
						if($data->captionTwo != ''){
							$html .= '<input type="hidden" class="commentCaptionTwo" value="'.$data->captionTwo.'"  />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionTwo" value="" />';	
						}
						
						if($data->cmntImgThree != ''){
							$html .= '<input type="hidden" class="commentImageThree" value="'.$data->cmntImgThree.'" />';
						}else{
							$html .= '<input type="hidden" class="commentImageThree" value="" />';	
						}
						
						if($data->captionThree != ''){
							$html .= '<input type="hidden" class="commentCaptionThree" value="'.$data->captionThree.'" />';	
						}else{
							$html .= '<input type="hidden" class="commentCaptionThree" value="" />';	
						}
					
					$html .= '</span><div class="usr_content_block"><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
				if(!empty($likequery)){
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
				}else{
					$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
				}
				$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntComp" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
				
				$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
				$query = $this->db->query($sql);
				$cmmntdata = $query->result();
				
				if(!empty($cmmntdata)){
					foreach($cmmntdata as $cdata){
						$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
						$userquery = $this->db->query($sql);
						$cmntuser = $userquery->result();
						
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
						}
						if(!empty($cmntuser[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($cmntuser[0]->picture_url)){
							$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($cmntuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div><p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p></div><span class="editCommentChildData">';
					
						if($cdata->content != ''){
							$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
						}else{
							$html .= '<div class="commentChildContent" style="display:none;"></div>';
						}
						$html .= '</span></div>';
					}
				}
				
				$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
				
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div></div>';
			}
		}else{
			$html = '';	
		}
		return $html;
    }
	
	
	
	
	
	
	
	
	
	public function editPComment($content,$cmntImg,$editId){
		$html = '';
		$input = array('content' => $content, 'cmntImg' => $cmntImg);
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_comment',$input,array('id'=>$editId));
	
		$sql ="SELECT * FROM vv_comment WHERE id = '".$editId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="user"> <span class="sm-pic">';
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
				if(!empty($data->cmntImg) && !empty($data->content)){
					$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}	
					
				}elseif(!empty($data->cmntImg)){
					$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
					$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							
							$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
						
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
					
					
				}else{
					$html .= '<span> '.$data->content.'</span></div><span class="editCommentData">';
					if(!empty($data->content)){
						$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
					}else{
						$html .= '<div class="commentContentP" style="display:none;"></div>';
					}
					
					if($data->cmntImg != ''){
						$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
					}else{
						$html .= '<input type="hidden" class="commentImageOne" value="" />';
					}
					
					$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
  <div class="_khz _4sz1 _4rw5 _3wv2">';
					if(!empty($likequery)){
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
					}else{
						$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
					}
					$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
						
					$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmnt" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
					
					
					$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
					
					$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntdata = $query->result();
					
					if(!empty($cmmntdata)){
						foreach($cmmntdata as $cdata){
							$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
							$userquery = $this->db->query($sql);
							$cmntuser = $userquery->result();
							
							$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
							}
							if(!empty($cmntuser[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($cmntuser[0]->picture_url)){
								$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
							
							if($cmntuser[0]->username != ''){
								$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
							}
							
							$html .= $cmntuser[0]->firstname.' '.$cmntuser[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
							$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
							
							$html .= '</div><span class="editCommentChildData">';
					
							if($cdata->content != ''){
								$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
							}else{
								$html .= '<div class="commentChildContent" style="display:none;"></div>';
							}
							$html .= '</span></div>';
						}
					}
					
					$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($userdata[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($userdata[0]->picture_url)){
						$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmnt lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
					$query = $this->db->query($sql);
					$cmmntCountData = $query->result();
					if($cmmntCountData[0]->total > 0){
						$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
					}
				}
				$html .= '</div>';
			}
		}else{
			$html = '';	
		}
		return $html;
    }
	
	
	
	
	
	
	
	public function editPollCommentOnComment($content,$editId){
		$html = '';
		$input = array('content' => $content);
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_comment',$input,array('id'=>$editId));
		
		$sql ="SELECT * FROM vv_comment WHERE id = '".$editId."'";
		$cmntquery = $this->db->query($sql);
		$cmntdata = $cmntquery->result();
		$cmntCount = count($cmntdata);
		if(!empty($cmntdata)){
			foreach($cmntdata as $data){
				$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="user"> <span class="sm-pic">';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				if(!empty($userdata[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($userdata[0]->picture_url)){
					$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span><span> '.$data->content.'</span> </div><p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$data->id.'">Delete</a></p><span class="editCommentChildData">';
					
				if($data->content != ''){
					$html .= '<div class="commentChildContent" style="display:none;">'.$data->content.'</div>';
				}else{
					$html .= '<div class="commentChildContent" style="display:none;"></div>';
				}
				$html .= '</span></div>';
			}
		}else{
			$html = '';	
		}
		return $html;
    }
	
	
	
	
	
	public function deleteLogs($id,$title,$userid){
		$html = '';
		if($title == 'post'){
			$sql ="DELETE FROM vv_logs WHERE log_id = '".$id."' && log_title = 'user_post' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$getresult = $query->result();
			
			if(!empty($getresult[0]->postimg)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->postimg)){
					unlink('./uploads/'.$getresult[0]->postimg);
				}
			}
			
			$sql ="DELETE FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_like WHERE logid = '".$id."' && log_title = 'post_like' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_likecounter WHERE logid = '".$id."' && title = 'post_like'";
			$query = $this->db->query($sql);
			
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_post' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			$cmntResult = $query->result();
			
			
			if(!empty($cmntResult[0]->cmntImg)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImg)){
					unlink('./uploads/'.$getresult[0]->cmntImg);
				}
			}
			
			if(!empty($cmntResult[0]->cmntImgTwo)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgTwo)){
					unlink('./uploads/'.$getresult[0]->cmntImgTwo);
				}
			}
			
			if(!empty($cmntResult[0]->cmntImgThree)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgThree)){
					unlink('./uploads/'.$getresult[0]->cmntImgThree);
				}
			}
			
			$sql ="DELETE FROM vv_cmntLike WHERE cmntId = '".$cmntResult[0]->id."' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_commentcounter WHERE logid = '".$id."' && title = 'user_post'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM  vv_comment WHERE id = '".$cmntResult[0]->id."' && cmntId = '".$cmntResult[0]->id."'";
			$query = $this->db->query($sql);
			
		}
		
		
		
		
		if($title == 'poll'){
			$sql ="DELETE FROM vv_logs WHERE log_id = '".$id."' && log_title = 'user_poll' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$getresult = $query->result();
			
			if(!empty($getresult[0]->imag)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->imag)){
					unlink('./uploads/'.$getresult[0]->imag);
				}
			}
			
			$sql ="DELETE FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_like WHERE logid = '".$id."' && log_title = 'poll_like' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_likecounter WHERE logid = '".$id."' && title = 'poll_like'";
			$query = $this->db->query($sql);
			
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			$cmntResult = $query->result();
			
			
			if(!empty($cmntResult[0]->cmntImg)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImg)){
					unlink('./uploads/'.$getresult[0]->cmntImg);
				}
			}
			
			if(!empty($cmntResult[0]->cmntImgTwo)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgTwo)){
					unlink('./uploads/'.$getresult[0]->cmntImgTwo);
				}
			}
			
			if(!empty($cmntResult[0]->cmntImgThree)){
				if(file_exists(site_url().'uploads/'.$getresult[0]->cmntImgThree)){
					unlink('./uploads/'.$getresult[0]->cmntImgThree);
				}
			}
			
			$sql ="DELETE FROM vv_cmntLike WHERE cmntId = '".$cmntResult[0]->id."' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM vv_commentcounter WHERE logid = '".$id."' && title = 'user_poll'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM  vv_comment WHERE id = '".$cmntResult[0]->id."' && cmntId = '".$cmntResult[0]->id."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM  vv_pollvoting WHERE pollid = '".$id."'";
			$query = $this->db->query($sql);
			
			$sql ="DELETE FROM  vv_pollresult WHERE pollid = '".$id."' && title = 'poll'";
			$query = $this->db->query($sql);
			
		}
		return 'success';
    }
	
	
	
	
	public function deleteSharedLogs($id,$title,$userid){
		$html = '';
		if($title == 'post'){
			$sql ="DELETE FROM vv_logs WHERE log_id = '".$id."' && log_title = 'user_post_shared' && userid = '".$userid."'";
			$query = $this->db->query($sql);
		}
		
		if($title == 'poll'){
			$sql ="DELETE FROM vv_logs WHERE log_id = '".$id."' && log_title = 'user_poll_shared' && userid = '".$userid."'";
			$query = $this->db->query($sql);
		}
		return 'success';
    }
	
	
	public function addAnsSurvey($qidOne,$ansOne,$qidTwo,$ansTwo,$qidThree,$ansThree,$qidFour,$ansFour,$sid,$userid){
		if($ansOne != ''){
			$input = array('userid' => $userid, 'sid' => $sid, 'qid' => $qidOne, 'value' => $ansOne);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->insert('vv_surveyanswer', $input);
		}
		if($ansTwo != ''){
			$input = array('userid' => $userid, 'sid' => $sid, 'qid' => $qidTwo, 'value' => $ansTwo);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->insert('vv_surveyanswer', $input);
		}
		if($ansThree != ''){
			$input = array('userid' => $userid, 'sid' => $sid, 'qid' => $qidThree, 'value' => $ansThree);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->insert('vv_surveyanswer', $input);
		}
		if($ansFour != ''){
			$input = array('userid' => $userid, 'sid' => $sid, 'qid' => $qidFour, 'value' => $ansFour);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->insert('vv_surveyanswer', $input);
		}
		
		$input = array('userid' => $userid, 'sid' => $sid);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_surveyParti', $input);
		
		return 'success';
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function loadMoreSurveyParti($id,$limitform){
		$html = '';
		$perpage = 5;
		$value = '';
		$sql ="SELECT * FROM vv_surveyParti WHERE sid = '".$id."' ORDER BY id DESC LIMIT ".$limitform.",".$perpage."";
		$query = $this->db->query($sql);
		$surveyData = $query->result();		
		if(!empty($surveyData) && $limitform > 0){
			foreach($surveyData as $sdata){
				$sql ="SELECT * FROM vv_users WHERE id = ".$sdata->userid;
				$query = $this->db->query($sql);
				$userdata = $query->result();
				
				$html .= '<li class="poll-item surveyAnsSection"><div class="row"><div class="col-md-10"><div class="p-ltop-content"></div><div class="p-lbtm-content"><div class="poll-category">';
							
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				
				$html .= '<span style="margin-right: 8px;">';
				
				
				if($userdata[0]->profile_pic_url != ''){
					$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" style="width:60px;">';
				}elseif($userdata[0]->picture_url != ''){
					$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'" style="width:60px;">';
				}else{
					$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png" style="width:60px;">';
				}
				
				$html .= '</span></a>';
				
				if($userdata[0]->username != ''){
					$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
				}
				
				$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Paticipated on '.date('M d,Y',strtotime($sdata->created)).'</span></div></div></div><div class="col-md-2" style="text-align:center;"><a class="viewDetails trand-button">View Details</a></div>';
				
				$sql ="SELECT * FROM vv_surveyanswer WHERE userid = '".$sdata->userid."' && sid = '".$id."'";
				$query = $this->db->query($sql);
				$answers = $query->result();
				
				if(!empty($answers)){
					$html .= '<div class="col-md-12 surveyAnswers">';
					$i = 1;
					$count = count($answers);
					foreach($answers as $answer){
						$sql ="SELECT * FROM vv_surveyQustn WHERE id = '".$answer->qid."'";
						$query = $this->db->query($sql);
						$questions = $query->result();
						$html .= '<div style="font-size:20px;"><strong>Q. '.$questions[0]->question.'</strong></div>';
						$html .= '<div style="margin-top:10px;">Ans: '.$answer->value.'</div>';
						if($i != $count){
							$html .= '<br><br>';		
						}
						$i++;
					}
					$html .= '</div>';
				}
				
				$html .= '</div></li>';
			}
		}else{
			$html .= 'Finish';
		}
		$nextrow = $limitform - 5;
		return $html.',,'.$nextrow;
    }
	
	
	
}

