<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminSinglePoll_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getSinglePoll(){
		$idNum = $this->session->userdata('idNum');
		$val = 0;
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
				$curntquery = $this->db->query($sql);
				$curntusers = $curntquery->result();
				
				$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= ' </div><div class="media-body"><h6 class="media-heading">';
				
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class=""> Created a Poll</span></h6><p>'.$pDate.'</p></div>';
					
				if($loguser == $r->user_id){
					$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
				}
				
				$html .= '</div>
					
				<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
				  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
					<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
					  <div class="_vv54ng">
						<ul class="_vv54nf" role="menu">
						  <li class="_vv54ni __vvMenuItem deleteBlock" id="'.$r->id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
						</ul>
					  </div>
					</div>
				  </div>
				</div>
				
				<form method="post" enctype="multipart/form-data" action="'.base_url().'SinglePoll/pollVoting?pollid='.$r->id.'"><div class="row">';
				
				if($r->question != '' && $r->imag != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}elseif($r->question != '' && $r->videourl != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}elseif($r->question){
					$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
				}elseif($r->videourl){
					$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}else{
					$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}
				
				$html .= '<div class="trand row">';
				
				
				$sql ="SELECT vote FROM vv_pollvoting WHERE pollid = '".$r->id."' AND userid = '".$loguser."'";
				$query = $this->db->query($sql);
				$getvalue = $query->result();
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						if(empty($getvalue)){
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
						}
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
							$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
				}
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				$html .= '</div></div><div class="col-md-4 col-sm-4">';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
				
				if(!empty($getvalue)){
					$html .= '';
				}else{
					$html .= '<button type="button" class="pollVote trand-button" value="" name="pollVote" disabled="disabled">Submit Vote</button>';
				}
				
				$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'poll_like'";
				$query = $this->db->query($sql);
				$getLikeId = $query->result();
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
				$squery = $this->db->query($sql);
				$totalShare = $squery->result();
				
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;	
				}else{
					$comment = 0;
				}
				
				$html .= '</div><div class="button"><input type="hidden" value="'.$loguser.'" name="puserid" id="puserid"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div><div class="beforePostSubmit"><div class="beforePostSubmit-content"><div class="beforePostSubmit-header"><span class="beforePostSubmit-close">&times;</span><h2>Want to submit vote?</h2></div><div class="beforePostSubmit-body"><div class="col-md-12" style="text-align:center;"><input type="submit" class="trand-button" value="Yes" name="pollVote" style="width:100px;"/><button type="button" class="trand-button noSubmitVote" style="width:100px;">No</button></div></div></div></div></form>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
				
				
				if(!empty($getLikeId)){
					$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "poll_like" >';
				}else{
					$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "poll_like">';
				}
				
				$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,5";
				$cmntquery = $this->db->query($sql);
				$cmntdata = $cmntquery->result();
				
				$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_spoll"  id="'.$r->id.'" cmnTitle="user_poll"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_poll_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
				$html .= '<div class="col-md-12 vvuficontainer singlePoll"><div class="vvufilist"><div class="cmntData">';
				
				if(!empty($cmntdata)){
					foreach($cmntdata as $data){
						$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
						
						if(!empty($data->cmntImg) && !empty($data->content)){
							$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
							if(!empty($data->content)){
								$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
							}else{
								$html .= '<div class="commentContentP" style="display:none;"></div>';
							}
							
							if($data->cmntImg != ''){
								$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
							}else{
								$html .= '<input type="hidden" class="commentImageOne" value="" />';
							}
						
							$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
							if(!empty($likequery)){
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
							}else{
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
							}
							$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
							if($loguser == $data->userid){
								$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
							}
					
							$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
									if($loguser == $cdata->userid){
										$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
									}
									
									$html .= '</div><span class="editCommentChildData">';
					
									if($cdata->content != ''){
										$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
									}else{
										$html .= '<div class="commentChildContent" style="display:none;"></div>';
									}
									$html .= '</span></div>';
								}
							}
							
							$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
							
							if(!empty($userdata[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($userdata[0]->picture_url)){
								$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}	
							
						}elseif(!empty($data->cmntImg)){
							$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><span class="editCommentData">';
							if(!empty($data->content)){
								$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
							}else{
								$html .= '<div class="commentContentP" style="display:none;"></div>';
							}
							
							if($data->cmntImg != ''){
								$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
							}else{
								$html .= '<input type="hidden" class="commentImageOne" value="" />';
							}
						
							$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
							if(!empty($likequery)){
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
							}else{
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
							}
							$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
							if($loguser == $data->userid){
								$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
							}
					
							$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
									if($loguser == $cdata->userid){
										$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
									}
									
									$html .= '</div><span class="editCommentChildData">';
						
									if($cdata->content != ''){
										$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
									}else{
										$html .= '<div class="commentChildContent" style="display:none;"></div>';
									}
									$html .= '</span></div>';
								}
							}
							
							$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
							
							if(!empty($userdata[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($userdata[0]->picture_url)){
								$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
							
							
						}else{
							$html .= '<span> '.$data->content.'</span></div><span class="editCommentData">';
							if(!empty($data->content)){
								$html .= '<div class="commentContentP" style="display:none;">'.$data->content.'</div>';
							}else{
								$html .= '<div class="commentContentP" style="display:none;"></div>';
							}
							
							if($data->cmntImg != ''){
								$html .= '<input type="hidden" class="commentImageOne" value="'.$data->cmntImg.'" />';
							}else{
								$html .= '<input type="hidden" class="commentImageOne" value="" />';
							}
						
							$html .= '</span><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2">';
							if(!empty($likequery)){
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt vv_after_like" likeid="'.$data->id.'" like="'.$likequery[0]->id.'">Upvote</a>';
							}else{
								$html .= '<a class="UFILikeLink UFIReactionLink cmntUnderCmnt likeUnderCmnt" likeid="'.$data->id.'" like="">Upvote</a>';
							}
							$html .= '</div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt replyUnderCmnt">Comment</a>';
					
							if($loguser == $data->userid){
								$html .= '<span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt editCmntPSingle" id="'.$data->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCmnt" id="'.$data->id.'">Delete</a>';
							}
					
							$html .= '<div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div>';
							
									if($loguser == $cdata->userid){
										$html .= '<p><a class="UFIReplyLink _460i cmntUnderCmnt editCUC" id="'.$cdata->id.'">Edit</a><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt removeCUC" id="'.$cdata->id.'">Delete</a></p>';
									}
									
									$html .= '</div><span class="editCommentChildData">';
					
									if($cdata->content != ''){
										$html .= '<div class="commentChildContent" style="display:none;">'.$cdata->content.'</div>';
									}else{
										$html .= '<div class="commentChildContent" style="display:none;"></div>';
									}
									$html .= '</span></div>';
								}
							}
							
							$html .= '</div><div class="cmmnt-box" style="border:none;"><div class="cmmnt"><div class="cmntBlock poll_cmt_block"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
							
							if(!empty($userdata[0]->profile_pic_url)){
								$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
							}elseif(!empty($userdata[0]->picture_url)){
								$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
							}else{
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
							}
							
							$html .= '</span></td><td style="position:relative;width: 94%;"><input type="text" cmntId = "'.$data->id.'" placeholder="Write a Comment..." class="commentBoxUCmntSingle lead emoji-picker-container" name="comment" data-emojiable="true"></span></td></tr></tbody></table><span class="fcg fss UFICommentTip commentChildFalse usr_content_block"><a href="#" class=" cancelChildEdit">Cancel</a></span></div></div></div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
						}
						
						$html .= '</div></div>';
					}
				}else{
					$html .= '';	
				}
				$html .= '</div><div class="cmmnt-box"><div class="cmmnt"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="cmnt_single_poll lead emoji-picker-container" name="comment" data-emojiable="true"><div class="enter singlePollEnter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span><span class="span2" style="display:none;"><img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;"></span></div><div class="compShow" style="display:none;"></div></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
				<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
				
				<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
				
				</div></div><p class="saveComment"><button type="button" class="trand-button saveThePCommentSingle" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div></div></div>';
				
				$html .= '<div direction="right" class="clearfix"><div class="_ohf rfloat"></div>';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button" style="display:none;">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}else{
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}
				
				$html .= '</div></div></div><input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$totalComnt[0]->total.'">';
				
				$html .= '</div></article></div></div>';
				$idNum++;
				$this->session->set_userdata('idNum',$idNum);
			}
			return $html;
		}else{
			redirect('./dashboard');
		}
    }
	
	
	
	
	
	public function getSinglePollBeforeLogin($id){
		$val = 0;
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				
				$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= ' </div><div class="media-body"><h6 class="media-heading">';
				
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class=""> Created a Poll</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
				if($r->question != '' && $r->imag != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}elseif($r->question != '' && $r->videourl != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}elseif($r->question){
					$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
				}elseif($r->videourl){
					$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}else{
					$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}
				
				$html .= '<div class="trand row">';
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
				}
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				$html .= '</div></div><div class="col-md-4 col-sm-4">';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
				$squery = $this->db->query($sql);
				$totalShare = $squery->result();
					
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;	
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button"><a href="'.base_url().'Login"><button type="button" class="trand-button">Submit Vote</button></div><div class="button"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div></form>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div>  </div>';
				
				$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,5";
				$cmntquery = $this->db->query($sql);
				$cmntdata = $cmntquery->result();
				
				$html .= '<div class="col-md-12 vvuficontainer singlePoll"><div class="vvufilist"><div class="cmntData">';
				
				if(!empty($cmntdata)){
					foreach($cmntdata as $data){
						$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
						
						if(!empty($data->cmntImg) && !empty($data->content)){
							$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}	
							
						}elseif(!empty($data->cmntImg)){
							$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
							
							
						}else{
							$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
		  <div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
						}
						
						$html .= '</div></div>';
					}
				}else{
					$html .= '';	
				}
				$html .= '</div>';
				
				$html .= '<div direction="right" class="clearfix"><div class="_ohf rfloat"></div>';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button" style="display:none;">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}else{
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}
				
				$html .= '</div></div></div><input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$totalComnt[0]->total.'">';
				
				$html .= '</div></article></div></div>';
				
			}
			return $html;
		}else{
			redirect('./Home');
		}
    }
	
	
	public function getSinglePollForAdmin($id){
		$val = 0;
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				
				$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= ' </div><div class="media-body"><h6 class="media-heading">';
				
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class=""> Created a Poll</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
				if($r->question != '' && $r->imag != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}elseif($r->question != '' && $r->videourl != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}elseif($r->question){
					$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
				}elseif($r->videourl){
					$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}else{
					$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}
				
				$html .= '<div class="trand row">';
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
				}
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				$html .= '</div></div><div class="col-md-4 col-sm-4">';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = unserialize($pollopt->title);
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
				$squery = $this->db->query($sql);
				$totalShare = $squery->result();
					
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;	
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div></form>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class="v_like"><i class="fa fa-share"></i>Share</span></div>  </div>';
				
				$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,5";
				$cmntquery = $this->db->query($sql);
				$cmntdata = $cmntquery->result();
				
				$html .= '<div class="col-md-12 vvuficontainer singlePoll"><div class="vvufilist"><div class="cmntData">';
				
				if(!empty($cmntdata)){
					foreach($cmntdata as $data){
						$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
						
						if(!empty($data->cmntImg) && !empty($data->content)){
							$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}	
							
						}elseif(!empty($data->cmntImg)){
							$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
							
							
						}else{
							$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
		  <div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
						}
						
						$html .= '</div></div>';
					}
				}else{
					$html .= '';	
				}
				$html .= '</div>';
				
				$html .= '<div direction="right" class="clearfix"><div class="_ohf rfloat"></div>';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button" style="display:none;">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}else{
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}
				
				$html .= '</div></div></div><input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$totalComnt[0]->total.'">';
				
				$html .= '</div></article></div></div>';
				
			}
			return $html;
		}else{
			redirect('./Home');
		}
    }
	
	
}