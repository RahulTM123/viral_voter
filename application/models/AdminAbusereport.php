<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminAbusereport extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getList($start, $limit) {
		$result = array();
		
		$sql ="select vv_report.*, vv_users.firstname, vv_users.lastname, vv_users.username from vv_report inner join vv_users on vv_report.reportby = vv_users.id order by id desc, status asc";
		$query = $this->db->query($sql);
		$values = $query->result_array();
		foreach($values as $value) {
			switch($value['reftype']) {
				case 'post':
				$sql ="select vv_post.id, user_id, postdata, vid, firstname, lastname, username, cat_name from vv_post inner join vv_users on vv_post.user_id = vv_users.id inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$value['refid'];
				$query = $this->db->query($sql);
				if($query->num_rows())
				$result[] = array('data' => $query->row_array(), 'report' => $value);
				break;
				
				case 'polls':
				$sql ="select vv_pollls.id, user_id, question, firstname, lastname, username, cat_name from vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$value['refid'];
				$query = $this->db->query($sql);
				if($query->num_rows())
				$result[] = array('data' => $query->row_array(), 'report' => $value);
				break;
				
				case 'comment':
				$sql ="SELECT vv_commentdata.content as postdata, '' as cat_name, userid, firstname, lastname, username FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_comment.id = ".$value['refid'];
				$query = $this->db->query($sql);
				if($query->num_rows())
				$result[] = array('data' => $query->row_array(), 'report' => $value);
				break;
				
				case 'postdata':
				break;
				
				case 'profile':
				$sql ="SELECT firstname, lastname, '' as postdata, '' as cat_name, username, id as userid FROM `vv_users` where vv_users.id = ".$value['refid'];
				$query = $this->db->query($sql);
				if($query->num_rows())
				$result[] = array('data' => $query->row_array(), 'report' => $value);
				break;
			}
		}
		
		$sql ="select count(id) as total from vv_report";		
		$query = $this->db->query($sql);
		//$result['total'] = $query->row_array();
		$result['result'] = $result;
		return $result;
    }
	
	
	public function getListByFilter($limit, $country_id, $state_id, $city_id, $gender, $type, $keyword, $dfrom, $dto, $start='', $interest='', $causes){
		$result = array();
		
		$sql ="select vv_report.*, vv_users.firstname, vv_users.lastname, vv_users.username from vv_report inner join vv_users on vv_report.reportby = vv_users.id where 1 ";
		if($dfrom!='')
			$sql .=" and vv_report.created >= '".$dfrom."'";
		if($dto!='')
			$sql .=" and vv_report.created <= '".$dto."'";
		if($type!='')
			$sql .=" and vv_report.reftype = '".$type."'";
		if($causes!='')
			$sql .=" and vv_report.report = '".$causes."'";
		
		$sql .=" order by id desc";
		
		$query = $this->db->query($sql);
		$values = $query->result_array();
		foreach($values as $value) {
			switch($value['reftype']) {
				case 'post':
				$sql ="select vv_post.id, user_id, postdata, vid, firstname, lastname, username, cat_name from vv_post inner join vv_users on vv_post.user_id = vv_users.id inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$value['refid'];
		
				if($country_id!='')
					$sql .=" and vv_users.country = '".$country_id."'";
				if($state_id!='')
					$sql .=" and vv_users.state = '".$state_id."'";
				if($city_id!='')
					$sql .=" and vv_users.city = '".$city_id."'";
				if($gender!='')
					$sql .=" and vv_users.gender = '".$gender."'";
				
				if($interest!='') {
					$sql .=" and catid = '".$interest."'";
					$query = $this->db->query($sql);
					
					if($query->num_rows())
						$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				else {
					$query = $this->db->query($sql);
					if($query->num_rows())
					$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				break;
				
				case 'polls':
				$sql ="select vv_pollls.id, user_id, question, firstname, lastname, username, cat_name from vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$value['refid'];
		
				if($country_id!='')
					$sql .=" and vv_users.country = '".$country_id."'";
				if($state_id!='')
					$sql .=" and vv_users.state = '".$state_id."'";
				if($city_id!='')
					$sql .=" and vv_users.city = '".$city_id."'";
				if($gender!='')
					$sql .=" and vv_users.gender = '".$gender."'";
				
				if($interest!='') {
					$sql .=" and catid = '".$interest."'";
					$query = $this->db->query($sql);
					
					if($query->num_rows())
						$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				else {
					$query = $this->db->query($sql);
					if($query->num_rows())
					$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				break;
				
				case 'comment':
				$sql ="SELECT vv_commentdata.content as postdata, '' as cat_name, userid, firstname, lastname, username, pid, ptype FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_comment.id = ".$value['refid'];
		
				if($country_id!='')
					$sql .=" and vv_users.country = '".$country_id."'";
				if($state_id!='')
					$sql .=" and vv_users.state = '".$state_id."'";
				if($city_id!='')
					$sql .=" and vv_users.city = '".$city_id."'";
				if($gender!='')
					$sql .=" and vv_users.gender = '".$gender."'";
				
				$query = $this->db->query($sql);
				$comment = $query->row_array();
				if($interest!='') {
					switch($comment['ptype']) {
						case 'polls':		
							$sql = 'SELECT * FROM `vv_pollls` where vv_pollls.id = '.$comment['pid'].' and vv_pollls.catid = "'.$interest.'"';
						break;
						
						case 'post':
							$sql = 'SELECT * FROM `vv_post` where vv_post.id = '.$comment['id'].' and vv_post.catid = "'.$interest.'"';
						break;
					}
					$query = $this->db->query($sql);
					
					if($query->num_rows())
						$result[] = array('data' => $comment, 'report' => $value);
				
				}
				else {
					if($query->num_rows())
					$result[] = array('data' => $comment, 'report' => $value);
				}
				break;
				
				case 'postdata':
				break;
				
				case 'profile':
				$sql ="SELECT firstname, lastname, '' as postdata, '' as cat_name, username, id as userid FROM `vv_users` where vv_users.id = ".$value['refid'];
		
				if($country_id!='')
					$sql .=" and vv_users.country = '".$country_id."'";
				if($state_id!='')
					$sql .=" and vv_users.state = '".$state_id."'";
				if($city_id!='')
					$sql .=" and vv_users.city = '".$city_id."'";
				if($gender!='')
					$sql .=" and vv_users.gender = '".$gender."'";
				
				if($interest!='') {
					$sql .=" and  find_in_set('".$interest."', interest) <> 0";
					$query = $this->db->query($sql);
					
					if($query->num_rows())
						$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				else {
					$query = $this->db->query($sql);
					if($query->num_rows())
					$result[] = array('data' => $query->row_array(), 'report' => $value);
				}
				break;
			}
		}
		
		$sql ="select count(id) as total from vv_report";		
		$query = $this->db->query($sql);
		//$result['total'] = $query->row_array();
		$result['result'] = $result;
		return $result;
    }
	
	
	public function getDetail($reftype, $refid, $id) {
		$sql ="select vv_report.*, vv_users.firstname, vv_users.lastname, vv_users.username from vv_report inner join vv_users on vv_report.reportby = vv_users.id where 1 and vv_report.id =".$id;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		$result = array();
		switch($reftype) {
			case 'post':
			$sql ="select vv_post.id, user_id, postdata, vid, firstname, lastname, username, cat_name from vv_post inner join vv_users on vv_post.user_id = vv_users.id inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = ".$refid;
			$query = $this->db->query($sql);
			$result[] = array('data' => $query->row_array(), 'report' => $value);
			break;
			
			case 'polls':
			$sql ="select vv_pollls.id, user_id, question, firstname, lastname, username, cat_name from vv_pollls inner join vv_users on vv_pollls.user_id = vv_users.id inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$refid;
			$query = $this->db->query($sql);
			$result[] = array('data' => $query->row_array(), 'report' => $value);
			break;
			
			case 'comment':
			$sql ="SELECT vv_commentdata.content, vv_comment.*, firstname, lastname, username FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where vv_comment.id = ".$refid;
			$query = $this->db->query($sql);
			$result[] = array('data' => $query->row_array(), 'report' => $value);
			break;
			
			case 'postdata':
			break;
			
			case 'profile':
			$sql ="SELECT id, firstname, lastname, username FROM `vv_users` where vv_users.id = ".$refid;
			$query = $this->db->query($sql);
			$result[] = array('data' => $query->row_array(), 'report' => $value);
			break;
		}
		
		$result['result'] = $result;
		return $result;
    }
	
	
	public function getRefRecord($refid, $reftype) {
		switch($reftype) {
			case 'post':
			break;
			
			case 'polls':
			break;
			
			case 'comment':
			break;
			
			case 'postdata':
			break;
		}
	}
	
	
	public function reportProfile($message, $blocktimeframe, $id, $delete) {
		$sql = "SELECT * from vv_report where id = ".$id;
		$query = $this->db->query($sql);
		$values = $query->row_array();
		$bt = ($blocktimeframe)?$blocktimeframe:100; // profile blocked
		if($delete!='') {
			$status = 6;
			$bt = 200; //profile deleted
		}
		else {
			$status = 5;
		}
		
		$sql = "update vv_users set status = ".$bt.", blocked_date = now() where id = ".$values['refid'];
		$query = $this->db->query($sql);
		
		$sql = "update vv_report set status = ".$status." where id = ".$id;
		$query = $this->db->query($sql);
	}
	
	
	public function reportAll($message, $ref, $profile, $blocktimeframe, $id, $delete) {
		$sql ="SELECT * from vv_report where id = ".$id;
		$query = $this->db->query($sql);
		$values = $query->row_array();
		
		
		if($profile==1) {
			$bt = ($blocktimeframe)?$blocktimeframe:100; // profile blocked
			if($delete!='') {
				$status = 3;
				$bt = 200; //profile deleted
			}
			else {
				$status = 4;
			}
			$sql = "update vv_report set status = ".$status." where id = ".$id;
			$query = $this->db->query($sql);
			
			switch($values['reftype']) {
				case 'post':
					$sql = "select user_id from vv_post where id = ".$values['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
					
					$sql = "update vv_post set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
					
					$sql = "update vv_timeline set status = 200 where reference = 'post' and refid = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'polls':
					$sql = "select user_id from vv_pollls where id = ".$values['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
					
					$sql = "update vv_pollls set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
					
					$sql = "update vv_timeline set status = 200 where reference = 'polls' and refid = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'comment':
					$sql = "select userid as user_id from vv_comment where id = ".$values['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
					
					$sql = "update vv_comment set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'postdata':
				break;
			}
			
			$sql = "update vv_users set status = ".$bt.", blocked_date = now() where id = ".$users['user_id'];
			$query = $this->db->query($sql);
		}
		else {
			$sql = "update vv_report set status = 2 where id = ".$id;
			$query = $this->db->query($sql);
			
			switch($values['reftype']) {
				case 'post':
					$sql = "update vv_post set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
					
					$sql = "update vv_timeline set status = 200 where reference = 'post' and refid = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'polls':
					$sql = "update vv_pollls set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
					
					$sql = "update vv_timeline set status = 200 where reference = 'polls' and refid = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'comment':
					$sql = "update vv_comment set status = 200 where id = ".$values['refid'];
					$query = $this->db->query($sql);
				break;
				
				case 'postdata':
				break;
			}
		}
		
	}
	
	public function getUserbyref($id){
		$sql ="SELECT * FROM vv_report where id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		
		switch($value['reftype']) {
				case 'post':
					$sql = "select user_id from vv_post where id = ".$value['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
				break;
				
				case 'polls':
					$sql = "select user_id from vv_pollls where id = ".$value['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
				break;
				
				case 'comment':
					$sql = "select userid as user_id from vv_comment where id = ".$value['refid'];
					$query = $this->db->query($sql);
					$users = $query->row_array();
				break;
				
				case 'profile':
					$users['user_id'] = $id;
				break;
			}
		
		$sql ="SELECT * FROM vv_users where id = ".$users['user_id'];
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
}