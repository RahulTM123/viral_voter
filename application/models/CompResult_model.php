<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class CompResult_model extends CI_Model {

    public function __construct()

    {
        $this->load->database();
    }
    
	public function getCompResults($id){
		$html = '';
		
		$sql ="SELECT question FROM vv_contest WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$compdata = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px; font-size:30px;">Q. '.$compdata[0]->question.'</div><div class="col-md-12" style="padding-top:40px;">';
		
		$sql = 'SELECT vv_comment.*,vv_users.firstname,vv_users.middlename,vv_users.lastname,vv_users.username,vv_users.profile_pic_url,vv_users.picture_url,count(vv_cmntLike.cmntId) as total FROM `vv_comment` LEFT JOIN `vv_cmntLike` ON vv_comment.id = vv_cmntLike.cmntId LEFT JOIN vv_users ON vv_users.id = vv_comment.userid WHERE logid = "'.$id.'" AND log_title = "compitition" OR vv_comment.id = vv_cmntLike.cmntId GROUP BY vv_cmntLike.cmntId ORDER BY total DESC';
		$query = $this->db->query($sql);
		$cmntData = $query->result();
		
		if(!empty($cmntData)){
			foreach($cmntData as $data){
				$html .= '<div class="col-md-3"><div class="card"><a href="'.base_url().'admin/contest_result_detail/'.$data->id.'" >';
				
				if($data->cmntImg != ''){
					$html .= '<img src="'.base_url().'uploads/'.$data->cmntImg.'" alt="" style="width:100%; height:200px;">';	
				}elseif($data->cmntImgTwo != ''){
					$html .= '<img src="'.base_url().'uploads/'.$data->cmntImgTwo.'" alt="" style="width:100%; height:200px;">';
				}else{
					$html .= '<img src="'.base_url().'uploads/'.$data->cmntImgThree.'" alt="" style="width:100%; height:200px;">';
				}
				
				$html .= '</a><h1 class="uploadBy">Uploded By:</h1><div class="media userBoxSec"><div class="media-left">';
				
				if($data->username != ''){
					$html .= '<a href="'.base_url().'user/'.$data->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$data->id.'" class="usr">';	
				}
				
				if(!empty($data->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$data->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($data->picture_url)){
					$html .= '<img src="'.$data->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= '</a></div><div class="media-body"><h6 class="media-heading userNameHeading">';
				
				if($data->username != ''){
					$html .= '<a href="'.base_url().'user/'.$data->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$data->id.'" class="usr">';	
				}
				
				$sql = 'SELECT COUNT(id) as total FROM vv_comment WHERE cmntId = "'.$data->id.'"';
				$query = $this->db->query($sql);
				$cmntUData = $query->result();
				
				$html .= $data->firstname.' '.$data->lastname.'</a></h6></div></div><div style="margin: 10px 0;"></div><p class="bottomSide"><span>'.$data->total.' Upvotes</span>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;<span>'.$cmntUData[0]->total.' Comments</span></p></div></div>';
			}
		}else{
			$html .= '<div style="text-align:center;">No Result Found!</div>';	
		}
		
		$html .= '</div>';
		
		return $html;
	}
	
	
	
	
	
	
	public function getCompResultsDetail($id,$page){
		$html = '';
		
		
		$perpage = 10;
		$calc = $perpage * $page;
		$start = $calc - $perpage;
		
		
		$sql ="SELECT * FROM vv_comment WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$cmntData = $query->result();
		
		$sql ="SELECT * FROM vv_contest WHERE id = '".$cmntData[0]->logid."'";
		$query = $this->db->query($sql);
		$compdata = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px; font-size:30px;"><div class="col-md-10">Q. '.$compdata[0]->question.'</div><div class="col-md-2" style="text-align:right;"><a href="'.base_url().'admin/contest_result/'.$cmntData[0]->logid.'"><button type="button" class="trand-button">&lt;&lt; Back</button></a></div></div><div class="col-md-12" style="padding-top:20px;padding-bottom:20px;">';
		
		if($cmntData[0]->cmntImg != ''){
			$html .= '<div class="col-md-1"><img src="'.base_url().'uploads/'.$cmntData[0]->cmntImg.'" alt="" style="width:60px; height:60px;"></div>';	
		}
		
		if($cmntData[0]->cmntImgTwo != ''){
			$html .= '<div class="col-md-1"><img src="'.base_url().'uploads/'.$cmntData[0]->cmntImgTwo.'" alt="" style="width:60px; height:60px;"></div>';
		}
		
		if($cmntData[0]->cmntImgThree != ''){
			$html .= '<div class="col-md-1"><img src="'.base_url().'uploads/'.$cmntData[0]->cmntImgThree.'" alt="" style="width:60px; height:60px;"></div>';
		}
		
		$html .= '</div>';
		
		$sql ="SELECT * FROM vv_cmntLike WHERE cmntId = '".$id."' LIMIT ".$start.", ".$perpage;
		$query = $this->db->query($sql);
		$getUpvote = $query->result();
		
		if(!empty($getUpvote)){
			$sql ="SELECT COUNT(id) as total FROM vv_cmntLike WHERE cmntId = '".$id."'";
			$query = $this->db->query($sql);
			$totalUpvote = $query->result();
			
			$html .= '<p style="text-align:right;color: #03b4da;">'.$totalUpvote[0]->total.' Upvotes</p>';
			
			//table part start here
			
			$html .= '<table class="table table-striped detailTable" width="100" cellpadding="1" cellspacing="1" border="0"><tbody><tr><th>Name</th><th>Action</th><th>Date</th></tr>';
			
			foreach($getUpvote as $upvote){
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$upvote->userid;
				$curntquery = $this->db->query($sql);
				$curntusers = $curntquery->result();
				
				$html .= '<tr><td><div class="media userBoxSec"><div class="media-left">';
				
				if($curntusers[0]->username != ''){
					$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
				}
				
				if(!empty($curntusers[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($curntusers[0]->picture_url)){
					$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= '</a></div><div class="media-body" style="width:70%;"><h6 class="media-heading userNameHeading">';
				
				if($curntusers[0]->username != ''){
					$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
				}
				
				$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a></h6></div></div></td><td>Upvote</td><td>'.date('d-m-Y H:i:s',strtotime($upvote->created)).'</td></tr>';	
				
				
			}
			
			$html .= '</tbody></table>';
			
			if($totalUpvote[0]->total > 0){
				$total = $totalUpvote[0]->total;
			}	
			
			$totalPages = ceil($total / $perpage);
			
			if($page <=1 ){
				$html .= "<span id='page_links' style='font-weight: bold;'>Prev</span>";
			}else{			
				$j = $page - 1;			
				$html .= "<span><a id='page_a_link' href='".base_url()."admin/contest_result_detail/".$id."/?page=$j'>< Prev</a></span>";			
			}
			
			for($i=1; $i <= $totalPages; $i++){			
				if($i<>$page){			
					$html .= "<span><a id='page_a_link' href='".base_url()."admin/contest_result_detail/".$id."/?page=$i'>$i</a></span>";			
				}else{			
					$html .= "<span id='page_links' style='font-weight: bold;'>$i</span>";		
				}		
			}
			
			if($page == $totalPages ){
				$html .= "<span id='page_links' style='font-weight: bold;'>Next ></span>";
			}else{
				$j = $page + 1;
				$html .= "<span><a id='page_a_link' href='".base_url()."admin/contest_result_detail/".$id."/?page=$j'>Next</a></span>";
			}

		}else{
			$html .= '<table class="table table-striped detailTable" width="100" cellpadding="1" cellspacing="1" border="0" style="border:1px solid #ddd;"><tbody><tr><th>Name</th><th>Action</th><th>Date</th><tr><td colspan="3">No Record Found!</td></tr></tbody></table>';
		}	
		
		return $html;
	}

}