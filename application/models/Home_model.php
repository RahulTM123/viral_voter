<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Home_model extends CI_Model {

    public function __construct()

    {
        $this->load->database();
    }
    
	public function topSixPollByVoting(){
		$sql ="SELECT logid FROM vv_pollresult ORDER BY total_vote DESC LIMIT 6";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			$pollids = array();
			foreach($value as $v){
				array_push($pollids,$v->logid);	
			}
			return $pollids;
		}else{
			return '';	
		}
    }
	
	public function topSixPollByLike(){
		$sql ="SELECT logid FROM vv_likecounter WHERE title = 'poll_like' ORDER BY totalLike DESC LIMIT 6";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			$pollids = array();
			foreach($value as $v){
				array_push($pollids,$v->logid);	
			}
			return $pollids;
		}else{
			return '';	
		}
    }
	
	
	
	
	public function getVideos(){
		return '';
		$html = '';
		$sql ="SELECT * FROM vv_post WHERE vid != '' ORDER BY id DESC LIMIT 6 ";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			$html .= '<div class="ui-block"><div class="ui-block-title"><h6 class="title">Popular Videos</h6></div> <article class="hentry post"><div  class="viralnav-row-padding"><div class="container"><div class="row">';
			
			foreach($value as $r){
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				$videoImg = str_replace("https://www.youtube.com/embed/","",$r->vid);
				$html .= '<div class="col-md-4"><a href="'.base_url().'post/'.$r->id.'" class="questn"><img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" width="200" height="200" class="img-fluid"/><p>'.$r->postdata.'</p></a><p>';
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a></p></div>';
			}
			
			$html .= '</div></div></div></article></div>';
		}
		return $html;
    }
	
	public function getUserName($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getTrendingPolls($page) {
		$sqluserp ="select vv_post.*, vv_categories.cat_name, (vv_post.upvote) as trending from vv_post left join vv_categories on vv_post.catid = vv_categories.id where vv_post.status = 1 order by trending desc, vv_post.created, id desc limit ".$page.",4";
		$queryp = $this->db->query($sqluserp);
		$trendingup = $queryp->result_array();
		
		//$sqluser ="select vv_pollls.*, vv_categories.cat_name, (vv_pollls.upvote+vv_pollls.participation+vv_pollls.sharing) as trending from vv_pollls left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.status = 1 and vv_pollls.end_date >= now() and vv_pollls.start_date > DATE_SUB(NOW(), INTERVAL 15 DAY) order by trending desc, id desc limit ".$page.",4";
		$sqluser ="select vv_pollls.*, vv_categories.cat_name, (vv_pollls.upvote) as trending from vv_pollls left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.status = 1 order by trending desc, vv_pollls.created, id desc limit ".$page.",4";
		$query = $this->db->query($sqluser);
		$trendingu = $query->result_array();
		
		$datarows = array();
		$results = array();
		$count = 0;
		foreach($trendingup as $tr) {
			if($tr['trending'] > 0) {
				$tr['reference'] = 'post';
				$datarows[$tr['trending']][] = $tr;
				$count++;
			}
		}
		foreach($trendingu as $tr) {
			if($tr['trending'] > 0) {
				$tr['reference'] = 'polls';
				$userid = $this->db->query('SELECT group_concat(userid) as userid FROM `vv_pollvoting` where pollid = '.$tr['id'].' group by pollid')->row_array();
				$tr['paruser'] = $userid['userid'];
				
				$datarows[$tr['trending']][] = $tr;
				$count++;
			}
		}
		krsort($datarows);
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		
		foreach($datarows as $dt) {
			foreach($dt as $rs) {
				switch($rs['reference']) {
					case 'post':					
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'post', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$post = $rs;
						
						if(!empty($post['img'])) {
							$postimg = $this->Dashboard_model->getPostImage($post['img']);
							$post['postimg'] = $postimg;
						}
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'post'), 'data' => $rs);
					break;
					
					case 'polls':
						
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'polls', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$polls = $rs;
						$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs['id']);
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'polls'), 'data' => $rs);
						
					break;
				}
			}
		}
		
		return $results;
	}
	
	public function getTrendingList($page) {

		/* Old Query 
		$sqluserp ="select vv_post.*, vv_categories.cat_name, (vv_post.upvote) as trending from vv_post left join vv_categories on vv_post.catid = vv_categories.id where vv_post.status = 1 order by trending desc, id desc limit ".$page.",10";
		*/

		$user_id=$this->session->userdata('userData')['userId'];
				$get_id=$this->db->select('*')
		                 ->where('id',$user_id)
		                 ->get('vv_users')->row();

		$sqluserp ="select vv_post.*, vv_categories.cat_name, (vv_post.upvote) as trending from vv_post
					 left join vv_categories on vv_post.catid = vv_categories.id
					 left join vv_users on vv_post.user_id = vv_users.id
					  where vv_post.status = 1 and vv_users.city = $get_id->city order by trending desc, id desc limit ".$page.",10";
		$queryp = $this->db->query($sqluserp);
		$trendingup = $queryp->result_array();
		
		$sqluser ="select vv_pollls.*, vv_categories.cat_name, (vv_pollls.upvote) as trending from vv_pollls left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.status = 1 order by trending desc, id desc limit ".$page.",10";
		$query = $this->db->query($sqluser);
		$trendingu = $query->result_array();
		
		$datarows = array();
		$results = array();
		$count = 0;
		foreach($trendingup as $tr) {
			if($tr['trending'] > 0) {
				$tr['reference'] = 'post';
				$datarows[$tr['trending']][] = $tr;
				$count++;
			}
		}
		foreach($trendingu as $tr) {
			if($tr['trending'] > 0) {
				$tr['reference'] = 'polls';
				$datarows[$tr['trending']][] = $tr;
				$count++;
			}
		}
		krsort($datarows);
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		
		foreach($datarows as $dt) {
			foreach($dt as $rs) {
				switch($rs['reference']) {
					case 'post':					
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
					
						if($userid)
							$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $rs['id']);
						else
							$upvoted = 0;
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'post', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$post = $rs;
						
						if(!empty($post['img'])) {
							$postimg = $this->Dashboard_model->getPostImage($post['img']);
							$post['postimg'] = $postimg;
						}
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'post'), 'data' => $rs, 'upvoted' => $upvoted);
					break;
					
					case 'polls':
						
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
					
						if($userid)
							$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $rs['id']);
						else
							$upvoted = array();
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'polls', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$polls = $rs;
						$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs['id']);
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'polls'), 'data' => $rs, 'upvoted' => $upvoted);
						
					break;
				}
			}
		}
		
		return $results;
	}
	
	public function getFilterTrendingList($sort, $category, $type, $page, $dfrom, $dto) {		
		$datarows = array();
		$results = array();
		$count = 0;
		krsort($datarows);
		if(($type=='') || ($type=='post')) {
			$sqluserp ="select vv_post.*, vv_categories.cat_name, (vv_post.upvote) as trending from vv_post left join vv_categories on vv_post.catid = vv_categories.id where vv_post.status = 1";
			
			if($dfrom!='')
				$sqluserp .=" and DATE(vv_post.created) >= '".$dfrom."'";
			
			if($dto!='')
				$sqluserp .=" and DATE(vv_post.created) <= '".$dto."'";
			
			if($category!='')
				$sqluserp .=" and catid = '".$category."'";
			
			$sqluserp .=" order by trending desc, id desc limit ".$page.",10";
			
			$queryp = $this->db->query($sqluserp);
			$trendingup = $queryp->result_array();
			foreach($trendingup as $tr) {
				if($tr['trending'] > 0) {
					$tr['reference'] = 'post';
					$datarows[$tr['trending']][] = $tr;
					$count++;
				}
			}
		}
		
		if(($type=='') || ($type=='polls')) {
			$sqluser ="select vv_pollls.*, vv_categories.cat_name, (vv_pollls.upvote) as trending from vv_pollls left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.status = 1";
			
			if($dfrom!='')
				$sqluser .=" and DATE(vv_pollls.start_date) >= '".$dfrom."'";
			
			if($dto!='')
				$sqluser .=" and DATE(vv_pollls.end_date) <= '".$dto."'";
			
			if($category!='')
				$sqluser .=" and catid = '".$category."'";
			
			$sqluser .=" order by trending desc, id desc limit ".$page.",10";
			
			$query = $this->db->query($sqluser);
			$trendingu = $query->result_array();
			foreach($trendingu as $tr) {
				if($tr['trending'] > 0) {
					$tr['reference'] = 'polls';
					$datarows[$tr['trending']][] = $tr;
					$count++;
				}
			}
		}
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		
		foreach($datarows as $dt) {
			foreach($dt as $rs) {
				switch($rs['reference']) {
					case 'post':					
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
					
						if($userid)
							$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $rs['id']);
						else
							$upvoted = 0;
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'post', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$post = $rs;
						
						if(!empty($post['img'])) {
							$postimg = $this->Dashboard_model->getPostImage($post['img']);
							$post['postimg'] = $postimg;
						}
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'post'), 'data' => $rs, 'upvoted' => $upvoted);
					break;
					
					case 'polls':
						
						$userinfo = $this->Dashboard_model->getUserInformation($rs['user_id']);
					
						if($userid)
							$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $rs['id']);
						else
							$upvoted = array();
						
						$textdesc = $this->Dashboard_model->getTimelineText($rs['user_id'], 'polls', $rs['id']);
						$rs['textdesc'] = $textdesc['textdesc'];
						$rs['tid'] = $textdesc['id'];
						
						$polls = $rs;
						$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs['id']);
						
						$results[] = array('userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs['id'], 0, 'polls'), 'data' => $rs, 'upvoted' => $upvoted);
						
					break;
				}
			}
		}
		
		return $results;
	}
	
	public function getActivePolls($start, $limit) {
		//$sql ="SELECT * FROM `vv_timeline` where status = 1 order by id desc limit ".$start.", ".$limit."";
		$sql ="SELECT * FROM `vv_timeline` where status = 1 and reference != 'adminpolls' order by id desc limit ".$start.", ".$limit."";
		
		$query = $this->db->query($sql);
		$result = $query->result();
		$datarows = array();
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		
		foreach($result as $rs) {
			switch($rs->reference) {
				case 'post':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $rs->refid);
					else
						$upvoted = 0;
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$post = $this->Dashboard_model->getUserPosts($rs->refid);
					
					if(!empty($post['img'])) {
						$postimg = $this->Dashboard_model->getPostImage($post['img']);
						$post['postimg'] = $postimg;
					}
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'post'), 'data' => (array)$rs, 'upvoted' => $upvoted);
				break;
				
				case 'polls':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $rs->refid);
					else
						$upvoted = array();
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$polls = $this->Dashboard_model->getUserPolls($rs->refid);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs->refid);
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'polls'), 'data' => (array)$rs, 'upvoted' => $upvoted);
					
				break;
				
				case 'timeline':
					$sql ="SELECT * FROM `vv_timeline` where status = 1 and id = ".$rs->refid;
				
					$query = $this->db->query($sql);
					$resultt = $query->row();
					
					$shareusers['userinfo'] = $this->Dashboard_model->getUserInformation($rs->shareby);
					$shareusers['tiinfo'] = (array)$rs;
					
					if($resultt) {
					switch($resultt->reference) {
						case 'post':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
							
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$post = $this->Dashboard_model->getUserPosts($resultt->refid);
							
							if(!empty($post['img'])) {
								$postimg = $this->Dashboard_model->getPostImage($post['img']);
								$post['postimg'] = $postimg;
							}
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'post'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
						break;
						
						case 'polls':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
								
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$polls = $this->Dashboard_model->getUserPolls($resultt->refid);
							$polls['polloption'] = $this->Dashboard_model->getUserPollOption($resultt->refid);
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'polls'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
							
						break;
					}
					
				break;
			}
			}
		}
		return $datarows;
	}
	
	public function getPollByFilter($start, $limit, $sortby, $category, $type, $dfrom, $dto) {
		$datarows = array();
		$cat = '';
			
		$df = ($dfrom!='')?" and DATE(vv_timeline.create_date) >= '".$dfrom."'":"";
		$dt = ($dto!='')?" and DATE(vv_timeline.create_date) <= '".$dto."'":"";
		
		$ty = ($type!='')?" and reference = '".$type."'":"";
			
		if($category != '') {
			$cat = ' and catid = '.$category;
		}
		
		$sb =" order by id ";
		
		if($sortby=='upvote') {
			$sb =" order by upvote ";
		}
		
		$sql = "select * from (SELECT vv_timeline.*, vv_pollls.upvote FROM `vv_timeline` inner join vv_pollls on vv_timeline.refid = vv_pollls.id where reference = 'polls' and vv_pollls.status = 1 ".$cat." ".$df." ".$dt." ".$sb." desc) as a UNION ALL select * from (SELECT vv_timeline.*, vv_post.upvote FROM `vv_timeline` inner join vv_post on vv_timeline.refid = vv_post.id where reference = 'post' and vv_post.status = 1 ".$cat." ".$df." ".$dt."  ".$sb." desc) as b ".$sb." desc limit ".$start.", ".$limit."";
		
		if(($sortby=='participation') || ($type=='poll')) {
		$sql = "SELECT vv_timeline.*, vv_pollls.upvote FROM `vv_timeline` inner join vv_pollls on vv_timeline.refid = vv_pollls.id where reference = 'polls' and vv_pollls.status = 1 ".$cat." ".$df." ".$dt." ".$sb." desc limit ".$start.", ".$limit."";
		}
		
		if(($type=='post')) {
		$sql = "SELECT vv_timeline.*, vv_post.upvote FROM `vv_timeline` inner join vv_post on vv_timeline.refid = vv_post.id where reference = 'post' and vv_post.status = 1 ".$cat." ".$df." ".$dt."  ".$sb." desc limit ".$start.", ".$limit."";
		}
		
		$query = $this->db->query($sql);
		$result = $query->result();
		
		$userid = '';
		if($this->session->userdata('userData')) {
			$userData = $this->session->userdata('userData');
			$userid = $userData['userId'];
		}
		
		foreach($result as $rs) {
			$select = ($type)?$type:$rs->reference;
			switch($select) {
				case 'post':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $rs->refid);
					else
						$upvoted = 0;
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$post = $this->Dashboard_model->getUserPosts($rs->refid);
					
					if(!empty($post['img'])) {
						$postimg = $this->Dashboard_model->getPostImage($post['img']);
						$post['postimg'] = $postimg;
					}
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'post'), 'data' => (array)$rs, 'upvoted' => $upvoted);
				break;
				
				case 'polls':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $rs->refid);
					else
						$upvoted = array();
					
					if($rs->taggedby)
						$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
					else
						$taggedby = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$polls = $this->Dashboard_model->getUserPolls($rs->refid);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs->refid);
					
					$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'polls'), 'data' => (array)$rs, 'upvoted' => $upvoted);
					
				break;
				
				case 'timeline':
					$sql ="SELECT * FROM `vv_timeline` where status = 1 and id = ".$rs->refid;
				
					$query = $this->db->query($sql);
					$resultt = $query->row();
					
					$shareusers['userinfo'] = $this->Dashboard_model->getUserInformation($rs->shareby);
					$shareusers['tiinfo'] = (array)$rs;
					
					if($resultt) {
					switch($resultt->reference) {
						case 'post':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
							
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$post = $this->Dashboard_model->getUserPosts($resultt->refid);
							
							if(!empty($post['img'])) {
								$postimg = $this->Dashboard_model->getPostImage($post['img']);
								$post['postimg'] = $postimg;
							}
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'post'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
						break;
						
						case 'polls':
							if($rs->taggedby)
								$taggedby = $this->Dashboard_model->getUserInformation($rs->taggedby);
							else
								$taggedby = array();
								
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$polls = $this->Dashboard_model->getUserPolls($resultt->refid);
							$polls['polloption'] = $this->Dashboard_model->getUserPollOption($resultt->refid);
							
							$datarows[] = array('shareusers' => $shareusers, 'taggedby' => $taggedby, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'polls'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
							
						break;
					}
					}
					
				break;
			}
		}
		return $datarows;
	}
	
	public function getArchivePolls($start, $limit) {
		$sql ="SELECT vv_pollls.*, vv_categories.cat_name, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id inner join vv_users on vv_pollls.user_id = vv_users.id where end_date < now() order by id desc limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getArchivePollByFilter($start, $limit, $country_id, $state_id, $city_id, $sortby, $category) {
		$result = array();
		$sql ="SELECT vv_pollls.*, vv_categories.cat_name, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id inner join vv_users on vv_pollls.user_id = vv_users.id where end_date < now() ";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($category!='')
			$sql .=" and  catid = '".$category."'";		
		if($sortby!='')
			$sql .=" order by ".$sortby." desc";
		else
			$sql .=" order by id desc";
		
		$sql .=" limit ".$start.", ".$limit."";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getCategories() {
		$sql ="SELECT * FROM `vv_categories` where status = 1 order by cat_name asc";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
	}
	
	public function getEmbedCode($id, $type) {
		$content = '';
		switch($type) {
			case 'polls':
				$sql ="SELECT vv_pollls.*, vv_polloption.option_id, vv_polloption.title, vv_polloption.img, vv_polloption.votes, paruser, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, total, vv_categories.cat_name FROM `vv_pollls` inner join vv_polloption on vv_pollls.id = vv_polloption.pollid left join (SELECT group_concat(userid) as paruser, pollid FROM `vv_pollvoting` where vv_pollvoting.pollid = ".$id.") as voting on vv_pollls.id = voting.pollid inner join (select sum(votes) as total, pollid as pid from vv_polloption where pollid = ".$id.") as vp on vv_pollls.id = vp.pid left join vv_users on vv_pollls.user_id = vv_users.id left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$id;
				$query = $this->db->query($sql);
				$polldata = $query->result();
				
				$content = '<link rel="stylesheet" type="text/css" href="'.base_url("assets/front/style.css").'"/>
							<link rel="stylesheet" type="text/css" href="'.base_url("assets/front/custom.css").'"/>';
				
				if($polldata[0]->username)
					$prourl = base_url().''.$polldata[0]->username;
				else
					$prourl = base_url().'user/id/'.$polldata[0]->id;
					
				$propic = ($polldata[0]->profile_pic_url)?base_url().'uploads/'.$polldata[0]->profile_pic_url:base_url().'images/user.png';
						
				$content .= '<div class="tm_head">
                        <div> <a href="'.$prourl.'" target="_blank"><img src="'.$propic.'" width="46"></a> </div>
                        <div> <a href="'.$prourl.'" class="usr" target="_blank">'.$polldata[0]->firstname.' '.$polldata[0]->lastname.'</a> created a poll in '.$polldata[0]->cat_name.'<br>
                          <small>'.date('M d, Y', strtotime($polldata[0]->start_date)).' &nbsp; Ends '.date('M d, Y', strtotime($polldata[0]->end_date)).'</small> </div>
                      </div>';
				
				$content .= '<h4><a href="'.base_url().'poll/'.$polldata[0]->id.'" class="questn">Q. '.$polldata[0]->question.'</a></h4>';
						
				if($polldata[0]->imag) {
					$content .= '<p><img src="'.base_url().'uploads/'.$polldata[0]->imag.'" width="100%"></p>';
				}
						
				$content .= '<div>';
				
				foreach($polldata as $opt)
					$content .= '<p><input type="radio" name="pollopt" value="'.$opt->option_id.'"> '.$opt->title.'</p>';
				
				$content .= '</div>
						<p align="center"><a href="'.base_url().'poll/'.$polldata[0]->id.'" class="trand-button" target="_blank">Submit</a></p>';
			break;
			
			case 'adminpolls':
			break;
		}
		return $content;
	}
	
	public function getActivities($userid, $start, $limit) {
		if($userid) {
			$friends = $this->Home_model->getFriendIDs($userid);
			if($friends->friends) {
				$fr_arr = explode(',', $friends->friends);
				$alluser = array_unique($fr_arr);
				$users =  implode(',', $fr_arr);
				$sql ="SELECT * FROM `vv_timeline` where status = 1 and userid in (".$users.") and taggedby = 0 order by id desc limit ".$start.", ".$limit."";
			}
			else
				$sql ="SELECT * FROM `vv_timeline` where status = 1 and userid in (".$userid.") and taggedby = 0 order by id desc limit ".$start.", ".$limit."";
		}
		else
			$sql ="SELECT * FROM `vv_timeline` where status = 1 and taggedby = 0 order by id desc limit ".$start.", ".$limit."";
		
		$query = $this->db->query($sql);
		$result = $query->result();
		$datarows = array();
		
		foreach($result as $rs) {
			switch($rs->reference) {
				case 'post':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $rs->refid);
					else
						$upvoted = 0;
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$post = $this->Dashboard_model->getUserPosts($rs->refid);
					
					if(!empty($post['img'])) {
						$postimg = $this->Dashboard_model->getPostImage($post['img']);
						$post['postimg'] = $postimg;
					}
					
					$datarows[] = array('shareusers' => $shareusers, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'post'), 'data' => (array)$rs, 'upvoted' => $upvoted);
				break;
				
				case 'polls':
					if($rs->shareby)
						$shareusers = $this->Dashboard_model->getUserInformation($rs->shareby);
					else
						$shareusers = array();
					
					if($userid)
						$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $rs->refid);
					else
						$upvoted = array();
					
					$userinfo = $this->Dashboard_model->getUserInformation($rs->userid);
					
					$polls = $this->Dashboard_model->getUserPolls($rs->refid);
					$polls['polloption'] = $this->Dashboard_model->getUserPollOption($rs->refid);
					
					$datarows[] = array('shareusers' => $shareusers, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $rs->refid, 0, 'polls'), 'data' => (array)$rs, 'upvoted' => $upvoted);
					
				break;
				
				case 'timeline':
					$sql ="SELECT * FROM `vv_timeline` where status = 1 and id = ".$rs->refid;
				
					$query = $this->db->query($sql);
					$resultt = $query->row();
					
					$shareusers['userinfo'] = $this->Dashboard_model->getUserInformation($rs->shareby);
					$shareusers['tiinfo'] = (array)$rs;
					
					if($resultt) {
					switch($resultt->reference) {
						case 'post':
							
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'post', $resultt->refid);
							else
								$upvoted = 0;
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$post = $this->Dashboard_model->getUserPosts($resultt->refid);
							
							if(!empty($post['img'])) {
								$postimg = $this->Dashboard_model->getPostImage($post['img']);
								$post['postimg'] = $postimg;
							}
							
							$datarows[] = array('shareusers' => $shareusers, 'userinfo' => $userinfo, 'timeline' => array('post' => $post), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'post'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
						break;
						
						case 'polls':
							
							if($userid)
								$upvoted = $this->Dashboard_model->getUserUpvoted($userid, 'polls', $resultt->refid);
							else
								$upvoted = array();
							
							$userinfo = $this->Dashboard_model->getUserInformation($resultt->userid);
							
							$polls = $this->Dashboard_model->getUserPolls($resultt->refid);
							$polls['polloption'] = $this->Dashboard_model->getUserPollOption($resultt->refid);
							
							$datarows[] = array('shareusers' => $shareusers, 'userinfo' => $userinfo, 'timeline' => array('polls' => $polls), 'comments' => $this->Home_model->getComments($userid, $resultt->refid, 0, 'polls'), 'data' => (array)$resultt, 'upvoted' => $upvoted);
							
						break;
					}
					}
					
				break;
			}
		}
		//echo '<pre>';
		//print_r($datarows);
		//echo '</pre>';
		
		return $datarows;
	}
	
	public function getFriendIDs($userid){
		$sql ="SELECT group_concat(friend_one, ',', friend_two) as friends FROM `vv_friends` where (friend_one = '".$userid."' or friend_two = '".$userid."') and friend_one != friend_two";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	public function getComments($userid, $id, $page, $reftype){
		$start = ($page > 1)?($page-1)*4:$page;
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, vv_users.verified, tt.total FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id left join (select count(id) as total, refid, reftype from vv_comment where vv_comment.refid = ".$id." and reftype = '".$reftype."' and status = 1) as tt on vv_comment.refid = tt.refid where vv_comment.status = 1 and vv_comment.refid = ".$id." and vv_comment.reftype = '".$reftype."' order by vv_comment.id desc limit ".$start.", 4";
		$query = $this->db->query($sql);
		$results = $query->result_array();
		
		$value = array();
		foreach($results as $key => $result) {
			$subsql = "select count(id) as subcomment from vv_comment where refid = ".$result['id']." and reftype='comment'";
			$subque = $this->db->query($subsql);
			$vl = $subque->row_array();
			$result['subcomment'] = $vl['subcomment'];
			
			$value[$key] = $result;
			if($userid)
				$value[$key]['upvoted'] = $this->Dashboard_model->getUserUpvoted($userid, 'comment', $result['id']);
			else
				$value[$key]['upvoted'] = 0;
		}
		return $value;
    }
	
	public function getPeopleKnow($userid=''){
		$sql ="SELECT id as userid, CONCAT(firstname, ' ', lastname) as fullname, profile_pic_url, username, verified FROM vv_users where id <> 1";
		if($userid!='')
			$sql .=" and id <> ".$userid;
		$sql .=" order by rand() limit 9";
		
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }
}