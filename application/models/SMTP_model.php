<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class SMTP_model extends CI_Model {

    public function __construct()

    {
        $this->load->database();
    }
    
	public function getSmtpDetails(){
		$sql ="SELECT * FROM vv_smtpsettings";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getTemplate(){
		$sql ="SELECT * FROM vv_emailtemplate";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function updateSettings($id,$email,$name,$reply,$host,$port,$encrpt,$username,$pass){
		$data = array('fromEmail' => $email ,'fromName' => $name ,'smtpPort' => $port ,'encrption' => $encrpt ,'replyTo' => $reply ,'host' => $host ,'username' => $username ,'password' => $pass , 'created' => date("Y-m-d H:i:s"));
		$update = $this->db->update('vv_smtpsettings',$data,array('id'=>$id));
    }
	
	public function insertSettings($email,$name,$reply,$host,$port,$encrpt,$username,$pass){
		$data = array('fromEmail' => $email ,'fromName' => $name ,'smtpPort' => $port ,'encrption' => $encrpt ,'replyTo' => $reply ,'host' => $host ,'username' => $username ,'password' => $pass , 'created' => date("Y-m-d H:i:s"));
		$insert = $this->db->insert('vv_smtpsettings',$data);
    }
	
	
	
	public function updateTemplate($id,$subject,$content){
		$data = array('subject' => $subject ,'content' => $content , 'created' => date("Y-m-d H:i:s"));
		$update = $this->db->update('vv_emailtemplate',$data,array('id'=>$id));
    }
	
	public function insertTemplate($subject,$content){
		$data = array('subject' => $subject ,'content' => $content , 'created' => date("Y-m-d H:i:s"));
		$insert = $this->db->insert('vv_emailtemplate',$data);
    }

}