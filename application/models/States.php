<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class States extends CI_Model{
	function __construct() {
		return false;
	}
	
	public function getStates() {
		$sql ="select * from vv_states where status = 1";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function totalStates($cid='', $sname='') {
		$sql ="select count(vv_states.id) as total from vv_states left join vv_countries on vv_states.country_id = vv_countries.id where 1";
		if($cid!='')
			$sql .=" and vv_states.country_id = '".$cid."'";
		if($sname!='')
			$sql .=" and vv_states.name like '%".$sname."%'";
		
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCountries() {
		$sql ="select * from vv_countries";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allStates($cid='', $sname='', $page='') {
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		$sql ="select vv_states.*, vv_countries.name as cname from vv_states left join vv_countries on vv_states.country_id = vv_countries.id where 1";
		if($cid!='')
			$sql .=" and vv_states.country_id = '".$cid."'";
		if($sname!='')
			$sql .=" and vv_states.name like '%".$sname."%'";
		
		$sql .=" order by country_id, vv_states.name limit $start, $limit";
		$query = $this->db->query($sql);
        return $query->result();
	}

	public function AllstateByCountry($cc) {
		$sql ="select * from vv_states where country_id = '".$cc."'";
		
        $query = $this->db->query($sql);
        return $query->result();
	}
	
	public function getCurrentState($cc) {
		$sql ="select * from vv_states where sortname = '".$cc."' limit 1";
		$query = $this->db->query($sql);
        $row = $query->result();
        return $row[0];
	}
	
	public function singleState($id) {
		$sql ="select * from vv_states where id = ".$id;
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function singleStateByCity($id) {
		$sql ="select * from vv_states where id = ".$id;
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function updateStatus($status, $id) {
		$sql ="update vv_states set status = ".$status." where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function editState($statename, $cid, $status, $id) {
		$sql ="update vv_states set status = ".$status.", name = '".$statename."', country_id = '".$cid."' where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function addState($statename, $cid, $status) {
		$sql ="insert into vv_states values ('','".$statename."','".$cid."',".$status.")";
		$query = $this->db->query($sql);
        return true;
	}
	
	public function deleteState($id) {
		$sql ="delete from vv_states where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
}