<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Countries extends CI_Model{
	function __construct() {
		return false;
	}
	
	public function getCountries() {
		$sql ="select * from vv_countries where status = 1 order by name";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function totalCountries($catname='') {
		if($catname!='')
			$sql ="select count(id) as total from vv_countries where name like '%".$catname."%'";
		else
			$sql ="select count(id) as total from vv_countries";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCountries($catname='', $page='') {
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		if($catname!='')
			$sql ="select * from vv_countries where name like '%".$catname."%' order by name limit $start, $limit";
		else
			$sql ="select * from vv_countries order by name limit $start, $limit";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function getCurrentCountry($cc) {
		$sql ="select * from vv_countries where sortname = '".$cc."' order by name limit 1";
		$query = $this->db->query($sql);
        $row = $query->result();
        return $row[0];
	}
	
	public function singleCountry($id) {
		$sql ="select * from vv_countries where id = ".$id;
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function updateStatus($status, $id) {
		$sql ="update vv_countries set status = ".$status." where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function editCountry($countryname, $countryshort, $status, $id) {
		$sql ="update vv_countries set status = ".$status.", name = '".$countryname."', sortname = '".$countryshort."' where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function addCountry($countryname, $countryshort, $status) {
		$sql ="insert into vv_countries values ('','".$countryname."','".$countryshort."',".$status.")";
		$query = $this->db->query($sql);
        return true;
	}
	
	public function deleteCountry($id) {
		$sql ="delete from vv_countries where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
}