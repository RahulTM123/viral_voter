<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SinglePoll_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getSinglePoll($id){
		$val = 0;
		$html = '';
		$sql ="SELECT vv_pollls.*, vv_polloption.option_id, vv_polloption.title, vv_polloption.img, vv_polloption.votes, paruser, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url, total, vv_categories.cat_name FROM `vv_pollls` inner join vv_polloption on vv_pollls.id = vv_polloption.pollid left join (SELECT group_concat(userid) as paruser, pollid FROM `vv_pollvoting` where vv_pollvoting.pollid = ".$id.") as voting on vv_pollls.id = voting.pollid inner join (select sum(votes) as total, pollid as pid from vv_polloption where pollid = ".$id.") as vp on vv_pollls.id = vp.pid left join vv_users on vv_pollls.user_id = vv_users.id left join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getSinglePollComments($id){
		$val = 0;
		$html = '';
		$sql ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.profile_pic_url FROM `vv_comment` inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id where reftype = 'polls' and vv_comment.status = 1 and vv_comment.refid = ".$id." order by vv_comment.id desc";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	
	
	
	public function getSinglePollBeforeLogin($id){
		$val = 0;
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				
				$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= ' </div><div class="media-body"><h6 class="media-heading">';
				
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class=""> Created a Poll</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
				if($r->question != '' && $r->imag != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}elseif($r->question != '' && $r->videourl != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}elseif($r->question){
					$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
				}elseif($r->videourl){
					$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}else{
					$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}
				
				$html .= '<div class="trand row">';
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = $pollopt->title;
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
				}
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				$html .= '</div></div><div class="col-md-4 col-sm-4">';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = $pollopt->title;
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
				$squery = $this->db->query($sql);
				$totalShare = $squery->result();
					
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;	
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button"><a href="'.base_url().'Login"><button type="button" class="trand-button">Submit Vote</button></div><div class="button"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div></form>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div>  </div>';
				
				$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,5";
				$cmntquery = $this->db->query($sql);
				$cmntdata = $cmntquery->result();
				
				$html .= '<div class="col-md-12 vvuficontainer singlePoll"><div class="vvufilist"><div class="cmntData">';
				
				if(!empty($cmntdata)){
					foreach($cmntdata as $data){
						$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
						
						if(!empty($data->cmntImg) && !empty($data->content)){
							$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}	
							
						}elseif(!empty($data->cmntImg)){
							$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
							
							
						}else{
							$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
		  <div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt" href="'.base_url().'Login">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt" href="'.base_url().'Login">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
						}
						
						$html .= '</div></div>';
					}
				}else{
					$html .= '';	
				}
				$html .= '</div>';
				
				$html .= '<div direction="right" class="clearfix"><div class="_ohf rfloat"></div>';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button" style="display:none;">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}else{
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}
				
				$html .= '</div></div></div><input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$totalComnt[0]->total.'">';
				
				$html .= '</div></article></div></div>';
				
			}
			return $html;
		}else{
			redirect('./Home');
		}
    }
	
	
	public function getSinglePollForAdmin($id){
		$val = 0;
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			foreach($value as $r){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
				$query = $this->db->query($sql);
				$users = $query->result();
				
				$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
				$query1 = $this->db->query($sql1);
				$pollresult = $query1->result();
				
				$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
				$pollquery = $this->db->query($poll);
				$polloption = $pollquery->result();
				
				
				
								
				$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				}
				
				$html .= ' </div><div class="media-body"><h6 class="media-heading">';
				
				
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
				}
				
				$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class=""> Created a Poll</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
				if($r->question != '' && $r->imag != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}elseif($r->question != '' && $r->videourl != ''){
					$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}elseif($r->question){
					$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
				}elseif($r->videourl){
					$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
				}else{
					$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
				}
				
				$html .= '<div class="trand row">';
				
				foreach($polloption as $pollopt){
					$html .= '<div class="col-md-6 col-sm-6">';
					$title = $pollopt->title;
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
						$html .= '<div class="media"><div class="media-left">';
						$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
						$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
						}else{
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}
					}
					$html .= '</div>';
				}
				
				//poll vote counting start here
				$pollvotearray = array();
				$pollperarray = array();
				$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
				$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
				if(!empty($pollresult)){
					$totalvote = count($pollresult);
					foreach($pollresult as $p){
						if($p->vote == 1){
							$optonevote++;	
						}
						if($p->vote == 2){
							$opttwovote++;	
						}
						if($p->vote == 3){
							$optthreevote++;	
						}
						if($p->vote == 4){
							$optfourvote++;	
						}
					}
					if($optonevote > 0){
						$optoneper = ($optonevote/$totalvote)*100;
					}
					if($opttwovote > 0){
						$opttwoper = ($opttwovote/$totalvote)*100;
					}
					if($optthreevote > 0){
						$optthreeper = ($optthreevote/$totalvote)*100;
					}
					if($optfourvote > 0){
						$optfourper = ($optfourvote/$totalvote)*100;
					}
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}else{
					array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
					array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
				}
				$html .= '</div></div><div class="col-md-4 col-sm-4">';
				
				foreach($polloption as $pollopt){
					$img = $text = '';
					$title = $pollopt->title;
					if(count($title) == '2'){
						$img = $title[1];
						$text = $title[0];
					}else{
						$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
						if(!empty($ext)){
							$img = $title[0];
						}else{
							$text = $title[0];
						}
					}
					$img = $pollopt->title;
					$text = $pollopt->img;
					if(!empty($img) && !empty($text)){
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}elseif(!empty($img)){
						$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
					}else{
						$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
					}
					$val++;
				}
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
				$squery = $this->db->query($sql);
				$totalShare = $squery->result();
					
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;	
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div></form>';
				
				$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class="v_like"><i class="fa fa-share"></i>Share</span></div>  </div>';
				
				$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,5";
				$cmntquery = $this->db->query($sql);
				$cmntdata = $cmntquery->result();
				
				$html .= '<div class="col-md-12 vvuficontainer singlePoll"><div class="vvufilist"><div class="cmntData">';
				
				if(!empty($cmntdata)){
					foreach($cmntdata as $data){
						$sql ="SELECT * FROM vv_users WHERE id = ".$data->userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
						}
						if(!empty($userdata[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($userdata[0]->picture_url)){
							$html .= '<img src="'.$userdata[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
						
						if($userdata[0]->username != ''){
							$html .= '<a href="'.base_url().''.$userdata[0]->username.'" class="usr_name">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'" class="usr_name">';	
						}
						
						$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span>';
						
						if(!empty($data->cmntImg) && !empty($data->content)){
							$html .= '<span> '.$data->content.'</span></div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}	
							
						}elseif(!empty($data->cmntImg)){
							$html .= '</div><p style="margin-bottom:5px;"><img src="'.base_url().'uploads/'.$data->cmntImg.'" style="border-radius: 15px;" class="img-fluid"></p><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me"><div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class=" UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
							
							
						}else{
							$html .= '<span> '.$data->content.'</span></div><div class="fsm fwn fcg UFICommentActions"> <span class="_6a _3-me">
		  <div class="_khz _4sz1 _4rw5 _3wv2"><a class="UFILikeLink UFIReactionLink cmntUnderCmnt">Upvote</a></div></span><span aria-hidden="true" role="presentation"> · </span><a class="UFIReplyLink _460i cmntUnderCmnt">Comment</a><div class="cmntUcmnt" style="border:none;"><div class="cmntDataOnCmnt">';
							
							$sql ="SELECT * FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntdata = $query->result();
							
							if(!empty($cmmntdata)){
								foreach($cmmntdata as $cdata){
									$sql ="SELECT * FROM vv_users WHERE id = ".$cdata->userid;
									$userquery = $this->db->query($sql);
									$cmntuser = $userquery->result();
									
									$html .= '<div class="clearfix"><div class="user"> <span class="sm-pic">';
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'">';	
									}
									if(!empty($cmntuser[0]->profile_pic_url)){
										$html .= '<img src="'.base_url().'uploads/'.$cmntuser[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
									}elseif(!empty($cmntuser[0]->picture_url)){
										$html .= '<img src="'.$cmntuser[0]->picture_url.'" class="img-fluid" style="width:40px;">';
									}else{
										$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
									}
									
									$html .= '</a></span> </div><div class="usr_content_block"><div class="user_cmmnt_block"> <span>';
									
									if($cmntuser[0]->username != ''){
										$html .= '<a href="'.base_url().''.$cmntuser[0]->username.'" class="usr_name">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$cmntuser[0]->id.'" class="usr_name">';	
									}
									
									$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.'</a></span> <span> '.$cdata->content.'</span> </div></div></div>';
								}
							}
							
							$html .= '</div></div></div>';
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE cmntId = '".$data->id."'";
							$query = $this->db->query($sql);
							$cmmntCountData = $query->result();
							if($cmmntCountData[0]->total > 0){
								$html .= '<div class="UFIReplyList"><div class="UFIRow UFIReplySocialSentenceRow _4204 _2o9m"><a class="UFICommentLink" role="button"><div direction="left" class="clearfix"><div class="_ohe lfloat"><div class="img _8o _8r UFIImageBlockImage"><img src="http://www.viralvoters.com/beta/assets/front/images/down-arrow.png" /></div></div><div class=""><div class="UFIImageBlockContent _42ef _8u"><span><a class="cmntReply">'.$cmmntCountData[0]->total.' Replies</a></span></div></div></div></a></div></div>';
							}
						}
						
						$html .= '</div></div>';
					}
				}else{
					$html .= '';	
				}
				$html .= '</div>';
				
				$html .= '<div direction="right" class="clearfix"><div class="_ohf rfloat"></div>';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button" style="display:none;">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}else{
					$html .= '<a href="" class="UFIPagerLink viewCmnt" role="button">View More comments<img src="http://www.viralvoters.com/beta/assets/front/images/loader.gif" class="img-fluid" id="sinLoaderImg"></a>';
				}
				
				$html .= '</div></div></div><input type="hidden" id="row" value="0"><input type="hidden" id="all" value="'.$totalComnt[0]->total.'">';
				
				$html .= '</div></article></div></div>';
				
			}
			return $html;
		}else{
			redirect('./Home');
		}
    }
	
	
}