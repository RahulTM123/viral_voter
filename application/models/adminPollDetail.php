<div id="page-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header">Admin Polls - <?php echo $stats[0]->question; ?></h1>
        <?php echo ($stats[0]->imag != '')?'<p><img src="'.base_url().'uploads/'.$stats[0]->imag.'" width="300" /></p>':''; ?>
        <?php echo ($stats[0]->videourl != '')?'<p><iframe width="300" height="" src="'.$stats[0]->videourl.'" frameborder="0" allowfullscreen></iframe></p>':''; ?>
        <p><strong>Category: </strong><?php echo $stats[0]->cat_name; ?></p>
        <p><strong>Location: </strong><?php echo $stats[0]->cityname; ?>, <?php echo $stats[0]->stname; ?>(<?php echo $stats[0]->ctname; ?>)</p>
      </div>
    </div>
  <div class="row">
    <div class="col-lg-12">
        <div class="panel-body">
          <div class="table-responsive" style="overflow-x: hidden;">
            <form method="get" action="<?php echo base_url();?>admin/admin-poll" id="filterForm">
            <div class="col-md-12" style="padding:0px; margin-bottom:10px;">
                <div class="col-md-3" style="padding-left:0px;">
                	<label>Country</label>
                	<select name="cid" id="cid" class="form-control">
                       <option value="">--Select--</option>
                       <?php foreach($countries as $country) {
                           $selected = '';
                           if($country->id == $current_country['id'])
                             $selected = ' selected="selected"';
                           echo '<option value="'.$country->id.'"'.$selected.'>'.$country->name.'</option>';
                       }
                    ?>
                     </select>
                </div>
                <div class="col-md-3" style="padding-left:0px;">
                    <label>State</label>
                    <select name="sid" id="sid" class="form-control">
                        <option value="">--Select--</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding-left:0px;">
                    <label>City</label>
                    <select name="ctid" id="ctid" class="form-control">
                        <option value="">--Select--</option>
                    </select>
                </div>
                <div class="col-md-3" style="padding-left:0px;">
                    <label>&nbsp;</label><br />
                    <button type="button" name="filter" id="pfilter" class="trand-button" style="padding: 2px 50px;font-size: 17px;">Search</button> 
                </div>
            </div>
            <div class="col-md-12" style="padding:0px; margin-bottom:10px;">
                <!--<div class="col-md-3" style="padding-left:0px;">
                    <label>By Votes</label>
                    <select name="voteFilter" id="voteFilter" class="form-control" style="margin-bottom:15px;">
                        <option value="">--Filter By Votes--</option>
                        <option value="high">Highest Vote</option>
                        <option value="low">Lowest Vote</option> 
                    </select>
                </div>-->
                <!--<div class="col-md-3">
                   <input type="text" class="Date form-control" placeholder="Select Date" name="pdate" id="pdate" />
                </div>-->
            </div>
            </form>
            <p>&nbsp;</p>
            <table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;">
              <tbody>
                <tr>
                  <th style="width:20px;">Sr No.</th>
                  <th>User</th>
                  <?php foreach($stats as $option) {
				  	echo '<th>';
					if($option->img)
				  		echo '<img src="'.base_url().'uploads/'.$option->img.'" width="60" />';
				  	echo $option->options.'</th>';
				  }
				  ?>
                  <th>Date / Time</th>
                  <th>User IP</th>
                </tr>
                <?php if(!empty($result)){
				$page = (isset($_REQUEST['page']))?$_REQUEST['page']:1;
				$limit = 10;
				$start = ($page)?($page-1)*$limit:0;
                $i = $start + 1;
                foreach($result as $value){ ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php if($value->username != '') {
                  echo '<a href="'.base_url().'user/'.$value->username.'">'.$value->firstname.' '.$value->lastname.'</a>';	
                  } else {
                  echo '<a href="'.base_url().'user/id/'.$value->id.'">'.$value->firstname.' '.$value->lastname.'</a>';	
                  } ?><br />(<?php echo $value->cityname.', '.$value->stname.'<br />('.$value->ctname.')'; ?>)</td>
                  <?php for($i=0;$i<count($stats);$i++) {
					  if($i==$value->voting_option)
				  		echo '<td><i class="fa fa-check" aria-hidden="true"></i></td>';
					  else
				  		echo '<td><i class="fa fa-times" aria-hidden="true"></i></td>';
				  }
				  ?>
                  <td><?php echo date('d M, Y h:i A',strtotime($value->created)); ?></td>
                  <td><a href="https://www.ip-tracker.org/locator/ip-lookup.php?ip=<?php echo trim($value->userip); ?>" target="_blank"><?php echo $value->userip; ?></a></td>
                </tr>
                <?php }  
				  } else {
					  echo '<tr><td colspan="7">No Result Found!</td></tr>';		
				  } ?>
              </tbody>
            </table>
          </div>
      </div>
    </div>
  </div>
</div>

