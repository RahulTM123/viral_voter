<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function addNewCategory($catName){
		$input = array('cat_name' => $catName );
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_categories', $input);
		$insertId = $this->db->insert_id();
		return  $insertId;
    }
	
	public function editCategory($catId,$catName){
		$input = array('cat_name' => $catName );
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->update('vv_categories', $input,array('id' => $catId));
		return  'Success';
    }
	
	public function getCatList($catname=''){
		if($catname!='')
			$sql ="select * from vv_categories where cat_name like '%".$catname."%' order by cat_name";
		else
			$sql ="select * from vv_categories order by cat_name";
		$query = $this->db->query($sql);
        return $query->result();
    }
	
	public function getCatListByFilter($filter){
		$html = '';
		
		if($filter == 'top'){
			$sql ="SELECT COUNT(c.id) as total,c.* FROM `vv_categories` as c LEFT JOIN `vv_logs` as l ON c.id = l.catid GROUP BY c.cat_name ORDER BY total DESC";
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			
			
			$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
			$recordsPerPage = 10;
			$start = ($page-1) * $recordsPerPage;
			$adjacents = "2";
			 
			$prev = $page - 1;
			$next = $page + 1;
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1;   
			
			$sql ="SELECT COUNT(c.id) as total,c.* FROM `vv_categories` as c LEFT JOIN `vv_logs` as l ON c.id = l.catid GROUP BY c.cat_name ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();	
		}else{
			$sql ="SELECT COUNT(c.id) as total,c.* FROM `vv_categories` as c LEFT JOIN `vv_logs` as l ON c.id = l.catid GROUP BY c.cat_name ORDER BY total ASC";	
			$query = $this->db->query($sql);
			$val = $query->result();
			$count = count($val);
			
			
			$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
			$recordsPerPage = 10;
			$start = ($page-1) * $recordsPerPage;
			$adjacents = "2";
			 
			$prev = $page - 1;
			$next = $page + 1;
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1;   
			
			$sql ="SELECT COUNT(c.id) as total,c.* FROM `vv_categories` as c LEFT JOIN `vv_logs` as l ON c.id = l.catid GROUP BY c.cat_name ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();	
		}
		
		if(!empty($result)){
			
			$i = 1;
			$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Category Name</th><th>Status</th><th>Action</th></tr>';
			foreach($result as $value){
				$html .= '<tr>
				<td>'.$i.'</td>
				<td>'.$value->cat_name.'</td>
				<td class="tdClass">';
				if($value->status == 1){
					$html .= '<a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activated</p></a><a href="category/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}else{
					$html .= '<a href="category/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivated</p></a></td>';
				}
				$html .= '<td class="tdClass"><a href="edit_category/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen btn-success">Edit</p></a><a href="category/delete?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
			$html .= '</tbody></table>';
		}else{
			$html = '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Category Name</th><th>Status</th><th>Action</th></tr><tr><td clospan="4"></td></tr></tbody></table>';
		}
		
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=\"?categoryFilter=".$filter."&page=".($prev)."\">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=\"?categoryFilter=".$filter."&page=".($counter)."\">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=\"?categoryFilter=".$filter."&page=".($counter)."\">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=\"?categoryFilter=".$filter."&page=".($lpm1)."\">$lpm1</a>";
					$html.= "<a href=\"?categoryFilter=".$filter."&page=".($lastpage)."\">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=\"1\"\">1</a>";
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=\"2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=\"?categoryFilter=".$filter."&page=".($counter)."\">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=".($lpm1)."\">$lpm1</a>";
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=".($lastpage)."\">$lastpage</a>";   
			   }else{
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=\"1\"\">1</a>";
				   $html.= "<a href=\"?categoryFilter=".$filter."&page=\"2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=\"?categoryFilter=".$filter."&page=".($counter)."\">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=\"?categoryFilter=".$filter."&page=".($next)."\">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
    }
	
	public function getCatById($id){
		$sql ="SELECT * FROM vv_categories WHERE id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function changeCatStatus($catId){
		$sql ="SELECT status FROM vv_categories WHERE id=".$catId;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_categories SET status = 0 WHERE id=".$catId;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_categories SET status = 1 WHERE id=".$catId;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function deleteCategory($catId){
		$sql ="DELETE FROM vv_categories WHERE id=".$catId;
		$query = $this->db->query($sql);
		return 1;
    }
	
	public function getVerifyCatList(){
		$sql ="select * from vv_verify_category where status = 1 order by name";
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }

    public function getAllcategories(){
		$sql ="select * from vv_categories where status = 1 order by cat_name";
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }
	
	
}