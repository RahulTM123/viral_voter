<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Competitions_model extends CI_Model {

    public function __construct()

    {
        $this->load->database();
    }
    
    public function getCompBeforeLogin($strt_date,$end_date,$category){
    	$andWhere = '';
    	if(isset($strt_date) && !empty($strt_date)) 
    	{
    		$andWhere .= ' and DATE(start_date)="'.$strt_date.'"';
    	}
    	if(isset($end_date) && !empty($end_date)) 
    	{
    		$andWhere .= ' and DATE(end_date)="'.$end_date.'"';
    	}
    	
    	if(isset($category) && !empty($category)) 
    	{
    		$andWhere .= ' and catid='.$category.'';
    	}
    	
		$html = '';
		// $sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		// $query = $this->db->query($sql);
		// $logs = $query->result();
		// if(!empty($logs)){
		 	$a = 1;
		// 	foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition ";
				//echo $sql; exit;
				$query = $this->db->query($sql);
				$value = $query->result();
				// echo '<pre>';
				// print_r($value);
				foreach($value as $r){
					// $startdate = date('Y-m-d',strtotime($r->created));
					// $enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					// if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					// 	$pDate = date('M, d Y',strtotime($r->created));
							$strings = strip_tags('Text texttexttexttexttexttext texttext text');
						if (strlen($strings) > 5) {

						    // truncate string
						    $stringCut = substr($strings, 0, 5);
						    $endPoint = strrpos($stringCut, ' ');

						    //if the string doesn't contain any space then it will cut without word basis.
						    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
						    $string .= '... <i class="fa fa-plus read_more" num="'.$a.'" style="color:#03b4da; font-size:10px;"></i>';
						}
						
						$html .= '<div class="row">';
						$html .= '<div class="col-md-12 headding-info">';
						$html .= '<div class="row">';
						$html .= '<div class="col-sm-3">';
						$html .= '<strong >Contest '.$a.'  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-6">';
						$html .= '<strong class="middle_contest">For the Indian Baby Contest 01  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-3">';
						$html .= '<button type="button" id="trigger_popup_fricc_'.$a.'" num="'.$a.'" class="trigger_popup_fricc w3-button w3-grey">Terms</button>';
						$html .= '</div>';

						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="hover_bkgr_fricc" id="hover_bkgr_fricc_'.$a.'">';
						$html .= '<span class="helper"></span>';
						$html .= '<div>';
						$html .= '<div class="popupCloseButton">X</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<form action="#" class="marginNone">';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label>Start:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->start_date)).'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_end_date.'">';
						$html .= '<label>End:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->end_date)).'</span>';
						$html .= '</p>';						
						
						$html .= '<p style="display:'.$r->show_country.'">';
						$html .= '<label> Country :</label>';
						$html .= '<span class="time">'.$r->country.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_state.'">';
						$html .= '<label> State :</label>';
						$html .= '<span class="time">'.$r->state.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_city.'">';
						$html .= '<label> City :</label>';
						$html .= '<span class="time">'.$r->city.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >Yes</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_cat.'">';
						$html .= '<label> Category:</label>';
						$html .= '<span class="time" >'.$r->catid.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_gender.'">';
						$html .= '<label> Gender:</label>';
						$html .= '<span class="time" >'.$r->gender.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_rewards.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >'.$r->rewards.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_age.'">';
						$html .= '<label> Age:</label>';
						$html .= '<span class="time" >'.$r->age_from.' to '.$r->age_to.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_title.'">';
						$html .= '<label>Terms :</label>';
						$html .= '<span class="time" id="terms_'.$a.'">'.$string.'</span><input type="hidden" value="'.$strings.'" id="terms_value_'.$a.'"><a class="less_more" id="less_more_'.$a.'" num="'.$a.'" style="color:#03b4da; font-size:10px; display:none;">Close</a>';
						$html .= '</p>';
						$html .= '</form>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<div class="row">';						
						$html .= '<div class="col-md-12">';
						$html .= '<div class="content">';
						$html .= '<img src="'.base_url("assets/front/contest_image/".$r->image."").'" alt="image">';
						$html .= '</div>';
						$html .= '<div class="box-container">';
						$html .= '<p class="participate-btn"><a href="#">Participate</a></p>';
						$html .= '<div class="thumbnail-img-box">';
						$html .= '<ul>';
						$html .= '<h4>Recent Participants <i class="fa fa-arrow-down"></i></h4>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Tom</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Rahul</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sunny</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Mac</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<div class="clr"></div>';
						$html .= '</ul>';
						$html .= '<a href="#" class="view-all">View All </a>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
					
					//} 
					$a++;
				}
		// 	}
		// }
		return $html;
	}

	public function getCompBeforeLogin1(){
		$html = '';
		$sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		$query = $this->db->query($sql);
		$logs = $query->result();
		if(!empty($logs)){
			foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition WHERE id = '".$log->log_id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						
						$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like"><a href="'.base_url().'login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
						
						
						$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td></tr></tbody></table></div></div></div></div></div></div></article></div></div>';
					}
				}
			}
		}
		return $html;
	}

	public function getCompForAdmin($strt_date,$end_date,$category){
    	$andWhere = '';
    	if(isset($strt_date) && !empty($strt_date)) 
    	{
    		$andWhere .= ' and DATE(start_date)="'.$strt_date.'"';
    	}
    	if(isset($end_date) && !empty($end_date)) 
    	{
    		$andWhere .= ' and DATE(end_date)="'.$end_date.'"';
    	}
    	
    	if(isset($category) && !empty($category)) 
    	{
    		$andWhere .= ' and catid='.$category.'';
    	}
    	
		$html = '';
		// $sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		// $query = $this->db->query($sql);
		// $logs = $query->result();
		// if(!empty($logs)){
			$a = 1;
			// foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition ";
				//echo $sql; exit;
				$query = $this->db->query($sql);
				$value = $query->result();
				
				foreach($value as $r){
					// $startdate = date('Y-m-d',strtotime($r->created));
					// $enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					// if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					// 	$pDate = date('M, d Y',strtotime($r->created));
							$strings = strip_tags('Text texttexttexttexttexttext texttext text');
						if (strlen($strings) > 5) {

						    // truncate string
						    $stringCut = substr($strings, 0, 5);
						    $endPoint = strrpos($stringCut, ' ');

						    //if the string doesn't contain any space then it will cut without word basis.
						    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
						    $string .= '... <i class="fa fa-plus read_more" num="'.$a.'" style="color:#03b4da; font-size:10px;"></i>';
						}
						
						$html .= '<div class="row">';
						$html .= '<div class="col-md-12 headding-info">';
						$html .= '<div class="row">';
						$html .= '<div class="col-sm-3">';
						$html .= '<strong >Contest '.$a.'  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-6">';
						$html .= '<strong class="middle_contest">For the Indian Baby Contest 01  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-3">';
						$html .= '<button type="button" id="trigger_popup_fricc_'.$a.'" num="'.$a.'" class="trigger_popup_fricc w3-button w3-grey">Terms</button>';
						$html .= '</div>';

						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="hover_bkgr_fricc" id="hover_bkgr_fricc_'.$a.'">';
						$html .= '<span class="helper"></span>';
						$html .= '<div>';
						$html .= '<div class="popupCloseButton">X</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<form action="#" class="marginNone">';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label>Start:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->start_date)).'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_end_date.'">';
						$html .= '<label>End:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->end_date)).'</span>';
						$html .= '</p>';						
						
						$html .= '<p style="display:'.$r->show_country.'">';
						$html .= '<label> Country :</label>';
						$html .= '<span class="time">'.$r->country.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_state.'">';
						$html .= '<label> State :</label>';
						$html .= '<span class="time">'.$r->state.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_city.'">';
						$html .= '<label> City :</label>';
						$html .= '<span class="time">'.$r->city.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >Yes</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_cat.'">';
						$html .= '<label> Category:</label>';
						$html .= '<span class="time" >'.$r->catid.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_gender.'">';
						$html .= '<label> Gender:</label>';
						$html .= '<span class="time" >'.$r->gender.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_rewards.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >'.$r->rewards.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_age.'">';
						$html .= '<label> Age:</label>';
						$html .= '<span class="time" >'.$r->age_from.' to '.$r->age_to.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_title.'">';
						$html .= '<label>Terms :</label>';
						$html .= '<span class="time" id="terms_'.$a.'">'.$string.'</span><input type="hidden" value="'.$strings.'" id="terms_value_'.$a.'"><a class="less_more" id="less_more_'.$a.'" num="'.$a.'" style="color:#03b4da; font-size:10px; display:none;">Close</a>';
						$html .= '</p>';
						$html .= '</form>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<div class="row">';						
						$html .= '<div class="col-md-12">';
						$html .= '<div class="content">';
						$html .= '<img src="'.base_url("assets/front/contest_image/".$r->image."").'" alt="image">';
						$html .= '</div>';
						$html .= '<div class="box-container">';
						$html .= '<p class="participate-btn"><a href="#">Participate</a></p>';
						$html .= '<div class="thumbnail-img-box">';
						$html .= '<ul>';
						$html .= '<h4>Recent Participants <i class="fa fa-arrow-down"></i></h4>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Tom</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Rahul</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sunny</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Mac</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<div class="clr"></div>';
						$html .= '</ul>';
						$html .= '<a href="#" class="view-all">View All </a>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';

						
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
					
					//} 
					$a++;
				}
		// 	}
		// }
		return $html;
	}
	/*
	public function getCompForAdmin(){
		$html = '';
		$sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		$query = $this->db->query($sql);
		$logs = $query->result();
		if(!empty($logs)){
			foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition WHERE id = '".$log->log_id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						
						$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="#" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like"><a href="#" class="_bl"><i class="fa fa-comment"></i>Comment</a></span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="#" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
						
						
						$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td></tr></tbody></table></div></div></div></div></div></div></article></div></div>';
					}
				}
			}
		}
		return $html;
	}
	
	public function getCompAfterLogin($userid){
		$html = '';
		$sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		$query = $this->db->query($sql);
		$logs = $query->result();
		if(!empty($logs)){
			foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition WHERE id = '".$log->log_id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
						$curntquery = $this->db->query($sql);
						$curntusers = $curntquery->result();
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						
						$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						
						$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$userid."' AND log_title = 'compitition'";
						$query = $this->db->query($sql);
						$getLikeId = $query->result();
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
						
						
						if(!empty($getLikeId)){
							$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "compitition" >';
						}else{
							$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "compitition">';
						}
						
						
						$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="compitition"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="compition_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
						
						
						$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment_comp lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalse usr_content_block"><a href="#" class=" cancelEdit">Cancel</a></span>
						
						<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
						
						<div class="col-md-3 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgOne" id="compImgOne'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
						<div class="col-md-3 colMdTwo" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgTwo" id="compImgTwo'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgTwo" class="hiddenCompImgTwo"></span></div>
						<div class="col-md-3 colMdThree" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgThree" id="compImgThree'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgThree" class="hiddenCompImgThree"></span></div>
						
						</div></div><p class="saveComment"><button type="button" class="trand-button saveTheComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButton" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
		
						<div class="enter"><label title="Attach a photo"><span class="commentPhoto"></span></label></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
						
						if($totalComnt[0]->total < 6){
							$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
						}else{
							$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button">View All comments</a>';
						}
						
						$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="pUserId" id="pUserId"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					}
				}
			}
		}
		return $html;
	}
	*/
	public function getCompAfterLogin($strt_date,$end_date,$category){
    	$andWhere = '';
    	if(isset($strt_date) && !empty($strt_date)) 
    	{
    		$andWhere .= ' and DATE(start_date)="'.$strt_date.'"';
    	}
    	if(isset($end_date) && !empty($end_date)) 
    	{
    		$andWhere .= ' and DATE(end_date)="'.$end_date.'"';
    	}
    	
    	if(isset($category) && !empty($category)) 
    	{
    		$andWhere .= ' and catid='.$category.'';
    	}
    	
		$html = '';
		// $sql ="SELECT * FROM vv_logs WHERE log_title = 'compition' ORDER BY id DESC";
		// $query = $this->db->query($sql);
		// $logs = $query->result();
		// if(!empty($logs)){
			$a = 1;
			// foreach($logs as $log){
				$sql ="SELECT * FROM vv_compitition";
				//echo $sql; exit;
				$query = $this->db->query($sql);
				$value = $query->result();
				
				foreach($value as $r){
					// $startdate = date('Y-m-d',strtotime($r->created));
					// $enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					// if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					// 	$pDate = date('M, d Y',strtotime($r->created));
							$strings = strip_tags('Text texttexttexttexttexttext texttext text');
						if (strlen($strings) > 5) {

						    // truncate string
						    $stringCut = substr($strings, 0, 5);
						    $endPoint = strrpos($stringCut, ' ');

						    //if the string doesn't contain any space then it will cut without word basis.
						    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
						    $string .= '... <i class="fa fa-plus read_more" num="'.$a.'" style="color:#03b4da; font-size:10px;"></i>';
						}
						
						$html .= '<div class="row">';
						$html .= '<div class="col-md-12 headding-info">';
						$html .= '<div class="row">';
						$html .= '<div class="col-sm-3">';
						$html .= '<strong >Contest '.$a.'  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-6">';
						$html .= '<strong class="middle_contest">For the Indian Baby Contest 01  </strong>';
						$html .= '</div>';
						$html .= '<div class="col-sm-3">';
						$html .= '<button type="button" id="trigger_popup_fricc_'.$a.'" num="'.$a.'" class="trigger_popup_fricc w3-button w3-grey">Terms</button>';
						$html .= '</div>';

						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="hover_bkgr_fricc" id="hover_bkgr_fricc_'.$a.'">';
						$html .= '<span class="helper"></span>';
						$html .= '<div>';
						$html .= '<div class="popupCloseButton">X</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<form action="#" class="marginNone">';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label>Start:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->start_date)).'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_end_date.'">';
						$html .= '<label>End:</label>';
						$html .= '<span class="time">'.date('d-m-Y',strtotime($r->end_date)).'</span>';
						$html .= '</p>';						
						
						$html .= '<p style="display:'.$r->show_country.'">';
						$html .= '<label> Country :</label>';
						$html .= '<span class="time">'.$r->country.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_state.'">';
						$html .= '<label> State :</label>';
						$html .= '<span class="time">'.$r->state.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_city.'">';
						$html .= '<label> City :</label>';
						$html .= '<span class="time">'.$r->city.' </span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_strt_date.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >Yes</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_cat.'">';
						$html .= '<label> Category:</label>';
						$html .= '<span class="time" >'.$r->catid.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_gender.'">';
						$html .= '<label> Gender:</label>';
						$html .= '<span class="time" >'.$r->gender.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_rewards.'">';
						$html .= '<label> Rewards:</label>';
						$html .= '<span class="time" >'.$r->rewards.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_age.'">';
						$html .= '<label> Age:</label>';
						$html .= '<span class="time" >'.$r->age_from.' to '.$r->age_to.'</span>';
						$html .= '</p>';
						$html .= '<p style="display:'.$r->show_title.'">';
						$html .= '<label>Terms :</label>';
						$html .= '<span class="time" id="terms_'.$a.'">'.$string.'</span><input type="hidden" value="'.$strings.'" id="terms_value_'.$a.'"><a class="less_more" id="less_more_'.$a.'" num="'.$a.'" style="color:#03b4da; font-size:10px; display:none;">Close</a>';
						$html .= '</p>';
						$html .= '</form>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="col-md-12 col-sm-12 col-xs-12">';
						$html .= '<div class="row">';						
						$html .= '<div class="col-md-12">';
						$html .= '<div class="content">';
						$html .= '<img src="'.base_url("assets/front/contest_image/".$r->image."").'" alt="image">';
						$html .= '</div>';
						$html .= '<div class="box-container">';
						$html .= '<p class="participate-btn"><a href="#">Participate</a></p>';
						$html .= '<div class="thumbnail-img-box">';
						$html .= '<ul>';
						$html .= '<h4>Recent Participants <i class="fa fa-arrow-down"></i></h4>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Tom</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Rahul</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sunny</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Mac</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<li>';
						$html .= '<a href="#">';
						$html .= '<img src="'.base_url("assets/front/images/user_1.png").'" alt="Scholarship"/>';
						$html .= '<span>Sam</span>';
						$html .= '</a>';
						$html .= '</li>';
						$html .= '<div class="clr"></div>';
						$html .= '</ul>';
						$html .= '<a href="#" class="view-all">View All </a>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';
						$html .= '</div>';

						
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
					
					//} 
					$a++;
				}
		// 	}
		// }
		return $html;
	}
	public function getUserName($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	

   

}