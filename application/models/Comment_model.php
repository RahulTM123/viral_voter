<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function submitComment($userid, $refid, $reftype, $comment){
		$sql ="insert into vv_comment (userid, refid, reftype, created) values ('".$userid."', '".$refid."', '".$reftype."', now())";
		$this->db->query($sql);
		$comid = $this->db->insert_id();
		
		if($comid > 0) {
			$sql ="insert into vv_commentdata (comment_id, content) values ('".$comid."', '".$comment."')";
			$this->db->query($sql);
			
			$sql ="update vv_pollls set comments = (comments+1) where id = ".$refid;
			$this->db->query($sql);
		}
		$this->session->set_flashdata('msg', 'New comment successfully posted');
		return 1;
    }
		
}