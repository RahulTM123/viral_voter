<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getUserList($limit, $page='') {
		$html = '';
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id WHERE 1 ";
		
		$numrows = $this->db->query($sql);
		
		$sql .=" and vv_users.status < 2 order by id desc limit $start, $limit";
		
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }
	
	
	public function getUserListByFilter($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page='', $interest='', $dfrom, $dto){
		$html = '';
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users 
		left join vv_cities on vv_users.city = vv_cities.id 
		left join vv_states on vv_users.state = vv_states.id 
		left join vv_countries on vv_users.country = vv_countries.id WHERE 1 ";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($gender!='')
			$sql .=" and gender = '".$gender."'";
		if($dfrom!='')
			$sql .=" and DATE(created) >= '".$dfrom."'";
		if($dto!='')
			$sql .=" and DATE(created) <= '".$dto."'";
		if($interest!='')
			$sql .=" and  find_in_set('".$interest."', interest) <> 0";
		if($keyword!='')
			$sql .=" and (firstname like '%".$keyword."%' or middlename like '%".$keyword."%' or lastname like '%".$keyword."%' or email like '%".$keyword."%')";
		
		//echo $sql;
		$numrows = $this->db->query($sql);
		
		$sql .=" and vv_users.status < 2 order by id desc limit $start, $limit";
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }

    public function changeUserStatusDeActivate($id){
	$sql ="UPDATE vv_users SET status = 0 WHERE id=".$id;
	$query = $this->db->query($sql);
	return 1;	
    }
    public function changeUserStatusActivate($id){
	$sql ="UPDATE vv_users SET status = 1 WHERE id=".$id;
	$query = $this->db->query($sql);
	return 1;	
    }
    public function changeUserStatusSpam($id){
	$sql ="UPDATE vv_users SET status = 2 WHERE id=".$id;
	$query = $this->db->query($sql);
	return 1;	
    }    

    public function deleteUser($id){
		/*$sql ="SELECT * FROM vv_users WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$qunserdata = unserialize($value[0]->poll_ques);
		if(count($qunserdata) == '2'){
			unlink('./uploads/'.$qunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($qunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$qunserdata[0]);
			}
		}
		
		$aunserdata = unserialize($value[0]->ans1);
		if(count($aunserdata) == '2'){
			unlink('./uploads/'.$aunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($aunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$aunserdata[0]);
			}
		}
		
		$a1unserdata = unserialize($value[0]->ans2);
		if(count($a1unserdata) == '2'){
			unlink('./uploads/'.$a1unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a1unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a1unserdata[0]);
			}
		}
		
		$a2unserdata = unserialize($value[0]->ans3);
		if(count($a2unserdata) == '2'){
			unlink('./uploads/'.$a2unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a2unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a2unserdata[0]);
			}
		}
		
		$a3unserdata = unserialize($value[0]->ans4);
		if(count($a3unserdata) == '2'){
			unlink('./uploads/'.$a3unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a3unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a3unserdata[0]);
			}
		}*/
		
		$sql ="UPDATE vv_users SET status = 99 WHERE id=".$id;
		$this->db->query($sql);
		return 1;
    }

    public function updateStatus($status, $userid) {		
		$sql ="UPDATE vv_users SET status = ".$status." WHERE id=".$userid;
		$this->db->query($sql);
		return 1;
    }
	
	public function getDeletedUserList($limit, $page='') {
		$html = '';
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id WHERE (vv_users.status = 3 or vv_users.status = 77 or vv_users.status = 200) ";
		
		$numrows = $this->db->query($sql);
		
		$sql .=" order by id desc limit $start, $limit";
		
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }
	
	
	public function getDeletedUserListByFilter($limit, $country_id, $state_id, $city_id, $gender, $keyword, $page='', $interest=''){
		$html = '';
		$start = ($page)?($page-1)*$limit:0;
		$result = array();
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id WHERE (vv_users.status = 3 or vv_users.status = 77 or vv_users.status = 200) ";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($gender!='')
			$sql .=" and gender = '".$gender."'";
		if($interest!='')
			$sql .=" and  find_in_set('".$interest."', interest) <> 0";
		if($keyword!='')
			$sql .=" and (firstname like '%".$keyword."%' or middlename like '%".$keyword."%' or lastname like '%".$keyword."%' or email like '%".$keyword."%')";
		
		//echo $sql;
		$numrows = $this->db->query($sql);
		
		$sql .=" order by id desc limit $start, $limit";
		$query = $this->db->query($sql);
		$result['data'] = $query->result();
		$result['numrows'] = $numrows->num_rows();
		return $result;
    }
	
	
	public function getProfession($id){
		
		$this->db->select('*');    
    $this->db->from('vv_users');
    $this->db->join('vv_usermeta', 'vv_users.id= vv_usermeta.userid');
    $query = $this->db->get();
    
    }
	public function getVerifyApplication(){
		$result = array();
		$sql ="SELECT *, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM `vv_usermeta` inner join vv_users on vv_usermeta.userid = vv_users.id left join vv_cities on vv_users.city = vv_cities.id inner join vv_states on vv_users.state = vv_states.id inner join vv_countries on vv_users.country = vv_countries.id where vv_users.verified = 0 and vv_usermeta.verified_date > 0";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
    }
	
	
	public function getFilterVerifyApplication($country_id, $state_id, $city_id, $gender, $keyword, $interest, $dfrom, $dto){
		$result = array();
		$sql ="SELECT *, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM `vv_usermeta` inner join vv_users on vv_usermeta.userid = vv_users.id left join vv_cities on vv_users.city = vv_cities.id inner join vv_states on vv_users.state = vv_states.id inner join vv_countries on vv_users.country = vv_countries.id where vv_users.verified = 0 and vv_usermeta.verified_date > 0";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($dfrom!='')
			$sql .=" and DATE(vv_usermeta.verified_date) >= '".$dfrom."'";
		if($dto!='')
			$sql .=" and DATE(vv_usermeta.verified_date) <= '".$dto."'";
		if($gender!='')
			$sql .=" and gender = '".$gender."'";
		if($interest!='')
			$sql .=" and  find_in_set('".$interest."', interest) <> 0";
		if($keyword!='')
			$sql .=" and (firstname like '%".$keyword."%' or middlename like '%".$keyword."%' or lastname like '%".$keyword."%' or email like '%".$keyword."%')";
		
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
    }
	
	
	public function getVerifyApplicationDetails($userid){
		$result = array();
		$sql ="SELECT vv_users.*, vv_usermeta.*, vv_verify_category.name as profession, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM `vv_usermeta`
		inner join vv_users on vv_usermeta.userid = vv_users.id left join vv_cities on vv_users.city = vv_cities.id
		inner join vv_states on vv_users.state = vv_states.id inner join vv_countries on vv_users.country = vv_countries.id
		left join vv_verify_category on vv_usermeta.verify_profession = vv_verify_category.id where vv_users.id = ".$userid;
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
    }
	
	
	public function updateVerifyApplication($status, $userid, $reject){
		$result = array();
		$sql ="update vv_users set verified = ".$status." where id = ".$userid;
		$query = $this->db->query($sql);
		$sql ="update vv_usermeta set reject_message = '".$reject."' where userid = ".$userid;
		$query = $this->db->query($sql);
		$sta = ($status==1)?'Approved':'Reject';
		$sql ="insert into vv_messages (userid, messageby, textdesc, reference, notify_status, readstatus, create_date, status) values ('".$userid."', '0', '".$sta." your profile verification. ".$reject."', 'verify', '1', '0', now(), '1')";
		$query = $this->db->query($sql);
    }
	
	
	public function getVerifiedApplication(){
		$result = array();
		$sql ="SELECT vv_users.*, vv_usermeta.*, vv_verify_category.name as vprofession, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM `vv_usermeta` 
		inner join vv_users on vv_users.id = vv_usermeta.userid
			inner join vv_verify_category on vv_verify_category.id = vv_usermeta.verify_profession
		left join vv_cities on vv_users.city = vv_cities.id 
		inner join vv_states on vv_users.state = vv_states.id 
		inner join vv_countries on vv_users.country = vv_countries.id where vv_users.verified = 1";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
    }
	
	
	public function getFilterVerifiedApplication($country_id, $state_id, $city_id, $gender, $keyword, $category_id, $dfrom, $dto){
		$result = array();
		$sql ="SELECT vv_users.*, vv_usermeta.*, vv_verify_category.name as vprofession, vv_cities.name as cityname,vv_states.name as stname, vv_countries.name as ctname FROM `vv_usermeta`
		inner join vv_users on vv_usermeta.userid = vv_users.id 
					inner join vv_verify_category on vv_verify_category.id = vv_usermeta.verify_profession

		left join vv_cities on vv_users.city = vv_cities.id
		inner join vv_states on vv_users.state = vv_states.id 
		inner join vv_countries on vv_users.country = vv_countries.id where vv_users.verified = 1";
		if($country_id!='')
			$sql .=" and vv_users.country = '".$country_id."'";
		if($state_id!='')
			$sql .=" and vv_users.state = '".$state_id."'";
		if($city_id!='')
			$sql .=" and vv_users.city = '".$city_id."'";
		if($dfrom!='')
			$sql .=" and DATE(vv_usermeta.verified_date) >= '".$dfrom."'";
		if($dto!='')
			$sql .=" and DATE(vv_usermeta.verified_date) <= '".$dto."'";
		if($gender!='')
			$sql .=" and gender = '".$gender."'";
		if($category_id!='')
			$sql .=" and  find_in_set('".$category_id."', verify_profession) <> 0";
		if($keyword!='')
			$sql .=" and (firstname like '%".$keyword."%' or middlename like '%".$keyword."%' or lastname like '%".$keyword."%' or email like '%".$keyword."%')";
		
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
    }
	
	
	public function getVerifycategory(){
	    //$userId= ' ';
		$result = array();
		$sql ="SELECT * FROM `vv_verify_category` order by name";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result;
			
    }
	
	
	public function getVerifycat($catid){
		$result = array();
		$sql ="SELECT * FROM `vv_verify_category` where id = ".$catid;
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
    }
	
	
	public function updateVerifycat($catid, $catname){
		$result = array();
		$sql ="update `vv_verify_category` set name = '".$catname."' where id = ".$catid;
		$query = $this->db->query($sql);
		return $result;
    }
	
	
	public function addVerifycategory($catname){
		$result = array();
		$sql ="insert into `vv_verify_category` (name) values ('".$catname."')";
		$query = $this->db->query($sql);
    }
	
	
	public function updateVerifycategory($catid, $status){
		$result = array();
		$sql ="update `vv_verify_category` set status = '".$status."' where id = ".$catid;
		$query = $this->db->query($sql);
    }
	
	
	public function deleteVerifycategory($catid){
		$result = array();
		$sql ="delete from `vv_verify_category` where id = '".$catid."'";
		$query = $this->db->query($sql);
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getUserbyid($id){
		$sql ="SELECT * FROM vv_users where id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->row_array();
		return $value;
    }
	
	public function mailtouser($userid, $body, $subject){
		$sql ="SELECT * FROM vv_smtpsettings";
		$query = $this->db->query($sql);
		$smtpDetails = $query->result();
		$userdt = $this->user->getUserbyid($userid);
        $mail = $this->phpmailerlib->load();
			try {
				//Server settings
				$mail->SMTPDebug = 0;                                 // Enable verbose debug output
				$mail->isSMTP();                                      // Set mailer to use SMTP             // Set mailer to use SMTP
				$mail->Host = $smtpDetails[0]->host;  // Specify main and backup SMTP servers
				$mail->SMTPAuth = true;                               // Enable SMTP authentication
				$mail->Username = $smtpDetails[0]->username;                 // SMTP username
				$mail->Password = $smtpDetails[0]->password;                           // SMTP password
				$mail->SMTPSecure = $smtpDetails[0]->encrption;                            // Enable TLS encryption, `ssl` also accepted
				$mail->Port = $smtpDetails[0]->smtpPort;                                    // TCP port to con
				//Recipients
				$mail->setFrom($smtpDetails[0]->replyTo, $smtpDetails[0]->fromName);
				$mail->addAddress($userdt['email'], $userdt['firstname'].' '.$userdt['lastname']);            // Name is optional
				$mail->addReplyTo($smtpDetails[0]->replyTo);
				$mail->isHTML(true);                                  // Set email format to HTML
				$mail->Subject = $subject;
				$mail->Body = $body;
	
				$mail->send();
				$i = 1;
			} catch (Exception $e) {
				$i = 0;
			}
	}
	
}