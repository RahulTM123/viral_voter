<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FrontSurvey_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getSurvey($loguser){
		$html = '';
		$idNum = $this->session->userdata('idNum');
		$sql ="SELECT * FROM vv_survey ORDER BY id DESC";
		$query = $this->db->query($sql);
		$value = $query->result();
		foreach($value as $r){
			$startdate = date('Y-m-d',strtotime($r->created));
			$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
			if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
				$pDate = date('M, d Y',strtotime($r->created));
				$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
				$curntquery = $this->db->query($sql);
				$curntusers = $curntquery->result();
				
				$sql ="SELECT * FROM vv_surveyQustn WHERE sid = ".$r->id;
				$query = $this->db->query($sql);
				$survey = $query->result();
				
				$sql ="SELECT COUNT(id) as total FROM vv_surveyParti WHERE sid = '".$r->id."' && userid != '".$loguser."'";
				$query = $this->db->query($sql);
				$surveyParti = $query->result();
				
				$sql ="SELECT id FROM vv_surveyParti WHERE sid = '".$r->id."' && userid = '".$loguser."'";
				$query = $this->db->query($sql);
				$surveyPartiResult = $query->result();
				
				$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				
				$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> Posted a Survey</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="#" class="questn" style="font-size:20px;">Q. '.$r->question.'</a></p></div>
				
				<div class="col-md-12 surveyParticipate" style="margin-bottom:50px;"> <span>';
				
				if(!empty($surveyPartiResult) && !empty($surveyParti)){
					if($curntusers[0]->username != ''){
						$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
					}
					
					$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a>';
					if($surveyParti[0]->total == 0 ){
						$html .=  ' Participated in this Survey</span> <br /><br />';
					}elseif($surveyParti[0]->total == 1){
						$html .=  ' and '.$surveyParti[0]->total.' member Participated</span> <br /><br />';	
					}else{
						$html .=  ' and '.$surveyParti[0]->total.' members Participated</span> <br /><br />';
					}
				}else{
					if($surveyParti[0]->total < 2 ){
						$html .=  $surveyParti[0]->total.' member Participated</span> <br /><br />';
					}else{
						$html .=  $surveyParti[0]->total.' members Participated</span> <br /><br />';	
					}	
				}
				
				if(empty($surveyPartiResult)){
				   if($surveyParti[0]->total > 0){
						$html .= '<a href="'.base_url().'view-participation/'.$r->id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';   
				   }
				   $html .= '<a style="margin-left:-10px;" class="participate"><button type="button" class="trand-button">Participate</button></a>';
				}else{
					$html .= '<a href="'.base_url().'view-participation/'.$r->id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';
				}
				 
				 
				$html .= '</div><div class="col-md-12 surveyQuestn" style="margin-bottom:50px; display:none;">';
				$x = 1;
				  foreach($survey as $s){
					  $html .= '<div style="margin-top:30px;"><label>'.$s->question.'</label><div><textarea style="width:100%; background:#fff; height:80px; resize:none; border:1px solid #ddd;" class="survey_u_q survey'.$x.'" qid="'.$s->id.'"></textarea></div></div>';
					  $x++;
				  }
				  
				$html .= '<div style="margin-top:30px;"><a style="margin-left:-10px;"><button type="button" class="trand-button submitSurvey" sid="'.$r->id.'">Submit</button></a></div></div>';
				
				
				
				$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'survey'";
				$query = $this->db->query($sql);
				$getLikeId = $query->result();
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'survey'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition">  <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
				
				
				if(!empty($getLikeId)){
					$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "survey" >';
				}else{
					$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "survey">';
				}
				
				
				$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="survey"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="survey_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
				
				
				$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
				
				if(!empty($curntusers[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
				}elseif(!empty($curntusers[0]->picture_url)){
					$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
				}
				
				$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
				
				<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
				
				<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
				
				</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
				
				<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
				
				if($totalComnt[0]->total < 6){
					$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
				}else{
					$html .= '<a class="UFIPagerLink" href="'.base_url().'compitition/'.$r->id.'" role="button">View All comments</a>';
				}
				
				$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$loguser.'" name="pUserId" id="pUserId"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div></div>';
				$idNum++;
				$this->session->set_userdata('idNum',$idNum);
			}
		}
		return $html;
    }
	
	public function getSurveyBeforeLogin(){
		$html = '';
		$sql ="SELECT * FROM vv_survey ORDER BY id DESC";
		$query = $this->db->query($sql);
		$value = $query->result();
		foreach($value as $r){
			$startdate = date('Y-m-d',strtotime($r->created));
			$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
			if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_surveyQustn WHERE sid = ".$r->id;
				$query = $this->db->query($sql);
				$survey = $query->result();
				
				$sql ="SELECT COUNT(id) as total FROM vv_surveyParti WHERE sid = '".$r->id."'";
				$query = $this->db->query($sql);
				$surveyParti = $query->result();
				
				$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				
				$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> Posted a Survey</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="#" class="questn" style="font-size:20px;">Q. '.$r->question.'</a></p></div>
				
				<div class="col-md-12 surveyParticipate" style="margin-bottom:50px;"> <span>';
				
				if($surveyParti[0]->total < 2 ){
					$html .=  $surveyParti[0]->total.' member Participated</span> <br /><br />';
				}else{
					$html .=  $surveyParti[0]->total.' members Participated</span> <br /><br />';	
				}
				
				if(empty($surveyPartiResult)){
				   if($surveyParti[0]->total > 0){
						$html .= '<a href="'.base_url().'view-participation/'.$r->id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';   
				   }
				   $html .= '<a href="'.base_url().'login" style="margin-left:-10px;"><button type="button" class="trand-button">Participate</button></a>';
				}
				 
				 
				$html .= '</div><div class="col-md-12 surveyQuestn" style="margin-bottom:50px; display:none;">';
				$x = 1;
				  foreach($survey as $s){
					  $html .= '<div style="margin-top:30px;"><label>'.$s->question.'</label><div><textarea style="width:100%; background:#fff; height:80px; resize:none; border:1px solid #ddd;" class="survey_u_q survey'.$x.'" qid="'.$s->id.'"></textarea></div></div>';
					  $x++;
				  }
				  
				$html .= '<div style="margin-top:30px;"><a style="margin-left:-10px;"><button type="button" class="trand-button submitSurvey" sid="'.$r->id.'">Submit</button></a></div></div>';
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'survey'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"><span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like"><a href="'.base_url().'login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></div></article></div></div>';
			}
		}
		return $html;
    }
	
	
	
	public function getSurveyForAdmin(){
		$html = '';
		$sql ="SELECT * FROM vv_survey ORDER BY id DESC";
		$query = $this->db->query($sql);
		$value = $query->result();
		foreach($value as $r){
			$startdate = date('Y-m-d',strtotime($r->created));
			$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
			if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
				$pDate = date('M, d Y',strtotime($r->created));
				
				$sql ="SELECT * FROM vv_surveyQustn WHERE sid = ".$r->id;
				$query = $this->db->query($sql);
				$survey = $query->result();
				
				$sql ="SELECT COUNT(id) as total FROM vv_surveyParti WHERE sid = '".$r->id."'";
				$query = $this->db->query($sql);
				$surveyParti = $query->result();
				
				$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
				$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
				
				$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> Posted a Survey</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="#" class="questn" style="font-size:20px;">Q. '.$r->question.'</a></p></div>
				
				<div class="col-md-12 surveyParticipate" style="margin-bottom:50px;"> <span>';
				
				
				if($surveyParti[0]->total < 2 ){
					$html .=  $surveyParti[0]->total.' member Participated</span> <br /><br />';
				}else{
					$html .=  $surveyParti[0]->total.' members Participated</span> <br /><br />';	
				}
				
				if(empty($surveyPartiResult)){
				   if($surveyParti[0]->total > 0){
						$html .= '<a href="'.base_url().'view-participation/'.$r->id.'" style="margin-left:-10px;"><button type="button" class="trand-button">View all Participation</button></a>';   
				   }
				}
				 
				 
				$html .= '</div><div class="col-md-12 surveyQuestn" style="margin-bottom:50px; display:none;">';
				$x = 1;
				  foreach($survey as $s){
					  $html .= '<div style="margin-top:30px;"><label>'.$s->question.'</label><div><textarea style="width:100%; background:#fff; height:80px; resize:none; border:1px solid #ddd;" class="survey_u_q survey'.$x.'" qid="'.$s->id.'"></textarea></div></div>';
					  $x++;
				  }
				  
				$html .= '<div style="margin-top:30px;"><a style="margin-left:-10px;"><button type="button" class="trand-button submitSurvey" sid="'.$r->id.'">Submit</button></a></div></div>';
				
				$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'survey'";
				$tquery = $this->db->query($sql);
				$totalLike = $tquery->result();
				
				$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalCommnt = $comntquery->result();
				
				$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'survey'";
				$comntquery = $this->db->query($sql);
				$totalComnt = $comntquery->result();
				
				if(!empty($totalCommnt)){
					$comment = $totalCommnt[0]->totalComment;
				}else{
					$comment = 0;
				}
				
				$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"><span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="#" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like"><a href="#" class="_bl"><i class="fa fa-comment"></i>Comment</a></span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="#" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></div></article></div></div></div>';
			}
		}
		return $html;
    }
	
}