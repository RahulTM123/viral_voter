<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserPoll_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getUserPollList(){
		$html = '';
		$sql ="SELECT poll.*,user.firstname,user.middlename,user.lastname FROM vv_pollls as poll LEFT JOIN vv_users as user ON poll.user_id = user.id";
		$query = $this->db->query($sql);
		$val = $query->result();	
		$count = count($val);
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($count/$recordsPerPage);
		$lpm1 = $lastpage - 1; 
		
		$sql ="SELECT poll.*,user.firstname,user.middlename,user.lastname,user.username FROM vv_pollls as poll LEFT JOIN vv_users as user ON poll.user_id = user.id ORDER BY id DESC LIMIT ".$start.",".$recordsPerPage;
		$query = $this->db->query($sql);
		$value = $query->result();
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Created By</th><th>Question</th><th>Start Date</th><th>Status</th><th>Action</th></tr>';
		if(!empty($value)){
			
			$sql ="SELECT COUNT(id) as total FROM vv_pollls";
			$query = $this->db->query($sql);
			$pollcounter = $query->result();
			
			
			$counter = 1;
			foreach($value as $v){
				$html .= '<tr><td>'.$counter.'</td>';
				if($v->username != ''){
					$html .= '<td><a href="'.base_url().'user/'.$v->username.'">'.$v->firstname.' '.$v->lastname.'</a></td>';	
				}else{
					$html .= '<td><a href="'.base_url().'user/id/'.$v->id.'">'.$v->firstname.' '.$v->lastname.'</a></td>';	
				}	
				$html .= '<td><a href="'.base_url().'admin/poll_result/'.$v->id.'">';
				if($v->question != ''){
					$html .= $v->question;	
				}elseif($v->imag != ''){
					$html .= '<img src="'.base_url().'uploads/'.$v->imag.'" style="width:60px" class="img-fluid"/>';	
				}else{
					$videoImg = str_replace("https://www.youtube.com/embed/","",$v->videourl);
					$html .= '<img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" width="60" class="img-fluid"/>';	
				}
				$html .= '</a></td><td>'.date('M, d Y',strtotime($v->created)).'</td>';
				
				if($v->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activated</p></a><a href="'.base_url().'admin/user_poll_list/change_status?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}
				if($v->status == 0){
					$html .= '<td><a href="'.base_url().'admin/user_poll_list/change_status?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivated</p></a></td>';
				}
				
				$html .= '<td><a href="'.base_url().'admin/user_poll_list/delete_poll?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				
				$counter++;	
			}
			
		}else{
			$html .= '<tr><td colspan="6" style="border-bottom: 1px solid #ddd;">No Result Found!</td><tr>';	
		}
		$html .= '</tbody></table>';
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=\"?page=".($prev)."\">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
					$html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
				   $html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
			   }else{
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=\"?page=".($next)."\">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
    }
	
	
	public function getUserPollListByFilter($catFil,$voteFil,$search){
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$linkArray = explode('&page',$actual_link);
		$actual_link = $linkArray[0];
		$html = '';
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		
		if($catFil != '' && $voteFil != '' && $search != ''){
			$sql ="SELECT * FROM  `vv_pollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != '' && $search != ''){
			$sql ="SELECT * FROM  `vv_pollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."'";
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') AND p.catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();	
		}elseif($voteFil != '' && $search != ''){
			$sql ="SELECT * FROM  `vv_pollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != '' && $voteFil != ''){
			$sql ="SELECT * FROM  `vv_pollvoting`";
			$query = $this->db->query($sql);
			$voteList = $query->result();
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE p.catid = '".$catFil."' GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != ''){
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE catid = '".$catFil."'";
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();
		}elseif($search != ''){
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%')";
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_users` as user ON p.user_id = user.id WHERE (user.firstname LIKE '%".$search."%' OR user.middlename LIKE '%".$search."%' OR user.lastname LIKE '%".$search."%' OR p.question LIKE '%".$search."%') LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();
		}else{
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(p.id) as total, p.*,user.firstname,user.middlename,user.lastname,user.username FROM `vv_pollls` as p LEFT JOIN `vv_pollvoting` as pv ON p.id = pv.pollid GROUP BY pv.pollid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
		}
		
		
		
		
		$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Created By</th><th>Question</th><th>Start Date</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			
			$sql ="SELECT COUNT(id) as total FROM vv_pollls";
			$query = $this->db->query($sql);
			$pollcounter = $query->result();
			
			
			$counter = 1;
			foreach($result as $v){
				$html .= '<tr><td>'.$counter.'</td>';
				if($v->username != ''){
					$html .= '<td><a href="'.base_url().'user/'.$v->username.'">'.$v->firstname.' '.$v->lastname.'</a></td>';	
				}else{
					$html .= '<td><a href="'.base_url().'user/id/'.$v->id.'">'.$v->firstname.' '.$v->lastname.'</a></td>';	
				}	
				$html .= '<td><a href="'.base_url().'admin/poll_result/'.$v->id.'">';
				if($v->question != ''){
					$html .= $v->question;	
				}elseif($v->imag != ''){
					$html .= '<img src="'.base_url().'uploads/'.$v->imag.'" style="width:60px" class="img-fluid"/>';	
				}else{
					$videoImg = str_replace("https://www.youtube.com/embed/","",$v->videourl);
					$html .= '<img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" width="60" class="img-fluid"/>';	
				}
				$html .= '</a></td><td>'.date('M, d Y',strtotime($v->created)).'</td>';
				
				if($v->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activated</p></a><a href="'.base_url().'admin/user_poll_list/change_status?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}
				if($v->status == 0){
					$html .= '<td><a href="'.base_url().'admin/user_poll_list/change_status?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivated</p></a></td>';
				}
				
				$html .= '<td><a href="'.base_url().'admin/user_poll_list/delete_poll?id='.$v->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				
				$counter++;	
			}
			
		}else{
			$html .= '<tr><td colspan="6" style="border-bottom: 1px solid #ddd;">No Result Found!</td><tr>';	
		}
		$html .= '</tbody></table>';
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=".$actual_link."&page=".($prev).">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
					$html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
				   $html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
			   }else{
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=".$actual_link."&page=".($next).">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
    }
	
	
	
	public function getUserPollResultDetail($optid,$qid){
		$html = '';
		$sql ="SELECT userid FROM vv_pollvoting WHERE pollid = '".$qid."' && vote = '".$optid."' ORDER BY id DESC";
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Name</th><th style="width:200px;">Action</th></tr>';
			$counter = 1;
			foreach($value as $v){
				$sql ="SELECT * FROM vv_users WHERE id = ".$v->userid;
				$query = $this->db->query($sql);
				$users = $query->result();
				$html .= '<tr><td>'.$counter.'</td><td><div class="media"><div class="media-left">';
				
				if(!empty($users[0]->profile_pic_url)){
					$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:80px">';
				}elseif(!empty($users[0]->picture_url)){
					$html .= '<img src="'.$users[0]->picture_url.'" style="width:80px">';
				}else{
					$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:80px">';
				}
				
				$html .= '</div><div class="media-body"><h6 class="media-heading">'.$users[0]->firstname.' '.$users[0]->lastname.'</h6></div></div><td>';
				if($users[0]->username != ''){
					$html .= '<a href="'.base_url().''.$users[0]->username.'" style="color:#fff;text-decoration:none;">';	
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" style="color:#fff;text-decoration:none;">';	
				}
				$html .= '<p class="trand-button" style="padding:8px; text-align:center;">View Timeline</p></a></td></tr>';
				
				$counter++;	
			}
			$html .= '</tbody></table>';
		}else{
			$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><td>No Result Found!</td><tr></tbody></table>';	
		}
		return $html;
    }
	
	
	
	public function getUserPollResult($id){
		$html = '';
		$sql ="SELECT * FROM vv_pollls WHERE id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if(!empty($value)){
			$sql ="SELECT total_vote FROM vv_pollresult WHERE logid = ".$id;
			$query = $this->db->query($sql);
			$totalVote = $query->result();
			if(!empty($totalVote)){
				$totalvote = $totalVote[0]->total_vote;
			}else{
				$totalvote = 0;
			}
			$html .= '<div class="col-md-12 detailTable">';
			if($value[0]->question != ''){
				$html .= '<div class="col-md-12"><div class="col-md-9"><h2 class="userPollQH2">Q.'.$value[0]->question.'</h2></div><div class="col-md-3"><p class="trand-button userTotalVote">Total Vote: '.$totalvote.'</p></div></div>';
				if($value[0]->imag != ''){
					$html .= '<div class="col-md-12"><img src="'.base_url().'uploads/'.$value[0]->imag.'" class="img-fluid userPollImageQ"/></div>';	
				}
				if($value[0]->videourl != ''){
					$videoImg = str_replace("https://www.youtube.com/embed/","",$value[0]->videourl);
					$html .= '<div><img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" class="img-fluid userPollImageQ"/></div>';
				}	
			}elseif($value[0]->imag != ''){
				$html .= '<img src="'.base_url().'uploads/'.$value[0]->imag.'" class="img-fluid userPollImageQ"/>';	
			}else{
				$videoImg = str_replace("https://www.youtube.com/embed/","",$value[0]->videourl);
				$html .= '<img src="http://img.youtube.com/vi/'.$videoImg.'/hqdefault.jpg" class="img-fluid userPollImageQ"/>';	
			}
			$html .= '</div><div class="col-md-12 userPollBeforeOpt">';
			$sql ="SELECT * FROM vv_polloption WHERE pollid = ".$id;
			$query = $this->db->query($sql);
			$polloption = $query->result();
			foreach($polloption as $option){
				$unserze = unserialize($option->title);
				$html .= '<div class="col-md-6"><div class="col-md-12 userPollOptOpt"><p class="userPollOptOptP">Option</p><div class="col-md-12">';
				if(count($unserze) == 2){
					$html .= '<div class="col-md-6"><img src="'.base_url().'uploads/'.$unserze[1].'" class="img-fluid userPollOptImg"></div><div class="col-md-6">'.$unserze[0].'</div>';	
				}else{
					$supported_image = array('gif','jpg','jpeg','png');
					$ext = strtolower(pathinfo($unserze[0], PATHINFO_EXTENSION));
					if (in_array($ext, $supported_image)) {
						$html .= '<div class="col-md-12 userPollOptInside"><img src="'.base_url().'uploads/'.$unserze[0].'"  class="img-fluid userPollOptImg"></div>';	
					} else {
						$html .= '<div class="col-md-12 userPollOptCon">'.$unserze[0].'</div>';
					}
				}
				$sql ="SELECT count(id) as total FROM vv_pollvoting WHERE pollid = '".$id."' && vote = '".$option->option_id."'";
				$query = $this->db->query($sql);
				$totalPollVote = $query->result();
				$html .= '</div><div class="col-md-12 userPollOptVote">Vote: '.$totalPollVote[0]->total.'</div><div class="userPollOverlay"><a href="'.base_url().'admin/poll_result_detail?optid='.$option->option_id.'&qid='.$id.'" class="userPollOptAnchor">View Details</a></div></div></div>';
			}
			$html .= '</div>';
		}else{
			$html .= '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><td>No Result Found!</td><tr></tbody></table>';	
		}
		return $html;
    }
	
	
	
	public function changeUserPollStatus($id){
		$sql ="SELECT status FROM vv_pollls WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_pollls SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_pollls SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}	
    }
	
	public function deleteUserPoll($id){
		$sql ="DELETE FROM vv_pollls WHERE id=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_polloption WHERE pollid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_pollresult WHERE logid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_pollvoting WHERE pollid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_logs WHERE log_id=".$id." && log_title = 'user_poll'";
		$query = $this->db->query($sql);
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
    public function getUserRecord(){
		$sql ="SELECT vv_users.firstname, vv_users.lastname, vv_users.username, vv_users.email, vv_users.gender, vv_pollls.*, vv_categories.cat_name FROM  vv_pollls INNER JOIN vv_users ON vv_pollls.user_id=vv_users.id inner join vv_categories on vv_pollls.catid = vv_categories.id WHERE vv_pollls.status=1";
		$query = $this->db->query($sql);
		$value = $query->result_array();
		return $value;
    }
	 public function getUserName($id){
	 	//var_dump($id);die();
		$sql ="SELECT * FROM vv_users WHERE id=$id";
		$query = $this->db->query($sql);
		return $value = $query->row();
		//return ($value->firstname." ".$value->lastname);
    }
     public function getUserCategories($id){
	 	//var_dump($id);die();
		$sql ="SELECT cat_name FROM vv_categories WHERE id=$id";
		$query = $this->db->query($sql);
		return $value = $query->row();
		//return ($value->firstname." ".$value->lastname);
    }
    public function getFilterRecord($data){
		//print_r($data);

		 
		  /*if(!empty($data['cat']) &&  (empty($data['dfrom']) && empty($data['dto']) && empty($data['gender']) && empty($data['cid']) && empty($data['sid']) && empty($data['ctid']) && empty($data['keyword'] )))
		  {
		  	 $sql ='SELECT * FROM  vv_pollls INNER JOIN vv_users ON vv_pollls.user_id=vv_users.id WHERE vv_pollls.catid='.$data["cat"].' ';
		  }
		  else if(!empty($data['cid']) &&  (empty($data['dfrom']) && empty($data['dto']) && empty($data['gender']) && empty($data['cat']) && empty($data['sid']) && empty($data['ctid']) && empty($data['keyword'] )))
		  {
		  	 $sql ='SELECT * FROM  vv_pollls INNER JOIN vv_users ON vv_pollls.user_id=vv_users.id WHERE vv_users.country="'.$data["cid"].'" ';
		  }
		  else if(!empty($data['dfrom']) &&  (empty($data['cat']) && empty($data['dto']) && empty($data['gender']) && empty($data['cat']) && empty($data['sid']) && empty($data['ctid']) && empty($data['keyword'] )))
		  {
		  	$val=$data['dfrom'];
		  	 $sql ='SELECT * FROM  vv_pollls INNER JOIN vv_users ON vv_pollls.user_id=vv_users.id WHERE vv_polls.start_date<="'.$val.'" ';
		  }*/
		 
		  	 /*$sql ='SELECT * FROM  vv_pollls INNER JOIN vv_users ON vv_pollls.user_id=vv_users.id WHERE vv_pollls.catid='.$data["cat"].'
		  OR (vv_pollls.start_date<="'.$data["dfrom"].'"
		  AND vv_pollls.end_date>="'.$data["dto"].'")
		  OR  vv_users.gender="'.$data["gender"].'"
		  OR  vv_users.country="'.$data["cid"].'"
		  OR  vv_users.state="'.$data["sid"].'"
		  OR  vv_users.city="'.$data["ctid"].'"
		  OR vv_pollls.question like "%'.$data["keyword"].'%"';*/
		  $sql ="select * from  vv_users inner join vv_pollls on vv_pollls.user_id=vv_users.id where vv_pollls.status=1 ";
		  
		  if($data['cid']!='' || !empty($data['cid']))
			$sql .=" and vv_users.country = '".$data['cid']."'";
		if($data['sid']!='' || !empty($data['sid']))
			$sql .=" and vv_users.state = '".$data['sid']."'";
		if($data['ctid']!='' || !empty($data['ctid']))
			$sql .=" and vv_users.city = '".$data['ctid']."'";
		if($data['gender']!='' || !empty($data['gender']))
			$sql .=" and vv_users.gender = '".$data['gender']."'";
		if($data['cat']!='' || !empty($data['cat']))
			$sql .=" and vv_pollls.catid = '".$data['cat']."'";
		if($data['keyword']!='' || !empty($data['keyword']))
			$sql .=" and vv_pollls.question like '%".$data['keyword']."%'";
		if($data['dfrom']!='' || !empty($data['dfrom']))
			$sql .=" and vv_pollls.start_date >='".$data['dfrom']."'";
		if($data['dto']!='' || !empty($data['dto']))
			$sql .=" and vv_pollls.end_date <='".$data['dto']."'";
		
		
		//die('hello');
		
		
		
		if($data['voteFilter'] !='' || $data['participation'] !='')
		{
			$sql .=" order by";
		if($data['voteFilter'] !='')                      
		{
			if($data['voteFilter'] == 'high')
				$sql .=" upvote desc ";
			else
				$sql .=" upvote asc ";
		}
		if($data['participation'] !='') 
		{
			if($data['participation'] == 'high')
				$sql .=" participation desc ";
			else
				$sql .=" participation asc ";
		}
	}
		//echo $sql;die();
		$query = $this->db->query($sql);
		//var_dump($value = $query->result());die
		return $value = $query->result();
		/*//$result['numrows'] = $numrows->num_rows();
		return $result;
		$query = $this->db->query($sql);
		//var_dump($query);die();
		 return $value = $query->result();
		//print_r($value);die();*/
    }
    public function deleteUserPoll1($id){
    	/*echo "model";
    	var_dump($id);die();*/
		$sql ="UPDATE vv_pollls SET status=0 WHERE id=".$id;
		$query = $this->db->query($sql);
		
		/*$sql ="DELETE FROM vv_polloption WHERE pollid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_pollresult WHERE logid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_pollvoting WHERE pollid=".$id;
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_logs WHERE log_id=".$id." && log_title = 'user_poll'";
		$query = $this->db->query($sql);*/
    }
    public function getSearchRecord($data){
		//var_dump($data);die();
		if(!empty($data['cat']))
		{
		$sql ='SELECT cat_name FROM vv_categories WHERE id='.$data['cat'];
		$query = $this->db->query($sql);
		$value = $query->row();
		$data['cat']=$value->cat_name;
		}
		if(!empty($data['cid']))
		{
		$sql ='SELECT name FROM vv_countries WHERE id='.$data['cid'];
		$query = $this->db->query($sql);
		$value = $query->row();
		$data['cid']=$value->name;
		}
		if(!empty($data['sid']))
		{
		$sql ='SELECT name FROM vv_states WHERE id='.$data['sid'];
		$query = $this->db->query($sql);
		$value = $query->row();
		$data['sid']=$value->name;
		}
		if(!empty($data['ctid']))
		{
		$sql ='SELECT name FROM vv_cities WHERE id='.$data['ctid'];
		$query = $this->db->query($sql);
		$value = $query->row();
		$data['ctid']=$value->name;
		}
		
		return $data;
	}

    /*public function deleteUser($id)
   {
    		$this->db->where('id', $id);
			$this->db->delete('vv_pollls');
			//redirect("admin/user_poll");
		}*/
	
}