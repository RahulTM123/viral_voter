<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Survey_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function addNewSurvey($title,$qustn,$catId,$img,$country_id,$state_id,$city_id){
		$input = array('catid' => $catId, 'question' => $title, 'image' => $country_id, 'country' => $country_id, 'state' => $state_id, 'city' => $city_id);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_survey', $input);
		$insertId = $this->db->insert_id();
		
		if(!empty($insertId)){
			for($i = 0; $i < count($qustn); $i++){
				if(!empty($qustn[$i])){
					$input = array('sid' => $insertId, 'question' => $qustn[$i]);
					$this->db->set('created', 'NOW()', FALSE);
					$this->db->set('modified', 'NOW()', FALSE);
					$this->db->insert('vv_surveyQustn', $input);
				}
			}
			
			$data = array('catid' => $catId, 'log_id' => $insertId, 'log_title' => 'survey', 'ip' => $_SERVER['SERVER_ADDR']);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->set('modified', 'NOW()', FALSE);
			$this->db->insert('vv_logs', $data);
		}
		return  $insertId;
    }
	
	public function getSurveyList(){
		$html = '';
		$sql ="SELECT * FROM vv_survey";
		$query = $this->db->query($sql);
		$val = $query->result();	
		$count = count($val);
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		$lastpage = ceil($count/$recordsPerPage);
		$lpm1 = $lastpage - 1; 
		
		$sql ="SELECT vv_survey.*, vv_categories.cat_name FROM `vv_survey` LEFT JOIN `vv_categories` ON vv_survey.catid = vv_categories.id LIMIT ".$start.",".$recordsPerPage;
		$query = $this->db->query($sql);
		$result = $query->result();
		
		$html = '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Image</th><th>Category</th><th>Start Date</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td style="width:75px;">'.$i.'</td>';
				
				$html .= '<td><a href="'.base_url().'admin/survey_details/'.$value->id.'">'.$value->question.'</a></td>';
				
				if($value->image != ''){
					$html .= '<td><img src="'.base_url().'uploads/'.$value->image.'" style="width:100px;height:60px;"></td>';
				}else{
					$html .= '<td></td>';
				}
				
				$html .= '<td>'.$value->cat_name.'</td><td>'.date('M, d Y',strtotime($value->created)).'</td>';
				
				if($value->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activate</p></a><a href="survey_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}
				if($value->status == 0){
					$html .= '<td><a href="survey_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivate</p></a></td>';
				}
				
				$html .= '<td><a href="'.base_url().'admin/survey_details/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View</p></a> <a href="'.base_url().'admin/edit_survey/'.$value->id.'" style="color:#fff;text-decoration:none;margin-left:10px;"><p class="buttonGreen btn-success">Edit</p></a> <a href="survey_list/delete_survey?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
		}else{
			$html .= '<tr><td colspan="7">No result found!</td></tr>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=\"?page=".($prev)."\">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
					$html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=\"?page=".($lpm1)."\">$lpm1</a>";
				   $html.= "<a href=\"?page=".($lastpage)."\">$lastpage</a>";   
			   }else{
				   $html.= "<a href=\"?page=\"1\"\">1</a>";
				   $html.= "<a href=\"?page=\"2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=\"?page=".($counter)."\">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=\"?page=".($next)."\">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
		
    }
	
	public function getSurveyListByFilter($catFil,$voteFil){
		$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$linkArray = explode('&page',$actual_link);
		$actual_link = $linkArray[0];
		$html = '';
		$page = (int) (!isset($_REQUEST['page']) ? 1 :$_REQUEST['page']);
		$recordsPerPage = 10;
		$start = ($page-1) * $recordsPerPage;
		$adjacents = "2";
		 
		$prev = $page - 1;
		$next = $page + 1;
		
		$sql ="SELECT * FROM vv_survey";
		$query = $this->db->query($sql);
		$voteList = $query->result();
		
		if($catFil != '' && $voteFil != ''){
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE c.catid = '".$catFil."' GROUP BY cmt.sid ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE c.catid = '".$catFil."' GROUP BY cmt.sid ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();	
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE c.catid = '".$catFil."' GROUP BY cmt.sid ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE c.catid = '".$catFil."' GROUP BY cmt.sid ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}	
			}
			
		}elseif($catFil != ''){
			$sql ="SELECT c.* FROM `vv_survey` as c LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE catid = '".$catFil."'";
			$query = $this->db->query($sql);
			$val = $query->result();	
			$count = count($val);
			$lastpage = ceil($count/$recordsPerPage);
			$lpm1 = $lastpage - 1; 
			
			$sql ="SELECT * FROM `vv_survey` as c LEFT JOIN `vv_categories` as cat ON c.catid = cat.id WHERE catid = '".$catFil."' LIMIT ".$start.",".$recordsPerPage;
			$query = $this->db->query($sql);
			$result = $query->result();
		}else{
			if($voteFil == 'high'){
				if(!empty($voteList)){
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id ORDER BY total DESC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id ORDER BY total DESC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}else{
				if(!empty($voteList)){
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id ORDER BY total ASC";
					$query = $this->db->query($sql);
					$val = $query->result();	
					$count = count($val);
					$lastpage = ceil($count/$recordsPerPage);
					$lpm1 = $lastpage - 1; 
					
					$sql ="SELECT COUNT(c.id) as total, c.*, cat.cat_name FROM `vv_survey` as c LEFT JOIN `vv_surveyParti` as cmt ON c.id = cmt.sid LEFT JOIN `vv_categories` as cat ON c.catid = cat.id ORDER BY total ASC LIMIT ".$start.",".$recordsPerPage;
					$query = $this->db->query($sql);
					$result = $query->result();
				}else{
					$lastpage = '';
					$result = '';	
				}
			}
		}
		$html = '<table width="100%" class="table table-striped detailTable" style="border:1px solid #ddd !important;"><tbody><tr><th>Sr No.</th><th>Question</th><th>Image</th><th>Category</th><th>Start Date</th><th>Status</th><th>Action</th></tr>';
		if(!empty($result)){
			$i = 1;
			foreach($result as $value){
				$html .= '<tr><td style="width:75px;">'.$i.'</td>';
				
				$html .= '<td><a href="'.base_url().'admin/survey_details/'.$value->id.'">'.$value->question.'</a></td>';
				
				if($value->image != ''){
					$html .= '<td><img src="'.base_url().'uploads/'.$value->image.'" style="width:100px;height:60px;"></td>';
				}else{
					$html .= '<td></td>';
				}
				
				$html .= '<td>'.$value->cat_name.'</td><td>'.date('M, d Y',strtotime($value->created)).'</td>';
				
				if($value->status == 1){
					$html .= '<td><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonGreen buttonDis">Activate</p></a><a href="survey_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Deactivate</p></a></td>';
				}
				if($value->status == 0){
					$html .= '<td><a href="survey_list/change_status?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">Activate</p></a><a href="#" class="buttonDis" style="color:#fff;text-decoration:none;"><p class="buttonRed buttonDis">Deactivate</p></a></td>';
				}
				
				$html .= '<td><a href="'.base_url().'admin/survey_details/'.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View</p></a> <a href="'.base_url().'admin/edit_survey/'.$value->id.'" style="color:#fff;text-decoration:none;margin-left:10px;"><p class="buttonGreen btn-success">Edit</p></a> <a href="survey_list/delete_survey?id='.$value->id.'" style="color:#fff;text-decoration:none;"><p class="buttonRed">Delete</p></a></td></tr>';
				$i++;
			}
		}else{
			$html .= '<tr><td colspan="7">No result found!</td></tr>';
		}
		$html .= '</tbody></table>';
		
		if($lastpage > 1){   
			$html .= "<div class='pagination'>";
			if ($page > 1)
				$html.= "<a href=".$actual_link."&page=".($prev).">&laquo; Previous&nbsp;&nbsp;</a>";
			else
				$html.= "<span class='disabled'>&laquo; Previous&nbsp;&nbsp;</span>";   
			if ($lastpage < 7 + ($adjacents * 2)){   
				for ($counter = 1; $counter <= $lastpage; $counter++){
					if ($counter == $page)
						$html.= "<span class='current'>$counter</span>";
					else
						$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				}
			}elseif($lastpage > 5 + ($adjacents * 2)){
				if($page < 1 + ($adjacents * 2)){
					for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
						if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
						else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
					}
					$html.= "...";
					$html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
					$html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
	
			   }elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)){
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "...";
				   for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++){
					   if($counter == $page)
						   $html.= "<span class='current'>$counter</span>";
					   else
						   $html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
				   $html.= "..";
				   $html.= "<a href=".$actual_link."&page=".($lpm1).">$lpm1</a>";
				   $html.= "<a href=".$actual_link."&page=".($lastpage).">$lastpage</a>";   
			   }else{
				   $html.= "<a href=".$actual_link."&page=1\"\">1</a>";
				   $html.= "<a href=".$actual_link."&page=2\"\">2</a>";
				   $html.= "..";
				   for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++){
					   if($counter == $page)
							$html.= "<span class='current'>$counter</span>";
					   else
							$html.= "<a href=".$actual_link."&page=".($counter).">$counter</a>";     
				   }
			   }
			}
			if($page < $counter - 1)
				$html.= "<a href=".$actual_link."&page=".($next).">Next &raquo;</a>";
			else
				$html.= "<span class='disabled'>Next &raquo;</span>";
	
			$html.= "</div>";       
		}
		return $html;
    }
	
	
	public function getSurveyDetails($id){
		$html = '';
		$sql ="SELECT s.*,COUNT(p.sid) as total FROM `vv_survey` as s LEFT JOIN `vv_surveyParti` as p ON s.id = p.sid WHERE s.id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px; font-size:30px;"><div class="col-md-7">Q. '.$value[0]->question.'</div><div class="col-md-3" style="text-align:right;"><a href="'.base_url().'admin/user_participation/'.$value[0]->id.'" style="text-decoration:none;"><span class="trand-button" style="padding:10px 20px;">';
		
		if($value[0]->total > 1){
			$html .= $value[0]->total." User's Participated</span>";	
		}else{
			$html .= $value[0]->total.' User Participated</span>';
		}
		
		$html .= '</a></div><div class="col-md-2" style="text-align:right;"><a href="'.base_url().'admin/survey_list"><button type="button" class="trand-button">&lt;&lt; Back</button></a></div></div>';
		
		$sql ="SELECT q.* FROM `vv_surveyQustn` as q WHERE q.sid = '".$id."' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$getQuestion = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px;"><table class="table table-striped detailTable" width="100" cellpadding="1" cellspacing="1" border="0" style="border:1px solid #ddd;"><tbody><tr><th>Question</th><th>Total Vote</th><th>Action</th></tr>';
		
		if(!empty($getQuestion)){
			foreach($getQuestion as $question){
				$sql ="SELECT COUNT(qid) as total FROM `vv_surveyanswer` WHERE qid = '".$question->id."'";
				$query = $this->db->query($sql);
				$totalVote = $query->result();
				$html .= '<tr><td style="width:60%">'.$question->question.'</td><td>'.$totalVote[0]->total.'</td><td><a href="'.base_url().'admin/survey_question_detail/'.$question->id.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View</p></a></td></tr>';	
			}
		}else{
			$html .= '<tr><td colspan="3">No Record Found!</td></tr>';
		}	
		$html .= '</tbody></table></div>';
		return $html;
    }
	
	public function getSurveyQuesDetails($id){
		$html = '';
		$sql ="SELECT a.*,u.id as uid,u.firstname,u.username FROM `vv_surveyanswer` as a LEFT JOIN `vv_users` as u ON a.userid = u.id WHERE a.qid = '".$id."'";
		$query = $this->db->query($sql);
		$getQuestion = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px;"><table class="table table-striped detailTable" width="100" cellpadding="1" cellspacing="1" border="0" style="border:1px solid #ddd;"><tbody><tr><th>#</th><th>Answer</th><th>User Name</th><th>Participated Date</th><th>Action</th></tr>';
		
		if(!empty($getQuestion)){
			$count = 1;
			foreach($getQuestion as $question){
				$html .= '<tr><td>'.$count.'</td><td style="width:50%">'.$question->value.'</td><td>'.$question->firstname.'</a></td><td>'.date('M, d Y',strtotime($question->value)).'</td><td>';
				
				if($question->username != ''){
					$html .= '<a href="'.base_url().'user/'.$question->username.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View User Profile</p></a>';
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$question->uid.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View User Profile</p></a>';
				}	
				$html .= '</td></tr>';	
				$count++;
			}
		}else{
			$html .= '<tr><td colspan="5">No Record Found!</td></tr>';
		}	
		$html .= '</tbody></table></div>';
		return $html;
    }
	
	
	public function userParticipation($id){
		$html = '';
		$sql ="SELECT a.*,u.id as uid,u.firstname,u.lastname,u.username FROM `vv_surveyanswer` as a LEFT JOIN `vv_users` as u ON a.userid = u.id WHERE a.sid = '".$id."'";
		$query = $this->db->query($sql);
		$getQuestion = $query->result();
		
		$html .= '<div class="col-md-12" style="padding-top:20px;"><table class="table table-striped detailTable" width="100" cellpadding="1" cellspacing="1" border="0" style="border:1px solid #ddd;"><tbody><tr><th>#</th><th>User Name</th><th>Action</th></tr>';
		
		if(!empty($getQuestion)){
			$count = 1;
			foreach($getQuestion as $question){
				$html .= '<tr><td>'.$count.'</td><td style="width:70%;">'.$question->firstname.' '.$question->lastname.'</a></td><td>';
				
				if($question->username != ''){
					$html .= '<a href="'.base_url().'user/'.$question->username.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View User Profile</p></a>';
				}else{
					$html .= '<a href="'.base_url().'user/id/'.$question->uid.'" style="color:#fff;text-decoration:none;"><p class="buttonGreen">View User Profile</p></a>';
				}	
				$html .= '</td></tr>';	
				$count++;
			}
		}else{
			$html .= '<tr><td colspan="3">No Record Found!</td></tr>';
		}	
		$html .= '</tbody></table></div>';
		return $html;
    }
	
	public function changeSurveyStatus($id){
		$sql ="SELECT status FROM vv_survey WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_survey SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_survey SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function deleteSurvey($id){
		$sql ="DELETE FROM vv_survey WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_surveyQustn WHERE sid = '".$id."'";
		$query = $this->db->query($sql);
		
		$sql ="DELETE FROM vv_surveyanswer WHERE sid = '".$id."'";
		$query = $this->db->query($sql);
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getSurveyData($id){
		$html = '';
		$sql ="SELECT * FROM vv_survey WHERE id = ".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$html .= '<div class="col-lg-12"><div class="col-lg-12"><div class="form-group"><input class="form-control" value="'.$value[0]->question.'" placeholder="Title Question" name="qTitle" id="qTitle"></div></div></div>';
		
		$sql ="SELECT * FROM vv_surveyQustn WHERE sid = '".$id."' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$survey = $query->result();
		
		if(!empty($survey)){
			$html .= '<div class="col-lg-12">';
			if(count($survey) == 4){
				foreach($survey as $s){
					$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" value="'.$s->question.'"  placeholder="Question" name="questn[]"><input type="hidden" value="'.$s->id.'" name="qId[]"></div></div>';
				}
			}elseif(count($survey) == 3){
				foreach($survey as $s){
					$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" value="'.$s->question.'"  placeholder="Question" name="questn[]"><input type="hidden" value="'.$s->id.'" name="qId[]"></div></div>';
				}
				$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div>';
			}elseif(count($survey) == 2){
				foreach($survey as $s){
					$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" value="'.$s->question.'"  placeholder="Question" name="questn[]"><input type="hidden" value="'.$s->id.'" name="qId[]"></div></div>';
				}
				$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div>';
			}else{
				foreach($survey as $s){
					$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" value="'.$s->question.'"  placeholder="Question" name="questn[]"><input type="hidden" value="'.$s->id.'" name="qId[]"></div></div>';
				}
				$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div>';
			}
			
		}else{
			$html .= '<div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div><div class="col-lg-6"><div class="form-group"><input class="form-control" placeholder="Question" name="questn[]"><input type="hidden" value="insert" name="qId[]"></div></div>';	
		}
		$html .= '</div><div class="col-lg-12">&nbsp;</div><div class="col-lg-12"><div class="col-lg-6"><div class="form-group"><select name="catName" class="form-control" required>';
		
		$sql ="SELECT * FROM vv_categories";
		$query = $this->db->query($sql);
		$category = $query->result();
		
		if(!empty($category)){
			foreach($category as $v){
				if($value[0]->catid == $v->id){
					$html .= '<option value="'.$v->id.'" selected="selected">'.$v->cat_name.'</option>';
				}else{
					$html .= '<option value="'.$v->id.'">'.$v->cat_name.'</option>';
				}
			}
		}
		
		$html .= '</select></div></div><div class="col-lg-6">';
		
		if($value[0]->image != ''){
			$html .= '<div class="form-group img_section" id="img_section" style="display:none;">';
		}else{
			$html .= '<div class="form-group img_section" id="img_section">';
		}
		
		$html .= '<div class="ans1img c_img" id="c_img" style="position: absolute;"><div class="upload-btn-wrapper"><button class="btnn">Upload Image</button><input id="survey" type="file" /></div></div></div></div></div><div class="col-lg-12"><div class="col-lg-6"><div class="form-group surImage">';
		
		if($value[0]->image != ''){
			$html .= '<div class="option-area text-area overlay_container" id="img_sec" style="width: 70px;"><input type="hidden" name="surveyImg" id="img" value="'.$value[0]->image.'"><img src="'.base_url().'uploads/'.$value[0]->image.'" id="simg" style="width:70px;"><div class="overlay" id="overlay">';
		}else{
			$html .= '<div class="option-area text-area overlay_container" id="img_sec" style="width: 70px; display:none;"><input type="hidden" name="surveyImg" id="img" value=""><img src="" id="simg" style="width:70px;"><div class="overlay">';	
		}
		
		$html .= '<a class="icon" title="Remove" id="removeSur"><i class="fa fa-remove" aria-hidden="true" style="color: #fff;"></i></a></div></div></div></div></div><div class="col-lg-12"><div class="col-lg-6"><div class="form-group"><input type="hidden" name="editSur" value="'.$id.'"/><button type="submit" name="save" id="save" value="'.$id.'" class="btn btn-default">Save</button></div></div>';
		
		return $html;
    }
	
	public function getSurveyParticipation($id){
		$html = '';
		$sql ="SELECT * FROM  vv_survey WHERE id = '".$id."'";
		$query = $this->db->query($sql);
		$logs = $query->result();
		
		$sql ="SELECT COUNT(id) as total FROM vv_surveyParti WHERE sid = '".$id."'";
		$query = $this->db->query($sql);
		$surveyParti = $query->result();
		
		if($surveyParti[0]->total > 0){
		
			$sql ="SELECT * FROM vv_surveyParti WHERE sid = '".$id."' ORDER BY id DESC LIMIT 5";
			$query = $this->db->query($sql);
			$surveyData = $query->result();
			
			$html = '<div class="span10 main"><div class="container"><div class="row"><div class="col-md-8"><div class="heading-line"><h3>Q. '.$logs[0]->question.'</h3></div></div><div class="col-md-4"><div class="shorting">';
			if($surveyParti[0]->total < 2){
				$html .= $surveyParti[0]->total.' member participated';			
			}else{
				$html .= $surveyParti[0]->total.' members participated';	
			}
			$html .= '</div></div></div></div><div class="poll-block"><div class="poll-list"><ul id="poll-list" class="poll_list_data" style="margin-bottom:0px;">';
			if(!empty($surveyData)){
				foreach($surveyData as $sdata){
					$sql ="SELECT * FROM vv_users WHERE id = ".$sdata->userid;
					$query = $this->db->query($sql);
					$userdata = $query->result();
					
					$html .= '<li class="poll-item surveyAnsSection"><div class="row"><div class="col-md-10"><div class="p-ltop-content"></div><div class="p-lbtm-content"><div class="poll-category">';
								
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					
					$html .= '<span style="margin-right: 8px;">';
					
					
					if($userdata[0]->profile_pic_url != ''){
						$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'" style="width:60px;">';
					}elseif($userdata[0]->picture_url != ''){
						$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'" style="width:60px;">';
					}else{
						$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png" style="width:60px;">';
					}
					
					$html .= '</span></a>';
					
					if($userdata[0]->username != ''){
						$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
					}
					
					$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Paticipated on '.date('M d,Y',strtotime($sdata->created)).'</span></div></div></div><div class="col-md-2" style="text-align:center;"><a class="viewDetails trand-button">View Details</a></div>';
					
					$sql ="SELECT * FROM vv_surveyanswer WHERE userid = '".$sdata->userid."' && sid = '".$id."' ORDER BY id ASC";
					$query = $this->db->query($sql);
					$answers = $query->result();
					
					if(!empty($answers)){
						$html .= '<div class="col-md-12 surveyAnswers">';
						$i = 1;
						$count = count($answers);
						foreach($answers as $answer){
							$sql ="SELECT * FROM vv_surveyQustn WHERE id = '".$answer->qid."'";
							$query = $this->db->query($sql);
							$questions = $query->result();
							$html .= '<div style="font-size:20px;"><strong>Q. '.$questions[0]->question.'</strong></div>';
							$html .= '<div style="margin-top:10px;">Ans: '.$answer->value.'</div>';
							if($i != $count){
								$html .= '<br><br>';		
							}
							$i++;
						}
						$html .= '</div>';
					}
					
					$html .= '</div></li>';
				}
			}
			$countpar = $surveyParti[0]->total - 5;
			$html .= '</ul></div></div>';
			if($surveyParti[0]->total > 5){
				$html .= '<div class="poll-block loadingPar" style="border: none;background: transparent;text-align: center;"><button type="button" class="trand-button moreParticipation" style="margin:5px;" value="'.$countpar.'" sid="'.$id.'">Load More</button></div>';
			}
			$html .= '</div>';
		}
		return $html;
	}
	
	
	
	
	public function editSurvey($title,$qustn,$qid,$catId,$img,$id){
		$sql ="SELECT image FROM vv_survey WHERE id = ".$id;
		$query = $this->db->query($sql);
		$result = $query->result();	
		if($result[0]->image != '' && $result[0]->image != $img){
			unlink('./uploads/'.$result[0]->image);	
		}
		
		$input = array('catid' => $catId, 'question' => $title, 'image' => $img);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->update('vv_survey', $input, array('id' => $id));
		
		for($i = 0; $i < count($qid); $i++){
			if($qid[$i] == 'insert'){
				if($qustn[$i] != ''){
					$input = array('sid' => $id, 'question' => $qustn[$i]);
					$this->db->set('created', 'NOW()', FALSE);
					$this->db->set('modified', 'NOW()', FALSE);
					$this->db->insert('vv_surveyQustn', $input);
				}
			}else{
				if($qustn[$i] != ''){
					$input = array('sid' => $id, 'question' => $qustn[$i]);
					$this->db->set('modified', 'NOW()', FALSE);
					$this->db->update('vv_surveyQustn', $input, array('id' => $qid[$i]));	
				}else{
					$sql ="DELETE FROM vv_surveyQustn WHERE id = '".$qid[$i]."'";
					$query = $this->db->query($sql);
				}
			}
		}
			
		$data = array('catid' => $catId);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->update('vv_logs', $data, array('log_id' => $id, 'log_title' => 'survey'));
		return  'Success';
    }
	
}