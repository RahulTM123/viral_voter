<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Quiz_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function addQuizName($name,$pages){
		$input = array('quiz_name' => $name, 'wheree' => $pages );
		$this->db->insert('vv_quiz_name', $input);
		$insertId = $this->db->insert_id();
		return  $insertId;
    }
	
	public function getQuizNameList(){
		$sql ="SELECT * FROM vv_quiz_name";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function changeQuizNameStatus($id){
		$sql ="SELECT status FROM vv_quiz_name WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_quiz_name SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_quiz_name SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function getCatList(){
		$sql ="SELECT * FROM vv_categories";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function addNewQuiz($catid,$quizid,$qus,$a1,$a2,$a3,$a4){
		$input = array('catid' => $catid, 'quizid' => $quizid, 'quiz_ques' => $qus, 'ans1' => $a1, 'ans2' => $a2, 'ans3' => $a3, 'ans4' => $a4 );
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_quiz', $input);
		$insertId = $this->db->insert_id();
		return  $insertId;
    }
	
	
	public function getQuizList(){
		$sql ="SELECT * FROM vv_quiz";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function changeQuizStatus($id){
		$sql ="SELECT status FROM vv_quiz WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		if($value[0]->status == 1){
			$sql ="UPDATE vv_quiz SET status = 0 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}else{
			$sql ="UPDATE vv_quiz SET status = 1 WHERE id=".$id;
			$query = $this->db->query($sql);
			return 1;
		}
		
    }
	
	public function deleteQuiz($id){
		$sql ="SELECT * FROM vv_quiz WHERE id=".$id;
		$query = $this->db->query($sql);
		$value = $query->result();
		
		$qunserdata = unserialize($value[0]->quiz_ques);
		if(count($qunserdata) == '2'){
			unlink('./uploads/'.$qunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($qunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$qunserdata[0]);
			}
		}
		
		$aunserdata = unserialize($value[0]->ans1);
		if(count($aunserdata) == '2'){
			unlink('./uploads/'.$aunserdata[1]);
		}else{
			$ext = strtolower(pathinfo($aunserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$aunserdata[0]);
			}
		}
		
		$a1unserdata = unserialize($value[0]->ans2);
		if(count($a1unserdata) == '2'){
			unlink('./uploads/'.$a1unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a1unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a1unserdata[0]);
			}
		}
		
		$a2unserdata = unserialize($value[0]->ans3);
		if(count($a2unserdata) == '2'){
			unlink('./uploads/'.$a2unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a2unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a2unserdata[0]);
			}
		}
		
		$a3unserdata = unserialize($value[0]->ans4);
		if(count($a3unserdata) == '2'){
			unlink('./uploads/'.$a3unserdata[1]);
		}else{
			$ext = strtolower(pathinfo($a3unserdata[0], PATHINFO_EXTENSION));
			if(!empty($ext)){
				unlink('./uploads/'.$a3unserdata[0]);
			}
		}
		
		$sql ="DELETE FROM vv_quiz WHERE id=".$id;
		$this->db->query($sql);
		return 1;
    }
}