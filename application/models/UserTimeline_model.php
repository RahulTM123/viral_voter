<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserTimeline_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function getAllLogs($userid){
		$sql = "SELECT * FROM vv_tagfriends WHERE tagid = '$userid'";
		$tagquery = $this->db->query($sql);
		$tagdata = $tagquery->result();
		$tag_ids = array();
		if(!empty($tagdata)){
			foreach($tagdata as $t){
				array_push($tag_ids,$t->tagid);
			}
		}
		if(!empty($tag_ids)){
			$tids = implode(',',$tag_ids);
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || userid IN (".$tids.") || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}else{
			$sql ="SELECT * FROM vv_logs WHERE userid = '$userid' || log_title = 'compition' || log_title = 'survey' ORDER BY id DESC";
		}
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }	
    
	public function getRowsData($logcreatd,$title,$id,$userid,$loguser){
		$idNum = $this->session->userdata('idNum');
		$html = '';
		$date = date('m/d/Y');
		
		
		//admin compition section start here
		if($title == 'compition'){
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && userid = '".$loguser."'";
			$query = $this->db->query($sql);
			$cmntvalue = $query->result();
			if(!empty($cmntvalue)){
				$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
						$curntquery = $this->db->query($sql);
						$curntusers = $curntquery->result();
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						
						$html .= '</div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text" style="font-size:40px;"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						
						$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'compitition'";
						$query = $this->db->query($sql);
						$getLikeId = $query->result();
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
						
						
						if(!empty($getLikeId)){
							$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "compitition" >';
						}else{
							$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "compitition">';
						}
						
						
						$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="compitition"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="compition_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
						
						
						$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment_comp lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalse usr_content_block"><a href="#" class=" cancelEdit">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-3 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgOne" id="compImgOne'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					<div class="col-md-3 colMdTwo" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgTwo" id="compImgTwo'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgTwo" class="hiddenCompImgTwo"></span></div>
					<div class="col-md-3 colMdThree" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgThree" id="compImgThree'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgThree" class="hiddenCompImgThree"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveTheComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButton" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>

					<div class="enter"><label title="Attach a photo"><span class="commentPhoto"></span></label></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
						
						if($totalComnt[0]->total < 6){
							$html .= '<a class="UFIPagerLink" href="#" role="button" style="display:none;">View All comments</a>';
						}else{
							$html .= '<a class="UFIPagerLink" href="#" role="button">View All comments</a>';
						}
						
						$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$loguser.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
						$idNum++;
						$this->session->set_userdata('idNum',$idNum);
					}
				}
			}
		}
		//admin compition section end here
		
		
		//admin compition shared section start here
		if($title == 'compition_shared'){
			$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($logcreatd));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					if($curntusers[0]->username != ''){
						$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
					}
					
					$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared viralvoters <a href="#">Compition</a></span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text" style="font-size:40px;"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
					
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'compitition'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>0</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "compitition" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "compitition">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span><span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="compitition"><i class="fa fa-comment"></i>Comment</span><span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="compition_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					
					
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
					
					if(!empty($curntusers[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
					}elseif(!empty($curntusers[0]->picture_url)){
						$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
					}
					
					$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment_comp lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalse usr_content_block"><a href="#" class=" cancelEdit">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-3 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgOne" id="compImgOne'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					<div class="col-md-3 colMdTwo" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgTwo" id="compImgTwo'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgTwo" class="hiddenCompImgTwo"></span></div>
					<div class="col-md-3 colMdThree" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"><a class="__9u __9t" rel="ignore"><div class="_3jk"><input title="Choose a file to upload" display="inline-block" type="file" class="_n _5f0v compImgThree" id="compImgThree'.$r->id.'"></div></a></span><span class="displayTwo" style="display:none;"></span></div><input type="hidden" name="hiddenCompImgThree" class="hiddenCompImgThree"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveTheComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButton" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>

					<div class="enter"><label title="Attach a photo"><span class="commentPhoto"></span></label></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="#" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="#" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$loguser.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//admin compition shared section end here
		
		//user post section start here
		if($title == 'user_post'){
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= '</div><div class="media-body"><h6 class="media-heading">';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'post'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Post with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}
						}
					}else{
						if($r->postdata != '' && $r->postimg != ''){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->postdata != '' && $r->videolink!= ''){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->postimg){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}elseif($r->videolink){
							$html .= ' Created a <a href="'.base_url().'post/'.$r->id.'" class="usr">Post</a>';
						}else{
							$html .= ' Created a Post';
						}
					}
					
					
					
					
					$html .= '</span></h6><p>'.$r->publish_date.'</p></div>';
					
					if($loguser == $r->user_id){
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
					}
					
					$html .= '</div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">
							  <li class="_vv54ni __vvMenuItem deleteBlock" id="'.$r->id.'" role="post"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<div class="col-md-12 col-sm-12"><div class="row">';
					
					if($r->postdata != '' && $r->postimg != ''){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}elseif($r->postdata != '' && $r->videolink != ''){
						$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}elseif($r->postdata){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
					}elseif($r->videolink){
						$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}else{
						$html .= '<div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'post_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$sql ="SELECT views FROM vv_postvideos WHERE postid = '".$r->id."'";
					$post = $this->db->query($sql);
					$postViews = $post->result();
					if(!empty($postViews)){
						$view = $postViews[0]->views;
					}else{
						$view = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>'.$view.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "post_like" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "post_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_post"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_post_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user post section end here
		
		
		
		//user post share section start here
		if($userid == $loguser){
			if($title == 'user_post_shared'){
				$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($logcreatd));
						$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
						$query = $this->db->query($sql);
						$users = $query->result();
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
						$curntquery = $this->db->query($sql);
						$curntusers = $curntquery->result();
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
						}elseif(!empty($users[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						}
						
						$html .= ' </div><div class="media-body"><h6 class="media-heading">';
						
						if($curntusers[0]->username != ''){
							$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
						}
						
						$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared ';
						
						if($users[0]->username != ''){
							$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
						}
						
						$html .= $users[0]->firstname.' '.$users[0]->lastname."'s <a href='".base_url()."post/".$r->id."' class='usr'>Post</a></span></h6><p>".$pDate."</p></div>";
					
						if($loguser == $r->user_id){
							$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
						}
						
						$html .= '</div>
					
						<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
						  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
							<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
							  <div class="_vv54ng">
								<ul class="_vv54nf" role="menu">
								  <li class="_vv54ni __vvMenuItem deleteSharedBlock" id="'.$id.'" role="post"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
								</ul>
							  </div>
							</div>
						  </div>
						</div>
						
						<div class="col-md-12 col-sm-12"><div class="row">';
						
						if($r->postdata != '' && $r->postimg != ''){
							$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
						}elseif($r->postdata != '' && $r->videolink != ''){
							$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
						}elseif($r->postdata){
							$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
						}elseif($r->videolink){
							$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
						}else{
							$html .= '<div class="col-md-12"><a href="'.base_url().'post/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
						}
						
						$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'post_like'";
						$query = $this->db->query($sql);
						$getLikeId = $query->result();
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$sql ="SELECT views FROM vv_postvideos WHERE postid = '".$r->id."'";
						$post = $this->db->query($sql);
						$postViews = $post->result();
						if(!empty($postViews)){
							$view = $postViews[0]->views;
						}else{
							$view = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>'.$view.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
						
						
						if(!empty($getLikeId)){
							$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "post_like" >';
						}else{
							$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "post_like">';
						}
						
						
						$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_post"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_post_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
						if($totalComnt[0]->total < 6){
							$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button" style="display:none;">View All comments</a>';
						}else{
							$html .= '<a class="UFIPagerLink" href="'.base_url().'post/'.$r->id.'" role="button">View All comments</a>';
						}
						
						$html.= '</div></div></div></div></div></article></div><input type="hidden" value="'.$userid.'" name="puserid" id="puserid"><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
						$idNum++;
						$this->session->set_userdata('idNum',$idNum);
					}
				}
			}
		}
		//user post share section end here
		
		
		
		//user poll section start
		if($title == 'user_poll'){
			//$opt = 1;
			$val = 0;
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
					$curntquery = $this->db->query($sql);
					$curntusers = $curntquery->result();
					
					$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
					$query1 = $this->db->query($sql1);
					$pollresult = $query1->result();
					
					$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
					$pollquery = $this->db->query($poll);
					$polloption = $pollquery->result();
					
					
					
									
					$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'poll'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Poll with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}	
						}
					}else{
						$html .= ' Created a Poll';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$pDate.'</p></div>';
					
					if($loguser == $r->user_id){
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
					$squery = $this->db->query($sql);
					$totalShare = $squery->result();
					
					$html .= '</div>
					
					<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
					  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
						<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
						  <div class="_vv54ng">
							<ul class="_vv54nf" role="menu">';
								
							if(empty($pollresult) && $totalShare[0]->total == 0){
								$html .= '<li class="_vv54ni __vvMenuItem" id="'.$r->id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Edit Poll</span></span></a></li>';	
							}
							$html .= '<li class="_vv54ni __vvMenuItem deleteBlock" id="'.$r->id.'" role="post"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
							</ul>
						  </div>
						</div>
					  </div>
					</div>
					
					<form method="post" enctype="multipart/form-data" action="'.base_url().'UserProfile/pollVoting?pollid='.$r->id.'"><div class="row">';
				
					if($r->question != '' && $r->imag != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}elseif($r->question != '' && $r->videourl != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}elseif($r->question){
						$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
					}elseif($r->videourl){
						$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}else{
						$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}
					
					$html .= '<div class="trand row">';
					
					
					$sql ="SELECT vote FROM vv_pollvoting WHERE pollid = '".$r->id."' AND userid = '".$loguser."'";
					$query = $this->db->query($sql);
					$getvalue = $query->result();
					
					foreach($polloption as $pollopt){
						$html .= '<div class="col-md-6 col-sm-6">';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							if(empty($getvalue)){
								
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							}
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
							}else{
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}
						}
						$html .= '</div>';
						// $opt++;
					}
					
					//poll vote counting start here
					$pollvotearray = array();
					$pollperarray = array();
					$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
					$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
					if(!empty($pollresult)){
						$totalvote = count($pollresult);
						foreach($pollresult as $p){
							if($p->vote == 1){
								$optonevote++;	
							}
							if($p->vote == 2){
								$opttwovote++;	
							}
							if($p->vote == 3){
								$optthreevote++;	
							}
							if($p->vote == 4){
								$optfourvote++;	
							}
						}
						if($optonevote > 0){
							$optoneper = ($optonevote/$totalvote)*100;
						}
						if($opttwovote > 0){
							$opttwoper = ($opttwovote/$totalvote)*100;
						}
						if($optthreevote > 0){
							$optthreeper = ($optthreevote/$totalvote)*100;
						}
						if($optfourvote > 0){
							$optfourper = ($optfourvote/$totalvote)*100;
						}
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}else{
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}
					$html .= '</div></div><div class="col-md-4 col-sm-4">';
					
					foreach($polloption as $pollopt){
						$img = $text = '';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
							}else{
								$text = $title[0];
							}
						}
						if(!empty($img) && !empty($text)){
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}elseif(!empty($img)){
							$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
						}else{
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}
						$val++;
					}
					
					$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
					
					if(!empty($getvalue)){
						$html .= '';
					}else{
						$html .= '<button type="button" class="pollVote trand-button" value="" name="pollVote" disabled="disabled">Submit Vote</button>';
					}
					
					$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'poll_like'";
					$query = $this->db->query($sql);
					$getLikeId = $query->result();
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;
					}else{
						$comment = 0;
					}
					
					$html .= '</div><div class="button"><input type="hidden" value="'.$loguser.'" name="puserid" id="puserid"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div><div class="beforePostSubmit"><div class="beforePostSubmit-content"><div class="beforePostSubmit-header"><span class="beforePostSubmit-close">&times;</span><h2>Want to submit vote?</h2></div><div class="beforePostSubmit-body"><div class="col-md-12" style="text-align:center;"><input type="submit" class="trand-button" value="Yes" name="pollVote" style="width:100px;"/><button type="button" class="trand-button noSubmitVote" style="width:100px;">No</button></div></div></div></div></form>';
					
					$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
					
					
					if(!empty($getLikeId)){
						$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "poll_like" >';
					}else{
						$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "poll_like">';
					}
					
					
					$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_poll"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_poll_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div>  </div>';
					$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
					if($totalComnt[0]->total < 6){
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button" style="display:none;">View All comments</a>';
					}else{
						$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button">View All comments</a>';
					}
					
					$html.= '</div></div></div></div></div></article></div><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
					$idNum++;
					$this->session->set_userdata('idNum',$idNum);
				}
			}
		}
		//user poll section end
		
		
		
		//user poll shared section start
		if($userid == $loguser){
			if($title == 'user_poll_shared'){
				//$opt = 1;
				$val = 0;
				$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($logcreatd));
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
						$query = $this->db->query($sql);
						$users = $query->result();
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$loguser;
						$curntquery = $this->db->query($sql);
						$curntusers = $curntquery->result();
						
						$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
						$query1 = $this->db->query($sql1);
						$pollresult = $query1->result();
						
						$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
						$pollquery = $this->db->query($poll);
						$polloption = $pollquery->result();
						
						
						
										
						$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" style="width:40px">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" style="width:40px">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
						}
						
						$html .= ' </div><div class="media-body"><h6 class="media-heading">';
						
						
						if($curntusers[0]->username != ''){
							$html .= '<a href="'.base_url().''.$curntusers[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$curntusers[0]->id.'" class="usr">';	
						}
						
						$html .= $curntusers[0]->firstname.' '.$curntusers[0]->lastname.'</a><span class=""> Shared ';
						
						if($users[0]->username != ''){
							$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
						}
						
						$html .= $users[0]->firstname.' '.$users[0]->lastname.' <a href="'.base_url().'poll/'.$r->id.'" class="questn">Poll</a></span></h6><p>'.$pDate.'</p></div>';
					
					
						$html .= '<div class="_vv6a uiPopover _vv5pbi _vvcmw _vvb1e _vv1wbl"><a class="_vv4xev _p"></a></div></div>
					
						<div class="vvContextualLayerPositioner vvLayer vvhidden_elem" style="width: 200px;opacity: 1; margin-top: -55px;float: right;">
						  <div class="vContextualLayer vvContextualLayerBelowRight" style="right: 0px;">
							<div class="_vv54nq _vv5pbk _vv558b _vv2n_z">
							  <div class="_vv54ng">
								<ul class="_vv54nf" role="menu">
								  <li class="_vv54ni __vvMenuItem deleteSharedBlock" id="'.$id.'" role="poll"><a class="_vv54nc"><span><span class="_vv54nh">Delete</span></span></a></li>
								</ul>
							  </div>
							</div>
						  </div>
						</div>
						
						<form method="post" enctype="multipart/form-data" action="'.base_url().'UserProfile/pollVoting?pollid='.$r->id.'"><div class="row">';
				
						if($r->question != '' && $r->imag != ''){
							$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
						}elseif($r->question != '' && $r->videourl != ''){
							$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
						}elseif($r->question){
							$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
						}elseif($r->videourl){
							$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
						}else{
							$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
						}
						
						$html .= '<div class="trand row">';
						
						
						$sql ="SELECT vote FROM vv_pollvoting WHERE pollid = '".$r->id."' AND userid = '".$loguser."'";
						$query = $this->db->query($sql);
						$getvalue = $query->result();
						
						foreach($polloption as $pollopt){
							$html .= '<div class="col-md-6 col-sm-6">';
							$title = unserialize($pollopt->title);
							if(count($title) == '2'){
								$img = $title[1];
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								if(empty($getvalue)){
									$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								}
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}else{
								$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
								if(!empty($ext)){
									$img = $title[0];
									$html .= '<div class="media"><div class="media-left">';
									if(empty($getvalue)){
										$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
									}
									$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
								}else{
									$text = $title[0];
									$html .= '<div class="media"><div class="media-left">';
									if(empty($getvalue)){
										$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
									}
									$html .= '</div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
								}
							}
							$html .= '</div>';
							// $opt++;
						}
						
						//poll vote counting start here
						$pollvotearray = array();
						$pollperarray = array();
						$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
						$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
						if(!empty($pollresult)){
							$totalvote = count($pollresult);
							foreach($pollresult as $p){
								if($p->vote == 1){
									$optonevote++;	
								}
								if($p->vote == 2){
									$opttwovote++;	
								}
								if($p->vote == 3){
									$optthreevote++;	
								}
								if($p->vote == 4){
									$optfourvote++;	
								}
							}
							if($optonevote > 0){
								$optoneper = ($optonevote/$totalvote)*100;
							}
							if($opttwovote > 0){
								$opttwoper = ($opttwovote/$totalvote)*100;
							}
							if($optthreevote > 0){
								$optthreeper = ($optthreevote/$totalvote)*100;
							}
							if($optfourvote > 0){
								$optfourper = ($optfourvote/$totalvote)*100;
							}
							array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
							array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
						}else{
							array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
							array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
						}
						
						$html .= '</div></div><div class="col-md-4 col-sm-4">';
						
						foreach($polloption as $pollopt){
							$img = $text = '';
							$title = unserialize($pollopt->title);
							if(count($title) == '2'){
								$img = $title[1];
								$text = $title[0];
							}else{
								$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
								if(!empty($ext)){
									$img = $title[0];
								}else{
									$text = $title[0];
								}
							}
							if(!empty($img) && !empty($text)){
								$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
							}elseif(!empty($img)){
								$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
							}else{
								$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
							}
							$val++;
						}
						
						$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button">';
						
						if(!empty($getvalue)){
							$html .= '';
						}else{
							$html .= '<button type="button" class="pollVote trand-button" value="" name="pollVote" disabled="disabled">Submit Vote</button>';
						}
						
						$sql ="SELECT id FROM vv_like WHERE logid = '".$r->id."' AND userid = '".$loguser."' AND log_title = 'poll_like'";
						$query = $this->db->query($sql);
						$getLikeId = $query->result();
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
						$squery = $this->db->query($sql);
						$totalShare = $squery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;
						}else{
							$comment = 0;
						}
						
						$html .= '</div><div class="button"><input type="hidden" value="'.$loguser.'" name="puserid" id="puserid"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div><div class="beforePostSubmit"><div class="beforePostSubmit-content"><div class="beforePostSubmit-header"><span class="beforePostSubmit-close">&times;</span><h2>Want to submit vote?</h2></div><div class="beforePostSubmit-body"><div class="col-md-12" style="text-align:center;"><input type="submit" class="trand-button" value="Yes" name="pollVote" style="width:100px;"/><button type="button" class="trand-button noSubmitVote" style="width:100px;">No</button></div></div></div></div></form>';
						
						$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like">';
						
						
						if(!empty($getLikeId)){
							$html .= '<a href="" class="vv_like vv_after_like" id="'.$r->id.'" likeid="'.$getLikeId[0]->id.'" logtitle = "poll_like" >';
						}else{
							$html .= '<a href="" class="vv_like" id="'.$r->id.'" likeid="" logtitle = "poll_like">';
						}
						
						
						$html .= '<i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like vv_cmnt" id="'.$r->id.'" cmnTitle="user_poll"><i class="fa fa-comment"></i>Comment</span> <span class="viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="" id="'.$r->id.'" logtitle="user_poll_shared" class="vv_share social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div>';
						
						$html .= '<div class="col-md-12 vvuficontainer"><div class="vvufilist"><div class="cmmnt-box"><div class="cmmnt"><div class="cmntBlock"><table style="width: 100%;"><tbody><tr><td style="width:40px;"><span class="sm-pic">';
						
						if(!empty($curntusers[0]->profile_pic_url)){
							$html .= '<img src="'.base_url().'uploads/'.$curntusers[0]->profile_pic_url.'" class="img-fluid" style="width:40px;">';
						}elseif(!empty($curntusers[0]->picture_url)){
							$html .= '<img src="'.$curntusers[0]->picture_url.'" class="img-fluid" style="width:40px;">';
						}else{
							$html .= '<img src="'.base_url("assets/front/images/user.png").'" class="img-fluid" style="width:40px;">';
						}
						
						$html .= '</span></td><td style="position:relative;width: 94.3%;" class="mainCompBox"><input type="text" placeholder="Write a Comment..." class="comment-box lead emoji-picker-container" name="comment" data-emojiable="true"></td></tr></tbody></table><span class="fcg fss UFICommentTip commentFalseP usr_content_block"><a href="#" class="cancelEditP">Cancel</a></span>
					
					<div class="vvCommentPhotoAttachedPreview _4soo pas photoSection usr_content_block"><span display="inline"><div class="vvScaledImageContainer"><div class="col-md-12">
					
					<div class="col-md-6 colMdOne" style="float:left"><span class="_kkr"><div class="_m _6a compImgContent"><span class="displayOne"></span></div><input type="hidden" name="hiddenCompImgOne" class="hiddenCompImgOne"></span></div>
					
					</div></div><p class="saveComment"><button type="button" class="trand-button saveThePComment" compTitle="postComment">Post</button></p></span><a href="#" role="button" title="Remove Photo" aria-label="Remove Photo" class="vvCloseButtonP" data-hover="tooltip" data-tooltip-alignh="center" data-tooltip-content="Remove Photo"><i class="fa fa-remove vvCloseButtonHighContrast img"></i></a></div>
					
					<div class="enter"><span class="span1"><label for="photoComp'.$idNum.'" title="Attach a photo"><span class="compPhoto"></span></label><input type="file" id="photoComp'.$idNum.'" class="photoComp" /><input type="hidden" name="compPhoto" class="compImg"></span></div></div></div></div><li class="cmntLoadImg"><img src="http://demo.hostadesign.com/viralvoters/assets/front/images/loader.gif" class="img-fluid" style="width:20px;margin-top: 5px;margin-bottom: 5px;" id="loaderImg"></li><div class="cmntData"></div><div direction="right" class="clearfix"><div class="_ohf rfloat"></div><div class="viewAllCmnt">';
					
						if($totalComnt[0]->total < 6){
							$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button" style="display:none;">View All comments</a>';
						}else{
							$html .= '<a href="'.base_url().'poll/'.$r->id.'" class="questn UFIPagerLink" role="button">View All comments</a>';
						}
						
						$html.= '</div></div></div></div></div></article></div><input type="hidden" class="all" value="'.$totalComnt[0]->total.'"></div>';
						$idNum++;
						$this->session->set_userdata('idNum',$idNum);
					}
				}
			}
		}
		//user poll shared section end
		
		
		
		
		return $html;
	}
	
	
	public function getUserAllFriends($userid,$loginUser){
		$html = '';
		$sql ="SELECT * FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
		$query = $this->db->query($sql);
		$value = $query->result();
		$html = '<div class="ui-block"><div class="container"><div class="vv_content clearfix" id="content" role="main" style="min-height:100px;"><div class="clearfix"><div class="find-friend" style="border:none !important;"><div class="col-md-12"><div class="row">';
		if(!empty($value)){
			foreach($value as $v){
				$sql ="SELECT * FROM vv_users WHERE id = '".$v->friend_one."' || id = '".$v->friend_two."'";
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix userbox col-md-6" style="border:none !important;"><div class="frndCls">';
				if(!empty($userdata)){
					foreach($userdata as $u){
						if($u->id != $userid){
							if($u->profile_pic_url == ''){
								if($u->picture_url != '' && $u->oauth_provider == 'site_login'){
									if($u->username != ''){
										$html .= '<a href="'.base_url().''.$u->username.'" class="user-image" tabindex="-1" aria-hidden="true">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$u->id.'" class="user-image" tabindex="-1" aria-hidden="true">';	
									}
									$html .= '<div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$u->picture_url.'"></div></a>';
								}elseif($u->picture_url != '' && $u->oauth_provider != 'site_login'){
									$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.$u->picture_url.'"></div></a>';
								}else{
									$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'assets/front/images/user.png"></div></a>';
								}
							}else{
								$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$u->profile_pic_url.'"></div></a>';
							}
							
							$getFrnd = $frnRqstSnd =  $acptRqust = '';
							if($u->id != $loginUser){
								$sql ="SELECT id FROM vv_friends WHERE (friend_one = '".$u->id."' && friend_two = '".$loginUser."' && status = '1') || (friend_one = '".$loginUser."' && friend_two = '".$u->id."' && status = '1')";
								$query = $this->db->query($sql);
								$getFrnd = $query->result();
								
								$sql ="SELECT id FROM vv_friends WHERE (friend_one = '".$loginUser."' && friend_two = '".$u->id."' && status = '0')";
								$query = $this->db->query($sql);
								$frnRqstSnd = $query->result();
								
								$sql ="SELECT id FROM vv_friends WHERE (friend_one = '".$u->id."' && friend_two = '".$loginUser."' && status = '0')";
								$query = $this->db->query($sql);
								$acptRqust = $query->result();
								
							}
							
							$html .= '<div class="clearfix user-detail"><div class="response-container"><div class="response-bttn"><div class="responsesimple"><div class="response-buttons">';
							
							if(!empty($getFrnd) || $u->id == $loginUser){
								/*$html .= '<button type="button"  class="dlt userFriend" style="margin-right: 5px;cursor:default;"><i class="fa fa-check"></i>
								<span style="margin-left: 10px;font-size:12px;" id="one">Friends</span></button>*/
								$html .='<button type="button"  class="dlt userFriend " id="Friend" style="margin-right: 5px;cursor:default;"><i class="fa fa-check"></i>
								<span >Friends</span></button>';
								$html .='';
								if($userid == $loginUser)
								$html .='<div class="two " id="friend_refbox_'.$u->id.'"><div class="arrow-up"></div><div class="two_bx"><a href="#" class="prevent_click" onclick="unfriend('.$u->id.',\'friend_refbox_'.$u->id.'\');">Unfriend</a></div></div>';
								
								
							}else if(!empty($frnRqstSnd)){
								$html .= '<button type="button" value="" class="confirm friend_request_send" style="opacity:0.5;cursor:default;" disabled="disabled"><i class="fa fa-user-plus _rqst_sent"></i><span class="_rqst">Friend Request Send</span></button>';	
							}else if(!empty($acptRqust)){
								$html .= '<button type="button" value="'.$acptRqust[0]->id.'" class="confirm cnfrm_req">Confirm request</button><button type="button" value="'.$acptRqust[0]->id.'" class="dlt notNow" frndId = "'.$u->id.'" userid="'.$loginUser.'">Not now</button>';
							}else{
								$html .= '<button type="button" value="'.$u->id.'" class="confirm add_friends" userid="'.$loginUser.'" style="width:100px;"><i class="fa fa-user-plus _rqst_sent"></i><span style="color: #fff;margin-left: 20px;font-size:12px;">Add Friend</span></button>';	
							}
							$html .= '</div></div></div></div><div class="nme"><div class="usr_name">';
							if($u->username != ''){
								$html .= '<a href="'.base_url().''.$u->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$u->id.'">';	
							}
							$html .= $u->firstname.' '.$u->lastname.'</a></div></div></div></div></div>';
						}
					}
				}
			}
		}else{
			$html .= 'No Friends Found!';
		}
		$html .= '</div></div></div></div></div></div></div>';
		return $html;
	}
	

	
	public function getUserAllFriendBeforeLogin($userid){
		$html = '';
		$sql ="SELECT * FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
		$query = $this->db->query($sql);
		$value = $query->result();
		$html = '<div class="ui-block"><div class="container"><div class="vv_content clearfix" id="content" role="main" style="min-height:100px;"><div class="clearfix"><div class="find-friend" style="border:none !important;"><div class="col-md-12"><div class="row">';
		if(!empty($value)){
			foreach($value as $v){
				$sql ="SELECT * FROM vv_users WHERE id = '".$v->friend_one."' || id = '".$v->friend_two."'";
				$userquery = $this->db->query($sql);
				$userdata = $userquery->result();
				$html .= '<div class="clearfix userbox col-md-6" style="border:none !important;"><div class="frndCls">';
				if(!empty($userdata)){
					foreach($userdata as $u){
						if($u->id != $userid){
							if($u->profile_pic_url == ''){
								if($u->picture_url != '' && $u->oauth_provider == 'site_login'){
									if($u->username != ''){
										$html .= '<a href="'.base_url().''.$u->username.'" class="user-image" tabindex="-1" aria-hidden="true">';	
									}else{
										$html .= '<a href="'.base_url().'user/id/'.$u->id.'" class="user-image" tabindex="-1" aria-hidden="true">';	
									}
									$html .= '<div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$u->picture_url.'"></div></a>';
								}elseif($u->picture_url != '' && $u->oauth_provider != 'site_login'){
									$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.$u->picture_url.'"></div></a>';
								}else{
									$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'assets/front/images/user.png"></div></a>';
								}
							}else{
								$html .= '<a href="#" class="user-image" tabindex="-1" aria-hidden="true"><div class="user-imagecontainer"><img class="scaleduserimage" src="'.base_url().'uploads/'.$u->profile_pic_url.'"></div></a>';
							}
							
							$html .= '<div class="clearfix user-detail"><div class="nme"><div class="usr_name">';
							if($u->username != ''){
								$html .= '<a href="'.base_url().''.$u->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$u->id.'">';	
							}
							$html .= $u->firstname.' '.$u->lastname.'</a></div></div></div></div></div>';
						}
					}
				}
			}
		}else{
			$html .= 'No Friends Found!';
		}
		$html .= '</div></div></div></div></div></div></div>';
		return $html;
	}
	
	
	
	
	public function getUserPolls($userid){
		$html = '';
		$sql ="SELECT * FROM vv_logs WHERE userid = '".$userid."' && log_title = 'user_poll' ORDER BY id ASC LIMIT 0,1";
		$query = $this->db->query($sql);
		$logs = $query->result();
		
		$sql ="SELECT count(id) as total FROM vv_logs WHERE userid = '".$userid."' && log_title = 'user_poll' ORDER BY id ASC";
		$query = $this->db->query($sql);
		$countrow = $query->result();
		
		$html = '<div class="span10 main"><div class="container"><div class="row"><div class="col-md-4"><div class="heading-line"><h3></h3></div></div><div class="col-md-8"><div class="shorting col-md-12"><div class="col-md-7" style="float:left; text-align:right;"><select class="category catAfterLogin"><option value="">Sort By</option><option value="recent">Most Recent</option><option value="vote">Total Vote</option><option value="upvote">Upvote</option><option value="comment">Comment</option></select></div>';
		
		$sql ="SELECT * FROM vv_categories";
		$query = $this->db->query($sql);
		$categories = $query->result();
		
		if(!empty($categories)){
			$html .= '<div class="col-md-5" style="float:left; text-align:right;"><select class="category" id="sortByPollUserCat"><option value="">--Select Category--</option>';
			foreach($categories as $cat){
				$html .= '<option value="'.$cat->id.'">'.$cat->cat_name.'</option>';	
			}
			$html .= '</select></div>';
		}	
		
		$html .= '</div></div></div></div><div class="poll-block" style="margin-top:10px;"><div class="poll-list"><ul id="poll-list" class="poll_list_data" style="margin-bottom:0px;">';
		
		if(!empty($logs)){
			foreach($logs as $l){
				$sql ="SELECT * FROM vv_pollls WHERE id = ".$l->log_id;
				$query = $this->db->query($sql);
				$value = $query->result();
				if(!empty($value)){
					foreach($value as $v){
						$sql ="SELECT total_vote FROM vv_pollresult WHERE logid = ".$v->id;
						$pollquery = $this->db->query($sql);
						$polldata = $pollquery->result();
						if(!empty($polldata)){
							$totalpoll = $polldata[0]->total_vote;
						}else{
							$totalpoll = '0';
						}
						
						$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
						$userquery = $this->db->query($sql);
						$userdata = $userquery->result();
						if(!empty($userdata)){
							$html .= '<li class="poll-item"><div class="row"><div class="col-md-7 col-xs-7 p-left-content"><div class="p-ltop-content"><h2 itemprop="name"><a href="'.base_url().'poll/'.$v->id.'" class="questn poll-title">'.$v->question.'</a></h2></div><div class="p-lbtm-content"><div class="poll-category">';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$html .= '<span class="author-avatar">';
							
							
							if($userdata[0]->profile_pic_url != ''){
								$html .= '<img class="avatar" src="'.base_url().'uploads/'.$userdata[0]->profile_pic_url.'">';
							}elseif($userdata[0]->picture_url != ''){
								$html .= '<img class="avatar" src="'.$userdata[0]->picture_url.'">';
							}else{
								$html .= '<img class="avatar" src="'.base_url().'assets/front/images/user.png">';
							}
							
							$html .= '</span></a>';
							
							if($userdata[0]->username != ''){
								$html .= '<a href="'.base_url().''.$userdata[0]->username.'">';	
							}else{
								$html .= '<a href="'.base_url().'user/id/'.$userdata[0]->id.'">';	
							}
							
							$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$v->id."' && log_title = 'poll_like'";
							$tquery = $this->db->query($sql);
							$totalLike = $tquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$v->id."' && log_title = 'user_poll'";
							$cquery = $this->db->query($sql);
							$totalComnt = $cquery->result();
							
							$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$v->id."' && log_title = 'user_poll_shared'";
							$squery = $this->db->query($sql);
							$totalShare = $squery->result();
							
							$html .= $userdata[0]->firstname.' '.$userdata[0]->lastname.' </a><span class="poll-time">Asked on '.date('M d,Y',strtotime($v->created)).'</span></div></div></div><div class="col-md-5 col-xs-5 p-right-content"><ul class="poll-statistic"><li><span class="question-views">'.$totalpoll.'</span><i class="fa fa-thumbs-up"></i>Total Vote</li><li><span class="question-views">'.$totalLike[0]->total.'</span><i class="fa fa-thumbs-up"></i>Upvote</li><li><span class="question-answers"> '.$totalComnt[0]->total.' </span><i class="fa fa-comment"></i>Comment</li><li><span class="question-votes"> '.$totalShare[0]->total.' </span><i class="fa fa-share"></i>Share</li></ul></div></div></li>';
							
						}
					}
				}
			}
			if($countrow[0]->total > 1){
				$html .= '<li style="text-align:center; list-style:none;" id="lastli"><button type="button" class="trand-button loadMorePollByUser" style="margin:5px;">Load More</button><input type="hidden" value="1" id="shortId" tbl="pollAsc" catid=""></li>';
			}
		}else{
			$html .= '<li class="poll-item">No Polls Found</li>';
		}
		$html .= '</ul><input type="hidden" value="'.$userid.'" id="userid"></div></div></div>';
		return $html;
	}
	
	
	
	
	public function addNewPoll($userid,$qvideolink,$qus,$qimag,$option,$cat,$frndids){
		if(!empty($qvideolink)){
			$videoArr = explode('v=',$qvideolink);
			$qvideolink = 'https://www.youtube.com/embed/'.$videoArr[1];
		}
		$input = array('user_id' => $userid, 'catid' => $cat, 'question' => $qus, 'imag' => $qimag, 'videourl' => $qvideolink);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_pollls', $input);
		$insertId = $this->db->insert_id();
		if(!empty($insertId)){
			if(!empty($option)){
				for($i = 0; $i < count($option); $i++){
					$opt = $i+1;
					$data = array('option_id' => $opt, 'pollid' => $insertId, 'title' => $option[$i]);
					$this->db->set('created', 'NOW()', FALSE);
					$this->db->set('modified', 'NOW()', FALSE);
					$this->db->insert('vv_polloption', $data);	
				}
			}
			if(!empty($frndids)){
				for($i = 0; $i < count($frndids); $i++){
					$opt = $i+1;
					$data = array('userid' => $userid, 'logid' => $insertId, 'title' => 'poll', 'tagid' => $frndids[$i]);
					$this->db->set('created', 'NOW()', FALSE);
					$this->db->insert('vv_tagfriends', $data);	
				}
			}
			$data = array('userid' => $userid, 'catid' => $cat, 'log_id' => $insertId, 'log_title' => 'user_poll', 'ip' => $_SERVER['SERVER_ADDR']);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->set('modified', 'NOW()', FALSE);
			$this->db->insert('vv_logs', $data);
		}
		return  $insertId;
    }
	 
	public function addNewPost($userid,$postdata,$postimg,$postvideo,$cat,$frndids){
		if(!empty($postvideo)){
			$videoArr = explode('v=',$postvideo);
			$postvideo = 'https://www.youtube.com/embed/'.$videoArr[1];
		}
		$pdate = date('M, d Y');
		$input = array('user_id' => $userid, 'catid' => $cat, 'postimg' => $postimg, 'videolink' => $postvideo, 'postdata' => $postdata, 'publish_date' => $pdate);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_post', $input);
		$insertId = $this->db->insert_id();
		
		if(!empty($insertId)){
			if(!empty($postvideo)){
				$input = array('postid' => $insertId, 'videolink' => $postvideo, 'views' => '0', 'status' => '1');
				$this->db->set('created', 'NOW()', FALSE);
				$this->db->insert('vv_postvideos', $input);
			}
			if(!empty($frndids)){
				for($i = 0; $i < count($frndids); $i++){
					$opt = $i+1;
					$data = array('userid' => $userid, 'logid' => $insertId, 'title' => 'post', 'tagid' => $frndids[$i]);
					$this->db->set('created', 'NOW()', FALSE);
					$this->db->insert('vv_tagfriends', $data);	
				}
			}
			$data = array('userid' => $userid, 'catid' => $cat, 'log_id' => $insertId, 'log_title' => 'user_post', 'ip' => $_SERVER['SERVER_ADDR']);
			$this->db->set('created', 'NOW()', FALSE);
			$this->db->set('modified', 'NOW()', FALSE);
			$this->db->insert('vv_logs', $data);
		}
		return  $insertId;
    }
	
	
	public function addPollVoting($pollid,$pollOpt,$userid){
		$input = array('pollid' => $pollid, 'vote' => $pollOpt, 'userid' => $userid);
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_pollvoting', $input);
		$insertId = $this->db->insert_id();
		return  $insertId;
     }
	 
	 
	 public function getCatList(){
		$sql ="SELECT * FROM vv_categories ORDER BY cat_name ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	
	public function getUserName($userid){
		$sql ="SELECT vv_users.*, vv_cities.name as cityname, vv_states.name as stname, vv_countries.name as ctname FROM vv_users left join vv_cities on vv_users.city = vv_cities.id left join vv_states on vv_users.state = vv_states.id left join vv_countries on vv_users.country = vv_countries.id WHERE 1 and vv_users.id = ".$userid;
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
    }
	
	public function getUserId($userId){
		$sql ="SELECT id FROM vv_users WHERE username = '".$userId."' || id = '".$userId."'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value[0]->id;
    }
	
	public function getCurrentUserId($username){
		$sql ="SELECT id FROM vv_users WHERE username = '".$username."' || id = '".$username."'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value->id;
    }
	
	
	public function totalFriend($userid){
		$sql ="SELECT count(id) as total, GROUP_CONCAT(friend_one, ',', friend_two) as friendid FROM vv_friends WHERE (friend_one = '".$userid."' || friend_two = '".$userid."') && status = '1'";
		$query = $this->db->query($sql);
		$value = $query->row();
		return $value;
    }
	
	
	public function friendfollowStatus($userid,$profileid){
		$result = array();
		$sqlfr ="SELECT * FROM vv_friends WHERE ((friend_one = '".$userid."' && friend_two = '".$profileid."') || (friend_one = '".$profileid."' && friend_two = '".$userid."')) && (friend_one != friend_two)";
		$queryfr = $this->db->query($sqlfr);
		if($queryfr->num_rows()) {
			$valuefr = $queryfr->row_array();
			$result['friend']['status'] = $valuefr['status'];
		}
		else {
			$result['friend']['status'] = '';
		}
		
		$sqlfo ="SELECT * FROM vv_follower WHERE userid = '".$profileid."' && followby = '".$userid."'";
		$queryfo = $this->db->query($sqlfo);
		$valuefo = $queryfo->row();
		if($queryfo->num_rows()) {
			$valuefo = $queryfo->row_array();
			$result['follower']['status'] = $valuefo['status'];
			$result['follower']['followtype'] = $valuefo['followtype'];
		}
		else {
			$result['follower']['status'] = '';
		}
		return $result;
    }
	
	
	
	
	
	public function getRowsDataBeforeLogin($logcreatd,$title,$id,$userid){
		$html = '';
		$date = date('m/d/Y');
		
		
		
		
		//admin compition section start here
		if($title == 'compition'){
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			$cmntvalue = $query->result();
			if(!empty($cmntvalue)){
				$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left"><img src="'.base_url("assets/front/images/user.png").'" style="width:40px"></div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;	
						}else{
							$comment = 0;
						}
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div></div>';
					}
				}
			}
		}
		//admin compition section end here
		
		
		
		
		//user post section start here
		if($title == 'user_post'){
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					//$pDate = $r->publish_date;
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= '</div><div class="media-body"><h6 class="media-heading">';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'post'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Post with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}
						}
					}else{
						$html .= ' Created a Post';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$r->publish_date.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row">';
					
					if($r->postdata != '' && $r->postimg != ''){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'Login"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}elseif($r->postdata != '' && $r->videolink != ''){
						$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px;"></iframe></div>';
					}elseif($r->postdata){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
					}elseif($r->videolink){
						$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px;"></iframe></div>';
					}else{
						$html .= '<div class="col-md-12"><a href="'.base_url().'Login"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;	
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div></div>';
				}
			}
		}
		//user post section end here
		
		
		
		
		
		
		
		//user poll section start
		if($title == 'user_poll'){
			$val = 0;
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
					$query1 = $this->db->query($sql1);
					$pollresult = $query1->result();
					
					$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
					$pollquery = $this->db->query($poll);
					$polloption = $pollquery->result();
					
					
					
									
					$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'poll'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Poll with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}	
						}
					}else{
						$html .= ' Created a Poll';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
					if($r->question != '' && $r->imag != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}elseif($r->question != '' && $r->videourl != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}elseif($r->question){
						$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
					}elseif($r->videourl){
						$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}else{
						$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}
					
					$html .= '<div class="trand row">';
					
					foreach($polloption as $pollopt){
						$html .= '<div class="col-md-6 col-sm-6">';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
							}else{
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}
						}
						$html .= '</div>';
						// $opt++;
					}
					
					//poll vote counting start here
					$pollvotearray = array();
					$pollperarray = array();
					$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
					$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
					if(!empty($pollresult)){
						$totalvote = count($pollresult);
						foreach($pollresult as $p){
							if($p->vote == 1){
								$optonevote++;	
							}
							if($p->vote == 2){
								$opttwovote++;	
							}
							if($p->vote == 3){
								$optthreevote++;	
							}
							if($p->vote == 4){
								$optfourvote++;	
							}
						}
						if($optonevote > 0){
							$optoneper = ($optonevote/$totalvote)*100;
						}
						if($opttwovote > 0){
							$opttwoper = ($opttwovote/$totalvote)*100;
						}
						if($optthreevote > 0){
							$optthreeper = ($optthreevote/$totalvote)*100;
						}
						if($optfourvote > 0){
							$optfourper = ($optfourvote/$totalvote)*100;
						}
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}else{
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}
					$html .= '</div></div><div class="col-md-4 col-sm-4">';
					
					foreach($polloption as $pollopt){
						$img = $text = '';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
							}else{
								$text = $title[0];
							}
						}
						if(!empty($img) && !empty($text)){
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}elseif(!empty($img)){
							$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
						}else{
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}
						$val++;
					}
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
					$squery = $this->db->query($sql);
					$totalShare = $squery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;	
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button"> <a href="'.base_url().'Login"><button type="button" class="trand-button">Submit Vote</button></div><div class="button"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div></form>';
					
					$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div></div>';
				}
			}
		}
		//user poll section end	
		
		return $html;
	}



    public function getRowsDataShowAdmin($logcreatd,$title,$id,$userid){
		$html = '';
		$date = date('m/d/Y');
		
		//user post section start here
		if($title == 'user_post'){
			$sql ="SELECT * FROM vv_post WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					//$pDate = $r->publish_date;
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= '</div><div class="media-body"><h6 class="media-heading">';
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'post'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Post with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}
						}
					}else{
						$html .= ' Created a Post';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$r->publish_date.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row">';
					
					if($r->postdata != '' && $r->postimg != ''){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div><div class="col-md-12"><a href="'.base_url().'Login"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}elseif($r->postdata != '' && $r->videolink != ''){
						$html .= '<div class="col-md-12"><p class="poll_text"><a class="questn">'.$r->postdata.'</a></p></div><div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}elseif($r->postdata){
						$html .= '<div class="col-md-12"><p class="poll_text">'.$r->postdata.'</p></div>';
					}elseif($r->videolink){
						$html .= '<div class="col-md-12"><iframe src="'.$r->videolink.'"" class="viralnav-margin-bottom" width="100%" height="400px"></iframe></div>';
					}else{
						$html .= '<div class="col-md-12"><a href="'.base_url().'Login"><img src="'.base_url().'uploads/'.$r->postimg.'"" class="viralnav-margin-bottom"></a></div>';
					}
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'post_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;	
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class="v_like"><i class="fa fa-share"></i>Share</span></div></div></article></div></div>';
					
					//$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$totalComnt[0]->total.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div></div>';
				
				}
			}
		}
		//user post section end here
		
		
		
		
		//admin compition section start here
		if($title == 'compition'){
			$sql ="SELECT * FROM vv_comment WHERE logid = '".$id."' && userid = '".$userid."'";
			$query = $this->db->query($sql);
			$cmntvalue = $query->result();
			if(!empty($cmntvalue)){
				$sql ="SELECT * FROM vv_contest WHERE id = '".$id."'";
				$query = $this->db->query($sql);
				$value = $query->result();
				foreach($value as $r){
					$startdate = date('Y-m-d',strtotime($r->created));
					$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
					if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
						$pDate = date('M, d Y',strtotime($r->created));
						
						$html = '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left"><img src="'.base_url("assets/front/images/user.png").'" style="width:40px"></div><div class="media-body"><h6 class="media-heading"><a href="'.base_url().'dashboard" class="usr">ViralVoters</a><span class=""> start a compitition</span></h6><p>'.$pDate.'</p></div></div><div class="col-md-12 col-sm-12"><div class="row"><div class="col-md-12"><p class="poll_text"><a href="'.base_url().'compitition/'.$r->id.'" class="questn" style="font-size:40px;">'.$r->question.'</a></p></div>';
						
						$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'compitition'";
						$tquery = $this->db->query($sql);
						$totalLike = $tquery->result();
						
						$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalCommnt = $comntquery->result();
						
						$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'compitition'";
						$comntquery = $this->db->query($sql);
						$totalComnt = $comntquery->result();
						
						if(!empty($totalCommnt)){
							$comment = $totalCommnt[0]->totalComment;	
						}else{
							$comment = 0;
						}
						
						$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class="v_like"><i class="fa fa-share"></i>Share</span></div></div></article></div></div>';
						
						/*$html .= '</div></div><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-eye"></i>10,000</span> <span class="v_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes"><i class="fa fa-comment"></i>'.$totalCommnt[0]->totalComment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div></div>';*/
					}
				}
			}
		}
		//admin compition section end here
		
		
		
		
		//user poll section start
		if($title == 'user_poll'){
			$val = 0;
			$sql ="SELECT * FROM vv_pollls WHERE id = '".$id."'";
			$query = $this->db->query($sql);
			$value = $query->result();
			foreach($value as $r){
				$startdate = date('Y-m-d',strtotime($r->created));
				$enddate = date("Y-m-d", strtotime ( '-1 days' ,strtotime("$startdate +1 month")));
				if(strtotime(date('Y-m-d')) <= strtotime($enddate)){
					$pDate = date('M, d Y',strtotime($r->created));
					
					$sql ="SELECT * FROM vv_users WHERE id = ".$r->user_id;
					$query = $this->db->query($sql);
					$users = $query->result();
					
					$sql1 ="SELECT vote FROM vv_pollvoting WHERE pollid = ".$r->id;
					$query1 = $this->db->query($sql1);
					$pollresult = $query1->result();
					
					$poll ="SELECT * FROM vv_polloption WHERE pollid = ".$r->id;
					$pollquery = $this->db->query($poll);
					$polloption = $pollquery->result();
					
					
					
									
					$html .= '<div class="ui-block"><div class="container"><article class="hentry post"><div class="media"><div class="media-left">';
					if(!empty($users[0]->profile_pic_url)){
						$html .= '<img src="'.base_url().'uploads/'.$users[0]->profile_pic_url.'" style="width:40px">';
					}elseif(!empty($users[0]->picture_url)){
						$html .= '<img src="'.$users[0]->picture_url.'" style="width:40px">';
					}else{
						$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px">';
					}
					
					$html .= ' </div><div class="media-body"><h6 class="media-heading">';
					
					
					if($users[0]->username != ''){
						$html .= '<a href="'.base_url().''.$users[0]->username.'" class="usr">';	
					}else{
						$html .= '<a href="'.base_url().'user/id/'.$users[0]->id.'" class="usr">';	
					}
					
					$html .= $users[0]->firstname.' '.$users[0]->lastname.'</a><span class="">'; 
					
					$tag ="SELECT tagid FROM vv_tagfriends WHERE logid = '".$r->id."' && title = 'poll'";
					$tagquery = $this->db->query($tag);
					$tagids = $tagquery->result();
					if(!empty($tagids)){
						$tagidarray = array();
						foreach($tagids as $t){
							array_push($tagidarray,$t->tagid);	
						}
						$sql ="SELECT * FROM vv_users WHERE id = ".$tagidarray[0];
						$query = $this->db->query($sql);
						$firstuser = $query->result();
						$left = count($tagidarray)-1;
						$html .= ' Created a Poll with ';
						if($firstuser[0]->username != ''){
							$html .= '<a href="'.base_url().''.$firstuser[0]->username.'" class="usr">';	
						}else{
							$html .= '<a href="'.base_url().'user/id/'.$firstuser[0]->id.'" class="usr">';	
						}
						$html .= $firstuser[0]->firstname.' '.$firstuser[0]->lastname.'</a>';
						if($left > 0){
							$html .= ' and '.$left;	
							if($left == 1){
								$html .= ' other';		
							}else{
								$html .= ' others';
							}	
						}
					}else{
						$html .= ' Created a Poll';
					}
					
					
					
					
					$html .= '</span></h6><p>'.$pDate.'</p></div></div><form><div class="row">';
				
					if($r->question != '' && $r->imag != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}elseif($r->question != '' && $r->videourl != ''){
						$html .= '<div class="col-md-12"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div></div><div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}elseif($r->question){
						$html .= '<div class="col-md-8 col-sm-8"><div class="container"><div class="row qs-heading"><h5 style="font-size:25px;">Q. <a href="'.base_url().'poll/'.$r->id.'" class="questn" style="font-size:25px;">'.$r->question.'</a></h5></div></div>';
					}elseif($r->videourl){
						$html .= '<div class="col-md-8 col-sm-8"><iframe src="'.$r->videourl.'"" class="viralnav-margin-bottom" style= "height: 20.00vw;width:100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>';
					}else{
						$html .= '<div class="col-md-8 col-sm-8"><a href="'.base_url().'poll/'.$r->id.'"><img src="'.base_url().'uploads/'.$r->imag.'"" class="viralnav-margin-bottom"></a>';
					}
					
					$html .= '<div class="trand row">';
					
					foreach($polloption as $pollopt){
						$html .= '<div class="col-md-6 col-sm-6">';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
							$html .= '<div class="media"><div class="media-left">';
							$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
							$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								$html .= '<img src="'.base_url().'uploads/'.$img.'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading"></h6><p></p></div></div>';
							}else{
								$text = $title[0];
								$html .= '<div class="media"><div class="media-left">';
								$html .= '<input type="radio" name="pollVoteOpt" value="'.$pollopt->option_id.'" class="trnd-radio"/>';
								$html .= '<img src="'.base_url("assets/front/images/user.png").'" style="width:40px; height:40px;"/></div><div class="media-body"><h6 class="media-heading">'.$text.'</h6><p></p></div></div>';
							}
						}
						$html .= '</div>';
						// $opt++;
					}
					
					//poll vote counting start here
					$pollvotearray = array();
					$pollperarray = array();
					$optonevote = $opttwovote = $optthreevote = $optfourvote =0;
					$optoneper = $opttwoper = $optthreeper = $optfourper = 0;
					if(!empty($pollresult)){
						$totalvote = count($pollresult);
						foreach($pollresult as $p){
							if($p->vote == 1){
								$optonevote++;	
							}
							if($p->vote == 2){
								$opttwovote++;	
							}
							if($p->vote == 3){
								$optthreevote++;	
							}
							if($p->vote == 4){
								$optfourvote++;	
							}
						}
						if($optonevote > 0){
							$optoneper = ($optonevote/$totalvote)*100;
						}
						if($opttwovote > 0){
							$opttwoper = ($opttwovote/$totalvote)*100;
						}
						if($optthreevote > 0){
							$optthreeper = ($optthreevote/$totalvote)*100;
						}
						if($optfourvote > 0){
							$optfourper = ($optfourvote/$totalvote)*100;
						}
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}else{
						array_push($pollvotearray,$optonevote,$opttwovote,$optthreevote,$optfourvote);
						array_push($pollperarray,$optoneper,$opttwoper,$optthreeper,$optfourper);
					}
					$html .= '</div></div><div class="col-md-4 col-sm-4">';
					
					foreach($polloption as $pollopt){
						$img = $text = '';
						$title = unserialize($pollopt->title);
						if(count($title) == '2'){
							$img = $title[1];
							$text = $title[0];
						}else{
							$ext = strtolower(pathinfo($title[0], PATHINFO_EXTENSION));
							if(!empty($ext)){
								$img = $title[0];
							}else{
								$text = $title[0];
							}
						}
						if(!empty($img) && !empty($text)){
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}elseif(!empty($img)){
							$html .= '<div class="skilbar"><span class="skill-name"><img src="'.base_url().'uploads/'.$img.'" height="20px" width="20px"/></span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';	
						}else{
							$html .= '<div class="skilbar"><span class="skill-name">'.$text.'</span><span class="voting">'.$pollvotearray[$val].'</span><div class="skill clearfix" data-percent="'.round($pollperarray[$val],2).'%"><div class="skillbar"></div><div class="skills">'.round($pollperarray[$val],2).'%</div></div></div>';
						}
						$val++;
					}
					
					$sql ="SELECT count(id) as total FROM vv_like WHERE logid = '".$r->id."' AND log_title = 'poll_like'";
					$tquery = $this->db->query($sql);
					$totalLike = $tquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_comment WHERE logid = '".$r->id."' && log_title = 'user_poll'";
					$comntquery = $this->db->query($sql);
					$totalComnt = $comntquery->result();
					
					$sql ="SELECT count(id) as total FROM vv_logs WHERE log_id = '".$r->id."' && log_title = 'user_poll_shared'";
					$squery = $this->db->query($sql);
					$totalShare = $squery->result();
					
					$sql ="SELECT totalComment FROM vv_commentcounter WHERE logid = '".$r->id."' && title = 'user_post'";
					$comntquery = $this->db->query($sql);
					$totalCommnt = $comntquery->result();
					
					if(!empty($totalCommnt)){
						$comment = $totalCommnt[0]->totalComment;	
					}else{
						$comment = 0;
					}
					
					$html .= '</div></div></form><div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$comment.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><i class="fa fa-thumbs-up"></i>Upvote</span> <span class="v_like"><i class="fa fa-comment"></i>Comment</span> <span class=" v_like"><i class="fa fa-share"></i>Share</span></div></div></article></div></div>';
					
					/*$html .= '</div></div><div class="container"><div class="col-md-7 col-xs-6"><div class="button"> <a href="'.base_url().'Login"><button type="button" class="trand-button">Submit Vote</button></div><div class="button"><button type="button" class="poll-button">Create Poll</button></div></div><div class=" col-md-5 col-xs-6"></div></div></form>';*/
					
					/*$html .= '<div class="bottom row"><div class="viral_post col-md-7"><div class="likebar-competition"> <span class="v_likes"><i class="fa fa-share"></i>'.$totalShare[0]->total.'</span> <span class="v_likes t_likes"><i class="fa fa-thumbs-up"></i>'.$totalLike[0]->total.'</span> <span class="v_likes t_cmnt"><i class="fa fa-comment"></i>'.$totalComnt[0]->total.'</span> </div></div><div class="viral_post-ryt col-md-5"><div class="likebar-compitition"><span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-thumbs-up"></i>Upvote</a></span> <span class="v_like"><a href="'.base_url().'Login" class="_bl"><i class="fa fa-comment"></i>Comment</a></span> <span class=" viralnav-dropdown-hover viralnav-hide-small v_like"><i class="fa fa-share"></i>Share<div class="share-hv viralnav-dropdown-content viralnav-card-4 viralnav-bar-block" style="width:100px"><a href="'.base_url().'Login" class="social viralnav-button"><strong>Timeline </strong></a><a href="#" class="social viralnav-button"><strong>Facebook </strong></a><a href="#" class="social viralnav-button"><strong>Google </strong></a><a href="#" class="social viralnav-button"><strong>Twitter </strong></a></div></span></div></div></article></div>';*/
				}
			}
		}
		//user poll section end	
		
		return $html;
	}
	
	
	public function getUserTimeline($userid){
		$sql ="SELECT * FROM vv_timeline WHERE userid = ".$userid." order by id desc";
		$query = $this->db->query($sql);
		$timeline = $query->result();
		
		$content = '';
		$usertimeline = array();
		
		foreach($timeline as $tl) {
			switch($tl->reference) {
				case 'competition':
				break;
				
				case 'surveys':
				break;
				
				case 'polls':
				break;
				
				case 'adminpolls':
					if($tl->shareby)
						$userinfo = $this->Dashboard_model->getUserInformation($tl->shareby);
					else
						$userinfo = $this->Dashboard_model->getAdminInfo();
					$usertimeline[] = array('timeline' => $this->Dashboard_model->getAdminPolls($tl->refid), 'userinfo' => $userinfo, 'data' => $tl);
				break;
			}
		}
		return $usertimeline;
    }
	
	
	public function quickStats($userid){
		$result = array();
		$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(participation) as tparticipation, SUM(upvote) as tupvote from vv_pollls where user_id = ".$userid." and status = 1";
		$query = $this->db->query($sql);
		$result['poll'] = $query->row_array();
		
		$sql ="select count(id) as total, SUM(comments) as tcomment, SUM(sharing) as tsharing, SUM(upvote) as tupvote from vv_post where user_id = ".$userid." and status = 1";
		$query = $this->db->query($sql);
		$result['post'] = $query->row_array();
		
		$sql ="SELECT count(id) as total FROM `vv_comment` where userid = ".$userid." and status = 1";
		$query = $this->db->query($sql);
		$result['comments'] = $query->row_array();
		
		return $result;
    }
	
	
	public function completeStats($userid){
		$result = array();
		$sql ="select * from vv_pollls where user_id = ".$userid." and status = 1 order by id desc limit 10";
		$query = $this->db->query($sql);
		$result['poll'] = $query->result_array();
		
		$sql ="select * from vv_post where user_id = ".$userid." and status = 1 order by id desc limit 10";
		$query = $this->db->query($sql);
		$result['post'] = $query->result_array();
		
		$sql ="select vv_comment.*, vv_commentdata.content from vv_comment inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where userid = ".$userid." and status = 1 order by vv_comment.id desc limit 10";
		$query = $this->db->query($sql);
		$comments = $query->result_array();
		
		foreach($comments as $comment) {
			if($comment['reftype'] == 'comment')
				$result['comments'][] = $this->UserTimeline_model->getParentRef($comment['refid']);
			else
				$result['comments'][] = $comment;
		}
		
		$sql ="select * from vv_upvote where userid = ".$userid." order by id desc limit 10";
		$query = $this->db->query($sql);
		$upvotes = $query->result_array();
		
		foreach($upvotes as $upvote) {
			switch($upvote['reference']) {
				case 'polls':
					$result['upvotes'][] = array('data' => $this->Dashboard_model->getUserPolls($upvote['refid']), 'type' => 'poll');
				break;
				
				case 'post':
					$result['upvotes'][] = array('data' => $this->Dashboard_model->getUserPosts($upvote['refid']), 'type' => 'post');
				break;
				
				case 'postdata':
					$post = $this->Dashboard_model->getPostByImage($upvote['refid']);
					$result['upvotes'][] = array('data' => $this->Dashboard_model->getUserPosts($post['postid']), 'type' => 'post');
				break;
			}
		}
		
		return $result;
    }
	
	
	public function getParentRef($id){
		$result = array();
		
		$sql ="select vv_comment.*, vv_commentdata.content from vv_comment inner join vv_commentdata on vv_comment.id = vv_commentdata.comment_id where vv_comment.id = ".$id." and status = 1 order by vv_comment.id desc";
		$query = $this->db->query($sql);
		$comments = $query->result_array();
		
		foreach($comments as $comment) {
			if($comment['reftype'] == 'comment')
				$result = $this->UserTimeline_model->getParentRef($comment['refid']);
			else
				$result = $comment;
		}
		
		return $result;
    }
}