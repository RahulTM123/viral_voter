<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FindFriend_Model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
	public function friend_section_data(){
		$sql ="SELECT * FROM vv_friends WHERE status IN ('2' ) ORDER BY status ASC";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	}
	
	public function userdata($userid){
		$sql ="SELECT * FROM vv_users WHERE id = ".$userid;
		$userquery = $this->db->query($sql);
		$userdata = $userquery->result();
		return $userdata;
	}
	
	public function friendRequest($user_id,$friend_id){
		$sql ="SELECT * FROM vv_friends WHERE (friend_one=".$user_id." OR friend_two=".$user_id.") AND (friend_one=".$friend_id." OR friend_two=".$friend_id.")";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	}
	
	public function countRequest($user_id){
		$sql ="SELECT count(id) as total FROM vv_friends WHERE friend_one=".$user_id." AND status = '0'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value[0]->total;
	}
	
	public function add_friend_request($user_id,$friend_id){
		$data = array('friend_one' => $user_id, 'friend_two' => $friend_id,'notify_status' => '0');
		$this->db->set('created', 'NOW()', FALSE);
		$this->db->set('modified', 'NOW()', FALSE);
		$this->db->insert('vv_friends', $data);
		$insertId = $this->db->insert_id();
		return $insertId;
	}
	
	
	public function check_request($user_id){
		$sql ="SELECT * FROM vv_friends WHERE friend_two=".$user_id." AND status = '0'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value;
	}
	
	public function confirmRequest($user_id){
		$sql ="SELECT count(id) as total FROM vv_friends WHERE friend_two=".$user_id." AND status = '0'";
		$query = $this->db->query($sql);
		$value = $query->result();
		return $value[0]->total;
	}
	
	public function confirm_request($id){
		$data = array('status' => '1');
		$this->db->set('modified', 'NOW()', FALSE);
		$update = $this->db->update('vv_friends',$data,array('id'=>$id));
		return $id;
	}
	
	public function delete_request($id){
		$this->db->where('id', $id);
   		$this->db->delete('vv_friends'); 
	}
	
	public function cancel_request($id){
		$this->db->where('id', $id);
   		$this->db->delete('vv_friends'); 
	}
	
}