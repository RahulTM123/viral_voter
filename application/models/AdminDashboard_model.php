<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminDashboard_model extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
	
    public function QuickStats(){
		$sqlpoll ="select count(id) as poll from vv_pollls";
		$querypoll = $this->db->query($sqlpoll);
		$data['poll'] = $querypoll->row_array();
		
		$sqlpost ="select count(id) as post from vv_post";
		$querypost = $this->db->query($sqlpost);
		$data['post'] = $querypost->row_array();
		
		$sqlcomment ="select count(id) as comment from vv_comment";
		$querycomment = $this->db->query($sqlcomment);
		$data['comment'] = $querycomment->row_array();
		
		$sqluser ="select count(id) as user from vv_users";
		$queryuser = $this->db->query($sqluser);
		$data['user'] = $queryuser->row_array();
		
		$sqlshare ="select count(id) as share from vv_share";
		$queryshare = $this->db->query($sqlshare);
		$data['share'] = $queryshare->row_array();
		
		$sqlrpoll ="SELECT vv_pollls.*, vv_categories.cat_name, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_pollls` left join vv_categories on vv_pollls.catid = vv_categories.id inner join vv_users on vv_pollls.user_id = vv_users.id order by vv_pollls.id desc limit 7";
		$queryrpoll = $this->db->query($sqlrpoll);
		$data['recentpoll'] = $queryrpoll->result_array();
		
		$sqlrpost ="SELECT vv_post.*, vv_categories.cat_name, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_post` left join vv_categories on vv_post.catid = vv_categories.id inner join vv_users on vv_post.user_id = vv_users.id order by vv_post.id desc limit 7";
		$queryrpost = $this->db->query($sqlrpost);
		$data['recentpost'] = $queryrpost->result_array();
		
		$sqlrcom ="SELECT vv_comment.*, vv_commentdata.content, vv_users.firstname, vv_users.lastname, vv_users.username FROM `vv_comment` left join vv_commentdata on vv_comment.id = vv_commentdata.comment_id inner join vv_users on vv_comment.userid = vv_users.id order by vv_comment.id desc limit 7";
		$queryrcom = $this->db->query($sqlrcom);
		$recentcom = $queryrcom->result_array();
		
		foreach($recentcom as $comment) {
			$reference = $comment;
			$ref = '';
			
			switch($reference['ptype']) {
				case 'polls':
					$sql = 'SELECT vv_categories.cat_name, vv_pollls.id as pid FROM `vv_pollls` inner join vv_categories on vv_pollls.catid = vv_categories.id where vv_pollls.id = (select pid from vv_comment where vv_comment.id = '.$reference['id'].')';
					$query = $this->db->query($sql);
					$ref = $query->row_array();
				break;
				
				case 'post':
					$sql = 'SELECT vv_categories.cat_name, vv_post.id as pid FROM `vv_post` inner join vv_categories on vv_post.catid = vv_categories.id where vv_post.id = (select pid from vv_comment where vv_comment.id = '.$reference['id'].')';
					$query = $this->db->query($sql);
					$ref = $query->row_array();
				break;
			}
			
			$data['recentcom'][] = array('content' => $comment, 'reference' => $ref);
		}
		
		return $data;
    }	
	
    public function change_password($password){
		$sql = 'UPDATE `vv_login` SET `pass` = MD5("'.$password.'") WHERE `vv_login`.`id` = 1;';
		$query = $this->db->query($sql);
    }	
}