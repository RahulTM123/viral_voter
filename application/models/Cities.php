<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cities extends CI_Model{
	function __construct() {
		return false;
	}
	
	public function getCities() {
		$sql ="select * from vv_cities where status = 1";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function totalCities($cid='', $sid='', $cname='') {
		$sql ="select count(vv_cities.id) as total from vv_cities left join (select vv_states.id as sid, vv_states.name as sname, vv_countries.id as cid, vv_countries.name as cname from vv_states inner join vv_countries on vv_states.country_id = vv_countries.id) as cstates on vv_cities.state_id = cstates.sid where 1";
		if($cid!='')
			$sql .=" and cid = '".$cid."'";
		if($sid!='')
			$sql .=" and sid = '".$sid."'";
		if($cname!='')
			$sql .=" and vv_cities.name like '%".$cname."%'";
		
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCountries() {
		$sql ="select * from vv_countries";
		$query = $this->db->query($sql);
        return $query->result();
	}
public function AllcityBystate($cc) {
		$sql ="select * from vv_cities where state_id = '".$cc."'";
		
        $query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allStates($cid='') {
		$sql ="select vv_states.*, vv_countries.name as cname from vv_states left join vv_countries on vv_states.country_id = vv_countries.id where vv_states.country_id = '".$cid."' order by vv_states.name";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCitiesByState($sid='') {
		if($sid!='')
			$sql ="select * from vv_cities where state_id = ".$sid;
		else
			$sql ="select * from vv_cities";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allStatesByCity($id) {
		$sql ="select * from vv_states where country_id = (SELECT country_id FROM `vv_cities` inner join vv_states on vv_cities.state_id = vv_states.id where vv_cities.id=".$id.")";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCitiesByCity($id) {
		$sql ="select * from vv_cities where state_id = (SELECT state_id FROM `vv_cities` where vv_cities.id=".$id.")";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function allCities($cid='', $sid='', $cname='', $page='') {
		$limit = 100;
		$start = ($page)?($page-1)*$limit:0;
		$sql ="select * from vv_cities left join (select vv_states.id as sid, vv_states.name as sname, vv_countries.id as cid, vv_countries.name as cname from vv_states inner join vv_countries on vv_states.country_id = vv_countries.id) as cstates on vv_cities.state_id = cstates.sid where 1";
		if($cid!='')
			$sql .=" and cid = '".$cid."'";
		if($sid!='')
			$sql .=" and sid = '".$sid."'";
		if($cname!='')
			$sql .=" and vv_cities.name like '%".$cname."%'";
		
		$sql .=" order by cid, sid, vv_cities.name limit $start, $limit";
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function getCurrentCity($cc) {
		$sql ="select * from vv_cities where sortname = '".$cc."' limit 1";
		$query = $this->db->query($sql);
        $row = $query->result();
        return $row[0];
	}
	
	public function singleCity($id) {
		$sql ="select * from vv_cities left join (select vv_states.id as sid, vv_states.name as sname, vv_countries.id as cid, vv_countries.name as cname from vv_states inner join vv_countries on vv_states.country_id = vv_countries.id) as cstates on vv_cities.state_id = cstates.sid where id = ".$id;
		$query = $this->db->query($sql);
        return $query->result();
	}
	
	public function updateStatus($status, $id) {
		$sql ="update vv_cities set status = ".$status." where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function editCity($cityname, $sid, $status, $id) {
		$sql ="update vv_cities set status = ".$status.", name = '".$cityname."', state_id = '".$sid."' where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
	
	public function addCity($cityname, $sid, $status) {
		$sql ="insert into vv_cities values ('','".$cityname."','".$sid."',".$status.")";
		$query = $this->db->query($sql);
        return true;
	}
	
	public function deleteCity($id) {
		$sql ="delete from vv_cities where id = ".$id;
		$query = $this->db->query($sql);
        return true;
	}
}